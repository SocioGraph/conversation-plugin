bash merge.2.0.sh --base 2.0 --version 2.0 --level babylon

## 12 Oct 2022 Feedback
1. Whiteboard width similar to Blue Box. 
2. Canvas height need to reduce.
3. Buttons and icons font size need to reduce on mobile layout.
4. Plugin & Avatar should be 30-35% in height, on the screen (Mobile, Tab, Desktop)
5. On Mobile, full width no need of left - right space
6. Place holder Height: Mobile: 3 lines, web: 2 lines
7. 

1200 - Tab
987 - Big Mobile
767 - Small Mobiles

_____

SSH Keys:

ssh-keygen -t rsa -b 4096 -C wasim@iamdave.ai

/Users/wasimahmed/.ssh/id_rsa_wasimdaveai

Host github.com
  HostName github.com
  IdentityFile ~/.ssh/id_rsa

Host bitbucket.org
  HostName bitbucket.org
  IdentityFile ~/.ssh/id_rsa_wasimdaveai


  ----


  ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzzdWaLBZaG0i4t3bhnq5nd+x9Q1Tb8I3G8SEt546m9diqGvAUWCao6SLXrGAJFEkWTP1Oytwf32jEnWZ4yhdI1965uh90RpndsWxCd/rw3kLOhNVUQm7ngmDdqDJv65/Yo/XVvTINsVzYF+oMyoT2WBl5C/0k0eKQf6RDL1L95RTCbHMLRpBoW+BeDGWj13cTo68vy4wTmkwtBtlC0a8fb/MlhIXakuXk4querNANxozuH7/+iV1sqXDWKTf5bKaYLo6XOkKzwYk/X2yRG1TQ94d4DAbCD8Xminh/vB+IsfFrz6qAliRPKEzCVvBuYDgiaioC4N+nb9mOsbD212gZhNqYdH0mZ2Snnpk16rcEcKE9ggJ4Eg6BWnxiTJoH8A0+u09Ff5kczZz1wvgutWMAXpb/RR498ch8x2EGJBg38sa/OFpVIBfH9RNeUVk6Gu9vpGjJ1rTlpdBAUXVwk5cZutAwsuW7Sy7tfBGZKErsOswSPJjmCSw+XmS/w8VEsqRS5YZJXxj6KPxIohdIZqnP+OzVNDRmBNjCev+PlR0oYsbt9jjrLiokpCVEPOigB56sZ16C4fqYCXSAQozFtSJoj6D9uVGdD2ZFxHWETlFW/ol+3mwBrndB4b075WUuRo5+WymrH3DKFBg/i+vrIJW+KrPzVyVven+PuCxmS1ItOw== wasim@iamdave.ai


  ____________

  ## Redis Installation
  https://phoenixnap.com/kb/install-redis-on-mac#:~:text=Option%202%3A%20Install%20Redis%20on%20Mac%20Without%20Homebrew,-The%20second%20method&text=newly%20created%20folder.-,curl%20%2DO%20http%3A%2F%2Fdownload.redis.io%2Fredis,redis%2Dstable'%20installation%20archive.

  