class Conversation extends DaveService {
    constructor(host, enterprise_id, conversation_id, settings, onSetupDone=null, onSetupError=null, onAction=null) {
        //class Conversation
        super(host,enterprise_id, settings);
        var that = this;
        this.enterprise_id = enterprise_id;
        this.conversation_id = conversation_id;
        this.host_url = host;
        
        this.voice_id = settings["voice_id"];
        this.customer_id = this.user_id;
        this.conversation_params = settings["conversation_params"];
        
        this.conversation_data = {};
        this.conversation = {};
        this.current_response = {};
        this.settings = settings;

        this._loading_conversation = true;
        this._loading_conversation_keywords = true;
        
        // this.streamer = new Streamer(that.host_url, that.enterprise_id, that.settings, function(s, m){
        //     that.eventCallback(s, m);
        // });

        var streamer_settings = {
            wakeup : settings["wakeup"]?settings["wakeup"]:(settings["wakeup_url"] ? true: false),
            asr : settings["asr"]? settings["asr"]: (settings["speech_recognition_url"] ? true: false),
            image_processing : settings["image_processing"]? settings["image_processing"]: (settings["image_recognition_url"] ? true: false),
            wakeup_server : settings["wakeup_url"], 
            asr_server : settings["speech_recognition_url"], 
            image_server :  settings["image_recognition_url"],
            image_classifier : settings["image_recognition_url"] ? "FaceDetectorCafe": undefined,  
            frames_to_capture : 30,
            recognizer : settings["recognizer_type"] || 'google', //Google || Kaldi || Reverie || ..
            model_name :"indian_english",
            conversation_id : that.conversation_id,
            enterprise_id : that.enterprise_id, 
            base_url : that.host_url,
            auto_asr_detect_from_wakeup: settings["auto_asr_detect_from_wakeup"] == undefined? false: settings["auto_asr_detect_from_wakeup"],
            vad : settings["vad"] == undefined? true: settings["vad"],
            audio_auto_stop: 10,
            asr_type :  settings["speech_recognition_type"] || "full", //full-'flacking'||"chunks"-'asteam'||"direct"-'sending-flag',
            wakeup_type : settings["wakeup_type"] || "chunks", //chunks-'asteam'||"direct"-'sending-flag',
            video_type : settings["video_stream_type"] || "chunks", //chunks-'asteam'||"direct"-'sending-flag'
            asr_additional_info : settings["additional_conversation_info"] || {},
            event_callback : function (event, data) {
                console.log(event, data);
                that.eventCallback(event, data);
            }
        }
        this.streamer = new Streamer(streamer_settings);
        
        this.onSetupDone = onSetupDone;
        this.onSetupError = onSetupError;
        this.keywords = [];
        this._req = null;
        this.event_callback = function(status, message){
            if(event_callback){
                that.eventCallback(status, message);
            }
        }
        this.onAction = onAction;
        this.loadConversation();
        this.loadKeywords();
        
    }
    changeUser(customer_id, api_key){
        this.current_response = {};
        this.customer_id = customer_id;
        this.user_id = customer_id;
        this.api_key = api_key;
        this.engagement_id = undefined;
        this.session_id = undefined;
        this.headers = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
            "X-I2CE-USER-ID": this.user_id,
            "X-I2CE-API-KEY": this.api_key
        }
        Utills.setCookie(this.authentication_cookie_key, JSON.stringify(this.headers), 240);
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.user_id;
        console.debug("user updated :: ", this.headers);
    }
    triggerEvents(status, message){
        if(this.onAction){
            this.onAction(status, message);
        }
    }

    post_interaction(obj, beacon=false){
        var that = this;
        var user_id_attr = this.settings["interaction_user_id_attr"] || this.settings["user_id_attr"]
        obj[user_id_attr] = this.customer_id;

        if(!obj[that.settings["stage_attr"] || "stage"]){
            console.debug("Interaction disabled")
            return;
        }

        extendData(obj, this.settings["additional_interaction_info"]).then(function(exdata){
            if(!exdata){
                return;
            }
            if(beacon){
                that.sendBeacon(`/object/${that.settings["interaction_model"]}`, exdata);
            }else{
                that.post(that.settings["interaction_model"], exdata, function(data){ 
                    console.debug(data)
                    that.eventCallback("interaction_response", data);
                }, function(err){
                    console.error(err)
                    that.eventCallback("interaction_response", err.error || err);
                });
            }
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }

    startSession(async=false){
        var that = this;
        if(!this.settings["session_model"]){
            return;
        }
        this.session_start_time = (new Date()).getTime();
        if(this.session_id){
            return
        }
        var obj = {};
        var user_id_attr = this.settings["session_user_id_attr"] || this.settings["user_id_attr"];
        var session_id_attr = this.settings["session_id_attr"] || that.settings["session_model"].toLowerCase()+"_id";
        
        obj[user_id_attr] = this.customer_id;
        obj[this.settings["conversation_id"]] = this.conversation_id;
        if(async && crypto){
            obj["_async"] = true;
            obj[session_id_attr] = crypto.randomUUID();
        }
        extendData(obj, this.settings["additional_session_info"]).then(function(exdata){
            that.post(that.settings["session_model"], exdata, function(data){ 
                console.debug(data)
                if(async && crypto){
                    data[session_id_attr] = obj[session_id_attr];
                }
                
                that.eventCallback("start_session", data);
                that.session_id = data[session_id_attr] || data["session_id"];
            }, function(err){
                console.error(err)
                that.eventCallback("start_session", err.error || err);
            });
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }
    updateSession(async, beacon=false){
        if(!this.settings["session_model"] || !this.session_start_time || !this.session_id){
            return;
        }
        var that = this;
        var diff = new Date().getTime() - this.session_start_time;
        var obj = {"_async": true, "async": async == undefined ? true : false};
        obj[this.settings["conversation_id"]] = this.conversation_id;
        obj[this.settings["session_duration_attr"] || "session_duration"] = diff/1000;
        this.session_start_time = undefined;
        if(beacon){
            this.sendBeacon(`/object/${this.settings["session_model"]}/${this.session_id}`, obj);
        }else{
            this.iupdate(this.settings["session_model"], this.session_id, obj, function(data){ 
                console.debug(data)
                that.eventCallback("update_session", data);
                deleteAllCookies();
            }, function(err){ 
                console.error(err)
                that.eventCallback("update_session", err.error || err);
            });
        }
    }
    endSession(obj){
        if(!this.settings["session_model"]){
            return;
        }
        var that = this;
        var obj = {"_async": true};
        obj[this.settings["conversation_id"]] = this.conversation_id;

        this.update(this.settings["session_model"], this.session_id, obj, function(data){ 
            console.debug(data)
            that.eventCallback("end_session", data);
            deleteAllCookies();
        }, function(err){
            console.error(err)
            that.eventCallback("end_session", err.error || err);
        });
    }
    
    startListenForWakeup(){
        this.streamer.startWakeupRecording({});
    }
    stopListenForWakeup(force){
        if(force){
            this.streamer.forceStopWakeup()
        }else{
            this.streamer.stopWakeupRecording();
        }
    }
    startImageStream(){
        this.streamer.startVideoRecording();
    }
    stopImageStream(){
        this.streamer.stopVideoRecording();
    }
    startRecordResponse(){
        // this.eventCallback("recording");
        // this.streamer.startRecordResponse(null, false, true, this.current_response["name"]);
        this.streamer.startVoiceRecording({
            voice_id: this.voice_id || 'english-male',
            query_type: "speech",
            language: this.language || 'english',
            system_response: this.current_response["name"]
        })
    }
    stopRecordResponse(){
        // this.eventCallback("speech_processing");
        // this.streamer.stopRecordingResponse();
        this.streamer.stopVoiceRecording();
    }

    
    
    
    eventCallback(status, message){
        console.log(status, message);
        //class Conversation
        if(status == "error"){
            this.triggerEvents("idel");
            //that.precessError({});
            this.processing = false;
        }else if(status == "interaction"){
            this.post_interaction({[this.settings["stage_attr"] || "stage"]: message});
        }else if(status == "mic_rejected" || status == "socket_error"){
            this.triggerEvents("mic_rejected");
        }else if(status == "asr-convResp"){
            this.processing = false;
            this.state = "idel";
            if(this.timeout_level_3){
                clearTimeout(this.timeout_level_3);
                this.timeout_level_3 = undefined;
            }
            if(this.timeout_timeout){
                clearTimeout(this.timeout_timeout);
                this.timeout_timeout = undefined;
            }
            
            if( typeof message  === "object" ){
                var dt = message || {};
                dt["auto_recording"] = message["auto_recording"];
                if( !dt["name"] ){
                    console.log("--------------- error response", dt);
                    this.triggerEvents("conversation-error", dt);        
                }else{
                    this.triggerEvents("conversation-response", dt);
                }
            }else{
                console.debug("--------------- not a json response", message);
                this.triggerEvents("conversation-error", dt);
            }
        }else if(status == "asr-recText"){
            console.debug("level -2 callback")
            this.triggerEvents("speech-detected", message.recognized_speech || "");
            var that = this;
            that.timeout_level_3 = setTimeout(function(){
                console.debug("level -3 callback")

                if(that.processing){
                    that.triggerEvents("delay", message.recognized_speech || "");
                }
            }, 15000);

        }else if(status == "imagews-capture_ended"){
            this.triggerEvents("capture_ended", message);
        }else if(status == "imagews-results"){
            if(message["classifier_result"]){
                for(var z of message["classifier_result"]){
                    if(z["Result"] == "Face"){
                        this.triggerEvents("person_detected", message);
                        if(this.settings["auto_image_stream_stop"] != false){
                            this.stopImageStream();
                        }
                    }
                }
            }
        }else if(status == "more_delay"){
            this.triggerEvents("delay","");
        }else if(status == "timeout"){
            if(this.state != "idel" && this.processing){
                console.log("Speech reco timed out")
                this.triggerEvents("timeout", {"error": "I'm not able to get an answer for that currently. Can you try with something simpler?"});
                
                this.processing = false;
                // setTimeout(function(){
                //     that.startListenForWakeup();
                // }, 1500);
            }
        }else if(status=="wakeup-hotword"){
            console.debug("hotword detected", message);
            this.triggerEvents("hotword", message);
            if(this.settings["auto_wakeup_listern_stop"] != false){
                this.streamer.forceStopWakeup();
            }
        }
        else if(status=="asr-processing"){
            // that.socketio.emit('stt', payload);
            this.triggerEvents("speech_processing");
            this.processing = true;
            this.state = "processing";
        }
        else if(status == "asr-disconnect"){
            if(this.state == "processing"){
                this.triggerEvents("socket_disconnected", {"error": "Your Internet connection was disrupted during the query. Please try saying this again."});
                this.processing = false;
            }
            this.state = "idel";
        } else if(status == "asr-recording"){
            this.triggerEvents("recording");
            this.state = "recording";

            this.abortReq();
        }else if (status == "asr-idel"){
            this.triggerEvents("idel", message);
            this.state = "idel";
            this.processing = false;
            this.abortReq();
        } else if(status == "asr-processing"){
            this.triggerEvents("processing", message);
            this.state = "processing";
            this.processing = true;
            this.abortReq();
        } else{
            console.debug(status, message);
            this.triggerEvents(status, message)
        }
        
        
        if(["error", "mic_rejected", "socket_error"].indexOf(status) > -1){
            // if(that.record_button){
            //     that.record_button.attr("disabled", true);
            // }
        }else{
            // if(that.record_button){
            //     that.record_button.attr("disabled", false);
            // }
        }
    }
    
    abortReq(){
        //class Conversation
        if(this._req){
            this._req.abort();
        }
        
    }
    abortAsrReq(){
        this.streamer.forceStopRecording();
    }
    checkSetupDone(){
        //class Conversation
        if(!this._loading_conversation && !this._loading_conversation_keywords){
            this.onSetupDone();
        }
    }
    
    loadConversation(){
        //class Conversation
        var ref = this;
        this.get("conversation", this.conversation_id, function(data){
            ref.conversation_data = data["data"];
            ref.conversation = data;
            ref._loading_conversation = false;
            ref.checkSetupDone();
        }, function(error){
            console.log(error);
            ref._loading_conversation = false;
            ref.checkSetupDone();
            throw error;
        })
    }
    
    loadKeywords(){
        //class Conversation
        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }
        
        var ref = this;
        this.getRaw("/conversation-keywords/"+ref.conversation_id, function(da){
            ref.keywords = da["keywords"] || [];
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        }, function(res){
            var msg;
            if(res.responseJSON){
                msg = "Looks like the system is under maintenance, I've been asked to say '"+res.responseJSON["error"]+"'";
            }else{
                msg = "Sorry, it seems like my team is tinkering around with me!! |I should be back online and ready to talk to you once this update is done..."
            }
            console.log(msg);
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        });
    }
    filterStates(kwd){
        //class Conversation
        if(!kwd) return this.keywords;
        function match_score(l, k){
            var x= l.filter(function(i){
                return i.toLowerCase().indexOf(k.toLowerCase()) != -1 ? true : false;
            });
            return x.length;
        }
        var keys = kwd.split(" ");
        keys.push(kwd);
        for(var i in this.keywords){
            var c = 0;
            for(var j in keys){
                c += match_score([this.keywords[i][0], this.keywords[i][1], this.keywords[i][2]], keys[j]);
                c += match_score(this.keywords[i][4], keys[j]);
            }
            this.keywords[i].score = c;
        }
        var x = this.keywords.filter(function(a){ return a.score; });
        return x.sort(function(a,b){ b.length - a.length });
    }
    
    getStateKeywords(state){
        //class Conversation
        for(var i of this.keywords){
            if(i[1] == state)
            return i
        }
        return [];
    }

    postConversationFeedback(rating, cb){
        if(rating <= 5 && rating >=1){
            this.postRaw(`/conversation-feedback/dave/${this.engagement_id}`, {
                "usefulness_rating": rating
            }, function(data){
                if(cb) cb(data)
            }, function(e){
                if(cb) cb(e)
            })
        }
    }
    
    start(successCallback, errorCallback){
        var that = this;
        //class Conversation
        // this.customer_id = customer_id;
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.customer_id;
        extendData({"query_type": "auto"}, this.settings["additional_conversation_info"]).then(function(data){
            that.getNext(data, null, function(data){
                successCallback(data)
                that.init_response = data;
            }, errorCallback);
        }).catch((err) => console.log("Failed to extend request object", err))

        
        this.streamer.set("customer_id", this.customer_id);
        this.streamer.set("api_key", this.api_key);
        // this.streamer.setupStreamers(this.conversation_id, this.customer_id, this.api_key);
    }

    getNext(data, title, successCallback, errorCallback){
        //class Conversation
        var async_ = data["async"] || true;
        // delete data["async"];
        
        var ref = this;
        if(this.engagement_id){
            data["engagement_id"] = this.engagement_id;
        }
        if(this.voice_id){
            data["voice_id"] = this.voice_id;
        }
        if(this.current_response){
            data["system_response"] = this.current_response["name"];
        }
        this.abortReq();

        data["synthesize_now"] = true;
        data["csv_response"] = true;
        
        console.debug("data: ", data);
        
        if(data["customer_response"]){
            var rx = title|| data["customer_response"];
            this.eventCallback("processing",  rx.indexOf("{") == -1 ? rx : (data.title || "Form response"));
        }
        this._req = this.postRaw(this.conversation_url, data, function(data){
            ref.engagement_id = data["engagement_id"]
            data["data"] = data["data"] || {};
            ref.voice_id = data["data"]["voice_id"] || ref.voice_id;
            ref.streamer.set("engagement_id", data["engagement_id"]);
            successCallback(data);
            ref.eventCallback("idel");
            ref.current_response = data;
            //ref.precessResponse(da, successCallback)
        }, function(error){
            errorCallback(error);
            ref.eventCallback("idel");
            //ref.processError(res, errorCallback);
        })
    }
    getNudge(state, successCallback, errorCallback){
        var ref = this;
        
        this.abortReq();
        var data = {}
        data["customer_state"] = state;
        data["system_response"] = this.current_response? this.current_response["name"]: null;
        if(this.engagement_id){
            data["engagement_id"] = this.engagement_id;
        }
        data["synthesize_now"] = true;
        data["csv_response"] = true;
        data["query_type"] = "auto";
        if(this.voice_id){
            data["voice_id"] = this.voice_id;
        }

        

        extendData(data, this.settings["additional_conversation_info"]).then(function(data){
            console.debug(data)
            ref.eventCallback("processing", "nudge");
            ref._req = ref.postRaw(ref.conversation_url, data, function(data){
                data["data"] = data["data"] || {};
                successCallback(data);
                ref.eventCallback("idel");
                //ref.precessResponse(da, successCallback)
            }, function(error){
                errorCallback(error);
                ref.eventCallback("idel");
                //ref.processError(res, errorCallback);
            })   
        }).catch((err) => console.log("Failed to extend request object", err))
        
    }
    
    next(response, state=null, query_type=null, title=null, successCallback=null, errorCallback=null){
        //class Conversation
        var that = this;
        var data = {};
        data["customer_response"] = response;
        if(state)
            data["customer_state"] = state;
        console.debug(this.current_response);
        data["system_response"] = this.current_response["name"];
        data["query_type"] = query_type;
        extendData(data, this.settings["additional_conversation_info"]).then(function(data){
            that.getNext(data, title, successCallback, errorCallback);    
        }).catch((err) => console.log("Failed to extend request object", err))
    }
    getKeywords(c=4, res=null){
        var kws = []
        res = res || this.current_response
        if(res){
            for(var i in res["options"]){
                kws.push([res["options"][i], undefined]);
                if(kws.length == c) break
            }
            var l = [];
            for(var i in res["state_options"]){
                var kys = this.getStateKeywords(i);
                if(kys.length == 6){
                    if(kys[5].length > 0){
                        l.push([kys[2], i]);
                    }else{
                        l.push([res["state_options"][i], i]);
                    }
                    /*
                            for(var z of kys[5]){
                                if(z.indexOf("**") >= 0) continue;
                                while(z.indexOf("__") > -1)
                                z = z.replace("__", "\"")
                                x.push(z);
                            }
                            l.push(x);*/
                }
            }
            for(var j of l){
                kws.push(j);
                if(kws.length >= c){
                    break;
                }
            }
        }
        return kws;
    }
}


class WhiteBoard{
    constructor(options){
        //class Whiteboard
        this.id = options.white_board_id;
        this.ele = djq("<div />");
        this.ele.addClass(options.class || 'dave-whiteboard');
        
        this.defaultBody = options.html || '';
        this.callbacks = djq.Callbacks();
        this.eventCallbacks = djq.Callbacks();
        
        this.ele.on("scroll", {"type": "scroll"}, function(e){
            e.data["position"] = djq(this).scrollTop();
            this.fire(e);
        })
    }
    
    html(html){
        //class Whiteboard
        if(html){
            var comp = djq(html); 
            var that = this;
            comp.find(".dave-video").each(function(){
                var res = {};
                var events = (djq(this).attr("data-key") || "").split(",");
                //var events = (djq(this).attr("data-events") || "").split(",");
                if(!djq(this).attr("data-key")){
                    throw "dave-action defined without data-key";
                }
                for(var ev of events){
                    var ev_mp = ev.split(":");
                    res["key"] = ev_mp[0];
                    res["type"] = ev_mp[1];
                    var ls_t = 0;
                    djq(this).bind(ev_mp[1], res, function(e){
                        if(ev_mp[1] == "timeupdate"){
                            e.data.value = djq(this)[0].currentTime;
                            var t = Math.floor(djq(this)[0].currentTime);
                            if( t%2 == 0 ){
                                if(t != ls_t){
                                    that.fire(e, true);
                                }
                                ls_t = t;
                            }
                        }else if(ev_mp[1] == "ended"){
                            e.data.value = "ended";
                            that.fire(e, true);
                        }else if(ev_mp[1] == "pause"){
                            e.data.value = djq(this)[0].currentTime;
                            that.fire(e, true);
                        }
                    });
                }
                djq(this).on('click', res, function(e){
                    e.data.value = djq(this).attr("data-value")
                    that.fire(e);
                });
            })
            
            comp.find(".dave-click").each(function(){
                var res = {};
                res["key"] = djq(this).attr("data-key");
                res["value"] = djq(this).attr("data-value");
                res["title"] = djq(this).attr("data-title");
                if(!res["key"] && !res["value"]){
                    throw "dave-action defined without data-key or data-value";
                }
                res["type"] = "clicked";
                djq(this).on('click', res, function(e){
                    e.data.value = djq(this).attr("data-value")
                    that.fire(e);
                })
            })

            comp.find(".dave-url").each(function(){
                var res = {};
                res["key"] = djq(this).attr("data-key");
                res["value"] = djq(this).attr("data-value");
                res["url"] = djq(this).attr("href");
                if(!res["key"] && !res["value"]){
                    throw "dave-action defined without data-key or data-value";
                }
                res["type"] = "url_click";
                djq(this).on('click', res, function(e){
                    e.data.value = djq(this).attr("data-value")
                    that.fire(e, true);
                })
            })

            comp.find("form.dave-form").each(function(){
                djq(this).on('submit', {"type": "form", "key": djq(this).attr("data-key"),"title":djq(this).attr("data-title")},  function(e){
                    var dt = djq(this).serialize()
                    console.debug(dt);
                    var urs = new URLSearchParams(dt);
                    var obx = {};
                    var ks = urs.keys();
                    var nxt = ks.next();
                    while(!nxt.done){
                        obx[nxt.value] = urs.get(nxt.value);
                        nxt = ks.next();
                    }
                    console.debug(obx);
                    //const data = Object.fromEntries(new URLSearchParams(dt));;
                    e.data["value"] = obx;
                    that.fire(e);
                })
            })
            
            this.defaultBody = html;
            this.ele.append(comp);
        }
        return this.ele;
    }
    
    watch(callback){
        //class Whiteboard
        this.callbacks.add(callback);
    }
    watchEvents(callback){
        //class Whiteboard
        this.eventCallbacks.add(callback);
    }    
    fire(data, eventType){
        //class Whiteboard
        if(eventType){
            this.eventCallbacks.fire(data);
        }else{
            this.callbacks.fire(data);
        }
    }    
    
    unWatch(){
        //class Whiteboard
        this.callbacks.empty();
    }
}



class Message{
    constructor(message, position="left", title= "Dave.Ai", time=null){
        //class Message
        if(typeof message== "object"){
            this.message = "Form submited successfully";
        }else{
            this.message = message;
        }
        this.position = position;
        this.title = title || "";
        if(time){
            var tt = typeof time == "string" ? new Date(time) : time;
            if(getFormattedDate(new Date()) === getFormattedDate(tt)){
                this.time = tt.getHours()+":"+tt.getMinutes();
            }else{
                this.time = getFormattedDate(tt)+" "+tt.getHours()+":"+tt.getMinutes();
            }
        }else{
            this.time = "";
        }
        this.ele = djq("<li class='message "+this.position+"'/>");
        this.makeElement()
    }
    
    highlight(str){
        //class Message
        str = str? str: "";
        var msg = this.message.toLowerCase();
        if(str){ 
            var ls = msg.split(str);
            msg = ls.join( "<span class='highlight'>"+str+"</span>")
        }
        this.makeElement(msg);
    }
    
    makeElement(str){
        //class Message
        str = str || this.message;
        var inner = djq(`<div style="display: table;">
        <div style="display: table-row;">
        <div class="text_wrapper" style="display: table-cell">
        <div class="text">${str}</div>
        </div>
        </div>
        <p style="margin-left: 5px;display: table-row;">
        <small style="float: left; margin-right: 10px;">${this.title}</small>
        <small style="float: right;">${this.time}</small>
        </p>
        </div>`);
        if(this.position == "right"){
            inner.css("float","right");
        }
        this.ele.empty().html(inner);
        return this.ele;
    }
    html(){
        //class Message
        return this.ele;
    }
}

