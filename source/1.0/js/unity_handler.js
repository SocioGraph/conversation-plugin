
var product_obj, unityInstance;
function receiveMessageFromUnity(id){
    var aud = document.getElementById("audio");
    console.debug("--------", id);
    if(aud.getAttribute("data-audio-id") != id){
        unityInstance.stopTalking()
        // try{
        //     unityInstance.SendMessage("head", "stopTalking")
        // }catch(e){
        //     console.log("Were not able to do stop talking", e);
        // }
        return;
    }
    if(aud){
        var muted = aud.getAttribute("force-mute") || "false";
        console.debug("Audio muted ::", muted);
        if(muted == "false"){
            aud.muted = false;
        }else{
            aud.muted = true;
        }
        // aud.play();
        const promise = aud.play();
        if(aud.getAttribute("defaults") == "false"){
            $("#message").placeholder_effect(aud.duration);
        }
        if (! aud.onended ) {
            aud.onended = function() {
                var audio = document.getElementById("audio");

                if(audio.getAttribute("data-audio-id") != id){
                    try{
                        unityInstance.SendMessage("head", "stopTalking")
                        if(audio.getAttribute("defaults") == "false"){
                            $("#message").placeholder_effect();
                        }
                    }catch(e){
                        console.log("Were not able to do stop talking", e);
                    }
                }
                
            }
        }
        if (promise !== undefined) {
            promise.then(() => {
                console.debug("Got the audio load promise here");
            }).catch(function(err) {
                aud.onended();
            });
        }
    }else{
        console.log("Aud is empty")
    }
}
var default_bg, defaul_avatar_id = "dave";
var screen_loading = true;
function sceneLoadedCallback(){
    console.log("scene loaded callback")
    screen_loading = false;
    //unityInstance.SendMessage("GameManagerOBJ", "showImage", "https://d3chc9d4ocbi4o.cloudfront.net/static/uploads/nasscom_vr_bg_new.png");
    //unityInstance.SendMessage("GameManagerOBJ","showPersona","eric")
    /*if(default_bg product_obj && product_obj["customizables"]  && product_obj["customizables"]["bg"]){
               unityInstance.SendMessage("GameManagerOBJ", "setTexture", product_obj["customizables"]["bg"]);
           }
           if(product_obj && product_obj["avatar_id"]){
               unityInstance.SendMessage("GameManagerOBJ", "showPersona", product_obj["avatar_id"]);
           }else{
               unityInstance.SendMessage("GameManagerOBJ","showPersona","eric")
           }*/
    if(default_bg){
        unityInstance.setTexture(default_bg);
        // try{
        //     unityInstance.SendMessage("GameManagerOBJ", "setTexture", default_bg);
        // }catch(e){
        //     console.log("Were not able to set background", e);
        // }
        // unityInstance.SendMessage("GameManagerOBJ", "setTexture", "spresso");
    }
    unityInstance.showPersona(defaul_avatar_id)
    // try{
    //     unityInstance.SendMessage("GameManagerOBJ", "showPersona", defaul_avatar_id);
    // }catch(e){
    //     console.log("Were not able to set persona", e);
    // }
    // unityInstance.SendMessage("GameManagerOBJ", "showPersona", "dave");
    // setTimeout(function(){
    //     unityInstance.SendMessage("GameManagerOBJ", "setTexture", "https://images.ctfassets.net/hrltx12pl8hq/7yQR5uJhwEkRfjwMFJ7bUK/dc52a0913e8ff8b5c276177890eb0129/offset_comp_772626-opt.jpg");
    // }, 3000)
}



function daveSceneLoaded(){
    //sceneLoadedCallback();
}

function enteredAreaCallback(type, value){
    if(type == "cs"){
        window.__dv_cbs_get("enteredArea").fire(value.startsWith("cs")? value : `cs_${value}`);
    }else if(type=="point"){
        window.__dv_cbs_get("pointReached").fire(value);
    }
}


class UnityManager {
    constructor(unityInstance, settings){
        this.unityInstance = unityInstance;
        this.head_name = settings["head_name"] || "head";
    }
    keyboardControlEnable(){
        try{
            this.unityInstance.SendMessage("GameManagerOBJ", "keyboardControlEnable", "");
        }catch(e){
            console.warn("Unable to enable keyboard controls", e);
        }
    }
    keyboardControlDisable(){
        try{
            this.unityInstance.SendMessage("GameManagerOBJ", "keyboardControlDisable", "");
        }catch(e){
            console.warn("Unable to disable keyboard controls", e);
        }
    }
    joystickControlEnable(){
        try{
            this.unityInstance.SendMessage("GameManagerOBJ", "joystickControlEnable", "");
        }catch(e){
            console.warn("Unable to enable joystick controls", e);
        }
    }
    joystickControlDisable(){
        try{
            this.unityInstance.SendMessage("GameManagerOBJ", "joystickControlDisable", "");
        }catch(e){
            console.warn("Unable to disable joystick controls", e);
        }
    }
    stopTalking(){
        try{
            this.unityInstance.SendMessage(this.head_name, "stopTalking");
        }catch(e){
            console.warn("Unable stop talking", e);
        }
    }

    startTalking(id, frames, shapes){
        try{
            this.unityInstance.SendMessage(this.head_name, 'StartTalking', JSON.stringify({
                "frames": frames,
                "shapes": shapes,
                "audio_id": id
            }))
        }catch(e){
            console.warn("Unable to make avatar talk")
        }
    }
    showPersona(id){
        try{
            this.unityInstance.SendMessage("GameManagerOBJ", "showPersona", id);
        }catch(e){
            console.warn("Were not able to update persona", e);
        }
    }
    setTexture(bg){
        try{
            this.unityInstance.SendMessage("GameManagerOBJ", "setTexture", bg);
        
        }catch(e){
            console.warn("Were not able to update texture", e);
        }
    }
    callGameManagerFunction(key, val){
        try{
            this.unityInstance.SendMessage("GameManagerOBJ", key, val);
        
        }catch(e){
            console.warn(`Failed to call GameManagerOBJ function '${key}'`, e);
        }
    }
    callAssistantFunction(key, val){
        this.unityInstance.SendMessage('Assistantcode', key, val);
    }
}

class SetupEnviron{
    static load(_b, settings){
        var progress = 0;
        var notify;
        var buildUrl = _b;


        if(settings && settings["default_bg"]){
            default_bg = settings["default_bg"];
        }else if(settings && settings["customizables"] && settings["customizables"]["bg"]){
            default_bg = settings["customizables"]["bg"];
        }else if(settings && settings["space_id"]){
            default_bg = settings["space_id"];
        }
        
        if(settings && (settings["default_avatar_id"]  || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"])){
            defaul_avatar_id = settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"];
        }
        

        function init(t, myResolve, myReject){
            function UnityError(message, err){
                console.log(message, err);
                //that.errorC("error", message);
                myReject(message);
            }
            function UnityLoaded(ut){
                unityInstance = new UnityManager(ut, settings);
                myResolve(unityInstance);
            }
            function UnityProgress(prog) {
                if (prog == 1){
                    console.log("Model loaded")    
                }
                progress = prog;
                console.debug("Loading..", progress);
                if(progress%0.1){
                    console.log("Loading..", progress);
                }
                if(notify){
                    notify(progress);
                }
            }
            if(t == 0){
                return;
            }
            try{
                var config = {
                    dataUrl: buildUrl + "/ProjectBuild.data",
                    frameworkUrl: buildUrl + "/ProjectBuild.framework.js",
                    codeUrl: buildUrl + "/ProjectBuild.wasm",
                    streamingAssetsUrl: "StreamingAssets",
                    companyName: "SocioGraph Solution",
                    productName: "Conversation Bot",
                    productVersion: "0.1",
                };

                createUnityInstance(document.querySelector("#unityContainer"), config, UnityProgress).then(UnityLoaded).catch(function(a, b){
                    UnityError(a, b);
                    init(t-1, myResolve, myReject);
                });


                //that.unityInstance = UnityLoader.instantiate("unityContainer",  that.settings["project_build"] || e["project_build"] || "https://d3arh16v2im64l.cloudfront.net/static/conversation/ProjectBuild.json", {onProgress: UnityProgress, onerror: UnityError});
                //unityInstance = that.unityInstance;
                //that.con.unityInstance = that.unityInstance;
                //break
            }catch(e){
                console.log("Cannot load unity due to error");
                console.log(e);
                if ( t == 0 ) {
                    alert("Cannot load 3d environment");
                }
                init(t-1, myResolve, myReject);
            }
        }
        var prom = new Promise(function(myResolve, myReject) {
            init(3, myResolve, myReject);
        });
        prom.progress  = (handler)=>{
            notify = handler
            return prom;
        }
        return prom
    }
    constructor(){

    }
}