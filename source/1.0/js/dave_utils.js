
var playBeep = (function beep() {
    var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
    return function() {
        snd.play();
    }
})();
var _empty_audio_played = false;
var playEmptyAudio= (function empty(){
    var snd = new Audio("data:audio/wav;base64,UklGRjIAAABXQVZFZm10IBIAAAABAAEAQB8AAEAfAAABAAgAAABmYWN0BAAAAAAAAABkYXRhAAAAAA==");
    return function(){
        if(_empty_audio_played) return;
        snd.play();
        _empty_audio_played=true
    }
})();
djq(document).ready(function(){
    djq(window).click(function(){
        playEmptyAudio();
    })
})
function set_cc(noresize) {
    var x = window.innerWidth;
    let canvas = djq("#unityContainer");
    if(window.innerWidth < 767){
        canvas.css("max-height", "50%");
        let s = djq( "#unityContainerP" ).children().first().offset()['top'] - djq( ".dave-chat").offset()['top'] ;
        djq( ".chat-header").css("top", s + "px")
        djq( ".dave-whiteboard").css("height", s+"px");
        djq( ".options-panel.image img").css("height", s+"px");
        canvas.css("width", 2*x + "px");
        canvas.css("height", x + "px");
        // canvas.css("bottom", "67px");
        canvas.css("right", "-35%");
        canvas.css("position", "absolute");
    } else {
        djq( ".chat-header").css("top", "unset")
        canvas.css("width", "100%");
        canvas.css("height", "100%");
    }
    if ( (!noresize) && set_cc.resize && Array.isArray(set_cc.resize) )  {
        reset_size.apply({}, set_cc.resize)
    }
}
function reset_size(offsetX, offsetY, scaleX, scaleY, call){
    offsetX = offsetX || 0;
    offsetY = offsetY || 0;
    scaleX = scaleX || 1;
    scaleY = scaleY || 1;
    try{
        if(window.innerWidth < 767){
            set_cc(true);
            setTimeout(function(){
                let canvas = document.getElementById("unityContainer");
                canvas.GLctxObject.GLctx.viewport(offsetX, offsetY, scaleX*canvas.width, scaleY*canvas.height);
                set_cc(true);
            }, 3000);
        } else if (window.innerWidth < window.innerHeight ){
            set_cc(true);
            setTimeout(function(){
                let canvas = document.getElementById("unityContainer");
                canvas.GLctxObject.GLctx.viewport(offsetX, offsetY, scaleX*canvas.width, scaleY*canvas.height);
            }, 3000);
        }    
    }
    catch(e){}
}

function getUrlParams(qd){
    qd = qd || {};
    if (location.search) location.search.substr(1).split("&").forEach(function (item) {
        var s = item.split("="),
            k = s[0],
            v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
        //(k in qd) ? qd[k].push(v) : qd[k] = [v]
        (qd[k] = qd[k] || []).push(v) // null-coalescing / short-circuit
    })
    return qd;
}

function Trim(strValue) {
    return strValue.replace(/^\s+|\s+$/g, '');
}

function replaceAll(strValue, matchval, replaceval="") {
    while(strValue.indexOf(matchval) > -1){
        strValue = strValue.replace(matchval, replaceval);
    }
    return strValue;
}


function _getCookie(key) {
    var result = null;
    if(document.cookie) {
    var mycookieArray = document.cookie.split(';');
    for(i=0; i<mycookieArray.length; i++) {
        var mykeyValue = mycookieArray[i].split('=');
        if(Trim(mykeyValue[0]) == key) result = mykeyValue[1];
    }
    }
    try{
    return JSON.parse(result);
    }catch(err){
    if(result) {
        return result;
    }
    }
    return null;
}

var deleteAllCookies = function() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substring(0, eqPos) : cookie;
        Utills.setCookie(name, "", -1);
    }
};

function _setCookie(key, value, hoursExpire) {
    if ( hoursExpire === undefined ) {
        hoursExpire = 24
    }
    var ablauf = new Date();
    var expireTime = ablauf.getTime() + (hoursExpire * 60 * 60 * 1000);
    ablauf.setTime(expireTime);
    if ( typeof value == "object" ) {
        value = JSON.stringify(value);
    }
    document.cookie = key + "=" + value + "; expires=" + ablauf.toGMTString() + "; path=/";
}
function loadScript(url, callback){
    var srpt = djq("<script />");
    srpt.attr("type", "application/javascript");
    srpt.attr("src", url);
    srpt.attr("async", false);
    srpt.attr("defer", false);
    djq("body").append(srpt);
    srpt.ready(callback);
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


function getFormattedDate(dt){
    if(typeof dt == "string"){
        dt = new Date(dt);
    }
    return dt.getDate()+"-"+(dt.getMonth()+1)+"-"+dt.getFullYear();
}


const InteractionStageEnum = Object.seal({
    OPENED:"opened",
    AUTO_OPENED: "auto_opened",
    SPEECH_INPUT: "speech-input", 
    MIC_ALLOWED :"mic_allowed", 
    MIC_REJECTED: "mic_rejected",
    MIC_REQUESTED:"mic_requested",
    CLICK: "click",
    TEXT_INPUT: "text-input",
    UNMUTED:"unmuted",
    MUTED:'muted',
    MINIMIZED:"minimized",
    RESUMED: "resumed",
    LEAVE_PAGE:"leave_page",
    set: function(key, val){
        this[key] = val;
    }
});


var isTouch = (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));

class Utills {
    static isTouch = isTouch;
    static makeid = makeid;
    static loadScript = loadScript;
    static getCookie = _getCookie;
    static setCookie = _setCookie;
    static getFormattedDate = getFormattedDate;
    static deleteAllCookies = deleteAllCookies;
    static cookie = {
        getCookie : _getCookie,
        setCookie : _setCookie
    };
    static stringOp = {
        trim: Trim,
        replaceAll: replaceAll
    }
}


var OneSignal = window.OneSignal || [];
var player_id;
OneSignal.push(function() {
    OneSignal.init({
        appId: "3ebf97b0-c92f-4fd7-a740-87cc4d1807db",
        autoRegister: true
    });
    OneSignal.getUserId(function(userId) {
        player_id = userId;
        console.log("OneSignal User ID:", userId);
    });
    OneSignal.isPushNotificationsEnabled(function(isEnabled) {
        if (isEnabled)
            console.log("Push notifications are enabled!");
        else
            console.log("Push notifications are not enabled yet.");    
    });
    OneSignal.on('subscriptionChange', function (isSubscribed) {
        console.log("The user's subscription state is now:", isSubscribed);
    });
    OneSignal.on('notificationDisplay', function (event) {
        var data = event.data;
    });
});


//Jquery add ons
(function(djq){
    function isMobile() {
        try{ document.createEvent("TouchEvent"); return true; }
        catch(e){ return false; }
    }
    djq.browser = {};
    djq.browser.mobile = isMobile();


    djq.makeOverlayGuide = function(settings){
        var ck = Utills.getCookie("overlayGuide");
        if(ck && ck["hints"]) return;
        var hints = settings["hints"];
        var arrow = settings["arrow"] || "http://d3chc9d4ocbi4o.cloudfront.net/arrow_guide.svg";
        var div = djq("<div />");
        if(djq.browser.mobile){
            div.attr("style", 'color: white; position: fixed;top: 0;left: 0;height: 100%;width: 100%;background: black;z-index: 100000;opacity: 0.5; font-size:15px');
        }else{
            div.attr("style", 'color: white; position: fixed;top: 0;left: 0;height: 100%;width: 100%;background: black;z-index: 100000;opacity: 0.5; font-size:20px');
        }
        var a = djq("<a href='javascript:void(0)' style='position: absolute; top: 10px; right: 10px; color: white;'>");
        a.html("Skip >");
        a.click(function(){
            div.remove();
            removed = true;
        });
        var removed = false;
        div.append(a);
        if(!settings["stepwise"]){
            div.click(function(){
                div.remove();
                removed = true;
            })
        }
        setTimeout(function(){
            if(!removed){
                div.remove();
            }
        }, 5000)
        djq("body").append(div);
        for(var i in hints){
            var offset = djq("#"+hints[i]["id"]).offset();
            if(!offset){
                continue;
            }
            var span = djq("<span style='position: absolute;'/>");
            var message;
            if(typeof hints[i]["hint"] == "object"){
                if(djq.browser.mobile){
                    message = hints[i]["hint"]["mobile"];
                }else{
                    message = hints[i]["hint"]["web"];
                }

            }else{
                message = hints[i]["hint"];
            }
            span.html("<b style='margin-left: -200px;margin-right: 30px; text-align: right; display: block;'>"+message+"</b>");
            var img = djq("<img src='"+arrow+"' style='height: 100px'/>");
            
            span.append(img);
            div.append(span);
            if(hints[i]["left"]){
                span.css("left", hints[i]["left"]);
            }else if((offset["left"] - djq("#"+hints[i]["id"])[0].offsetWidth/2) < 0){
                span.css("left", (offset["left"] + djq("#"+hints[i]["id"])[0].offsetWidth/2)+"px");
            }else{
                span.css("left", (offset["left"] - djq("#"+hints[i]["id"])[0].offsetWidth/2 )+"px");
            }
            span.css("top", (offset["top"] - (span.height() + djq(window).scrollTop() - 10)) +"px");
        }
        Utills.setCookie("overlayGuide", {"hints": "off"});
    }
})(djq);


(function(djq){
    djq.fn.enter = function(callback){
        var calb = djq.Callbacks();
        calb.add(callback);

        djq(this).keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                console.debug(djq(this).val(), "Entered")
                console.debug(calb);
                calb.fire(djq(this).val());
            }
        });
    }
    djq.fn.fit_text = function(text, min_size=null){
        let fontSize = 16;
        if( window.innerWidth < 767 ){
            fontSize = 16;
            min_size = min_size || 11;
        }else{
            min_size = min_size || 13;
            fontSize = 16;
        }
        var list_text = text.split("|");
        var len = list_text[0];
        for(var i of list_text){
            if(i.length > len.length){
                len = i;
            }
        }
        var span = djq("<span style='width: 100%;font-size:"+fontSize+"px;'/>");
        span.html(len);
        let el = djq(this);
        el.html(span);
        let l = el.parent().parent().height() - 10;
        for (let i = fontSize; i >= min_size; i--) {
            span.css("font-size", fontSize+"px");
            if ( l >= (span.height() * list_text.length) ) {
                break
            }
            fontSize--;
        }
        el.html("");
        for(var i of list_text){
            var span = djq("<span style='width: 100%'/>");
            span.css("font-size", fontSize+"px");
            span.html(i);
            el.append(span);
        }
    }
    $.fn.placeholder_effect = function(time){
        return;
        console.log($(this).html(), time);
        if(time == undefined){
            console.log("Placeholder effect :: ",$(this).attr("backup-text"));
            $(this).find("span").each(function(){
                if($(this).attr("backup-text")){
                    $(this).text($(this).text()+(" "+$(this).attr("backup-text")));
                }
                $(this).attr("backup-text", "");
                $(this).show();
            })
            return;
        }
        var count = 0 ;
        $(this).find("span").each(function(){
            for(var z of $(this).text().split(" ")){
                count = count + z.length;
            }
            $(this).attr("backup-text", $(this).text());
            $(this).hide();
            $(this).text("");
        });
        var ms = (time * 1000) / (count + 10);
        var cur = 1;
        var el = $(this);
        function show_next_word(){
            var span = el.find("span:nth-child("+cur+")");
            console.log(cur, ms, span);
            if(span.length == 0){
                if(span.attr("backup-text")){
                    span.text(
                        (span.text()+" "+span.attr("backup-text")).trim()
                    );
                    span.attr("backup-text", "");
                }
                span = el.find("span:nth-child("+(cur+1)+")");
                if(span.length == 0){
                    return;
                }else{
                    cur++;
                    show_next_word();   
                    return;
                }
            }
            var sent = (span.attr("backup-text") || "").split(" ");
            const firstWord = sent.shift();
            if(firstWord){
                span.text(
                    (span.text()+" "+firstWord).trim()
                );
            }
            span.show();
            if(sent.length == 0){
                cur++;
            }
            span.attr("backup-text", sent.join(" "));
            setTimeout(show_next_word, ms*firstWord.length);
        }
        show_next_word();
    }
})(jQuery);

function encodeFlac(binData, recBuffers, isVerify, isUseOgg){
    var ui8_data = new Uint8Array(binData);
    var sample_rate=0,
    channels=0,
    bps=0,
    total_samples=0,
    block_align,
    position=0,
    recLength = 0,
    meta_data;
    
    function write_callback_fn(buffer, bytes, samples, current_frame){
        recBuffers.push(buffer);
        recLength += bytes;
        // recLength += buffer.byteLength;
    }
    
    function metadata_callback_fn(data){
        console.info('meta data: ', data);
        meta_data = data;
    }
    
    
    var wav_parameters = wav_file_processing_read_parameters(ui8_data);	
    // convert the PCM-Data to the appropriate format for the libflac library methods (32-bit array of samples)
    // creates a new array (32-bit) and stores the 16-bit data of the wav-file as 32-bit data
    var buffer_i32 = wav_file_processing_convert_to32bitdata(ui8_data.buffer, wav_parameters.bps, wav_parameters.block_align);
    
    if(!buffer_i32){
        var msg = 'Unsupported WAV format';
        console.error(msg);
        return {error: msg, status: 1};
    }
    
    var tot_samples = 0;
    var compression_level = 5;
    var flac_ok = 1;
    var is_verify = isVerify;
    var is_write_ogg = isUseOgg;
    
    var flac_encoder = Flac.create_libflac_encoder(
        wav_parameters.sample_rate, 
        wav_parameters.channels, 
        wav_parameters.bps, 
        compression_level, 
        tot_samples, 
        is_verify
    );
    if (flac_encoder != 0){
        var init_status = Flac.init_encoder_stream(flac_encoder, write_callback_fn, metadata_callback_fn, is_write_ogg, 0);
        flac_ok &= init_status == 0;
        console.debug("flac init: " + flac_ok);
    } else {
        Flac.FLAC__stream_encoder_delete(flac_encoder);
        var msg = 'Error initializing the decoder.';
        console.error(msg);
        return {error: msg, status: 1};
    }
    
    
    var isEndocdeInterleaved = true;
    var flac_return;
    if(isEndocdeInterleaved){		
        flac_return = Flac.FLAC__stream_encoder_process_interleaved(
            flac_encoder, 
            buffer_i32, buffer_i32.length / wav_parameters.channels
            );
    } else {	
        var ch = wav_parameters.channels;
        var len = buffer_i32.length;
        var channels = new Array(ch).fill(null).map(function(){ return new Uint32Array(len/ch)});
        for(var i=0; i < len; i+=ch){
            for(var j=0; j < ch; ++j){
                channels[j][i/ch] = buffer_i32[i+j];
            }
        }
        
        flac_return = Flac.FLAC__stream_encoder_process(flac_encoder, channels, buffer_i32.length / wav_parameters.channels);
    }
    
    if (flac_return != true){
        console.error("Error: FLAC__stream_encoder_process_interleaved returned false. " + flac_return);
        flac_ok = Flac.FLAC__stream_encoder_get_state(flac_encoder);
        Flac.FLAC__stream_encoder_delete(flac_encoder);
        return {error: 'Encountered error while encoding.', status: flac_ok};
    }
    
    flac_ok &= Flac.FLAC__stream_encoder_finish(flac_encoder);
    
    Flac.FLAC__stream_encoder_delete(flac_encoder);
    
    return {metaData: meta_data, status: flac_ok};
}
function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}
function _isVerify() {
    return true;
}
function _isUseOgg() {
    return false;
}


function extendData(data, extend_object){
    extend_object = extend_object || {};
    data = data || {};
    let myPromise = new Promise(function(myResolve, myReject) {
        if(typeof extend_object == 'function'){
            var extend_data = extend_object(data);
            if(typeof extend_data == "object"){
                myResolve(extend_data);
            }else{
                myReject("Failed to extend object");
            }
        }else if(typeof extend_object == 'string'){
            djq.getJSON(extend_object, function(exd){
                var d = {...data, ...exd}
                myResolve(d);
            }, function(err){
                myReject(err);
            })
        }else{
            var d = {...data, ...extend_object}
            myResolve(d);
        }
    });
    return myPromise;
}

function todata(data, extend_object, callback){
    if(typeof extend_object == 'function'){
        extend_object(data, function(data){
            callback(data);
        })
    }else{
        var d = {...data, ...extend_object}
        callback(d);
    }
}


function detectVoice(stream, start, end) {
	// Create MediaStreamAudioSourceNode
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
	audioContext = new AudioContext();

    var source = audioContext.createMediaStreamSource(stream);
    // Setup options
    
    var options = {
        source: source,
        energy_threshold_ratio_neg: 0.3,
        stop_timeout: 1500,
        voice_stop: function() {
            console.debug('voice_stop');
            end();
        }, 
        voice_start: function() {
            console.debug('voice_start');
            start();
        }
    }; 

    // Create VAD
    var vad = new VAD(options);
}


(function(djq){
    djq.fn.setupConversationUi = function(settings){
        var dave_chat = djq("<div />");
        dave_chat.addClass("dave-chat");
        dave_chat.attr("id", "default-panel");

        var header = djq("<div />");
        header.addClass("chat-header");
        var full_screen = djq(`<a id="fullscreen" type="expand" class="butn butn-default butn-white fullscreen">
               <i class='fa fa-expand requestfullscreen'></i>
               <i class='fa fa-compress exitfullscreen' style="display: none"></i>
               </a>`);
        //var cc = djq('<button id="cc" class="butn butn-default butn-white cc" style="position: absolute;z-index: 12; font-size: 0.9rem"><i class="fa fa-commenting" aria-hidden="true"></i></button>');
        var mute = djq('<button id="mute" class="butn butn-dark cc butn-lg" style="font-size: 12px"><i class="fa fa-volume-up" aria-hidden="true"></i></button>');
        var close = djq('<button id="close_message" class="butn butn-dark cc butn-lg" style="z-index:20;font-size: 20px;position: absolute;right: -3px;border-radius: 50%;top: -10px;padding: 4px;padding-right: 10px;padding-left: 10px;"><i class="fa fa-times" aria-hidden="true"></i></button>');
        var buttons_panel = djq(`<div style='opacity: 1; position: absolute; z-index: 1000;'/>`);

        var message = djq(`<div class="message-block"><div class="dave-message" id="dave-message" style="">
               <span class="current-message" id="message" style=""></span>
               </div></div>`);

        //header.append(cc);
        buttons_panel.append(mute);
        //buttons_panel.append(full_screen);
        header.append(buttons_panel);
        header.append(message);
        dave_chat.append(close);

        var body = djq("<div style='overflow-x: hidden; overflow-y: hidden;'/>");
        body.addClass("chat-body");
        body.addClass("slim-scroll");

        var content = djq("<div class='content' />");
        var whiteboard = djq("<div class='whiteboard' id='whiteboard' />");
        var webgl = djq("<div class='webgl-content' />");
        var container = djq("<div id='unityContainerP' />")
        var cont = djq("<canvas id='unityContainer' style='background: transparent linear-gradient(0deg, #EBF0F9E5 0%, #EDF0F6EE 1%, #F0F1F3F8 4%, #F1F1F2FD 8%, #F2F2F2 18%) 0% 0% no-repe     at padding-box;'></canvas>");


        body.append(content);
        content.append(whiteboard);
        whiteboard.append(webgl);
        container.append(cont)
        webgl.append(container);

        var footer = $('<div class="bottom-wraper" id="reponse_message_panel">'+
                '<div style="text-align: center;">'+
               '<div class="reponse-wraper" style="padding: 10px 20px;">'+
               '<div class="cb__list"><span class="bubble typing">....</span></div>'+
               '</div>'+
               '</div>'+
               '</div>'+
               '<div class="bottom-wraper" id="reponse_panel">'+
               '<div id="try-saying">'+
               '<span class="try_saying"> Try saying </span>'+
               '</div>'+
               '<div id="shortcut"></div>'+
                '<div class="quick_access" id="quick_access"></div>'+
                '<div class="searchable" style="display:none">'+
               'Suggestions:'+
                    '<ul class="list-group" id="searchable">'+
                    '</ul>'+
                    '</div>'+
                '<div class="text-wraper" style="padding: 1.5px;">'+
               '<input type="text" style="line-height: 3; font-size: 12px;" id="input-text" placeholder="'+settings.placeholder+'"  autocomplete="off"></input>'+
                '<span class="send" id="input-send"><i class="fa fa-chevron-right"></i></span>'+
                '</div>'+
               '<div class="dave-input-buttons" style="position: unset; display: inline-block;padding-left: 10px;">'+
               '<button class="butn butn-default speak_butn" id="record">'+
               '<span><i id="microphone" class="fa fa-microphone fa-2x"> </i></span>'+
               '</button>'+
               '</div>'+
               '</div>');

        dave_chat.append(header);
        dave_chat.append(body);
        dave_chat.append(footer);
        djq(this).append(dave_chat);

        if(!djq.fullscreen || !djq.fullscreen.isNativelySupported()){
            djq("#fullscreen").hide()
        }
        djq('#fullscreen').click(function() {
            if(djq(this).attr("type") == "expand"){
                djq('body').fullscreen();
                djq(this).attr("type", "collapse")
                djq(this).find(".requestfullscreen").first().hide();
                djq(this).find(".exitfullscreen").first().show();
            }else{
                djq(this).attr("type", "expand")
                djq(this).find(".requestfullscreen").first().show();
                djq(this).find(".exitfullscreen").first().hide();
                djq.fullscreen.exit();
            }
            return false;
        });
        // djq('#fullscreen .exitfullscreen').click(function() {
        //     return false;
        // });
        // djq("#cc").click(function(){
        //     djq("#dave-message").toggle();
        //     var cc_class = djq(this).find("i").first().attr("class");
        //     if(cc_class.indexOf("fa-commenting") == -1){
        //         djq(this).find("i").first().attr("class", "fa fa-commenting");
        //     }else{
        //         djq(this).find("i").first().attr("class", "fa fa-times");
        //     }
        // });
    }

    djq.fn.setupMetaverseConversationUi = function(settings){
        var dave_chat = djq("<div />");
        dave_chat.addClass("dave-chat");
        dave_chat.attr("id", "default-panel");

        var header = djq("<div />");
        header.attr("style", "background: rgba(0, 0, 0, 0.3);color: white;");
        header.addClass("chat-header");
        var full_screen = djq(`<a id="fullscreen" type="expand" class="butn butn-default butn-white fullscreen">
               <i class='fa fa-expand requestfullscreen'></i>
               <i class='fa fa-compress exitfullscreen' style="display: none"></i>
               </a>`);
        //var cc = djq('<button id="cc" class="butn butn-default butn-white cc" style="position: absolute;z-index: 12; font-size: 0.9rem"><i class="fa fa-commenting" aria-hidden="true"></i></button>');
        var mute = djq('<button id="mute" class="butn white cc butn-lg" style="font-size: 12px"><i class="fa fa-volume-up" aria-hidden="true"></i></button>');
        var close = djq('<button id="close_message" class="butn butn-dark cc butn-lg" style="z-index:20;font-size: 20px;position: absolute;right: -3px;border-radius: 50%;top: -10px;padding: 4px;padding-right: 10px;padding-left: 10px;"><i class="fa fa-times" aria-hidden="true"></i></button>');
        var buttons_panel = djq(`<div style='opacity: 1; position: absolute; z-index: 1000;'/>`);

        var message = djq(`<div class="message-block"><div class="dave-message" id="dave-message" style="">
               <span class="current-message" id="message" style=""></span>
               </div></div>`);

        //header.append(cc);
        buttons_panel.append(mute);
        //buttons_panel.append(full_screen);
        header.append(buttons_panel);
        header.append(message);
        // dave_chat.append(close);

        var body = djq("<div style='overflow-x: hidden; overflow-y: hidden;'/>");
        body.addClass("chat-body");
        body.addClass("slim-scroll");

        var content = djq("<div class='content' />");
        var whiteboard = djq("<div class='whiteboard' id='whiteboard' />");
        var webgl = djq("<div class='webgl-content' />");
        var container = djq("<div id='unityContainerP' />")
        var cont = djq("<canvas id='unityContainer' style='background: transparent linear-gradient(0deg, #EBF0F9E5 0%, #EDF0F6EE 1%, #F0F1F3F8 4%, #F1F1F2FD 8%, #F2F2F2 18%) 0% 0% no-repeat padding-box;'></canvas>");


        body.append(content);
        content.append(whiteboard);
        whiteboard.append(webgl);
        container.append(cont)
        webgl.append(container);

        var footer = djq(`
                <style>
                    #input-text::placeholder{ color: rgba(255,255,255,0.7);}
                    .dave_conversation .speak_butn{ background-color: rgba(255,255,255,0.5) }
                </style>
               <div class="bottom-wraper" id="reponse_message_panel">
               <div style="text-align: center;">
               <div class="reponse-wraper" style="padding: 10px 20px;">
               <div class="cb__list"><span class="bubble typing">....</span></div>
               </div>
               </div>
               </div>
               <div class="bottom-wraper" id="reponse_panel">
               <div id="try-saying">
               <span class="try_saying"> Try saying </span>
               </div>
               <div id="shortcut"></div>
               <div class="quick_access" id="quick_access">
               </div>
               <div class="text-wraper" style="padding: 1.5px;background-color:rgba(255,255,255, 0.5)">
               <input type="text" style="line-height: 3; font-size: 12px; color:white; " id="input-text" placeholder="${settings.placeholder}"  autocomplete="off"></input>
               <span class="send" id="input-send"><i class="fa fa-chevron-right"></i></span>
               </div>
               <div class="dave-input-buttons" style="position: unset; display: inline-block;padding-left: 10px;">
               <button class="butn butn-default speak_butn" id="record">
               <span><i id="microphone" class="fa fa-microphone fa-2x"> </i></span>
               </button>
               </div>
               </div>
               `);

        dave_chat.append(header);
        dave_chat.append(body);
        dave_chat.append(footer);
        djq(this).append(dave_chat);

        if(!djq.fullscreen || !djq.fullscreen.isNativelySupported()){
            djq("#fullscreen").hide()
        }
        djq('#fullscreen').click(function() {
            if(djq(this).attr("type") == "expand"){
                djq('body').fullscreen();
                djq(this).attr("type", "collapse")
                djq(this).find(".requestfullscreen").first().hide();
                djq(this).find(".exitfullscreen").first().show();
            }else{
                djq(this).attr("type", "expand")
                djq(this).find(".requestfullscreen").first().show();
                djq(this).find(".exitfullscreen").first().hide();
                djq.fullscreen.exit();
            }
            return false;
        });
        // djq('#fullscreen .exitfullscreen').click(function() {
        //     return false;
        // });
        // djq("#cc").click(function(){
        //     djq("#dave-message").toggle();
        //     var cc_class = djq(this).find("i").first().attr("class");
        //     if(cc_class.indexOf("fa-commenting") == -1){
        //         djq(this).find("i").first().attr("class", "fa fa-commenting");
        //     }else{
        //         djq(this).find("i").first().attr("class", "fa fa-times");
        //     }
        // });
    }
})(djq);
