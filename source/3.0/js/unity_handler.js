
var product_obj,
unityInstance,
build_type = "unity";
function onPictureTaken(ImageBase64) {
  console.log(ImageBase64);
  
  if (!window.__dv_cbs["DownloadSelfy"]) {
    var a = document.createElement("a");
    a.href = ImageBase64;
    a.download = new Date().toISOString() + ".png";
    a.click();
  } else {
    window.__dv_cbs_get("DownloadSelfy").fire(ImageBase64);
  }
}
function keyaLoadedCallback() {
  console.log("Keya loaded on page");
  keya_loaded = true;
  window.__dv_cbs_get("KeyaLoadedCallback").fire();
}
function assistantLoadedCallback() {
  console.log("Assistant loaded on page");
  keya_loaded = true;
  window.__dv_cbs_get("AssistantLoadedCallback").fire();
}
function receiveMessageFromUnity(id) {
  var aud = document.getElementById("audio");
  console.debug("--------", id);
  if (aud.getAttribute("data-audio-id") != id) {
    unityInstance.stopTalking();
    // try{
    //     unityInstance.SendMessage("head", "stopTalking")
    // }catch(e){
    //     console.log("Were not able to do stop talking", e);
    // }
    return;
  }
  if (aud) {
    console.log(
      "Audio duration :: ",
      aud.duration,
      "Audio currentTime :: ",
      aud.currentTime
      );
      var muted = aud.getAttribute("force-mute") || "false";
      console.debug("Audio muted ::", muted);
      if (muted == "false") {
        aud.muted = false;
      } else {
        aud.muted = true;
      }
      // aud.play();
      const promise = aud.play();
      
      if (aud.getAttribute("defaults") == "false") {
        djq("#message").placeholder_effect(aud.duration);
      }
      if (!aud.onended) {
        aud.onended = function() {
          var audio = document.getElementById("audio");
          if (audio.getAttribute("data-audio-id") != id) {
            unityInstance.stopTalking();
            djq("#message").placeholder_effect();
          }
          // try{
          //     unityInstance.SendMessage("head", "stopTalking")
          // }catch(e){
          //     console.log("Were not able to do stop talking", e);
          // }
        };
      }
      if (promise !== undefined) {
        promise
        .then(() => {
          console.debug("Got the audio load promise here");
        })
        .catch(function(err) {
          console.log(err);
          // try{
          //     unityInstance.SendMessage("head", "stopTalking")
          // }catch(e){
          //     console.log("Were not able to do stop talking", e);
          // }
          aud.onended();
          window.__dv_cbs_get("AudioPlayFailed").fire();
        });
      }
    } else {
      console.log("Aud is empty");
    }
  }
  
  var default_bg,
  defaul_avatar_id = "dave";
  var screen_loading = true;
  function sceneLoadedCallback(params) {
    console.log("scene loaded callback");
    screen_loading = false;
    if (params) { 
      window.__dv_cbs_get("SceneLoaded").fire(params);
      return;
    }
    if (default_bg) {
      unityInstance.setTexture(default_bg);
    }
    if (build_type == "unity") {
      unityInstance.showPersona(defaul_avatar_id);
    }
    
  }
  
  function avatarLoadedCallback() {
    unityInstance.loadedchar = true;
    window.__dv_cbs_get("AvatarLoaded").fire();
  }
  
  function daveSceneLoaded() {
    //sceneLoadedCallback();
  }
  
  function enteredAreaCallback(type, value) {
    if (type == "cs") {
      window.__dv_cbs_get("enteredArea").fire(value);
    } else if (type == "point") {
      window.__dv_cbs_get("pointReached").fire(value);
    } else {
      window.__dv_cbs_get("enteredArea-" + type).fire(value);
    }
  }
  function requestData() {
    window.__dv_cbs_get("unityDataRequest").fire();
  }


function onCallback(jsString) { 
  var jsn = JSON.parse(jsString);
  console.debug("onCallback from unity", jsn);

  if (jsn["action"] == "SceneLoaded") {
    sceneLoadedCallback(jsString);
  } else { 
    window.__dv_cbs_get("UnityActionRequest").fire(jsn);
  }
}
  
  class UnityManager {
    constructor(unityInstance, settings) {
      this.unityInstance = unityInstance;
      this.setAvatar = false;
      this.head_name = settings["head_name"] || "head";

      this.dynamic_unity = settings["dynamic_unity"] || false;

      this.keyboard_enable = false;
      this.joystick_enable = false;
      this.muted = false;
    }
    updateSettings() {
      var sets = {
        "action": "ChangeSettings",
        "mute": this.muted,
        "keyboardActive": this.keyboard_enable,
        "joystick": this.joystick_enable
      }
      console.debug("Updating Settings ::: ", sets)
      this.callGameManagerFunction("UnityFunction", JSON.stringify(sets));
    }
    keyboardControlEnable() {
      console.log("keyboard control enabled");
      if (this.dynamic_unity) { 
        this.keyboard_enable = true;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "keyboardControlEnable",
          ""
        );
      } catch (e) {
        console.warn("Unable to enable keyboard controls", e);
      }
    }
    keyboardControlDisable() {
      console.log("keyboard control disabled");
      if (this.dynamic_unity) { 
        this.keyboard_enable = false;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "keyboardControlDisable",
          ""
        );
      } catch (e) {
        console.warn("Unable to disable keyboard controls", e);
      }
    }
    joystickControlEnable() {
      console.log("joystick control enable");
      if (this.dynamic_unity) { 
        this.joystick_enable = true;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "joystickControlEnable",
          ""
        );
      } catch (e) {
        console.warn("Unable to enable joystick controls", e);
      }
    }
    joystickControlDisable() {
      console.log("joystick control disabled");
      if (this.dynamic_unity) { 
        this.joystick_enable = false;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "joystickControlDisable",
          ""
        );
      } catch (e) {
        console.warn("Unable to disable joystick controls", e);
      }
    }
    stopTalking() {
      console.log("Trying to Stop talking");
      try {
        this.unityInstance.SendMessage(this.head_name, "stopTalking");
      } catch (e) {
        console.warn("Unable stop talking", e);
      }
    }
            
    startTalking(id, frames, shapes) {
      console.log("Trying to Start talking ::", id);
      try {
        this.unityInstance.SendMessage(
          this.head_name,
          "StartTalking",
          JSON.stringify({
            frames: frames,
            shapes: shapes,
            audio_id: id
          })
        );
      } catch (e) {
        console.warn("Unable to make avatar talk");
      }
    }
    showPersona(id) {
      console.log("Trying to show persona ::", id);
      if (this.dynamic_unity) { 
        this.callGameManagerFunction("UnityFunction", JSON.stringify({"action":"ShowPersona","avatar_name":id}));

        return;
      }
      try {
        this.unityInstance.SendMessage("GameManagerOBJ", "showPersona", id);
        this.setAvatar = true;
      } catch (e) {
        console.warn("Were not able to update persona", e);
      }
    }
    setTexture(bg) {
      console.log("Setting texture :: ", bg);
      try {
        this.unityInstance.SendMessage("GameManagerOBJ", "setTexture", bg);
      } catch (e) {
        console.warn("Were not able to update texture", e);
      }
    }
    callGameManagerFunction(key, val) {
      console.log("Calling game manager object :: ", key, val);
      try {
        this.unityInstance.SendMessage("GameManagerOBJ", key, val);
      } catch (e) {
        console.warn(`Failed to call GameManagerOBJ function '${key}'`, e);
      }
    }
    muteBGM() {
      if (this.dynamic_unity) { 
        this.muted = true;
        this.updateSettings();
        return
      }
      this.callGameManagerFunction("mute", "");
    }
    unmuteBGM() {
      if (this.dynamic_unity) { 
        this.muted = false;
        this.updateSettings();
        return
      }
      this.callGameManagerFunction("unmute", "");
    }
    callAssistantFunction(key, val) {
      console.log("Calling assistant function :: ", key, val);
      this.unityInstance.SendMessage("Assistantcode", key, val);
    }
  }

class BabylonManager {
  constructor(model_path, engine, scene, camera, settings) {
    console.log("Settings in Babylon Manager", settings);
    this.engine = engine;
    this.scene = scene;
    this.camera = camera;
    this.talk = false;
    this.lastTime = 0;
    this.shapesIndex = 0;
    this.framesIndex = 0;
    this.snd = null;
    this.shapes = null;
    this.frames = null;
    this.headers = [];
    this.volume = settings["volume"] || 1.0;
    this.queue = [];
    
    this.settings = settings;
    this.setAvatar = true;
    this.head_name = settings["head_name"] || "head";
    this.loadScene(model_path);
    //this.sceneLoadedCallback();
    this.render();
  }
  
  loadScene(model_path) {
    model_path =
    "https://s3.ap-south-1.amazonaws.com/models.iamdave.ai/glb-models/dave_waist.glb";
    var that = this;
    BABYLON.SceneLoader.ImportMesh("", "", model_path, that.scene, function() {
      that.scene.createDefaultCameraOrLight(false, false, false);
      that.scene.animationGroups.forEach(function(animationGroup) {
        animationGroup.stop();
      });
      //that.sceneLoadedCallback();
    });
  }
  
  render() {
    var that = this;
    that.engine.runRenderLoop(function() {
      if (that.talk) {
        let shape = that.shapes[that.shapesIndex];
        let x = (Date.now() - that.lastTime) / 1000;
        if (!shape) {
          that.talk = false;
          that.shapesIndex = 0;
          that.framesIndex = 0;
          that.scene.render();
          return;
        }
        while (x > shape["timestamp"]) {
          console.debug("Went ahead");
          that.shapesIndex += 1;
          shape = that.shapes[that.shapesIndex];
          if (!shape) {
            that.talk = false;
            that.shapesIndex = 0;
            that.framesIndex = 0;
            that.scene.render();
            return;
          }
        }
        if (x >= shape["timestamp"] - 1.0 / 25.0 && x <= shape["timestamp"]) {
          that.setMorphTargets(shape);
          that.shapesIndex += 1;
        } else if (x < shape["timestamp"] - 1.0 / 25.0) {
          console.debug("Lagging behind");
        }
        var frame = that.frames[that.framesIndex];
        if (frame != null && x >= frame["timestamp"]) {
          that.playAnimation(frame);
          that.framesIndex += 1;
        }
        if (that.shapesIndex >= that.shapes.length) {
          that.talk = false;
          that.shapesIndex = 0;
          that.framesIndex = 0;
        }
      }
      that.scene.render();
    });
  }
  
  //Callback function
  sceneLoadedCallback() {
    
  }
  
  play(response) {
    //console.log("PLAY IS CALLING", response);
    var that = this;
    if (that.snd) {
      console.warn("Sound already playing");
      that.queue.push(response);
      return that.snd;
    }
    that.snd = new Audio(response["voice"]);
    that.shapes = that.processData(response["shapes"]);
    that.frames = that.processDataFrames(response["frames"]);
    that.shapesIndex = 0;
    that.framesIndex = 0;
    that.snd.volume = that.volume; //that.volume;
    that.snd.addEventListener("ended", function() {
      that.snd.pause();
      that.pauseAnimsandMorphs();
      that.snd = null;
      if (that.queue.length > 0) {
        that.play(that.queue.shift());
      }
    });
    that.snd.addEventListener("canplay", function() {
      //DAVE_SETTINGS.execute_custom_callback("on_canplay_audio", [snd]);
      
      that.execute_custom_callback("on_canplay_audio", [that.snd]);
      that.lastTime = Date.now();
      that.talk = true;
      try {
        that.snd.play();
      } catch (err) {
        console.error(err);
        that.snd = null;
      }
    });
    that.snd.addEventListener("pause", function() {
      that.pauseAnimsandMorphs();
    });
    return that.snd;
  }
  
  execute_custom_callback(str, param) {
    console.log("Hello from execute_custom_callback");
  }
  
  processData(allText) {
    var that = this;
    var allTextLines = allText.split(/\r\n|\n/);
    that.headers = allTextLines[0].split(",");
    var lines = [];
    for (var i = 1; i < allTextLines.length; i++) {
      var data = allTextLines[i].split(",");
      if (data.length == that.headers.length) {
        var tarr = {};
        for (var j = 0; j < that.headers.length; j++) {
          tarr[that.headers[j]] = parseFloat(data[j]);
        }
        lines.push(tarr);
      }
    }
    return lines;
  }
  
  processDataFrames(allText) {
    var that = this;
    var allTextLines = allText.split(/\r\n|\n/);
    that.headers = allTextLines[0].split(",");
    var lines = [];
    for (var i = 1; i < allTextLines.length; i++) {
      var data = allTextLines[i].split(",");
      if (data.length == that.headers.length) {
        var tarr = {};
        for (var j = 0; j < that.headers.length; j++) {
          console.debug("got ani data = " + that.headers[j] + " = " + data[j]);
          if (that.headers[j] == "timestamp") {
            tarr[that.headers[j]] = parseFloat(data[j]);
          } else {
            tarr[that.headers[j]] = data[j];
          }
        }
        lines.push(tarr);
      }
    }
    return lines;
  }
  
  setMorphTargets(shape) {
    var that = this;
    for (var key in shape) {
      if (key != "timestamp" && shape[key] != null) {
        var s = that.scene.getMorphTargetByName(key);
        if (s !== null) {
          s.influence = shape[key];
        }
      }
    }
  }
  
  pauseAnimsandMorphs() {
    var that = this;
    that.talk = false;
    that.stopMorphs();
    that.playIdleAnimation();
  }
  
  playAnimation(animationName) {
    var that = this;
    if (animationName["animation"] != null) {
      console.log("trying to play = " + animationName["animation"]);
      let ani = that.scene.getAnimationGroupByName(animationName["animation"]);
      if (ani) {
        try {
          ani.play(false);
          ani.loopAnimation = false;
          ani.isAdditive = true;
        } catch (error) {
          console.error(error);
        }
      }
    }
  }
  
  stopMorphs() {
    var that = this;
    if (that.headers != []) {
      for (i = 1; i < that.headers.length; i++) {
        var s = that.scene.getMorphTargetByName(that.headers[i]);
        if (s != null) {
          s.influence = 0.00000001;
        }
      }
    }
  }
  
  playIdleAnimation() {
    var that = this;
    let ani = null;
    that.scene.animationGroups.every(function(a) {
      if (a.name.match(/weight_shift/g) || a.name.match(/idle/g)) {
        ani = a;
        return false;
      }
      return true;
    });
    if (ani) {
      console.debug("trying to play idle animation");
      try {
        ani.play(true);
        ani.loopAnimation = true;
      } catch (error) {
        console.log(error);
      }
    }
  }
  
  muteAvatar() {
    var that = this;
    if (that.snd) {
      that.snd.volume = 0.0;
    }
    that.volume = 0.0;
  }
  
  unmuteAvatar() {
    var that = this;
    if (that.snd) {
      that.snd.volume = 1.0;
    }
    that.volume = 1.0;
  }
  
  muteBGM() {
    console.log("MUTE AVATAR");
    var that = this;
    that.muteAvatar();
    //this.callGameManagerFunction("mute", "");
  }
  
  unmuteBGM() {
    console.log("UNMUTE AVATAR");
    var that = this;
    that.unmuteAvatar();
    //this.callGameManagerFunction("unmute", "");
  }
  
  stopTalking() {
    var that = this;
    if (that.snd) {
      that.snd.pause();
      that.pauseAnimsandMorphs();
      that.snd = null;
      that.queue = [];
    }
    
    /*try {
      this.unityInstance.SendMessage(this.head_name, "stopTalking");
    } catch (e) {
      console.warn("Unable stop talking", e);
    }*/
  }
  
  startTalking(id, frames, shapes) {
    console.log("START TALKING ID", id);
    console.log("START TALKING frames", frames);
    console.log("START TALKING shapes", shapes);
    try {
      var that = this;
      // that.snd = new Audio(response["voice"]);
      that.shapes = that.processData(shapes);
      that.frames = that.processDataFrames(frames);
      that.shapesIndex = 0;
      that.framesIndex = 0;
      that.talk = true;
      that.lastTime = Date.now();
      receiveMessageFromUnity(id);
      return that.snd;
    } catch (e) {
      console.warn("Unable to make avatar talk");
    }
  }
  
  showPersona(id) {
    
  }
  
  setTexture(bg) {
    
  }
}

class SetupEnviron {
  static load(_b, settings) {
    if (build_type == "babylon") {
      var scene = null;
      var camera = null;
      var assetsManager = null;
      
      function init(t, myResolve, myReject) {
        var canvas = document.getElementById("unityContainer");
        var engine = new BABYLON.Engine(
          canvas,
          true,
          {
            timeStep: 1.0 / 10.0,
            deterministicLockstep: true,
            lockstepMaxSteps: 30,
            limitDeviceRatio: 3
          },
          true
        );
          
        var createScene = function() {
          scene = new BABYLON.Scene(engine);
          scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
          camera = new BABYLON.FreeCamera(
            "camera_dave",
            new BABYLON.Vector3(0, 1.5, 2),
            scene
          );
          camera.setTarget(new BABYLON.Vector3(0, 1.5, 0));
          camera.viewport = new BABYLON.Viewport(0, -1, 1, 2.75);
          //camera.attachControl(canvas, false);
          assetsManager = new BABYLON.AssetsManager(scene);
          return scene;
        };
          
        var onetime = 1;
        scene = createScene();
        
        unityInstance = new BabylonManager(_b, engine, scene, camera, settings);
        myResolve(unityInstance);
      }
        
      var prom = new Promise(function(myResolve, myReject) {
        init(3, myResolve, myReject);
      });
      prom.progress = handler => {
        notify = handler;
        return prom;
      };
      return prom;
        
      //this.loadScene(model_path, sceneLoadedCallback);
      // var that = this;
    } else {
      var progress = 0;
      var notify;
      var buildUrl = _b;
      
      if (settings && settings["default_bg"]) {
        default_bg = settings["default_bg"];
      } else if (
        settings &&
        settings["customizables"] &&
        settings["customizables"]["bg"]
      ) {
        default_bg = settings["customizables"]["bg"];
      } else if (settings && settings["space_id"]) {
        default_bg = settings["space_id"];
      }
      
      if (settings && (settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"])) {
          defaul_avatar_id = settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"];
      }
        
      function init(t, myResolve, myReject) {
        function UnityError(message, err) {
          console.log(message, err);
          //that.errorC("error", message);
          myReject(message);
        }
        function UnityLoaded(ut) {
          unityInstance = new UnityManager(ut, settings);
          myResolve(unityInstance);
        }
        function UnityProgress(prog) {
          if (prog == 1) {
            console.log("Model loaded");
          }
          progress = prog;
          console.debug("Loading..", progress);
          if (progress % 0.1) {
            console.log("Loading..", progress);
          }
          if (notify) {
            notify(progress);
          }
        }
        if (t == 0) {
          return;
        }
        try {
          var config = {
            dataUrl: buildUrl + "/ProjectBuild.data",
            frameworkUrl: buildUrl + "/ProjectBuild.framework.js",
            codeUrl: buildUrl + "/ProjectBuild.wasm",
            streamingAssetsUrl: "StreamingAssets",
            companyName: "SocioGraph Solution",
            productName: "Conversation Bot",
            productVersion: "0.1"
          };
          
          createUnityInstance(
            document.querySelector("#unityContainer"),
            config,
            UnityProgress
          )
          .then(UnityLoaded)
          .catch(function(a, b) {
            UnityError(a, b);
            init(t - 1, myResolve, myReject);
          });
            
          //that.unityInstance = UnityLoader.instantiate("unityContainer",  that.settings["project_build"] || e["project_build"] || "https://d3arh16v2im64l.cloudfront.net/static/conversation/ProjectBuild.json", {onProgress: UnityProgress, onerror: UnityError});
          //unityInstance = that.unityInstance;
          //that.con.unityInstance = that.unityInstance;
          //break
        } catch (e) {
          console.log("Cannot load unity due to error");
          console.log(e);
          if (t == 0) {
            alert("Cannot load 3d environment");
          }
          init(t - 1, myResolve, myReject);
        }
      }
      var prom = new Promise(function(myResolve, myReject) {
        init(3, myResolve, myReject);
      });
      prom.progress = handler => {
        notify = handler;
        return prom;
      };
      return prom;
    }
  }
  constructor() {}
}
