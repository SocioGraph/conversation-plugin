
class Conversation extends DaveService {
    conversation_data = {};
    conversation = {};
    current_response = {};
    keywords = [];
    _req = null;
    session_id = null;
    session_start_time = null;
    init_response = null;
    voice_id = null;
    constructor(host, enterprise_id, conversation_id, settings) {
        //class Conversation
        super(host,enterprise_id, settings);
        this.settings = settings;

        //Setting the conversation and streamer
        this._wakeup = settings["wakeup"] ? settings["wakeup"] : (settings["wakeup_url"] ? true : false);
        this._asr = settings["asr"] ? settings["asr"] : (settings["speech_recognition_url"] ? true : false);
        this._image_processing = settings["image_processing"]? settings["image_processing"]: (settings["image_recognition_url"] ? true: false)
        this._wakeup_server = settings["wakeup_url"];
        this._asr_server = settings["speech_recognition_url"];
        this._image_server = settings["image_recognition_url"];
        this._image_classifier = settings["image_recognition_url"] ? "FaceDetectorCafe" : undefined;
        this._frames_to_capture = 30;
        this._recognizer = settings["recognizer_type"] || 'google'; //Google || Kaldi || Reverie || ..
        this._model_name = "indian_english";
        this._auto_asr_detect_from_wakeup = settings["auto_asr_detect_from_wakeup"] == undefined ? false : settings["auto_asr_detect_from_wakeup"];
        this._vad = settings["vad"];
        this._audio_auto_stop = 10;
        this._asr_type = settings["speech_recognition_type"] || "full"; //full-'flacking'||"chunks"-'asteam'||"direct"-'sending-flag',
        this._wakeup_type = settings["wakeup_type"] || "chunks"; //chunks-'asteam'||"direct"-'sending-flag',
        this._video_type = settings["video_stream_type"] || "chunks"; //chunks-'asteam'||"direct"-'sending-flag'
        this._additional_conversation_info = settings["additional_conversation_info"] || {};

        this.setConversation(conversation_id);


        //session info
        this._session_model = this.settings["session_model"];
        if (this._session_model) { 
            this._session_user_id_attr = this.settings["session_user_id_attr"] || this.settings["user_id_attr"];
            this._session_id_attr = this.settings["session_id_attr"] || this._session_model.toLowerCase()+"_id";
        }
    }
    setConversation(conversation_id) { 
        this.conversation_id = conversation_id;
        this._loading_conversation = true;
        this._loading_conversation_keywords = true;
        this.voice_id = this.settings["voice_id"];

        
        var streamer_settings = {
            wakeup : this._wakeup,
            asr : this._asr,
            image_processing : this._image_processing,
            wakeup_server : this._wakeup_server, 
            asr_server : this._asr_server, 
            image_server :  this._image_recognition_url,
            image_classifier : this._image_classifier,  
            frames_to_capture : this._frames_to_capture,
            recognizer : this._recognizer, //Google || Kaldi || Reverie || ..
            model_name : this._model_name,
            conversation_id : conversation_id,
            enterprise_id : this.enterprise_id, 
            base_url : this.host_url,
            auto_asr_detect_from_wakeup: this._auto_asr_detect_from_wakeup,
            vad : this._vad,
            audio_auto_stop: this._audio_auto_stop,
            asr_type :  this._asr_type,
            wakeup_type : this._wakeup_type,
            video_type : this._video_type,
            asr_additional_info :this._additional_conversation_info,
            event_callback : (event, data) => {
                console.log(event, data);
                this.eventCallback(event, data);
            }
        }
        this.streamer = new Streamer(streamer_settings);
        this.loadConversation();
        this.loadKeywords();
    }

    changeUser(user_id, api_key) {
        super.changeUser(user_id, api_key);
        this.user_id = user_id;
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.user_id;
        this.engagement_id = undefined;
        this.session_id = undefined;
    }
    
    triggerEvents(status, message){
        window.__dv_cbs_get("ConversationAction").fire(status, message);
    }

    post_interaction(obj, beacon=false){
        var user_id_attr = this.settings["interaction_user_id_attr"] || this.settings["user_id_attr"]
        obj[user_id_attr] = this.user_id;

        if(!obj[this.settings["stage_attr"] || "stage"]){
            console.debug("Interaction disabled")
            return;
        }

        extendData(obj, this.settings["additional_interaction_info"]).then((exdata)=>{
            if(!exdata){
                return;
            }
            if(beacon){
                this.sendBeacon(`/object/${this.settings["interaction_model"]}`, exdata);
            }else{
                this.post(this.settings["interaction_model"], exdata, (data)=>{ 
                    console.debug(data)
                    this.eventCallback("interaction_response", data);
                }, (err)=>{
                    console.error(err)
                    this.eventCallback("interaction_response", err.error || err);
                });
            }
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }

    startSession(async=false){
        if(!this._session_model || this.session_id){
            return;
        }
        
        this.session_start_time = (new Date()).getTime();
        var obj = {
            [this._session_user_id_attr]: this.user_id,
            "conversation_id": this.conversation_id,
            
        };
        if(async && crypto){
            obj["_async"] = true;
            obj[this._session_id_attr] = crypto.randomUUID();
        }
        extendData(obj, this.settings["additional_session_info"]).then((exdata)=>{
            this.post(this._session_model, exdata, (data)=>{ 
                console.debug(data)
                if(async && crypto){
                    data[this._session_id_attr] = obj[this._session_id_attr];
                }
                
                this.eventCallback("start_session", data);
                this.session_id = data[this._session_id_attr];
            }, (err)=>{
                console.error(err)
                this.eventCallback("start_session", err.error || err);
            });
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }
    updateSession(async, beacon=false){
        if(!this.settings["session_model"] || !this.session_start_time || !this.session_id){
            return;
        }
        var that = this;
        var diff = new Date().getTime() - this.session_start_time;
        var obj = {
            "_async": true,
            "async": async == undefined ? true : false,
            "conversation_id": this.conversation_id,
            [this.settings["session_duration_attr"] || "session_duration"]: diff/1000
        };
        this.session_start_time = undefined;
        if(beacon){
            this.sendBeacon(`/patch/${this._session_model}/${this.session_id}`, obj);
        }else{
            this.iupdate(this._session_model, this.session_id, obj, (data)=>{ 
                console.debug(data)
                this.eventCallback("update_session", data);
                // deleteAllCookies();
            }, function(err){ 
                console.error(err)
                this.eventCallback("update_session", err.error || err);
            });
        }
    }
    endSession(obj){
        if(!this._session_model){
            return;
        }
        var obj = { "_async": true, "conversation_id": this.conversation_id };
        this.update(this._session_model, this.session_id, obj, (data)=>{ 
            console.debug(data)
            this.eventCallback("end_session", data);
            deleteAllCookies();
        }, function(err){
            console.error(err)
            this.eventCallback("end_session", err.error || err);
        });
    }
    
    
    eventCallback(status, message){
        console.log(status, message);
        //class Conversation
        if(status == "error"){
            this.triggerEvents("idel");
            //that.precessError({});
            this.processing = false;
        }else if(status == "interaction"){
            this.post_interaction({[this.settings["stage_attr"] || "stage"]: message});
        }else if(status == "mic_rejected" || status == "socket_error"){
            this.triggerEvents("mic_rejected");
        }else if(status == "asr-convResp"){
            this.processing = false;
            this.state = "idel";
            if(this.timeout_level_3){
                clearTimeout(this.timeout_level_3);
                this.timeout_level_3 = undefined;
            }
            if(this.timeout_timeout){
                clearTimeout(this.timeout_timeout);
                this.timeout_timeout = undefined;
            }
            
            if( typeof message  === "object" ){
                var dt = message || {};
                dt["auto_recording"] = message["auto_recording"];
                if( !dt["name"] ){
                    console.log("--------------- error response", dt);
                    this.triggerEvents("conversation-error", dt);        
                }else{
                    this.triggerEvents("conversation-response", dt);
                }
            }else{
                console.debug("--------------- not a json response", message);
                this.triggerEvents("conversation-error", dt);
            }
        }else if(status == "asr-recText"){
            console.debug("level -2 callback")
            this.triggerEvents("speech-detected", message.recognized_speech || "");
            var that = this;
            that.timeout_level_3 = setTimeout(function(){
                console.debug("level -3 callback")

                if(that.processing){
                    that.triggerEvents("delay", message.recognized_speech || "");
                }
            }, 15000);

        }else if(status == "imagews-capture_ended"){
            this.triggerEvents("capture_ended", message);
        }else if(status == "imagews-results"){
            if(message["classifier_result"]){
                for(var z of message["classifier_result"]){
                    if(z["Result"] == "Face"){
                        this.triggerEvents("person_detected", message);
                        if(this.settings["auto_image_stream_stop"] != false){
                            this.stopImageStream();
                        }
                    }
                }
            }
        }else if(status == "more_delay"){
            this.triggerEvents("delay","");
        }else if(status == "timeout"){
            if(this.state != "idel" && this.processing){
                console.log("Speech reco timed out")
                this.triggerEvents("timeout", {"error": "I'm not able to get an answer for that currently. Can you try with something simpler?"});
                
                this.processing = false;
                // setTimeout(function(){
                //     that.startListenForWakeup();
                // }, 1500);
            }
        }else if(status=="wakeup-hotword"){
            console.debug("hotword detected", message);
            this.triggerEvents("hotword", message);
            if(this.settings["auto_wakeup_listern_stop"] != false){
                this.streamer.forceStopWakeup();
            }
        }
        else if(status=="asr-processing"){
            // that.socketio.emit('stt', payload);
            this.triggerEvents("speech_processing");
            this.processing = true;
            this.state = "processing";
        }
        else if(status == "asr-disconnect"){
            if(this.state == "processing"){
                this.triggerEvents("socket_disconnected", {"error": "Your Internet connection was disrupted during the query. Please try saying this again."});
                this.processing = false;
            }
            this.state = "idel";
        } else if(status == "asr-recording"){
            this.triggerEvents("recording");
            this.state = "recording";

            this.abortReq();
        }else if (status == "asr-idel"){
            this.triggerEvents("idel", message);
            this.state = "idel";
            this.processing = false;
            this.abortReq();
        } else if(status == "asr-processing"){
            this.triggerEvents("processing", message);
            this.state = "processing";
            this.processing = true;
            this.abortReq();
        } else{
            console.debug(status, message);
            this.triggerEvents(status, message)
        }
        
        
        if(["error", "mic_rejected", "socket_error"].indexOf(status) > -1){
            // if(that.record_button){
            //     that.record_button.attr("disabled", true);
            // }
        }else{
            // if(that.record_button){
            //     that.record_button.attr("disabled", false);
            // }
        }
    }
    
    abortReq(){
        //class Conversation
        if(this._req){
            this._req.abort();
        }
        
    }
    abortAsrReq(){
        this.streamer.forceStopRecording();
    }
    checkSetupDone(){
        //class Conversation
        if(!this._loading_conversation && !this._loading_conversation_keywords){
            window.__dv_cbs_get("ConversationLoaded").fire(this.conversation_id);
        }
    }
    
    loadConversation(){
        //class Conversation
        var ref = this;
        this.get("conversation", this.conversation_id, function(data){
            ref.conversation_data = data["data"];
            ref.conversation = data;
            ref._loading_conversation = false;
            ref.checkSetupDone();
        }, function(error){
            console.log(error);
            ref._loading_conversation = false;
            ref.checkSetupDone();
            throw error;
        })
    }
    
    loadKeywords(){
        //class Conversation
        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }
        
        var ref = this;
        this.getRaw("/conversation-keywords/"+ref.conversation_id, function(da){
            ref.keywords = da["keywords"] || [];
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        }, function(res){
            var msg;
            if(res.responseJSON){
                msg = "Looks like the system is under maintenance, I've been asked to say '"+res.responseJSON["error"]+"'";
            }else{
                msg = "Sorry, it seems like my team is tinkering around with me!! |I should be back online and ready to talk to you once this update is done..."
            }
            console.log(msg);
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        });
    }
    filterStates(kwd){
        //class Conversation
        if(!kwd) return this.keywords;
        function match_score(l, k){
            var x= l.filter(function(i){
                return i.toLowerCase().indexOf(k.toLowerCase()) != -1 ? true : false;
            });
            return x.length;
        }
        var keys = kwd.split(" ");
        keys.push(kwd);
        for(var i in this.keywords){
            var c = 0;
            for(var j in keys){
                c += match_score([this.keywords[i][0], this.keywords[i][1], this.keywords[i][2]], keys[j]);
                c += match_score(this.keywords[i][4], keys[j]);
            }
            this.keywords[i].score = c;
        }
        var x = this.keywords.filter(function(a){ return a.score; });
        return x.sort(function(a,b){ b.length - a.length });
    }
    
    getStateKeywords(state){
        //class Conversation
        for(var i of this.keywords){
            if(i[1] == state)
            return i
        }
        return [];
    }

    postConversationFeedback(rating, feedback, cb) {
        cb = cb || function () { };
        if(rating <= 5 && rating >=1){
            this.postRaw(`/conversation-feedback/dave/${this.engagement_id}`, {
                "usefulness_rating": rating,
                "feedback": feedback
            }, cb, cb)
        }
    }
    
    start(successCallback, errorCallback){
        var that = this;
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.user_id;
        extendData({"query_type": "auto"}, this._additional_conversation_info).then(function(data){
            that.getNext(data, null, function(data){
                successCallback(data)
                that.init_response = data;
            }, errorCallback);
        }).catch((err) => console.log("Failed to extend request object", err))

        
        this.streamer.set("customer_id", this.user_id);
        this.streamer.set("api_key", this.api_key);
    }

    getNext(data, title, successCallback, errorCallback){
        var ref = this;
        data = {
            ...data, ...{
                system_response: this.current_response ? this.current_response["name"] : null,
                voice_id: this.voice_id,
                engagement_id: this.engagement_id,
                synthesize_now: true,
                csv_response: true
            }
        }
        this.abortReq();
        console.debug("data: ", data);
        
        if(data["customer_response"]){
            var rx = title|| data["customer_response"];
            this.eventCallback("processing",  rx.indexOf("{") == -1 ? rx : (data.title || "Form response"));
        }
        this._req = this.postRaw(this.conversation_url, data, function(data){
            ref.engagement_id = data["engagement_id"]
            ref.current_response = data;
            data["data"] = data["data"] || {};
            ref.voice_id = data["data"]["voice_id"] || ref.voice_id;
            ref.streamer.set("engagement_id", data["engagement_id"]);
            successCallback(data);
            ref.eventCallback("idel");
            //ref.precessResponse(da, successCallback)
        }, function(error){
            errorCallback(error);
            ref.eventCallback("idel");
            //ref.processError(res, errorCallback);
        })
    }
    getNudge(state, successCallback, errorCallback){
        var ref = this;
        
        this.abortReq();
        var data = {
            customer_state: state,
            system_response: this.current_response ? this.current_response["name"] : null,
            engagement_id: this.engagement_id,
            synthesize_now: true,
            csv_response: true,
            query_type: "auto",
            voice_id: this.voice_id
        }
        extendData(data, this._additional_conversation_info).then(function(data){
            console.debug(data)
            ref.eventCallback("processing", "nudge");
            ref._req = ref.postRaw(ref.conversation_url, data, function(data){
                data["data"] = data["data"] || {};
                successCallback(data);
                ref.eventCallback("idel");
                //ref.precessResponse(da, successCallback)
            }, function(error){
                errorCallback(error);
                ref.eventCallback("idel");
                //ref.processError(res, errorCallback);
            })   
        }).catch((err) => console.log("Failed to extend request object", err))   
    }
    
    next(response, state=null, query_type=null, title=null, successCallback=null, errorCallback=null){
        //class Conversation
        var that = this;
        var data = {
            customer_response: response,
            system_response: this.current_response? this.current_response["name"]: null,
            customer_state: state,
            query_type: query_type
        };
        extendData(data, this._additional_conversation_info).then(function(data){
            that.getNext(data, title, successCallback, errorCallback);    
        }).catch((err) => console.log("Failed to extend request object", err))
    }
    getKeywords(c=4, res=null){
        var kws = []
        res = res || this.current_response
        if(res){
            for(var i in res["options"]){
                kws.push([res["options"][i], undefined]);
                if(kws.length == c) break
            }
            var l = [];
            for(var i in res["state_options"]){
                var kys = this.getStateKeywords(i);
                if(kys.length == 6){
                    if(kys[5].length > 0){
                        l.push([kys[2], i]);
                    }else{
                        l.push([res["state_options"][i], i]);
                    }
                    /*
                            for(var z of kys[5]){
                                if(z.indexOf("**") >= 0) continue;
                                while(z.indexOf("__") > -1)
                                z = z.replace("__", "\"")
                                x.push(z);
                            }
                            l.push(x);*/
                }
            }
            for(var j of l){
                kws.push(j);
                if(kws.length >= c){
                    break;
                }
            }
        }
        return kws;
    }


    startListenForWakeup(){
        this.streamer.startWakeupRecording({});
    }
    stopListenForWakeup(force){
        if(force){
            this.streamer.forceStopWakeup()
        }else{
            this.streamer.stopWakeupRecording();
        }
    }
    startImageStream(){
        this.streamer.startVideoRecording();
    }
    stopImageStream(){
        this.streamer.stopVideoRecording();
    }
    startRecordResponse(){
        // this.eventCallback("recording");
        // this.streamer.startRecordResponse(null, false, true, this.current_response["name"]);
        this.streamer.startVoiceRecording({
            voice_id: this.voice_id || 'english-male',
            query_type: "speech",
            language: this.language || 'english',
            system_response: this.current_response["name"]
        })
    }
    stopRecordResponse(){
        // this.eventCallback("speech_processing");
        // this.streamer.stopRecordingResponse();
        this.streamer.stopVoiceRecording();
    }
}