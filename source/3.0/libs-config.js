var _davelibs = {
    "__default__": [
        {"__default__": asset_path(`lib/jquery-3.6.0.min.js`), "async": false }
    ],
    "bootstrap": [{
        "__default__": "https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
    },{
        "__default__": "https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
    },{
        "__default__": "//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    }],
    "speech": [{
        "__default__": asset_path("dist/js/socket.io-mini.js")
    },{
        "__default__": asset_path("dist/js/socket.stream.io-mini.js")
    },{
        "__default__": asset_path("dist/js/RecordRTC-mini.js")
    },{
        "__default__": asset_path("lib/flac-dep.js")
    },{
        "__default__": asset_path("dist/js/vad-mini.js")
    }],
    "camara":[
        {
            "__default__": asset_path("lib/cameraControls.js")
        }
    ],
    "avatar-conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'avatar-conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'avatar-conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'avatar-conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/style.css'),
        "production": asset_path('dist', VERSION, 'style.min.css'),
        "staging": asset_path('dist', VERSION, 'style.min.css')
    }],
    "metaverse-conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'metaverse-conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'metaverse-conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'metaverse-conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/metaverse-style.css'),
        "production": asset_path('dist', VERSION, 'metaverse-style.min.css')
    }],
    "babylon-conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'babylon-conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'babylon-conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'babylon-conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/babylon-style.css'),
        "production": asset_path('dist', VERSION, 'babylon-style.min.css')
    }],
    "unity": [
        {
            "__default__": "//unity-plugin.iamdave.ai/unity/"+UNITY_FOLDER+"/ProjectBuild.loader.js"
        }
    ],
    "babylon": [{
        "__default__": asset_path('source', VERSION, 'css/babylon-style.css')
    }],
    "avatar-conversation":["bootstrap", "avatar-conversation-libs", "speech", "camara", "unity"],
    "chatbot":[],
    "kiosk": ["bootstrap", "speech", "unity", "avatar-conversation-libs"],
    "babylon-avatar-conversation":["bootstrap",  "speech", "babylon","babylon-conversation-libs"],
    "metaverse-conversation": ["bootstrap", "metaverse-conversation-libs", "speech", "camara", "unity"]
}