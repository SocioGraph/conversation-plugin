var me = document.querySelector('script[id="dave-settings"]');
var data = me.dataset;
let DAVE_ASSET_PATH = (data.asserts || (document.currentScript && (new URL(document.currentScript.src)).origin))+"/" || "https://unity-plugin.iamdave.ai/conversation-plugin/";
let VERSION = data.version || '2.0';
let LOAD_TYPE = data.loadfunction || 'window-load';
let UNITY_FOLDER = data.unity_folder || 'brands_arena';
let CUSTOM_BUNDLE = data.custom_bundles || '';
CUSTOM_BUNDLE = CUSTOM_BUNDLE.split(",")
CUSTOM_BUNDLE = CUSTOM_BUNDLE.filter((i) => { if (i) return i })
console.log(CUSTOM_BUNDLE);

function asset_path(){
    var s = DAVE_ASSET_PATH;
    if(s == "/"){
        s = "";
    }
    for(var i of arguments){
        if(!s){
            s = i;
            continue;
        }
        if(i.startsWith("/")){
            i = i.substring(1);
        }
        if(s.endsWith("/")){
            s+=i;
        }else{
            s+="/"+i;
        }
    }
    return s;
}
//Dave events part
var callbacks = function(){
    return {
        list: [],
        add: function(){
            function isFunction( obj ) {
                return typeof obj === "function" && typeof obj.nodeType !== "number" &&
                typeof obj.item !== "function";
            }
            if(isFunction(arguments[0])){
                this.list.push(arguments[0]);
            }
        },
        fire: function(){
            for(var fun of this.list){
                fun.apply({},arguments);
            }
        },
        clear: function(){this.list=[]}
    }
}

window.daveRegisterCallback = function(key, func){
    if(!window.__dv_cbs[key]){
        window.__dv_cbs[key] = callbacks();
    }
    window.__dv_cbs[key].add(func);
}
window.__dv_cbs = window.__dv_cbs || {
    "on_dave_libs_loaded": callbacks(),
    "on_dave_init": callbacks(),
    "on_action_callback": callbacks()
};
window.__dv_cbs_get = function(key){
    function __message(){
        console.warn(`no callback named ${key}`);
    }
    if(!window.__dv_cbs[key]){__message();window.__dv_cbs[key] = callbacks(); }
    return window.__dv_cbs[key];
}

function __load_js(src, onload, trys=3){
    // Load the script
    console.log("loading ::", src)
    const script = document.createElement("script");
    script.src = src;
    script.type = 'text/javascript';
    script.addEventListener('load', () => {
        console.log(src+" has been loaded successfully");
        if(onload) onload();
        // use jQuery below
    });
    script.addEventListener("error", (ev) => {
        console.log("Error on loading file", ev);
        if(trys){
            __load_js(src, onload, trys-1);
        }
    });
    document.head.appendChild(script);
}
function  __load_css(src, onload, trys=3){
    // Load the script
    const script = document.createElement("link");
    script.href = src;
    script.rel = 'stylesheet';
    script.addEventListener('load', () => {
        console.log(src+" has been loaded successfully");
        if(onload) onload();
        // use jQuery below
    });
    script.addEventListener("error", (ev) => {
        console.log("Error on loading file", ev);
        if(trys){
            __load_css(src,  onload, trys-1);
        }
    });
    document.head.appendChild(script);
}

const _davelibs = {
    "__default__": [
        // {"__default__": asset_path(`lib/jquery-3.6.0.min.js`), "async": false }
    ],
    "bootstrap": [],
    "speech": [{
        "__default__": asset_path("dist/js/socket.io-mini.js")
    },{
        "__default__": asset_path("dist/js/socket.stream.io-mini.js")
    },{
        "__default__": asset_path("dist/js/RecordRTC-mini.js")
    },{
        "__default__": asset_path("lib/flac-dep.js")
    },{
        "__default__": asset_path("dist/js/vad-mini.js")
    }],
    "camara":[{
        "__default__": asset_path("lib/cameraControls.js")
    }],
    "unity": [{
        "__default__": DAVE_ASSET_PATH.replace("conversation-plugin", "unity")+UNITY_FOLDER+"/ProjectBuild.loader.js"
    }],
    "babylon": [{
        "__default__": asset_path('source', VERSION, 'css/babylon-style.css')
    }],
    "conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/style.css'),
        "production": asset_path('dist', VERSION, 'style.min.css'),
        "staging": asset_path('dist', VERSION, 'style.min.css')
    }],
    "conversation":["bootstrap", "conversation-libs", "speech-cm", "camara-cm"]
}
class LibFunction{
    
    constructor(environment = "staging") {
        this._dave_libs = _davelibs;
        this._scrpts =[];
        this.environment = environment;
        var libs = this.__fetch_component("__default__", environment);
        this.__load_file(libs, "on_dave_init");
    }
    __fetch_component(key) {
        var cm = false;
        if (key.endsWith("-cm")) { 
            cm = true;
            key = key.replace("-cm", "");
        }
        if(key.endsWith("js") || key.endsWith("css")){
            return [[key, 1]]
        }
        if (cm && CUSTOM_BUNDLE.length > 0 && CUSTOM_BUNDLE.indexOf(key) == -1) { 
            return []
        }
        console.log(key, this._dave_libs, this._dave_libs[key]);
        console.log("loaded")
        var _scrpts = [];
        
        for(var z of this._dave_libs[key]){
            if(typeof z != 'object' && (z.indexOf("js") != -1 || z.indexOf("css") != -1)){
                _scrpts.push([z, 1]);
            }else if(typeof z != 'object'){
                _scrpts = _scrpts.concat(this.__fetch_component(z, this.environment))
            }else{
                _scrpts.push([(z[this.environment] || z["__default__"]), z["priority"] || 1]);
            }
        }
        return _scrpts;
    }
    __load_file(libs, callback_key="on_dave_libs_loaded"){
        if(libs.length == 0) return
        var fil = libs.shift()
        var that =this;
        var fn = __load_css;
        if(fil[0].endsWith("js")){
            fn = __load_js;
        }
        fn(fil[0], function(){
            if(fil[1] == 1){
                that.__load_file(libs, callback_key);
            }
            if(fil[1] != 3){
                that.total_libs--;
                if(that.total_libs == 0){
                    // window.__dv_cbs["on_dave_libs_loaded"].fire("Test arg")
                    window.__dv_cbs_get(callback_key).fire()
                }
            }
        });
        if(fil[1] == 3){
            that.total_libs--;
            if(that.total_libs == 0){
                // window.__dv_cbs["on_dave_libs_loaded"].fire("Test arg")
                window.__dv_cbs_get(callback_key).fire()
            }
        }
        if(fil[1] != 1){
            that.__load_file(libs, callback_key);
        }
        // console.log(fil);
    }
    __load_component(src){
        var that = this;
        var libs = [];
        var cslib = []
        var ll = src.split(",");
        for(var i of ll){
            if(i.indexOf(".") > -1){
                cslib = cslib.concat(this.__fetch_component(i, this.environment));
            }else{
                libs = libs.concat(this.__fetch_component(i, this.environment));
            }
        }
        console.log(libs);
        this.total_libs = libs.length;
        this.__load_file(libs);
        window.daveRegisterCallback("on_dave_libs_loaded", function(ref){
            that.__load_file(cslib, "on_custom_libs_loaded");
        });
    }
}

function init_dave_plugin(tim=1){
    var after_loaded = function() {
        var ll = new LibFunction(data.environment);
        ll.__load_component(data.component);
        // __load_libs(data.component, data.environment);
    };
    setTimeout(after_loaded, tim);
}


__load_js(asset_path(`lib/jquery-3.6.0.min.js`), () => {
    console.debug("Loaded dave jquery lib");
    if(LOAD_TYPE == "oninteraction"){
        var fu = function(){
            init_dave_plugin();
            // window.removeEventListener('click', fu);
        }
        djq(window).one('click', fu);
    }else if(LOAD_TYPE == "onload"){
        init_dave_plugin();
    }else if(LOAD_TYPE.startsWith("after-")){
        init_dave_plugin(parseInt(LOAD_TYPE.split("-")[1] || "1")*1000);
    }else{
        window.addEventListener('load', init_dave_plugin);
    }
}, 3);