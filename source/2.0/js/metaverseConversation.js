

build_type = 'metaverse';
class MetaverseConversation extends ConversationHandler{
    constructor(settings){
        settings["head_name"] =  settings["head_name"] || "SciFi_Head";
        super(settings);
        this.avatar_loaded = false;
        this.audios_list = [];
        this.pre_resp="";
        this.head_name = settings["head_name"];
        this.showenkeya = false;
        this.assistant = settings["assistant"] || "Keya";
    }
    setUpConversationUI(){
        super.setUpConversationUI();
        super.set("message_id", "message");
        super.set("shortcuts_id", undefined);
        super.set("whiteboard_id", undefined);
        super.set("quick_access_id", "quick_access");
        super.set("audio_id", "audio");
        super.set("textbox_id", "input-text");
        super.set("mic_id", "record")
        super.set("sendbutton_id", "send");
        // super.set("sendbutton_id", "input-send");
        super.set("mute_button", "mute");
        
        // djq("body").append("<div id='dave_conversation__' class='dave_conversation full' style='display:none'></div>");
        // djq("#dave_conversation__").setupMetaverseConversationUi({placeholder:this.default_placeholder || "Type here..."});
        // djq("body").append(`<audio id="audio" type="audio/wav" src="" style="display: none" controls playsinline='true'>
        // Your browser does not support the HTML5 Audio element.
        // </audio>`);
        var that = this;
        this.audio = document.getElementById((this.settings["audio_id"] || "audio"));
        for(var z of [this.textbox_id, this.mic_id, this.sendbutton_id]){
            if(z){
                djq("#"+z).focus(function(){
                    that.unityInstance.keyboardControlDisable()
                })
                djq("#"+z).blur(function(){
                    that.unityInstance.keyboardControlEnable()
                })
            }
        }
        this.loadAvatar();
        this.setUpEvents();
    }
    mute(){
        super.mute();
        if(this.audio){
            this.audio.muted = true;
            this.audio.setAttribute("force-mute", true);
            djq("#"+this.mute_button).find("i").first().attr("class", "fa fa-volume-off");
        }
    }
    unmute(){
        super.mute();
        if(this.audio){
            this.audio.muted = false;
            this.audio.setAttribute("force-mute", false);
            djq("#"+this.mute_button).find("i").first().attr("class", "fa fa-volume-up");
        }
    }
    stopTalking(){
        this.audio.pause();
        this.audio_playing = false;
        this.unityInstance.stopTalking();        
    }
    pause(){
        super.pause();
        this.audio.src = undefined;
        this.unityInstance.stopTalking()
        this.audios_list = [];
        this.unityInstance.keyboardControlDisable();
        this.play_followups = [];
    }
    resume(){
        var that = this;
        // super.resume();
        // if(that.audio){
        //     that.audio.play();
        // }
        if(that.conversation.session_id){
            that.triggerEvents("resume", "resume");
            that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.RESUMED});
        }else{
            that.triggerEvents("opened", "opened");
            that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.OPENED});
        }
        that.createSession();
        that.audios_list = [];
        
        
        setTimeout(function(){
            // set_cc();
            that.onResponse(that.conversation.current_response);
            that.unityInstance.keyboardControlEnable();
        }, 1000); 
        this.unmute();
        
        // this.unityInstance.showPersona('kotak_player');
    }
    
    updateQuickAccessButtons(res){
        if(this.quick_access_id){
            djq("#"+this.quick_access_id).empty();
            var that =this;
            var kws = this.conversation.getKeywords(6, res);
            for(var i of kws){
                var butn = djq("<li  style='cursor:pointer'/>");
                // butn.addClass("butn butn-dark butn-sm");
                butn.html(i[0]);
                butn.attr("value", i[0]);
                // butn.attr("value", i);
                butn.attr("key", i[1]);
                // butn.attr("style", "margin-right: 5px");
                djq("#"+this.quick_access_id).append(butn);
                butn.click(function(){
                    console.debug(djq(this).attr('value') || djq(this).attr('key'), djq(this).attr('key'), djq(this).attr("title"));
                    that.postConversation(djq(this).attr('value'), djq(this).attr('key'), djq(this).attr('title'), "click");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK});
                    that.triggerEvents("interaction", "click");
                });
            }
        }
    }
    audioRecord(stop){
        var that = this;
        if(that.intervel){
            clearInterval(that.intervel);
        }
        that.intervel = undefined;


        if(stop){
            djq("#"+that.mic_id).html('<img class="recordbtn" src="/img/record.png" alt="">');
            djq(".text-wraper").first().show();
            djq("#"+this.quick_access_id).show();
            djq("#"+this.shortcuts_id).show();
            djq("#try-saying").show();
            return
        }else{
            that.state = "recording";
            djq("#"+that.mic_id).html('<img class="recordbtn" src="/img/recording.png" alt="">');
            djq(".text-wraper").first().hide();
            djq("#quick_access").hide();
            djq("#shortcut").hide();
            djq("#try-saying").hide();
            window.__dv_cbs_get("dave_state").fire("recording");
        }

        // if(stop){
        //     djq("#"+that.mic_id).removeClass("active");
        //     djq("#"+that.mic_id).html('<i id="microphone" class="fa fa-microphone fa-2x"></i>');
        //     djq(".text-wraper").first().show();
        //     djq("#"+this.quick_access_id).show();
        //     djq("#"+this.shortcuts_id).show();
        //     djq("#try-saying").show();
        //     return;
        // }else{
        //     that.state = "recording";
        //     window.__dv_cbs_get("recording").fire();
        //     var sec=0;
        //     djq("#"+that.mic_id).addClass("active");
        //     var intv = djq("<span>00:00</span>");
        //     djq("#"+that.mic_id).find(".fa-microphone").hide();
        //     djq("#"+that.mic_id).append(intv);
        //     djq("#"+that.mic_id).append("<br />");
        //     djq("#"+that.mic_id).append('<span><i class="fa fa-stop fa-2x"></i></span>');
            
        //     that.intervel = setInterval(function(){
        //         sec++;
        //         var min = parseInt(sec/60);
        //         var sc = sec%60;
        //         intv.html( (min < 10 ? "0"+min : min) +":"+ ( sec < 10 ? "0"+sc : sc ));
        //     },1000);
        //     djq(".text-wraper").first().hide();
        //     djq("#quick_access").hide();
        //     djq("#shortcut").hide();
        //     djq("#try-saying").hide();
        // }
        djq("#"+that.mic_id).show();
    }
    setUpEvents(){
        var that = this;
        if(this.textbox_id){
            djq("#"+this.textbox_id).enter(function(val){
                if(val.trim()){
                    that.postConversation(val, null, val, "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.TEXT_INPUT});
                    that.triggerEvents("interaction", "text-input");
                }
            });
        }
        if(this.sendbutton_id){
            djq("#"+this.sendbutton_id).click(function(){
                if(djq("#"+that.textbox_id).val().trim()){
                    that.postConversation(djq("#"+that.textbox_id).val(), null, djq("#"+that.textbox_id).val(), "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: "text-input"});
                    that.triggerEvents("interaction", "text-input");
                }
            });
        }
        if(this.mic_id){
            djq("#"+this.mic_id).click(function(){
                if(that.state == "idel"){
                    // djq(this).hide();
                    that.conversation.startRecordResponse();
                    // that.audioRecord();
                }else if(that.state == "recording"){
                    that.conversation.stopRecordResponse();
                }
                console.log("Current state is", that.state)
            })
        }
        if(this.mute_button){
            djq("#"+this.mute_button).click(function(){
                var cc_class = djq(this).find("i").first().attr("class");
                if(cc_class.indexOf("fa-volume-up") == -1){
                    that.unmute();
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.UNMUTED});
                    that.triggerEvents("interaction", "unmuted");
                }else{
                    that.mute();
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MUTED});
                    that.triggerEvents("interaction", "muted");
                }
            });
        }
        djq("#close_message").click(function(){
            // djq("#dave_conversation__").hide(200);
            that.pause();
            that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MINIMIZED});
            that.triggerEvents("interaction", "minimized");
        })
        
        // window.daveRegisterCallback("before_nudge_play", function(s){
        //     that.showAssistant();
        // })
        // window.daveRegisterCallback("after_nudge_play", function(s){
        //     that.hideAssistant();
        // })
    }
    showAssistant(){
        if ( this.showenkeya ) { return;}
        this.showenkeya=true;
        if (this.unityInstance.dynamic_unity) {
            this.unityInstance.callGameManagerFunction(
                "UnityFunction",
                JSON.stringify({
                  action: "Show"+this.assistant
                })
            );
        } else { 
            this.unityInstance.callAssistantFunction('Show'+this.assistant, JSON.stringify({}));
        }
        window.__dv_cbs_get("avatar_action").fire("Show"+this.assistant);
    }
    hideAssistant(){
     	if ( !this.showenkeya ) { return;}
        this.showenkeya = false;
        if (this.unityInstance.dynamic_unity) {
            this.unityInstance.callGameManagerFunction(
                "UnityFunction",
                JSON.stringify({
                action: "Hide"+this.assistant
                })
            );
        } else { 
            this.unityInstance.callAssistantFunction('Hide'+this.assistant, JSON.stringify({}));
        }
        
        window.__dv_cbs_get("avatar_action").fire('Hide'+this.assistant);
    }
    
    onLoadedCallback(){
        if(this.conversation_loaded && this.avatar_loaded){
            this.addShortcutsButtons();
            this.conversationLoadedCallback(this.conversation.conversation_data);
            this.unityInstance.keyboardControlDisable();
            this.loaded = true;
            var that = this;
            window.daveRegisterCallback("enteredArea", function(cs){
                that.enteredAreaCallback(cs);
            })
            window.daveRegisterCallback("pointReached", function(cs){
                that.pointReachedCallback(cs);
            })
            setTimeout(function(){
                if(!that.minimize){
                    that.createSession();
                    that.unityInstance.keyboardControlEnable();
                    // that.unityInstance.showPersona('kotak_player');
                    djq("#dave_conversation__").show(200, function(){
                        that.onResponse(that.conversation.init_response);
                        // set_cc();
                        that.triggerEvents("chat_started", "chat_started");
                        
                        set_cc();
                        that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.AUTO_OPENED});
                    });
                }
                
                djq(window).on("beforeunload", function(){
                    // that.clearSession(false);
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.LEAVE_PAGE}, true);
                    that.clearSession(false);
                })
                
            }, 2000)
        }
    }
    loadAvatar(){
        var that = this;
        this.settings["head_name"] = this.head_name;
        var t = SetupEnviron.load(this.settings["unity_url"], this.settings);
        t.then(function(ut){
            that.avatar_loaded = true;
            that.unityInstance = ut;
            that.onLoadedCallback();
            // set_cc();
        }, function(err){
            console.error(err);
        });
        t.progress(function(pro){
            console.debug(pro);
        })
    }
    
    
    playAnimations(audo, force) {
        if (this.paused) {
            setTimeout(() => {
                this.playAnimations(audo, force);
            }, 1000)
            return
        }
        var that = this;
        if (!this.audios_list.length && !audo) return;
        if (!audo) {
            audo = this.audios_list.splice(0, 1);
            audo = audo[0];
        }
        this.unityInstance.stopTalking();
        this.prev_audo = audo;
        this.temp_id = audo["id"];
        // var tt = this.audio.cloneNode(true);
        var muted = this.audio.getAttribute("force-mute");
        // this.audio.remove();
        // this.audio = tt;
        this.audio.currentTime=0;
        this.audio.setAttribute("data-audio-id", that.temp_id);
        this.audio.setAttribute("force-mute", muted || false);
        this.audio.muted = muted || false;
        this.audio.src = audo["audio"];
        document.body.appendChild(this.audio);
        console.debug("play init id :: ", audo["id"]);
        this.audio.load();
        // this.audio.play();
        var played_anims = false;
        function _lis() {
            if(played_anims){
                return;
            }
            played_anims = true;
            console.log("Clearing the callbacks list ::");
            window.daveRegisterCallback('KeyaLoadedCallback', function(){
                console.log("Keya showen callback and playing animations")
                that.initiateAudioPlay(audo, force);
                window.__dv_cbs_get("KeyaLoadedCallback").clear();
            });
            window.daveRegisterCallback('AssistantLoadedCallback', function(){
                console.log("Assistant showen callback and playing animations")
                that.initiateAudioPlay(audo, force);
                window.__dv_cbs_get("AssistantLoadedCallback").clear();
            });
            that.showAssistant();
        }
        this.audio.removeEventListener("canplaythrough", _lis, false);
        this.audio.addEventListener("canplaythrough", _lis, true);
        this.audio.onended= function() {
            console.log("Audio ended callback");
            that.audio_playing = false;
            if(that.playing_id != that.audio.getAttribute("data-audio-id")) return;
            console.log("Clearing audio ");
            if(that.playing_id.startsWith("nudge")){
                window.__dv_cbs_get("after_nudge_play").fire("hide"+this.assistant);
            }
            that.unityInstance.stopTalking();
            if(!that.conversation.processing){
                console.log("Conversation processing");
                that.onIdelState();
            }
            if(that.state == "idel" && !that.load__){
                that.pre_resp = "";
            }
            if(audo["onEnds"]){
                audo["onEnds"](that, that.playing_id);
            }else{
                that.playFromTheQueue(that, that.playing_id);
            }
            that.hideAssistant();
            window.__dv_cbs_get("audioPlay").fire("ended");
        }
    }
    
    // For Konaverse( Don't Remove this)
    // playAnimations(audo, force) {
    //     var ios = djq.browser.is_iphone;
    //     if (this.paused || this.audio_playing) {
    //     if (this.audio_playing && this.audios_list[0]["id"] == this.latest_audio_id) {
    //         this.audios_list.slice(0, 1)
    //         return;
    //     }
    //     setTimeout(() => {
    //         this.playAnimations(audo, force);
    //     }, 1000)
    //     return
    //     }
    //     console.log("PLAY ANIMATION", audo);
    //     var that = this;
    //     if (!this.audios_list.length && !audo) return;
    //     if (!audo) {
    //     audo = this.audios_list.splice(0, 1);
    //     audo = audo[0];
    //     }
    //     this.unityInstance.stopTalking();
    //     this.prev_audo = audo;
    //     this.temp_id = audo["id"];
    //     if (!ios) {

    //     var tt = this.audio.cloneNode(true);
    //     this.audio.remove();
    //     var muted = this.audio.getAttribute("force-mute");
    //     this.audio = tt;
    //     document.body.appendChild(this.audio);
    //     }
    //     this.audio.setAttribute("data-audio-id", that.temp_id);
    //     this.audio.setAttribute("force-mute", muted || false);
    //     // this.audio.muted = muted || false;
    //     this.audio.setAttribute("src", audo["audio"])
    //     this.audio.setAttribute("main-response", audo["main_response"])

    //     console.debug("play init id :: ", audo["id"]);

    //     this.audio_playing = true;
    //     var played_anims = false;
    //     function _lis() {

    //         if(played_anims){
    //             return;
    //         }
    //         played_anims = true;
    //         console.log("Clearing the callbacks list ::");
    //         window.daveRegisterCallback('KeyaLoadedCallback', function(){
    //             console.log("Keya showen callback and playing animations")
    //             that.initiateAudioPlay(audo, force);
    //             window.__dv_cbs_get("KeyaLoadedCallback").clear();
    //         });
    //         window.daveRegisterCallback('AssistantLoadedCallback', function(){
    //             console.log("Assistant showen callback and playing animations")
    //             that.initiateAudioPlay(audo, force);
    //             window.__dv_cbs_get("AssistantLoadedCallback").clear();
    //         });

    //         that.showAssistant();
    //         // if (audo["id"] != that.temp_id && that.temp_id != that.audio.getAttribute("data-audio-id")) {
    //         //     return;
    //         // }

    //         // function playShapes_frames(id, frames, shapes) {
    //         //     if (
    //         //     that.forced_stop &&
    //         //     that.forced_stop != that.audio.getAttribute("data-audio-id")
    //         //     ) {
    //         //     console.log("We have forced stopped, so don't play:" + id);
    //         //     return;
    //         //     }
    //         //     if (
    //         //     ["speech-detected", "processing", "idel"].indexOf(that.state) < 0 &&
    //         //     that.load__ &&
    //         //     !force
    //         //     ) {
    //         //     console.log(
    //         //         "State is not idle: " +
    //         //         that.state +
    //         //         " or we have loaded something else " +
    //         //         that.load__ +
    //         //         " already:" +
    //         //         id
    //         //     );
    //         //     return;
    //         //     }
    //         //     if (!audo["defaults"]) {
    //         //     that.onTalkingState();
    //         //     }
    //         //     if (!that.paused) {
    //         //     // if (that.unityInstance.setAvatar) {
    //         //         setTimeout(function () {
    //         //         that.unityInstance.startTalking(id, frames, shapes);
    //         //         }, 1);
    //         //     // }
    //         //     // that.conversation.hold(true);
    //         //     that.playing_id = id;
    //         //     console.debug("++++++Playing ID:" + id);
    //         //     } else {
    //         //     console.debug("++++++Paused ID:" + id);
    //         //     }
    //         //     //djq("#message").html(list[i][1]);
    //         // }
    //         // that.audio.removeEventListener("canplaythrough", _lis)
    //         // playShapes_frames(audo["id"], audo["frames_csv"], audo["shapes_csv"]);
    //     }
    //     this.audio.removeEventListener("canplaythrough", _lis);
    //     this.audio.addEventListener("canplaythrough", _lis);
    //     // this.audio.addEventListener("seeked", () => {
    //     //   if (ios) {
    //     //     that.audio.setAttribute("src", "data:audio/wav;base64,UklGRjIAAABXQVZFZm10IBIAAAABAAEAQB8AAEAfAAABAAgAAABmYWN0BAAAAAAAAABkYXRhAAAAAA==")
    //     //   }
    //     // })
    //     function ended() {
    //         that.audio_playing = false;
    //         if (that.playing_id != that.audio.getAttribute("data-audio-id")) return;
    //         that.audio.removeEventListener('ended', ended);
    //         if (ios) {
    //             that.audio.removeEventListener('seeked', ended);
    //             that.audio.removeEventListener('seeking', ended);
    //             that.audio.setAttribute("src", getEmptyAudio());
    //             // that.audio.setAttribute("src", getNoiseBase64());
    //         }
    //         that.unityInstance.stopTalking();
    //         if (!that.conversation.processing) {
    //             that.onIdelState();
    //         }
    //         if (that.state == "idel" && !that.load__) {
    //             that.pre_resp = "";
    //         }
    //         if (audo["onEnds"]) {
    //             audo["onEnds"](that, that.playing_id);
    //         } else {
    //             that.playFromTheQueue(that, that.playing_id);
    //         }
    //         that.hideAssistant();
    //         if (that.audio.getAttribute("main-response")) {
    //             window.__dv_cbs_get("ResponsePlayEnded").fire(audo)
    //         }
    //     }

    //     if (ios) {
    //     this.audio.addEventListener("seeked", ended);
    //     this.audio.addEventListener("seeking", ended);
    //     } else {
    //     this.audio.addEventListener("ended", ended);
    //     }
    //     this.audio.load();
    // }
    
    initiateAudioPlay(audo, force){
        var that = this;
        if (audo["id"] != that.temp_id && that.temp_id != that.audio.getAttribute("data-audio-id")) return;
        
        function playShapes_frames(id, frames, shapes) {
            if (that.forced_stop && that.forced_stop != that.audio.getAttribute("data-audio-id")) {
                console.log("We have forced stopped, so don't play:" + id)
                return;
            }
            if ((["speech-detected", "processing", "idel"].indexOf(that.state) < 0 && that.load__) && !force) {
                console.log("State is not idle: " + that.state + " or we have loaded something else " + that.load__ + " already:" + id)
                return;
            }
            if(!audo["defaults"]){
                that.onTalkingState();
            }
            if (!that.paused) {
                that.unityInstance.startTalking(id, frames, shapes);
                that.audio_playing = true;
                // that.conversation.hold(true);
                that.playing_id = id;
                console.debug("++++++Playing ID:" + id);
            } else {
                console.debug("++++++Paused ID:" + id);
            }
            window.__dv_cbs_get("audioPlay").fire("started");
            //djq("#message").html(list[i][1]);
        }
        playShapes_frames(audo["id"], audo["frames_csv"], audo["shapes_csv"]);
    }

    playDefaults(state, message, that) {
        super.playDefaults(state, message);
        
        // eslint-disable-next-line no-redeclare
        var that = that || this;
        
        function __load(level) {
            djq.ajax({
                "url": that.host_url + "/defaults/" + that.voice_id + "/" + level + "/" + that.enterprise_id,
                "success": function (data) {
                    if(data["audio"] && data["subfolder"]){
                        that.addAudioToQueue(data["audio"], state, "defaults", function (that, pid) {
                            that.playFromTheQueue(that, pid)
                        }, null, null, data["subfolder"], false);
                    }
                }
            });
        }
        if(state != "level_one"){
            __load(state);
        }
    }
    
    playFromTheQueue(that, playing_id){
        var that = that || this;
        if(that.audios_list.length){
            that.playAnimations(null, false);
            return;
        }
        
        if(that._timer){
            clearTimeout(that._timer);
        }
        if(playing_id != that.latest_audio_id) return;
        if(!that.play_followups.length){
            return;
        }
        (function(tm){
            var tf = function(){
                if(that.paused){
                    setTimeout(tf, tm);     
                }
                if(that.state != "idel" || that.load__) return;
                if(that.audios_list.length){
                    that.playAnimations(null, false);
                }
                else if(that.play_followups.length){
                    that.playNudge(that.play_followups[0], function(data){that.playNudge(data)});
                    that.play_followups.splice(0, 1);
                }else{}
            }
            that._timer = setTimeout(tf, tm);
        })(that.conversation.current_response["wait"] || 10000);
    }    
    addAudioToQueue(audio, id, conversation_id, onEnds, frames, shapes, subfolder, force){
        var that =this;
        this.forced_stop = false;
        var id_ = id+"___"+makeid(4);
        console.debug("audio adding id :: ", id_);
        this.latest_audio_id = id_;
        if(force){
            this.audios_list = [];
            //this.play_followups = [];
            /*if(this.audio){
                this.audio.muted = true;
                this.unityInstance.SendMessage("head", "stopTalking")
            }*/
            this.forced_stop = id_;
            if(this._timer){
                clearTimeout(this._timer);
            }
        }
        if(this._timer && onEnds){
            clearTimeout(this._timer);
        }
        
        var aurl;
        if(audio.indexOf("http")>=0){
            aurl = audio;
        }else{
            aurl = this.host_url+"/"+audio; 
        }
        var conversation_id = conversation_id || this.conversation_id;
        var ob = {
            id: id_,
            audio: aurl,
            onEnds: onEnds,
            defaults: (conversation_id=="defaults")
        }
        this.audios_list.push(ob);
        
        var fil ="?voice_id="+this.voice_id;
        if(subfolder)
        fil += "&subfolder="+subfolder;
        
        
        function addFramesShapes(frames, shapes) {
            if (frames) {
                ob["frames_csv"] = frames;
            }
            if (shapes) {
                ob["shapes_csv"] = shapes;
            }
            if (ob["frames_csv"] && ob["shapes_csv"]) {
                if (!that.audio_playing) {
                    that.playAnimations(null, force);
                }
            }
        }
        
        var furl, surl;
        if(!frames){
            furl = this.host_url+"/frames/"+conversation_id+"/"+id+"/"+this.enterprise_id+fil;
            djq.ajax({
                "url": furl,
                "success":function(data){
                    addFramesShapes(data)
                }
            })
        }else{
            addFramesShapes(frames);
        }
        if(!shapes){
            surl = this.host_url+"/shapes/"+conversation_id+"/"+id+"/"+this.enterprise_id+fil;
            djq.ajax({
                "url": surl,
                "success": function(data2){
                    addFramesShapes(null, data2);
                }
            })
        }else{
            addFramesShapes(null, shapes);
        }
    }
    
    nudgeResponse(response) {
        super.nudgeResponse(response);
        var that = this;
        if(response['data']['voice']) {
            response['response_channels']['voice'] = response['data']['voice'];
        }
        window.__dv_cbs_get("before_nudge_play").fire("show"+this.assistant);
        if (response["response_channels"]) {
            if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
                // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
                if (!response["response_channels"]["shapes"].endsWith(".csv")) {
                    this.addAudioToQueue(response["response_channels"]["voice"], "nudge_"+response["name"]+"_response", that.conversation_id, function (that, pid) {
                        that.playFromTheQueue(that, pid)
                    }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
                } else {
                    this.addAudioToQueue(response["response_channels"]["voice"], "nudge_"+response["name"]+"_response", that.conversation_id, function (that, pid) {
                        that.playFromTheQueue(that, pid)
                    }, null, null, null, true);
                }
            }
            //delete response["response_channels"];
        }
    }
    addNudges(lst){
        this.play_followups = lst || [];
    }
    processSystemResponse(response, after_played, partial) {
        super.processSystemResponse(response);
        this.state="speaking";
        if(partial){
            return;
        }
        // this.displayResponse();
        var that = this;
        if (response["data"]) {
            if (response["data"]["avatar_model_name"] || response["data"]["avatar_id"]) {
                that.unityInstance.showPersona(response["data"]["avatar_model_name"] || response["data"]["avatar_id"])
                that.voice_id = response["data"]["voice_id"] || response["data"]["avatar_id"];
                // eslint-disable-next-line
                defaul_avatar_id = response["data"]["avatar_model_name"] || response["data"]["avatar_id"]
            }
            if (response["data"]["bg"]) {
                that.unityInstance.setTexture(response["data"]["bg"])
                // eslint-disable-next-line
                default_bg = response["data"]["bg"];
            }
            
            if (response["data"]["function"] && response["data"]["url"]) {
                that.unityInstance.callGameManagerFunction(response["data"]["function"], response["data"]["url"]);
            }
            if (response["data"]["redirect"]) {
                window.open(response["data"]["redirect"], '_blank');
            }
        }
        if (response["response_channels"]) {
            if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
                // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
                if (!response["response_channels"]["shapes"].endsWith(".csv")) {
                    this.addAudioToQueue(response["response_channels"]["voice"], "cs_"+response["name"]+"_response", that.conversation_id, function (that, pid) {
                        if (after_played) {
                            after_played()
                        }
                        that.playFromTheQueue(that, pid)
                    }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
                } else {
                    this.addAudioToQueue(response["response_channels"]["voice"], "cs_"+response["name"]+"_response", that.conversation_id, function (that, pid) {
                        if (after_played) {
                            after_played()
                        }
                        that.playFromTheQueue(that, pid)
                    }, null, null, null, true);
                }
            }
            //delete response["response_channels"];
        }
    }
    onError(error){
        super.onError(error);
        this.playDefaults("timeout");
    }
    
    onTalkingState(){
        var that = this;
        this.state= "speaking";
        window.__dv_cbs_get("dave_state").fire("speaking");
        djq(".cb__list").first().empty();
        djq(".cb__list").first().html('<span class="bubble typing bubble_text">....</span>');
        if(!this.conversation.processing){
            var cancel_ = djq("<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Stop</button>");
            djq(".cb__list").first().append(cancel_);
            cancel_.click(function(){
                that.stopTalking();
                that.onIdelState(true);
            });
        }
        this.toggle_response_panel();
    }
    onIdelState(repeat){
        this.load__ = false;
        this.audio_playing = false;
        this.state = "idel";
        this.audioRecord(true);
        this.toggle_response_panel(true);
        this.displayResponse();
        this.conversation.processing = false;
        this.pre_resp = "";
        window.__dv_cbs_get("dave_state").fire("idel");
    }
    
    onProcessingRequest(_text) {
        // TODO animation for processing
        console.debug("Reached on processing request", _text);
        
        this.displayResponse(_text, true);
        this.toggle_response_panel();
        window.__dv_cbs_get("dave_state").fire("processing");
    }
    
    micRejected(){}
    onRecording(){
        // this.unityInstance.pauseMusic()
        console.log("Started detecting");
        this.audioRecord();
        // this.conversation.post_interaction({ [this.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
        this.triggerEvents("interaction", "speech-input");
    }
    onSpeechDetect(_text) {
        //TODO stub method
        console.log("On speech detected state", _text)
        this.displayResponse(_text);
        this.toggle_response_panel();
        this.conversation.post_interaction({ [this.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
        window.__dv_cbs_get("speech_dected").fire(_text);
    }
    
    toggle_response_panel(idel){
        if(idel){
            djq("#reponse_panel").show();
            djq("#reponse_message_panel").hide();
        }else{
            djq("#reponse_panel").hide();
            djq("#reponse_message_panel").show();
        }
    }
    //custom function
    displayResponse(txt, cancelFalse=true){
        var that = this;
        if(!txt){
            cancelFalse = false
        }
        txt = txt || "....";
        djq(".cb__list").first().empty();
        if(txt.indexOf(".") == -1){
            that.pre_resp +=" "+ txt;
            djq(".cb__list").first().html('<span class="bubble typing bubble_text">'+that.pre_resp+'</span>');
        }else{
            djq(".cb__list").first().html('<span class="bubble typing bubble_text">'+txt+'</span>');
        }
        if(cancelFalse){
            var cancel_ = djq("<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Cancel</button>");
            djq(".cb__list").first().append(cancel_);
            cancel_.click(function(){
                that.processSystemResponse(that.conversation.current_response, true, true);
                that.onIdelState(true);
                that.conversation.abortReq();
                that.conversation.abortAsrReq();
            });
        }
    }
    postConversation(text, state_=null, title=null, query_type=null){
        var sts = super.postConversation(text, state_, title, query_type);
        if(!this.conversation.engagement_id){
            this.createSession();
        }
        return sts;
    }
    enteredAreaCallback(customer_state){
        var a = replaceAll(customer_state, "_", " ")
        if(a.startsWith("cs ")){
            a = a.replace("cs ", "");
        }
        this.postConversation(a, customer_state, null, "auto");
        this.postConversation(customer_state, customer_state, null, "auto");
    }
    pointReachedCallback(value){
        console.log(`Point reached :: ${value}`);
        // var that = this;
        // this.conversation.list("point", {"point_name": value, "conversation_id": this.conversation.conversation_id}, function(data){
        //     if(data["data"].length > 0){
        //         var val = data["data"][0]["point_value"];
        //         that.conversation.iupdate("person", that.conversation.customer_id, {"total_points": val, "_async":true}, function(data){
        //             console.log(data);
        //         });
        //         window.__dv_cbs_get("point_collected").fire(val);
        //     }
        // }, function(err){
            
        // })
    }
    
}
