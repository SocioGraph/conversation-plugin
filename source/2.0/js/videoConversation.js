//
build_type = "video";
function set_cc() { }
class videoConversation extends ConversationHandler {
  constructor(settings) {
    console.log("Settings", settings);
    super(settings);
    this.avatar_loaded = true;
    this.audios_list = [];
    this.pre_resp = "";
    this.currentIndex = 0;
    this.transcript = null;
  }

  setPlaceholder(m, pos, title, time) {
    if (typeof m == "string") {
      m = new Message(m, pos, title || m, time);
      this.messages.push(m);
    }
    if (this.message_id) {
      djq("#" + this.message_id).html(m.message);
    }
  }
  setUpConversationUI() {
    super.setUpConversationUI();
    super.set("message_id", "message");
    super.set("shortcuts_id", undefined);
    super.set("whiteboard_id", "whiteboard");
    super.set("quick_access_id", "quick_access");
    super.set("video_id", "video");
    super.set("textbox_id", "input-text");
    super.set("mic_id", "record");
    super.set("sendbutton_id", "input-send");
    super.set("searchable_id", "searchable");
    // super.set("sendbutton_id", "input-send");
    super.set("mute_button", "mute");

    djq("body").append(
      "<div id='dave_conversation__' class='dave_conversation full' style='display:none'></div>"
    );
    djq("#dave_conversation__").setupVideoConversationUi({
      placeholder: this.default_placeholder || "Type here"
    });



    this.setUpEvents();
    // const scrollContainer = document.getElementById("quick_access");
    // scrollContainer.addEventListener("wheel", (evt) => {
    //     scrollContainer.scrollLeft += evt.deltaY;
    // });

    var that = this;
    this.audio = document.getElementById(this.settings["video_id"] || "video");

    // if (djq.browser.is_iphone) {
    //   this.audio.setAttribute("muted", 'true')
    //   djq(document).one('click', () => {
    //     this.audio.removeAttribute("muted")
    //   })
    // }
    // this.audio.src = "https://general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/smre/videos_demo/idle-animation_high.mp4";
  }

  mute() {
    super.mute();
    if (this.audio) {
      this.audio.muted = true;
      this.audio.setAttribute("force-mute", true);
      djq("#" + this.mute_button)
        .find("i")
        .first()
        .attr("class", "fa fa-volume-off");
    }
  }

  unmute() {
    super.mute();
    if (this.audio) {
      this.audio.muted = false;
      this.audio.setAttribute("force-mute", false);
      djq("#" + this.mute_button)
        .find("i")
        .first()
        .attr("class", "fa fa-volume-up");
    }
  }

  stopTalking() {
    this.audio.pause();
    this.audio_playing = false;
    var videoEle = document.getElementById("video")
    if (videoEle) {
      videoEle.pause(); // Pause the video if it's playing
      videoEle.removeAttribute('src'); // Remove the src attribute
      videoEle.load();
    }
    this.playIdleAnimation();
    this.state = "idel";
    window.__dv_cbs_get("StopTalkingCalled").fire()
  }

  pause() {
    super.pause();
    this.stopTalking();
    this.audio.src = undefined;
    this.audios_list = [];
    this.play_followups = [];
  }

  resume() {
    var that = this;
    // super.resume();
    // if(that.audio){
    //     that.audio.play();
    // }
    if (that.conversation.session_id) {
      that.triggerEvents("resume", "resume");
      that.conversation.post_interaction({
        [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.RESUMED
      });
    } else {
      that.triggerEvents("opened", "opened");
      that.conversation.post_interaction({
        [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.OPENED
      });
    }
    that.createSession();
    that.audios_list = [];

    if (that.state == "idel") {
      setTimeout(function () {
        // set_cc();
        that.onResponse(that.conversation.current_response);
      }, 1000 * (that.conversation.session_id ? 1 : that.settings["init_reponse_delay"] || 1));
    }

    this.unmute();

  }

  /*updateQuickAccessButtons(res) {
    if (this.quick_access_id) {
      djq("#" + this.quick_access_id).empty();
      var that = this;
      var kws = this.conversation.getKeywords(6, res);
      for (var i of kws) {
        var butn = djq("<li  style='cursor:pointer'/>");
        // butn.addClass("butn butn-dark butn-sm");
        butn.html(i[0]);
        butn.attr("value", i[0]);
        // butn.attr("value", i);
        butn.attr("key", i[1]);
        // butn.attr("style", "margin-right: 5px");
        djq("#" + this.quick_access_id).append(butn);
        butn.click(function () {
          console.debug(
            djq(this).attr("value") || djq(this).attr("key"),
            djq(this).attr("key"),
            djq(this).attr("title")
          );
          that.postConversation(
            djq(this).attr("value"),
            djq(this).attr("key"),
            djq(this).attr("title"),
            "click"
          );
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK
          });
          that.triggerEvents("interaction", "click");
        });
      }
    }
  }*/

  audioRecord(stop) {
    console.log("AUDIO REOCRD", stop);
    var that = this;
    if (that.intervel) {
      clearInterval(that.intervel);
    }
    that.intervel = undefined;
    if (stop) {
      djq("#" + that.mic_id).removeClass("recording");
      djq("#" + that.mic_id).addClass("record");
      /*djq("#" + that.mic_id).html(
        '<i id="microphone" class="fa fa-microphone fa-2x"></i>'
      );*/

      djq(".input_box").first().show();
      djq("#" + this.quick_access_id).show();
      djq("#" + this.shortcuts_id).show();
      djq("#try-saying").show();

      djq("#" + that.mic_id).find("#record").show();
      djq("#" + that.mic_id).find("span").remove();
      djq("#" + that.mic_id).find("br").remove();
      window.__dv_cbs_get("audioRecordStopped").fire();

      return;
    }
    else {
      that.state = "recording";
      var sec = 0;
      djq("#" + that.mic_id).removeClass("record");
      var intv = djq("<span>00:00</span>");
      /*djq("#" + that.mic_id)
        .find(".fa-microphone")
        .hide();*/

      // djq("#" + that.mic_id)
      //   .find("#record")
      //   .hide();

      // djq("#" + that.mic_id).append(intv);
      djq("#" + that.mic_id).addClass("recording");
      //djq("#" + that.mic_id).append("<br />");
      // djq("#" + that.mic_id).append(
      //   '<span><i class="fa fa-stop"></i></span>' // fa-2x
      // );

      // that.intervel = setInterval(function () {
      //   sec++;
      //   var min = parseInt(sec / 60);
      //   var sc = sec % 60;
      //   intv.html(
      //     (min < 10 ? "0" + min : min) + ":" + (sec < 10 ? "0" + sc : sc)
      //   );
      // }, 1000);

      // djq(".input_box").first().hide();
      // djq("#quick_access").hide();
      djq("#shortcut").hide();
      djq("#try-saying").hide();
    }
    djq("#" + that.mic_id).show();
  }

  setUpEvents() {
    var that = this;
    if (this.textbox_id) {
      djq("#" + this.textbox_id).enter(function (val) {
        if (val.trim()) {
          djq(".searchable").hide();
          that.postConversation(val, null, val, "type");
          that.conversation.post_interaction({
            [that.settings["stage_attr"] ||
              "stage"]: InteractionStageEnum.TEXT_INPUT
          });
          that.triggerEvents("interaction", "text-input");
        }
      });
      djq("#" + this.textbox_id).keyup(function (e) {
        var val = e.currentTarget.value
        that.updateSearchables(val.trim());
      });
    }
    if (this.sendbutton_id) {
      djq("#" + this.sendbutton_id).click(function () {
        if (
          djq("#" + that.textbox_id)
            .val()
            .trim()
        ) {
          that.postConversation(
            djq("#" + that.textbox_id).val(),
            null,
            djq("#" + that.textbox_id).val(),
            "type"
          );
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: "text-input"
          });
          that.triggerEvents("interaction", "text-input");
        }
      });
    }
    if (this.mic_id) {
      djq("#" + this.mic_id).click(function () {
        if (that.state == "idel") {
          // that.audioRecord();
          djq(this).hide();
          that.conversation.startRecordResponse();
        } else if (that.state == "recording") {
          that.conversation.stopRecordResponse();
        }
      });
    }
    if (this.mute_button) {
      djq("#" + this.mute_button).click(function () {
        var cc_class = djq(this).attr("class");

        if (cc_class.indexOf(" mute") > -1) {
          that.unmute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] ||
              "stage"]: InteractionStageEnum.UNMUTED
          });
          that.triggerEvents("interaction", "unmuted");

          djq(this).removeClass("mute").addClass("unmute")//.css("background-image", "url(/source/2.0/css/icons/babylon/Unmute.png)");
        } else {
          that.mute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MUTED
          });
          that.triggerEvents("interaction", "muted");

          djq(this).removeClass("unmute").addClass("mute")//.css("background-image", "url(/source/2.0/css/icons/babylon/Mute.png)");
        }

        /*var cc_class = djq(this)
          .find("i")
          .first()
          .attr("class");
        if (cc_class.indexOf("fa-volume-up") == -1) {
          that.unmute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] ||
              "stage"]: InteractionStageEnum.UNMUTED
          });
          that.triggerEvents("interaction", "unmuted");
        } else {
          that.mute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MUTED
          });
          that.triggerEvents("interaction", "muted");
        }*/
      });
    }
    djq("#close_message").click(function () {
      console.log("CLOSE THE AVATAR");
      // djq("#dave_conversation__").hide(200);
      that.pause();
      that.conversation.post_interaction({
        [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MINIMIZED
      });
      that.triggerEvents("interaction", "minimized");
    });


  }


  onLoadedCallback() {
    if (this.conversation_loaded && this.avatar_loaded) {
      this.addShortcutsButtons();
      this.conversationLoadedCallback(this.conversation.conversation_data);
      this.loaded = true;
      var that = this;
      /*window.daveRegisterCallback("enteredArea", function(cs) {
        that.enteredAreaCallback(cs);
      });
      window.daveRegisterCallback("pointReached", function(cs) {
        that.pointReachedCallback(cs);
      });*/
      setTimeout(function () {
        if (!that.minimize) {
          that.createSession();
          djq("#dave_conversation__").show(200, function () {
            setTimeout(() => {
              that.onResponse(that.conversation.init_response);
            }, 1000 * (that.settings["init_reponse_delay"] || 1))

            // set_cc();
            that.triggerEvents("chat_started", "chat_started");

            // set_cc();
            that.conversation.post_interaction({
              [that.settings["stage_attr"] ||
                "stage"]: InteractionStageEnum.AUTO_OPENED
            });
          });
        }

        djq(window).on("beforeunload", function () {
          // that.clearSession(false);
          that.conversation.post_interaction(
            {
              [that.settings["stage_attr"] ||
                "stage"]: InteractionStageEnum.LEAVE_PAGE
            },
            true
          );
          that.clearSession(false);
        });
      }, 2000);
    }
    if (window.navigator.platform == "iPhone" && screen.width < 480) {
      djq('.actions').css({ "right": "0" });
      djq('#input-send').css({ "left": "90%" });
      djq('#record').css({ "left": "10px", "position": "relative" });
    }
  }
  // playIdleAnimation() {
  //   this.audio.src = "https://d36sr1d3aypaln.cloudfront.net/smre/videos_demo/low/idle-animation_low_2.mp4";
  //   this.audio.setAttribute("loop", "true");
  // }
  playIdleAnimation() { }
  playAnimations(audo, force) {
    var ios = djq.browser.is_iphone;
    if (this.paused || this.audio_playing) {
      if (this.audio_playing && this.audios_list[0]["id"] == this.latest_audio_id) {
        this.audios_list.slice(0, 1)
        return;
      }
      setTimeout(() => {
        this.playAnimations(audo, force);
      }, 1000)
      return
    }
    console.log("PLAY ANIMATION", audo);
    var that = this;
    if (!that.audios_list.length && !audo) return;
    if (!audo) {
      audo = that.audios_list.splice(0, 1);
      audo = audo[0];
    }
    that.audio.pause();
    that.prev_audo = audo;
    that.temp_id = audo["id"];
    if (!ios) {

      var tt = that.audio.cloneNode(true);
      that.audio.remove();
      var muted = that.audio.getAttribute("force-mute");
      that.audio = tt;
      document.body.appendChild(that.audio);
    }
    that.audio.muted = false;
    that.audio.setAttribute("preload", "auto");
    that.audio.setAttribute("data-audio-id", that.temp_id);
    that.audio.setAttribute("force-mute", muted || false);

    var tmp_pre = djq("<video muted playsinline />");

    function onCanPlay() {
      tmp_pre.off("canplay", onCanPlay);
      tmp_pre.off("load", onCanPlay);
      that.audio.setAttribute("src", audo["video"]);
      that.audio.currentTime = 0;
      that.audio.play();
    }

    function onLoadedMetadata() {
      tmp_pre.off("loadedmetadata", onLoadedMetadata);
      that.audio.setAttribute('muted', 'true');
      that.audio.setAttribute("src", audo["video"]);
      that.audio.currentTime = 0.01;
      that.audio.play();
    }

    tmp_pre.on("canplay load", onCanPlay);

    if (ios) {
      tmp_pre.on("loadedmetadata", onLoadedMetadata);
    }

    tmp_pre.attr("src", audo["video"]);

    // $("#audioToggle").click(function () {
    //   that.audio.muted = !that.audio.muted;
    //   $("#audioToggle").toggleClass("muted", that.audio.muted);
    //   $("#audioToggle").toggleClass("unmuted", !that.audio.muted);
    // });
    // if (!ios) {
    //   document.getElementById("audioToggle").style.display = "none";
    // }
    that.audio.removeAttribute("loop");

    that.audio.setAttribute("main-response", audo["main_response"])

    console.debug("play init id :: ", audo["id"]);

    that.audio_playing = true;
    function _lis() {
      if (
        audo["id"] != that.temp_id &&
        that.temp_id != that.audio.getAttribute("data-audio-id")
      ) {
        return;
      }

      function playShapes_frames(id) {
        if (
          that.forced_stop &&
          that.forced_stop != that.audio.getAttribute("data-audio-id")
        ) {
          console.log("We have forced stopped, so don't play:" + id);
          return;
        }
        if (
          ["speech-detected", "processing", "idel"].indexOf(that.state) < 0 &&
          that.load__ &&
          !force
        ) {
          console.log(
            "State is not idle: " +
            that.state +
            " or we have loaded something else " +
            that.load__ +
            " already:" +
            id
          );
          return;
        }
        if (!audo["defaults"]) {
          that.onTalkingState();
        }
        if (!that.paused) {
          // that.audio.src = audo["video"];
          // that.conversation.hold(true);
          that.playing_id = id;
          console.debug("++++++Playing ID:" + id);
        } else {
          console.debug("++++++Paused ID:" + id);
        }
        //djq("#message").html(list[i][1]);
      }
      that.audio.removeEventListener("canplaythrough", _lis)
      playShapes_frames(audo["id"]);
    }
    that.audio.removeEventListener("canplaythrough", _lis);
    that.audio.addEventListener("canplaythrough", _lis);


    function ended() {
      that.audio_playing = false;
      that.currentIndex = 0;
      if (that.playing_id != that.audio.getAttribute("data-audio-id")) return;
      that.audio.removeEventListener('ended', ended);
      if (ios) {
        that.audio.removeEventListener('ended', ended);
        that.audio.setAttribute("src", getEmptyAudio());
        // that.audio.setAttribute("src", getNoiseBase64());
      }
      that.stopTalking();

      if (!that.conversation.processing) {
        that.onIdelState();
      }
      if (that.state == "idel" && !that.load__) {
        that.pre_resp = "";
      }
      if (audo["onEnds"]) {
        audo["onEnds"](that, that.playing_id);
      } else {
        that.playFromTheQueue(that, that.playing_id);
      }
      if (that.audio.getAttribute("main-response")) {
        window.__dv_cbs_get("ResponsePlayEnded").fire(audo)
      }
    }

    if (ios) {
      that.audio.addEventListener("ended", ended);
    } else {
      that.audio.addEventListener("ended", ended);
    }
    that.audio.load();
    console.log("transcript---", that.transcript)
    
    if (that.transcript && that.transcript.length > 0) {
      that.audio.addEventListener('playing', () => {
        document.getElementById(that.message_id).innerText = "";
        that.currentIndex = 0;
      })
      that.audio.addEventListener('timeupdate', () => {
        if (that.currentIndex < that.transcript.length) {
          let parts = that.transcript[that.currentIndex].split(",");
          let startTime = parseFloat(parts[0]);
          let word = parts[2].trim();
          console.log(startTime, word)
          if (that.audio.currentTime >= startTime) {
            document.getElementById(that.message_id).innerText += " " + word + " ";
            that.currentIndex++;
            document.getElementById(that.message_id).scrollTop = document.getElementById(that.message_id).scrollHeight;
          }
        }
      });
    }
  }

  playDefaults(state, message, that) {
    // super.playDefaults(state, message);
    return false;

    // eslint-disable-next-line no-redeclare
    var that = that || this;
    function __load(level) {
      djq.ajax({
        url:
          that.host_url +
          "/defaults/" +
          (that.settings["defaults_audio_id"] || that.voice_id) +
          "/" +
          level +
          "/" +
          that.enterprise_id,
        success: function (data) {
          // if (!data["video"]) {
          //   data["video"] = "https://d36sr1d3aypaln.cloudfront.net/smre/videos_demo/low/idle-animation_low_2.mp4"
          //   // data["video"]="https://general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/smre/videos/idle-animation.mp4"
          // }
          that.addAudioToQueue(
            data["video"],
            data["word_timings"] || null,
            "level_" + state,
            "defaults",
            function (that, pid) {
              that.playFromTheQueue(that, pid);
            },
            data["subfolder"],
            false,
            false
          );
          window.__dv_cbs_get("on_play_defaults").fire(that, state, message, data);
        }
      });
    }
    __load(state);
  }

  playFromTheQueue(that, playing_id) {
    var that = that || this;
    if (that.audios_list.length) {
      that.playAnimations(null, false);
      return;
    }

    if (that._timer) {
      clearTimeout(that._timer);
    }
    if (playing_id != that.latest_audio_id) return;
    if (!that.play_followups.length) {
      return;
    }
    (function (tm) {
      var tf = function () {
        if (that.paused) {
          setTimeout(tf, tm);
        }
        if (that.state != "idel" || that.load__) return;
        if (that.audios_list.length) {
          that.playAnimations(null, false);
        } else if (that.play_followups.length) {
          that.playNudge(that.play_followups[0], function (data) {
            that.playNudge(data);
          });
          that.play_followups.splice(0, 1);
        } else {
        }
      };
      that._timer = setTimeout(tf, tm);
    })(that.conversation.current_response["wait"] || 10000);
  }
  addAudioToQueue(
    video,
    word_timings,
    id,
    conversation_id,
    onEnds,
    subfolder,
    force,
    main_response,
    response_data
  ) {
    var that = this;
    this.forced_stop = false;
    var id_ = id + "___" + makeid(4);
    console.debug("audio adding id :: ", id_);
    this.latest_audio_id = id_;
    if (word_timings) {
      this.transcript = word_timings.trim().split("\n");
    } else {
      this.transcript = null
    }
    console.log("---------transcript -word_timings------", this.transcript)
    if (force) {
      this.audios_list = [];
      this.forced_stop = id_;
      if (this._timer) {
        clearTimeout(this._timer);
      }
    }
    if (this._timer && onEnds) {
      clearTimeout(this._timer);
    }

    var aurl;
    if (video) {
      if (video.indexOf("http") >= 0) {
        aurl = video;
      } else {
        aurl = this.host_url + "/" + video;
      }
    }

    var conversation_id = conversation_id || this.conversation_id;
    var ob = {
      id: id_,
      video: video,
      onEnds: onEnds,
      defaults: conversation_id == "defaults",
      main_response: main_response,
      response_data: response_data
    };
    this.audios_list.push(ob);

    var fil = "?voice_id=" + this.voice_id;
    if (subfolder) fil += "&subfolder=" + subfolder;
    if (!that.audio_playing) {
      that.playAnimations(null, force);
    }
  }

  nudgeResponse(response) {
    super.nudgeResponse(response);
    var that = this;
    // if (response["data"]["video"]) {
    //   response["response_channels"]["video"] = response["data"]["video"]
    // }
    // response["response_channels"]["video"]="https://general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/smre/videos/ccp_Init.mp4"
    if (response["response_channels"]) {
      var voice = response["response_channels"]["voice"];
      var word_timings=response["response_channels"]["word_timings"];
      if (response["response_channels"]["video"]) {
        this.addAudioToQueue(
          response["response_channels"]["video"],
          word_timings,
          "nudge_" + response["name"] + "_response",
          that.conversation_id,
          function (that, pid) {
            that.playFromTheQueue(that, pid);
          },
          null,
          true,
          false,
          response
        );
      } else {
        this.addAudioToQueue(
          voice,
          word_timings,
          "nudge_" + response["name"] + "_response",
          that.conversation_id,
          function (that, pid) {
            that.playFromTheQueue(that, pid);
          },
          null,
          true,
          false,
          response
        );
      }
      //delete response["response_channels"];
    }
  }
  addNudges(lst) {
    this.play_followups = lst || [];
  }
  processSystemResponse1(response, after_played) {
    if (!response.response_channels["word_timings"]) {
      if (response.data && response.data.redirect_placeholder) {
        this.setPlaceholder(response.data["redirect_placeholder"], "left", null);
        delete response.data.redirect_placeholder
      } else {
        this.setPlaceholder(response["placeholder"], "left", null);
      }
    }
    this.setWhiteboard(response);
    this.updateQuickAccessButtons(response);

    if (response["data"]) {
      this.addNudges(response["data"]["_follow_ups"]);
    }
    if (this.actionCallbacks) {
      this.actionCallbacks(response, "conversation_response");
    }
    this.status = "idel";
  }
  processSystemResponse(response, after_played, partial) {
    this.processSystemResponse1(response);
    this.state = "speaking";
    window.__dv_cbs_get("StopProcessingInputState").fire();
    if (partial) {
      return;
    }
    // this.displayResponse();
    var that = this;
    if (response["data"]) {
      if (
        response["data"]["avatar_model_name"] ||
        response["data"]["avatar_id"]
      ) {
        // that.unityInstance.showPersona(
        //   response["data"]["avatar_model_name"] || response["data"]["avatar_id"]
        // );
        that.voice_id =
          response["data"]["voice_id"] || response["data"]["avatar_id"];
        // eslint-disable-next-line
        defaul_avatar_id =
          response["data"]["avatar_model_name"] ||
          response["data"]["avatar_id"];
      }
      if (response["data"]["bg"]) {
        // that.unityInstance.setTexture(response["data"]["bg"]);
        // eslint-disable-next-line
        default_bg = response["data"]["bg"];
      }

      // if (response["data"]["function"] && response["data"]["url"]) {
      //   that.unityInstance.callGameManagerFunction(
      //     response["data"]["function"],
      //     response["data"]["url"]
      //   );
      // }
      if (response["data"]["redirect"]) {
        window.open(response["data"]["redirect"], "_blank");
      }
    }
    if (response["response_channels"]) {
      var voice = response["response_channels"]["voice"];
      var video = response["response_channels"]["video"];
      var word_timings = response["response_channels"]["word_timings"] || null;

      if (response.data && response.data.redirect_video_url && response.data.client_actions.length!=0) {
        video = response.data.redirect_video_url || video
        word_timings = response.data.redirect_response_channels.word_timings || word_timings
        delete response.data.redirect_video_url;
      }
      if (video) {
        this.addAudioToQueue(
          video,
          word_timings,
          "cs_" + response["name"] + "_response",
          that.conversation_id,
          function (that, pid) {
            if (after_played) {
              after_played();
            }
            that.playFromTheQueue(that, pid);
          },
          null,
          true,
          true,
          response
        );
      } else {
        this.addAudioToQueue(
          voice,
          word_timings,
          "cs_" + response["name"] + "_response",
          that.conversation_id,
          function (that, pid) {
            if (after_played) {
              after_played();
            }
            that.playFromTheQueue(that, pid);
          },
          null,
          true,
          true,
          response
        );
      }
      //delete response["response_channels"];
    }
  }
  onError(error) {
    super.onError(error);
    this.playDefaults("timeout");
  }
  eventCallback(status, message) {
    if (status == "signup" || status == "interaction_response" || status == "start_session" || status == "end_session" || status == "hotword" || status == "person_detected" || status == "capture_ended") {
      this.triggerEvents(status, message);
      if (status == "signup") {
        this.status = status;
      }
    }
    if (!this.loaded) {
      return;
    }

    if (status == "idel") {
      if (!this.load__) {
        this.onIdelState();
        this.state = status;
      }
    } else if (status == "processing") {
      // djq('.whiteboard .btns').hide()

      if (message != 'nudge') {
        // this.playDefaults("level_one");
        this.onProcessingRequest(message);
      }
      this.state = status;
    }
    else if (status == "speech_processing") {
      this.onProcessingRequest("....");
      this.state = "processing";
    }
    else if (status == "conversation-error") {
      this.state = "idel";
      this.onError(message);
      this.state = "idel";
    } else if (status == "conversation-response") {
      this.onResponse(message);
      this.state = "idel";
    } else if (status == "speech-detected") {
      this.onSpeechDetect(message);
      this.state = status;
    } else if (status == "delay") {
      // this.playDefaults("more_delay");
      this.state = "processing";
    } else if (status == "timeout") {
      // this.playDefaults("timeout");
      this.onError(message);
      this.onIdelState();
      this.state = "idel";
    } else if (status == "socket_disconnected") {
      this.onError(message);
      this.state = "idel";
    } else if (status == "recording") {
      this.onRecording();
      this.state = "recording";
    } else if (status == "end_session") {
      if (this.session_expired_callback) {
        this.session_expired_callback();
      }
      this.state = "session_expired";
    } else if (status == "mic_rejected") {
      this.micRejected();
      this.state = "idel";
    }
    console.debug("Event callback in converstaion handiler", status, message);
  }
  onTalkingState() {
    console.log("AVATAR IS SPEAKING");
    window.__dv_cbs_get("SpeakingState").fire();
    var that = this;
    this.state = "speaking";
    djq("#disable_layer")
      .first()
      .empty();
    djq(".chat_box").removeClass("dave-speech-stop");
    djq(".questions .dave-stop-speech").remove();
    djq(".chat_box").addClass("dave-speech-stop");
    // djq("#stop-talking").remove();
    // djq("#disable_layer")
    //   .first()
    //   .html('<span class="bubble typing bubble_text">....</span>');
    if (!this.conversation.processing) {

      var cancel_ = djq(
        // "<button href='javascript:void(0)' class='butn butn-dark butn-sm dave-stop-speech' style='font-size: 10px;'> Stop</button>"
        // "<button class='btns stop-talking' id='stop-talking'></button>"
        "<div class='dave-stop-speech'><button class='dave-stop-img'></button>Stop Response</div>"
      );
      djq(".questions")// djq("#disable_layer")
        .first()
        .append(cancel_);
      cancel_.click(function () {
        that.stopTalking();
        that.onIdelState(true);
        djq("#stop-talking").remove();
        window.__dv_cbs_get("StopButtonClicked").fire();
      });
    }
    // djq("#disable_layer").remove();
    // djq("#chat_box").append('<div id="disable_layer"><span>Wait for Avatar to complete...</span></div>');
    this.toggle_response_panel("....");
  }
  onIdelState(repeat) {
    console.log("AVATAR IS ON IDEL");
    this.load__ = false;
    this.audio_playing = false;
    this.state = "idel";
    this.audioRecord(true);
    this.toggle_response_panel("", true);
    // this.displayResponse();
    this.conversation.processing = false;
    this.pre_resp = "";
    // djq("#disable_layer").remove();
  }

  onProcessingRequest(_text) {
    // TODO animation for processing
    console.debug("Reached on processing request", _text);

    // this.displayResponse(_text, true);
    this.toggle_response_panel("Processing....");
  }

  micRejected() { }
  onRecording() {
    console.log("Started detecting");
    this.audioRecord();
    // this.conversation.post_interaction({ [this.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
    this.triggerEvents("interaction", "speech-input");
  }
  onSpeechDetect(_text) {
    //TODO stub method
    console.log("On speech detected state", _text);
    djq(".input_box #input-text").val(_text);
    window.__dv_cbs_get("SpeechDetected").fire();

    // this.displayResponse(_text);
    this.toggle_response_panel(_text);
    djq("#" + this.mic_id).removeClass("recording");
    djq("#" + this.mic_id).addClass("record");
    this.conversation.post_interaction({
      [this.settings["stage_attr"] ||
        "stage"]: InteractionStageEnum.SPEECH_INPUT
    });
  }

  toggle_response_panel(message, idel) {
    // if (idel) {
    //   djq("#disable_layer").hide();
    //   djq("#disable_layer>button").first().remove();
    // } else {
    //   djq("#disable_layer").show();
    //   djq("#disable_layer>span").html(message || "Processing...");
    // }

    if (idel) {
      djq("#disable_layer").hide();
      // djq("#stop-talking").remove();
      djq(".questions .dave-stop-speech").remove();
      djq(".chat_box").removeClass("dave-speech-stop");
    }
    window.__dv_cbs_get("ProcessingInputState").fire(message);
  }
  //custom function
  displayResponse(txt, cancelFalse = true) {
    var that = this;
    if (!txt) {
      cancelFalse = false;
    }
    txt = txt || "....";
    // djq(".cb__list")
    //   .first()
    //   .empty();
    if (txt.indexOf(".") == -1) {
      that.pre_resp += " " + txt;
      // djq(".cb__list")
      //   .first()
      //   .html(
      //     '<span class="bubble typing bubble_text">' + that.pre_resp + "</span>"
      // );
      djq("#disable_layer>span").html(that.pre_resp);
    } else {
      // djq(".cb__list")
      //   .first()
      //   .html('<span class="bubble typing bubble_text">' + txt + "</span>");
      djq("#disable_layer>span").html(txt);
    }
    if (cancelFalse) {
      var cancel_ = djq(
        "<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Cancel</button>"
      );
      // djq(".cb__list")
      //   .first()
      //   .append(cancel_);
      djq("#disable_layer").append(cancel_)
      cancel_.click(function () {
        that.processSystemResponse(
          that.conversation.current_response,
          true,
          true
        );
        that.onIdelState(true);
        that.conversation.abortReq();
        that.conversation.abortAsrReq();
      });
    }
  }
  postConversation(text, state_ = null, title = null, query_type = null) {
    var sts = super.postConversation(text, state_, title, query_type);
    if (!this.conversation.engagement_id) {
      this.createSession();
    }
    return sts;
  }
  enteredAreaCallback(customer_state) {
    this.postConversation(customer_state, customer_state, null, "auto");
  }
  pointReachedCallback(value) {
    console.log(`Point reached :: ${value}`);
    var that = this;
    this.conversation.list(
      "point",
      { point_name: value, conversation_id: this.conversation.conversation_id },
      function (data) {
        if (data["data"].length > 0) {
          var val = data["data"][0]["point_value"];
          that.conversation.iupdate(
            "person",
            that.conversation.customer_id,
            { total_points: val, _async: true },
            function (data) {
              console.log(data);
            }
          );
          window.__dv_cbs_get("point_collected").fire(val);
        }
      },
      function (err) { }
    );
  }
}
