class ChatbotConversation extends ConversationHandler {
    constructor(settings) {
        super(settings);
        this.pre_resp = "";
    }

    setUpConversationUI(){
        super.setUpConversationUI();
        super.set("quick_access_id", "quick_access");
        super.set("textbox_id", "input-text");
        super.set("sendbutton_id", "input-send");

        this.setUpEvents();
    }

    setUpEvents(){
        var that = this;
        if(this.textbox_id){
            djq("#"+this.textbox_id).enter(function(val){
                if(val.trim()){
                    that.postConversation(val, null, val, "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.TEXT_INPUT});
                    that.triggerEvents("interaction", "text-input");
                }
            });
            djq("#"+this.textbox_id).keyup(function(e){
                var val = e.currentTarget.value
                that.updateSearchables(val.trim());
            });
            
        }
        if(this.sendbutton_id){
            djq("#"+this.sendbutton_id).click(function(){
                if(djq("#"+that.textbox_id).val().trim()){
                    that.postConversation(djq("#"+that.textbox_id).val(), null, djq("#"+that.textbox_id).val(), "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: "text-input"});
                    that.triggerEvents("interaction", "text-input");
                }
            });
        }
    }
    onLoadedCallback(){
        if(this.conversation_loaded){
            this.addShortcutsButtons();
            this.conversationLoadedCallback(this.conversation.conversation_data);
            this.loaded = true;
            var that = this;
            djq(window).on("beforeunload", function(){
                // that.clearSession(false);
                that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.LEAVE_PAGE});
                that.clearSession(false);
            })
        }
    }

    setPlaceholder(m, pos, title, time) {
        
        if(typeof m == "string"){
            m = new Message(m, pos, title || m, time);
            this.messages.push(m);
        }
        djq("#messages").append(m.html());
    }
    setWhiteboard() { }
    addMessage() { }
    processSystemResponse(response, after_played, partial) {
        //super.processSystemResponse(response);
        if(partial){
            return;
        }
        var that = this;
        if (response["data"]) {
          if (response["data"]["function"] && response["data"]["url"]) {
            that.unityInstance.callGameManagerFunction(response["data"]["function"], response["data"]["url"])
          }
          if (response["data"]["redirect"]) {
            window.open(response["data"]["redirect"], '_blank');
          }
        }
    }
}