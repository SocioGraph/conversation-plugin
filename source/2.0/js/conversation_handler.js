/**
 * @description Handler class is abstract class which has all the abstract functions which used to handle in ui, you have to extend this functanality in you type of conversation class
 * @example class AvatarConversation extends ConversationHandler
 */

class ConversationHandler {
    /**
     * @description Handler class is abstract class which has all the abstract functions which used to handle in ui, you have to extend this functanality in you type of conversation class
     * @param {object} settings settings is and object type params which should contain host_url, conversation_ids, default_conversation, 
     * @example var settings = {
     * "enterprise_id": "dave_restaurant", 
     * "conversation_id": "deployment_kiosk",
     * "event_type": "deployment",
     * "event_id": "kiosk",
     * "host_url": "https://staging.iamdave.ai",
     * "signup_apikey": "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
     * "unity_url": "https://s3.ap-south-1.amazonaws.com/unity-plugin.iamdave.ai/unity/dev_empty_bg" //unity model url,
     * "type": "kiosk" //type of event,
     * "default_placeholder" : "Type here" //placeholder on input text box,
     * "recognizer_type": "google" //ASR engine name google/kaldi,
     * "session_model": "session" //session model name,
     * "user_id_attr": "customer_id" //user id attribute in the customer model,
     * "interaction_model": "interaction" // interaction model name,
     * "speech_recognition_url": "https://speech.iamdave.ai" //ASR speech server url,
     * "image_processing_url": "https://speech.iamdave.ai" //Image processing server url,
     * "avatar_id": "dave-english-male" //avatar id of the avatar need to load,
     * "voice_id": "english-male" //Speech languange,
     * "additional_session_info": {} // aditional session attributes will post to the session model ,
     * "signup_params": {} //aditional signup params to pass to while creating signup,
     * "session_time": 50 //waiting time for each session,
     * "conversationLoadedCallback": function(data){} //callback function will respond to this callback function,
     * "session_expired_callback": function(){} //Callback function for on session expired,
     * "actionCallbacks":function(data){} //will return some conversation data after every state
     * }
     */
    constructor(settings) {
        settings["vad"] = settings["vad"] == undefined ? true : settings["vad"];
        this.settings = settings;
        this.authentication_cookie_key = "authentication";
        this.minimize = false;
        for (var k in settings) {
            this.set(k, settings[k]);
        }

        var _cids = this.conversation_ids || this.conversation_id;
        if (!_cids) {
            throw Error("Conversation ids required");
        }
        if (typeof _cids == "string") {
            _cids = _cids.split(",");
        }
        this.conversation_ids = _cids;
        this.conversation_id = _cids.length == 0 || !this.default_conversation ? _cids[0] : this.default_conversation;
        
        this.con_map = {};
        this.messages = [];
        this.whiteboards = [];
        

        //statuses
        this.state = "idel";
        this.audio_playing = false;
        this.load__ = false;
        this.conversation_loaded = false;
        this.loaded = false;
        this.filtered_keywords = undefined;
        this._init_response = null;

        extendData(this._init_response, this.settings["init_respose"]).then((sdata) =>{
            console.log("init response")
            if(sdata){
                this._init_response = sdata;
            }
        })


        this.setupEnvironment();
    }
    
    triggerEvents(action, data) {
        console.debug("Triggered event for ::", action);
        if (this.actionCallbacks && data != undefined) {
            setTimeout(() => {
                this.actionCallbacks(data, action);
            }, 0)
        }
    }
    onLoadedCallback() {
        if (this.conversation_loaded) {
            this.onResponse(this.conversation.init_response);
            if (this.conversationLoadedCallback) {
                this.conversationLoadedCallback(this.conversation.conversation_data);
                this.addShortcutsButtons();
                this.loaded = true;
            }
        }
    }
    set(key, value) {
        this[key] = value;
    }
    get(key) {
        return this[key];
    }
    /**
     * @description This function is use change the converstion 
     * @example ds.setConversation("deployment_kiosk_kannada")
     * @param {string} conversation_id Its id of conversation which you wants to change
     * @returns will returns true if the converstation id exists in conversations list sent through constructor
     */
    setConversation(conid) {
        if (this.con_map[conid]) {
            this.conversation_id = conid;
            this.conversation = this.con_map[conid];
            this.refreshUI();
            return true;
        } else {
            return false;
        }
    }



    /**
     * @description This function is use for updating the state of the coversation
     * @example this.evetCallback("idel", null)
     * @param {string} status type of the state
     * @param {string} conversation_id Its id of conversation which you wants to change
     * @returns null
     */
    eventCallback(status, message) {
        if (status == "signup" || status == "interaction_response" || status == "start_session" || status == "end_session" || status == "hotword" || status == "person_detected" || status == "capture_ended") {
            this.triggerEvents(status, message);
            if (status == "signup") {
                this.status = status;
            }
        }
        if (!this.loaded) {
            return;
        }
        
        if (status == "idel") {
            if (!this.load__) {
                this.onIdelState();
                this.state = status;
            }
        } else if (status == "processing") {
            // djq('.whiteboard .btns').hide()
            
            if (message != 'nudge') {
                this.playDefaults("level_one");
                this.onProcessingRequest(message);
            }
            this.state = status;
        }
        else if (status == "speech_processing") {
            this.onProcessingRequest("....");
            this.state = "processing";
        }
        else if (status == "conversation-error") {
            this.state = "idel";
            this.onError(message);
            this.state = "idel";
        } else if (status == "conversation-response") {
            this.onResponse(message);
            this.state = "idel";
        } else if (status == "speech-detected") {
            this.onSpeechDetect(message);
            this.state = status;
        } else if (status == "delay") {
            this.playDefaults("more_delay");
            this.state = "processing";
        } else if (status == "timeout") {
            this.playDefaults("timeout");
            this.onError(message);
            this.onIdelState();
            this.state = "idel";
        } else if (status == "socket_disconnected") {
            this.onError(message);
            this.state = "idel";
        } else if (status == "recording") {
            this.onRecording();
            this.state = "recording";
        } else if (status == "end_session") {
            if (this.session_expired_callback) {
                this.session_expired_callback();
            }
            this.state = "session_expired";
        } else if (status == "mic_rejected") {
            this.micRejected();
            this.state = "idel";
        }
        console.debug("Event callback in converstaion handiler", status, message);
    }

    micRejected() { }
    getAuthDetails(cb) {
        var that = this;
        var ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        if (!ds.checkAuth()) {
            that.createUser(ds, function (ds) {
                cb(ds);
            });
        } else {
            cb(ds);
            that.triggerEvents("signup", "User loaded from cookies");
        }
    }

    updateNewUser() {
        var that = this;
        this.createUser(undefined, function (ds) {
            that.state = "idel";
            for (var con of that.conversation_ids) {
                that.con_map[con].changeUser(ds.user_id, ds.api_key)
            }
        });
    }

    setupEnvironment() {
        var that = this;
        function _load(ds) {
            var unity_url_exists = false;
            if (that.settings["unity_url"]) {
                that.setUpConversationUI();
                unity_url_exists = true;
            }
            if (that.event_type && that.event_id) {
                ds.get(that.event_type, that.event_id, function (evdata) {
                    that.settings = djq.extend(evdata, that.settings);
                    if (!unity_url_exists) {
                        that.setUpConversationUI();
                    }
                    that.loadConversations(evdata);
                }, function (error) {
                    console.log(error.message || error);
                    that.triggerEvents("failed", error);
                })
            } else {
                if (!unity_url_exists) {
                    that.setUpConversationUI();
                }
                that.loadConversations(that.settings);
            }
        }
        this.getAuthDetails(function (ds) {
            _load(ds)
        })
    }
    postConversationFeedback(rating, text, cb) {
        this.conversation.postConversationFeedback(rating, text, cb);
    }
    createUser(ds, cb) {
        var that = this;
        if (!ds) {
            ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        }
        var ob = this.settings["signup_params"] || this.settings["additional_signup_info"] || {};
        extendData({}, ob).then(function (sdata) {
            if (that.settings["email_attr"]) {
                var em_att = that.settings["email_attr"];
                sdata[em_att] = "dinesh+" + makeid(10) + "@i2ce.in";
            }
            sdata["validated"] = true;
            ds.signup(sdata, function (data) {
                console.log("Signup done", data);
                that.customer_id = ds.user_id;
                that.headers = ds.headers;
                that.triggerEvents("signup", data);
                cb(ds);
            }, function (error) {
                console.log("Signup failed", error);
                that.triggerEvents("signup", error.error || error);
            })
        }).catch(function (err) {
            console.error("Signup failed", err);
        })
    }
    patchUser(data, success, error) {
        var ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        console.debug(ds);
        ds.update(ds.customer_model_name, ds.customer_id, data, success, error);
    }

    /**
     * @description This function is use for loading all the conversations
     * @example //internal function will get called from constructor
     * @returns returns through information through 'onSignup' callback
     */
    loadConversations() {
        var that = this;
        
        var loadedcount = 0;
        for (var con of this.conversation_ids) {
            (function (con) {
                that.con_map[con] = new Conversation(that.host_url, that.enterprise_id, con, that.settings, function () {
                    console.log(con + " Conversation loaded");
                    // that.customer_id = that.con_map[con].user_id;
                    that.load_status = "initcon";
                    var default_response = false;
                    function __loaded() {
                        loadedcount++;
                        if (loadedcount == that.conversation_ids.length) {
                            that.conversation_loaded = true;
                            that.onLoadedCallback();
                        }
                    }
                    function _default_init(){
                        that.con_map[con].start(function (data) {
                            that.con_map[con].init_response = data;
                            if (!default_response) {
                                __loaded();
                            }
                        }, function (error) {
                            that.con_map[con].init_response = error;
                            console.error(error);
                            if (!default_response) {
                                __loaded();
                            }
                        }, function (s, m) {
                            that.eventCallback(s, m);
                        });
                    }
                    if (that.settings["init_responses"] && that.settings["init_responses"][con]) {
                        that.init_response = null; 
                        extendData({}, that.settings["init_responses"][con]).then((sd)=>{
                            // that.settings["init_responses"][con];
                            if(Object.keys(sd).length){
                                that.init_response = sd;
                                var rf = that.con_map[con];
                                rf.init_response = sd;
                                rf.current_response = sd;
                                rf.conversation_url = "/conversation/"+rf.conversation_id+"/"+rf.customer_id;
                                rf.streamer.set("customer_id", rf.customer_id);
                                rf.streamer.set("api_key", rf.api_key);
                                __loaded();
                                default_response = true;
                            }else{
                                _default_init();
                            }
                        })
                    }else{
                        _default_init();
                    }
                }, function (error) {
                    console.error(con + " Conversation loading failed ", error);
                }, function (s, m) {
                    that.eventCallback(s, m);
                });
                if (that.conversation_id == con) {
                    that.conversation = that.con_map[con];
                }
            })(con);
        }
    }

    /**
     * @description abstract function will get call once the conversation is loaded
     */
    setUpConversationUI() {
        if (this.settings["default_avatar_id"])
            defaul_avatar_id = this.settings["default_avatar_id"];
        if (this.settings["default_bg"])
            default_bg = this.settings["default_bg"];
    }
    
    //conversation post functions
    playDefaults(state, text) {
        // 1 2 3 level_one level_two level_three
        // 4 timeout
        // 0 speech_is_none
        var that = this;
        //do something
        
        if (state == "level_one") {
            this.play_followups = [];
            that.setPlaceholder("Loading...", "left")
            
        }

        if (text) {
            // that.onProcessingRequest(text);
            that.user_reponse = { "customer_response": text };
        }
    }

    
    /**
     * @description This function is for posting conversation which will call backend apis
     * @example //ds.postConversation("i am looking for a suv car", null, null);
     * @example //ds.postConversation("yes", "cs_confirm", "Yes");
     * @param {string} text customer response the reponse you got from the user
     * @param {string} state_ is optional which represents the customer_state in the conversation
     * @param {string} title title to show while processing
     * @returns returns through callbacks
     */
    postConversation(text, state_ = null, title = null, query_type = null) {
        var that = this;
        this.updateSearchables();
        if (!(text || state_) || this.state != "idel" || this.load__ || this.audio_playing) {
            return false;
        }
        if (this._timer) {
            clearTimeout(this._timer);
        }
        this.user_reponse = { "customer_state": state_ };
        
        // this.onTalkingState();
        
        this.state = "processing";
        this.play_followups = [];
        this.audio_playing = false;
        this.load__ = true;
        window.__dv_cbs_get("ShowProcessingInputState").fire();
        title = title ? title : text;
        // this.onProcessingRequest((title.indexOf("{") == -1 ? title : "Form response"));
        this.toggle_response_panel("Processing....");
        //djq("audio").each(function(){ djq(this).remove(); })
        // this.mute();
        // this.playDefaults("level_one", "", that);
        this._level_two_timer = setTimeout(function () {
            if (that.load__) {
                that.playDefaults("level_two", "", that);
            }
        }, 8000)
    
        this.setWhiteboard();
        // this.conversation.hold(true);
        var m = new Message(text, "right", "You");
        this.addMessage(m, "left")
        //djq("#whiteboard").append(loading_whiteboard.html())
        this.setPlaceholder("Loading...", "left");
        this.conversation.next(text, state_, query_type, title, function (data) { that.onResponse(data) }, function (data) { that.onError(data) });
        // this.setPlaceholder("Loading....");   
        
        return true;
    }
    playNudge(state) {
        var that = this;
        this.conversation.getNudge(state, function (data) {
            that.nudgeResponse(data);
        }, function (error) {
            // that.nudgeResponse(data);
            console.error(error);
        })
    }

    //onresponse ui actions
    whiteBoardActions(event, event1) {
        var data = event.data;
        if (!data && event1) {
            data = event1.data;
        }
        var m = new Message(data.value, "right", "You");
        // that.setPlaceholder(m)
        this.addMessage(m);
        if (typeof data.value == "object") {
            data.value = JSON.stringify(data.value);
        }
        console.debug(data.value, data.key);
        this.triggerEvents(data["type"], data);
        this.postConversation(data.value, data.key, data.title, "click");
    }
    whiteBoardEvents(event, event1) {
        var data = event.data;
        if (!data && event1) {
            data = event1.data;
        }
        var m = new Message(data.value, "right", "You");
        // this.setPlaceholder(m)
        this.addMessage(m);
        if (typeof data.value == "object") {
            data.value = JSON.stringify(data.value);
        }
        this.triggerEvents(data["type"], data);
        this.conversation.postEvent(data.key, data.value);
    }
    setWhiteboard(response) {
        var that = this;
        
        if (!this.whiteboard_id) {
            return;
        }
        
        if (this.whiteboard_ov) {
            this.wb_h = djq(".dave-whiteboard").css("height");
            this.whiteboard_ov.remove();
            this.whiteboard_ov = null;
        }
        if (!response) {
            var loading_whiteboard = this.settings["loading_whiteboard"] || this.conversation.conversation_data['loading_whiteboard'];
            if (loading_whiteboard) {
                this.whiteboard_ov = djq('<div data-id="loading"><div class="dave-whiteboard"><div class="options-panel image"><div><img src="' + loading_whiteboard + '" style="width: 100%;"></div></div></div></div>');
                djq("#" + this.whiteboard_id).append(this.whiteboard_ov);
            }
        } else {
            var wb = response["whiteboard"];
            if (wb) {
                var w = new WhiteBoard({ "white_board_id": response["name"] });
                w.html(wb);
                w.watch(function (e, e1) { that.whiteBoardActions(e, e1) });
                w.watchEvents(function (e, e1) { that.whiteBoardEvents(e, e1) });

                this.whiteboard_ov = djq("<div/>");
                this.whiteboard_ov.append(w.ele);

                djq("#" + this.whiteboard_id).append(this.whiteboard_ov);
                this.whiteboards.push(w.ele);
            } else {
                wb = this.settings["default_whiteboard"] || this.conversation.conversation_data['default_whiteboard'];
                if (wb) {
                    this.whiteboard_ov = djq('<div data-id="loading"><div class="dave-whiteboard"><div class="options-panel image"><div><img src="' + (wb) + '" style="width: 100%;"></div></div></div></div>');
                    djq("#" + this.whiteboard_id).append(this.whiteboard_ov);
                }
            }
        }
        if (this.wb_h) {
            djq(".dave-whiteboard").css('height', this.wb_h);
        }
        var close_wb = djq('<button class="btns" id="close_wb"></button>');
        // whiteboard.append(close_wb);
        djq('.dave-whiteboard').append(close_wb);

        //Hide whiteboard
        djq("#close_wb").click(function () {
            djq("#whiteboard").hide();
        });
    }
    addMessage(m, pos = "left") {
        this.messages.push(m);
        // djq("#messages").html(m.html());
        //     djq(".messages-div").first().animate({
        //         scrollTop: djq(".message").last().offset().top+80
        //     })
    }

    setPlaceholder(m, pos, title, time) {
        if (typeof m == "string") {
            m = new Message(m, pos, title || m, time);
            this.messages.push(m);
        }
        if (this.message_id) {
            djq("#" + this.message_id).fit_text(m.message);
        }
    }
    createAccessButton(title, key, value) { 
        var that = this;
        var butn = djq("<button />");
        butn.addClass("butn butn-dark butn-sm");
        butn.html(title);
        butn.attr("value", value || key);
        // butn.attr("value", i);
        butn.attr("key", key);
        butn.attr("style", "margin-right: 5px");
        
        butn.click(function(){
            console.debug(djq(this).attr('value') || djq(this).attr('key'), djq(this).attr('key'), djq(this).attr("title"));
            var st = that.postConversation(djq(this).attr('value'), djq(this).attr('key'), djq(this).attr('title'), "click");
            
            // that.triggerEvents("interaction", "click");
            if (st) {
                that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK});
                that.triggerEvents("interaction", "click");    
                djq(this).addClass("active");
                that.updateInput();
            }
        });
        return butn;
    }
    updateQuickAccessButtons(res){
        if(this.quick_access_id){
            djq("#"+this.quick_access_id).empty();
            var that =this;
            var kws = this.conversation.getKeywords(6, res);
            for(var i of kws){
                var b = this.createAccessButton(i[0], i[1], i[0])
                // djq().append(b);
                djq("#"+this.quick_access_id).append(b);
            }
        }
    }
    updateSearchables(text){
        if(this.searchable_id){
            djq("#"+this.searchable_id).empty();
            djq("#"+this.searchable_id).parent().hide();
            if(!text || text.length < 3) return;
            var that =this;
            var kws = 0;
            for(var i of this.conversation.keywords){
                if(i[2].toLowerCase().indexOf(text.toLowerCase()) != -1){
                    var li = djq("<li />");
                    li.addClass("list-group-item");
                    li.html(`<span>${i[2]}</span>`);
                    li.attr("value", i[2]);
                    // butn.attr("value", i);
                    li.attr("key", i[1]);
                    li.attr("style", "margin-right: 5px");
                    djq("#"+this.searchable_id).append(li);
                    li.click(function(){
                        console.debug(djq(this).attr('value') || djq(this).attr('key'), djq(this).attr('key'), djq(this).attr("title"));
                        
                        var st = that.postConversation(djq(this).attr('value'), djq(this).attr('key'), djq(this).attr('title'), "click");
                        if (st) {
                            that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK});
                            that.updateInput();
                            that.triggerEvents("interaction", "click");    
                            djq(this).addClass("active");
                        }
                    });
                    kws++;
                    if(kws == 15){
                        break
                    }
                    djq("#"+that.searchable_id).parent().show();
                }
            }
        }
    }
    addShortcutsButtons(){
        if(this.conversation.conversation_data["buttons"] && this.shortcuts_id){
            var sbs = this.conversation.conversation_data["buttons"] || [];
            var that = this;
            djq("#"+this.shortcuts_id).empty();
            for(var i of sbs){
                // var butn = djq("<button />");
                // butn.addClass("butn butn-dark butn-sm");
                // butn.html(i["title"]);
                // butn.attr("key", i["customer_state"]);
                // butn.attr("value", i["customer_response"]);
                // butn.attr("title", i["title"]);
                // butn.attr("style", "margin-right: 5px");
                var b = this.createAccessButton(i["title"], i["customer_state"], i["customer_response"])
                djq("#"+this.shortcuts_id).append(b);
            }
            // djq("button[key]").each(function(){
            //     djq(this).click(function(){
            //         console.debug(djq(this).attr('value') || djq(this).attr('key'), djq(this).attr('key'), djq(this).attr("title"));
            //         var st = that.postConversation(djq(this).attr('value') || djq(this).attr('key'), djq(this).attr('key'), djq(this).attr("title"), "click");
            //         if (st) {
            //             that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK });
                    
            //             that.triggerEvents("interaction", "click");    
            //             djq(this).addClass("active");
            //         }
                    
            //     });

            // })
        }
    }
    onSpeechDetect(text){}
    onRecording(){}
    refreshUI(rs){
        rs = rs ? rs :this.conversation.init_response;
        if(!rs){ return };
        
        this.setPlaceholder(rs["placeholder"]);
        this.setWhiteboard(rs["whiteboard"]);
        this.updateQuickAccessButtons(rs);
        this.addShortcutsButtons();
    }

    //on response callbacks
    onResponse(response){
        var that =this;
        this.load__ = false;
        this.state = "speaking";
        // window.__dv_cbs_get("StopProcessingInputState").fire();
        // this.toggle_response_panel("Processing....");
        this.toggle_response_panel("",true);
        console.debug(response)
        // this.conversation.hold(false);
        if(!response || Object.keys(response) == 0) return;

        if(this._level_two_timer){
            clearTimeout(this._level_two_timer);
            this._level_two_timer = null;
        }

        if(response["speech_is_none"]){
            that.playDefaults("speech_is_none");
            this.onIdelState(true);
            return;
        }else if(response["error"]){
            this.processSystemResponse({
                data: {},
                placeholder: response["error"]
            });
        }else{
            this.processSystemResponse(response);
        }

        
        // set_cc();
        //if(this.temp && this.temp["name"] == response["name"] && this.temp["placeholder"] == response["placeholder"] && !force){
        
        this.updateInput("");
        this.triggerEvents("conversation", response["data"]);
        
    }
    onError(error){
        this.load__ = false;
        if(this._level_two_timer){
            clearTimeout(this._level_two_timer);
            this._level_two_timer = null;
        }
        this.state = "idel";
        this.audio_playing = false;
        this.onIdelState(true);
        // this.setPlaceholder(error.error || "Failed to process your request. Please try again.");
        this.setPlaceholder("I am sorry I was not able to fetch an answer to your query this time. Please try again in some time." )
        window.__dv_cbs_get("StopProcessingInputState").fire();
        if(this.conversation.current_response && this.conversation.current_response["name"] == error["name"]){
            if(this.user_reponse && this.user_reponse["customer_response"] && this.user_reponse["customer_response"].indexOf("{") == -1){
                djq("#"+that.textbox_id).val(this.user_reponse["customer_response"]);
            }
        }
    }
    nudgeResponse(response){
        var that =this;
        this.load__ = false;
        this.state = "idel";
        this.toggle_response_panel("....", true);
        console.debug(response)
        // this.conversation.hold(false);
        if(!response || Object.keys(response).length == 0) return;

        if(this.actionCallbacks){
            this.actionCallbacks(response, "nudge_response");
        }
        // var text = new Message(response["placeholder"]);
        // djq("#message").fit_text(response["placeholder"]);
        // this.toggle_response_panel("....",false);
        if(response["placeholder"]){
            this.setPlaceholder(response["placeholder"], "left", null);
        }
        this.onTalkingState();
        
        var ow = response["data"]["overwrite_whiteboard"] == undefined ? response["overwrite_whiteboard"] : response["data"]["overwrite_whiteboard"];
        var mw = this.conversation.current_response["data"]["maintain_whiteboard"] == undefined ? this.conversation.current_response["maintain_whiteboard"] : this.conversation.current_response["data"]["maintain_whiteboard"];
        
        if(response["whiteboard"] && ( ow || mw )){
            this.setWhiteboard(response);
        }
    }
    //playing audio and action sequence
    playAnimations(){}
    playFromTheQueue(){}
    addAudioToQueue(){}
    addNudges(lst){}
    
    //run sequence of actions like animation, audio, update whiteboard, update bg ...etc
    processSystemResponse(response, after_played){
        if (!response.response_channels["word_timings"]) {
            if (response.data && response.data.redirect_placeholder) {
              this.setPlaceholder(response.data["redirect_placeholder"], "left", null);
              delete response.data.redirect_placeholder
            } else {
              this.setPlaceholder(response["placeholder"], "left", null);
            }
          }
        this.setWhiteboard(response);
        this.updateQuickAccessButtons(response);

        if(response["data"]){
            this.addNudges(response["data"]["_follow_ups"]);
        }
        if(this.actionCallbacks){
            this.actionCallbacks(response, "conversation_response");
        }
        this.status = "idel";
    }
    

    //ui states
    onTalkingState(){}
    onIdelState(repeat){}
    onProcessingRequest(text){}

    updateInput(text){
        if(this.textbox_id){
            djq("#"+this.textbox_id).val(text || "");
        }
    }
    mute(){
        if(this.audio){
            this.audio.muted = true;
        }
    }
    unmute(){
        if(this.audio){
            this.audio.muted = false;
        }
    }
    pause(){
        this.audio_playing = false;
        this.mute();
        this.conversation.updateSession();
    }
    resume(){
        this.unmute();
        this.conversation.startSession();
        this.onResponse(this.conversation.current_response);
    }

    createSession(){
        console.log("Started new session for user :: ", this.customer_id);
        this.conversation.startSession();
    }
    clearSession(async){
        console.log("End of session for user :: ", this.customer_id);
        // this.conversation.endSession();
        this.conversation.updateSession(async, true);
        // this.triggerEvents("session_expired_callback", true);
    }


    startWakeupStream(){
        this.conversation.startListenForWakeup();
    }
    stopWakeupStream(force){
        this.conversation.stopListenForWakeup(force);
        // this.triggerRandomEvent(true);
    }

    startImageStream(){
        this.conversation.startImageStream();
    }
    stopImageStream(){
        this.conversation.stopImageStream();
    }

}


// class ChatBot extends ConversationHandler{}