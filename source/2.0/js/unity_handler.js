var product_obj,
  unityInstance,
  build_type = "unity";
function onPictureTaken(ImageBase64) {
  console.debug(ImageBase64);

  if (!window.__dv_cbs["DownloadSelfy"]) {
    var a = document.createElement("a");
    a.href = ImageBase64;
    a.download = new Date().toISOString() + ".png";
    a.click();
  } else {
    window.__dv_cbs_get("DownloadSelfy").fire(ImageBase64);
  }
}
function keyaLoadedCallback() {
  console.debug("Keya loaded on page");
  keya_loaded = true;
  window.__dv_cbs_get("KeyaLoadedCallback").fire();
}
function assistantLoadedCallback() {
  console.debug("Assistant loaded on page");
  keya_loaded = true;
  window.__dv_cbs_get("AssistantLoadedCallback").fire();
}
function receiveMessageFromUnity(id) {
  var aud = document.getElementById("audio");
  console.debug("--------", id);
  if (aud.getAttribute("data-audio-id") != id) {
    unityInstance.stopTalking();
    return;
  }
  if (aud) {
    console.debug("Audio duration :: ", aud.duration, "Audio currentTime :: ", aud.currentTime);
    var muted = aud.getAttribute("force-mute") || "false";
    console.debug("Audio muted ::", muted);
    if (muted == "false") {
      aud.muted = false;
    } else {
      aud.muted = true;
    }
    // aud.play();
    var promise;
    // if(!djq.browser.is_iphone)
    promise = aud.play();

    if (aud.getAttribute("defaults") == "false") {
      djq("#message").placeholder_effect(aud.duration);
    }
    // if (!aud.onended) {
    //   aud.addEventListener("ended", function ended() {
    //     // aud.removeEventListener('ended', ended);
    //     var audio = document.getElementById("audio");
    //     if (audio.getAttribute("data-audio-id") != id) {
    //       unityInstance.stopTalking();
    //       djq("#message").placeholder_effect();
    //     }
    //     // try{
    //     //     unityInstance.SendMessage("head", "stopTalking")
    //     // }catch(e){
    //     //     console.log("Were not able to do stop talking", e);
    //     // }
    //   });
    // }
    if (promise !== undefined) {
      promise
        .then(() => {
          console.debug("Got the audio load promise here");
        })
        .catch(function (err) {
          console.error(err);
          // try{
          //     unityInstance.SendMessage("head", "stopTalking")
          // }catch(e){
          //     console.log("Were not able to do stop talking", e);
          // }
          // aud.onended();
          aud.dispatchEvent("ended")
          window.__dv_cbs_get("AudioPlayFailed").fire();
        });
    }
  } else {
    console.warn("Aud is empty");
  }
}

var default_bg,
  defaul_avatar_id = "dave";
var screen_loading = true;
function sceneLoadedCallback(params) {
  console.debug("scene loaded callback");
  screen_loading = false;
  if (params) {
    window.__dv_cbs_get("SceneLoaded").fire(params);
    return;
  }
  if (default_bg) {
    unityInstance.setTexture(default_bg);
  }
  if (build_type == "unity") {
    unityInstance.showPersona(defaul_avatar_id);
  }

}

function avatarLoadedCallback() {
  unityInstance.loadedchar = true;
  window.__dv_cbs_get("AvatarLoaded").fire();
}

function daveSceneLoaded() {
  //sceneLoadedCallback();
}

function enteredAreaCallback(type, value) {
  if (type == "cs") {
    window.__dv_cbs_get("enteredArea").fire(value);
  } else if (type == "point") {
    window.__dv_cbs_get("pointReached").fire(value);
  } else {
    window.__dv_cbs_get("enteredArea-" + type).fire(value);
  }
}
function requestData() {
  window.__dv_cbs_get("unityDataRequest").fire();
}


function onCallback(jsString) {
  var jsn = JSON.parse(jsString);
  console.debug("onCallback from unity", jsn);

  if (jsn["action"] == "SceneLoaded") {
    sceneLoadedCallback(jsn);
  } else if (jsn["action"] == "AnimationReady") {
    receiveMessageFromUnity(jsn["audio_id"]);
  } else if (jsn["action"] == "AssistantLoaded") {
    window.__dv_cbs_get("AssistantLoadedCallback").fire();
  } else {
    window.__dv_cbs_get("UnityAction").fire(jsn);
  }
}

class UnityManager {
  constructor(unityInstance, settings) {
    this.unityInstance = unityInstance;
    this.setAvatar = false;
    this.head_name = settings["head_name"] || "head";

    this.dynamic_unity = settings["dynamic_unity"] || false;

    this.keyboard_enable = false;
    this.joystick_enable = false;
    this.muted = false;
  }
  updateSettings() {
    var sets = {
      "action": "ChangeSettings",
      "mute": this.muted,
      "keyboardActive": this.keyboard_enable,
      "joystick": this.joystick_enable
    }
    console.debug("Updating Settings ::: ", sets)
    this.callGameManagerFunction("UnityFunction", JSON.stringify(sets));
  }
  keyboardControlEnable() {
    console.debug("keyboard control enabled");
    if (this.dynamic_unity) {
      this.keyboard_enable = true;
      this.updateSettings();
      return
    }
    try {
      this.unityInstance.SendMessage("GameManagerOBJ", "keyboardControlEnable", "");
    } catch (e) {
      console.warn("Unable to enable keyboard controls", e);
    }
  }
  keyboardControlDisable() {
    console.debug("keyboard control disabled");
    if (this.dynamic_unity) {
      this.keyboard_enable = false;
      this.updateSettings();
      return
    }
    try {
      this.unityInstance.SendMessage("GameManagerOBJ", "keyboardControlDisable", "");
    } catch (e) {
      console.warn("Unable to disable keyboard controls", e);
    }
  }
  joystickControlEnable() {
    console.debug("joystick control enable");
    if (this.dynamic_unity) {
      this.joystick_enable = true;
      this.updateSettings();
      return
    }
    try {
      this.unityInstance.SendMessage("GameManagerOBJ", "joystickControlEnable", "");
    } catch (e) {
      console.warn("Unable to enable joystick controls", e);
    }
  }
  joystickControlDisable() {
    console.debug("joystick control disabled");
    if (this.dynamic_unity) {
      this.joystick_enable = false;
      this.updateSettings();
      return
    }
    try {
      this.unityInstance.SendMessage("GameManagerOBJ", "joystickControlDisable", "");
    } catch (e) {
      console.warn("Unable to disable joystick controls", e);
    }
  }
  stopTalking() {
    console.debug("Trying to Stop talking");
    if (this.dynamic_unity) {
      this.callGameManagerFunction("UnityFunction",
        JSON.stringify({
          action: "StopTalking"
        }));

      return;
    }
    try {
      this.unityInstance.SendMessage(this.head_name, "stopTalking");
    } catch (e) {
      console.warn("Unable stop talking", e);
    }
  }

  startTalking(id, frames, shapes) {
    console.debug("Trying to Start talking ::", id);
    if (this.dynamic_unity) {
      this.callGameManagerFunction("UnityFunction",
        JSON.stringify({
          action: "StartTalking",
          avatar_name: id,
          frames: frames,
          shapes: shapes,
          audio_id: id
        }));

      return;
    }
    try {
      this.unityInstance.SendMessage(this.head_name, "StartTalking",
        JSON.stringify({
          frames: frames,
          shapes: shapes,
          audio_id: id
        }));
    } catch (e) {
      console.warn("Unable to make avatar talk");
    }
  }
  showPersona(id) {
    console.debug("Trying to show persona ::", id);
    if (this.dynamic_unity) {
      this.callGameManagerFunction("UnityFunction", JSON.stringify({ "action": "ShowPersona", "avatar_name": id }));

      return;
    }
    try {
      this.unityInstance.SendMessage("GameManagerOBJ", "showPersona", id);
      this.setAvatar = true;
    } catch (e) {
      console.warn("Were not able to update persona", e);
    }
  }
  setTexture(bg) {
    console.debug("Setting texture :: ", bg);
    try {
      this.unityInstance.SendMessage("GameManagerOBJ", "setTexture", bg);
    } catch (e) {
      console.warn("Were not able to update texture", e);
    }
  }
  callGameManagerFunction(key, val) {
    console.debug("Calling game manager object :: ", key, val);
    try {
      this.unityInstance.SendMessage("GameManagerOBJ", key, val);
    } catch (e) {
      console.warn(`Failed to call GameManagerOBJ function '${key}'`, e);
    }
  }
  muteBGM() {
    if (this.dynamic_unity) {
      this.muted = true;
      this.updateSettings();
      return
    }
    this.callGameManagerFunction("mute", "");
  }
  unmuteBGM() {
    if (this.dynamic_unity) {
      this.muted = false;
      this.updateSettings();
      return
    }
    this.callGameManagerFunction("unmute", "");
  }
  callAssistantFunction(key, val) {
    console.debug("Calling assistant function :: ", key, val);
    this.unityInstance.SendMessage("Assistantcode", key, val);
  }
}

class BabylonManager {
  constructor(engine, scene, camera, settings) {
    console.debug("Settings in Babylon Manager", settings);
    this.engine = engine;
    this.scene = scene;
    this.camera = camera;
    this.talk = false;
    this.lastTime = 0;
    this.shapesIndex = 0;
    this.framesIndex = 0;
    this.snd = null;
    this.shapes = null;
    this.frames = null;
    this.headers = [];
    this.volume = settings["volume"] || 1.0;
    this.queue = [];
    this.settings = settings;
    this.setAvatar = true;
    this.head_name = settings["head_name"] || "head";


    // this.loadScene(model_path);
    //this.sceneLoadedCallback();
    this.engine.setHardwareScalingLevel(1 / window.devicePixelRatio);
    this.isVisible = false;
    this.render();
    this.INACTIVITY_THRESHOLD = 30000;
    this.inactivityTimer = null;
    this.isRendering = false;
    this.mouseEvents = ['mousemove', 'keydown', 'click', 'scroll'];

    // Bind methods
    this.handleInactivity = this.handleInactivity.bind(this);
    this.resetInactivityTimer = this.resetInactivityTimer.bind(this);
    this.setupInactivityTimer = this.setupInactivityTimer.bind(this);

    this.setupInactivityTimer();
    this.resetInactivityTimer();
    this.idleBlinkArr = [
      {
        "shapes": {
          "blink": 1
        },
        "period": 10,
      },
    ]
    this.idleHeadArr = [
      {
        "shapes": {
          "look_right": 0.4,
          "look_down": 0.2,
        },
        "period": 50
      },
      {
        "shapes": {
          "look_right": 0.4,
          "look_up": 0.2,
        },
        "period": 50
      },
      {
        "shapes": {
          "look_left": 0.3,
          "look_up": 0.1,
        },
        "period": 50
      },
      {
        "shapes": {
          "look_left": 0.3,
          "look_down": 0.1,
        },
        "period": 50
      },
      {
        "shapes": {
          "look_right": 0.3,
          "look_down": 0.1,
        },
        "period": 50
      },
      {
        "shapes": {
          "head_right": 0.3,
          "head_up": 0.3,
          "look_right": 0.3,
          "look_up": 0.3,
        },
        "period": 80
      },
      {
        "shapes": {
          "head_right": 0.4,
          "head_down": 0.3,
          "look_left": 0.3,
          "look_down": 0.3,
        },
        "period": 80
      },
      {
        "shapes": {
          "head_left": 0.3,
          "head_up": 0.3,
          "look_left": 0.3,
          "look_up": 0.3,
        },
        "period": 80
      },
      {
        "shapes": {
          "head_left": 0.4,
          "head_down": 0.3,
          "look_left": 0.3,
          "look_down": 0.3,
        },
        "period": 80
      },
      {
        "shapes": {
          "head_right": 0.3,
          "head_down": 0.3,
        },
        "period": 100
      },
      {
        "shapes": {
          "head_right": 0.3,
          "head_up": 0.3,
        },
        "period": 100
      },
      {
        "shapes": {
          "head_left": 0.3,
          "head_up": 0.3,
        },
        "period": 100
      },
      {
        "shapes": {
          "head_left": 0.3,
          "head_down": 0.3,
        },
        "period": 100
      }
    ]
    console.debug("IsVisible status -- ", this.isVisible);
    if (this.isVisible) {
      this.playIdleAnimation();
    }

  }
  handleInactivity() {
    console.debug('No interaction detected for 30 sec.');
    if (this.engine) {
      this.engine.stopRenderLoop();
      this.pauseAnimsandMorphs();
      this.isRendering = false;
    }
  }

  resetInactivityTimer() {
    console.debug('user interaction detected.');
    clearTimeout(this.inactivityTimer);
    this.inactivityTimer = setTimeout(this.handleInactivity, this.INACTIVITY_THRESHOLD);
    if (!this.isRendering) {
      this.render();
      window.__dv_cbs_get("userInteractionDetected").fire();
      this.isRendering = true;
    }
  }

  setupInactivityTimer() {
    this.mouseEvents.forEach(event => window.addEventListener(event, this.resetInactivityTimer));
  }
  renderLoopCallback(fps) {
    if (window.__dv_cbs_get("fpsCallback")) {
      window.__dv_cbs_get("fpsCallback").fire(fps);
    }
  }
  render() {
    var that = this;
    that.engine.runRenderLoop(function () {
      that.renderLoopCallback(engine.getFps())
      if (that.talk) {
        let shape = that.shapes[that.shapesIndex];
        let x = (Date.now() - that.lastTime) / 1000;
        if (!shape) {
          that.talk = false;
          that.shapesIndex = 0;
          that.framesIndex = 0;
          that.scene.render();
          return;
        }
        while (x > shape["timestamp"]) {
          console.debug("Went ahead");
          that.shapesIndex += 1;
          shape = that.shapes[that.shapesIndex];
          if (!shape) {
            that.talk = false;
            that.shapesIndex = 0;
            that.framesIndex = 0;
            that.scene.render();
            return;
          }
        }
        if (x >= shape["timestamp"] - 1.0 / 25.0 && x <= shape["timestamp"]) {
          that.setMorphTargets(shape);
          that.shapesIndex += 1;
        } else if (x < shape["timestamp"] - 1.0 / 25.0) {
          console.debug("Lagging behind");
        }
        var frame = that.frames[that.framesIndex];
        if (frame != null && x >= frame["timestamp"]) {
          that.playAnimation(frame);
          that.framesIndex += 1;
        }
        if (that.shapesIndex >= that.shapes.length) {
          that.talk = false;
          that.shapesIndex = 0;
          that.framesIndex = 0;
        }
      } else {
        that.idleShapes('idleBlink');
        that.idleShapes('idleHead');
      }
      that.scene.render();
    });
  }



  play(response) {
    var that = this;
    that.queue.push(response);
    // that.snd = new Audio(response["voice"]);
    that.shapes = that.processData(response["shapes"]);
    that.frames = that.processDataFrames(response["frames"]);
    that.shapesIndex = 0;
    that.framesIndex = 0;
    window.__dv_cbs_get("OnBabylonPlay").fire(that);
    return that.snd;
  }

  processData(allText) {
    var that = this;
    var allTextLines = allText.split(/\r\n|\n/);
    that.headers = allTextLines[0].split(",").filter(function (v) { return v });
    var lines = [];
    for (var i = 1; i < allTextLines.length; i++) {
      var data = allTextLines[i].split(",");
      if (data.length == that.headers.length) {
        var tarr = {};
        for (var j = 0; j < that.headers.length; j++) {
          tarr[that.headers[j]] = parseFloat(data[j]);
        }
        lines.push(tarr);
      }
    }
    return lines;
  }

  processDataFrames(allText) {
    var that = this;
    var allTextLines = allText.split(/\r\n|\n/);
    var headers = allTextLines[0].split(",").filter(function (v) { return v });
    var lines = [];
    for (var i = 1; i < allTextLines.length; i++) {
      var data = allTextLines[i].split(",");
      if (data.length == headers.length) {
        var tarr = {};
        for (var j = 0; j < headers.length; j++) {
          console.debug("got ani data = " + headers[j] + " = " + data[j]);
          if (headers[j] == "timestamp") {
            tarr[headers[j]] = parseFloat(data[j]);
          } else {
            tarr[headers[j]] = data[j];
          }
        }
        lines.push(tarr);
      }
    }
    return lines;
  }

  setMorphTargets(shape) {
    var that = this;
    for (var key in shape) {
      that.setMorphTarget(key, shape[key]);
    }
    window.__dv_cbs_get("OnBabylonSetMorphTargets").fire(that, shape);
  }

  setMorphTarget(key, value) {
    var that = this;
    if (key != "timestamp" && value != null && value != undefined) {
      let lst = []
      for (let l of that.scene.morphTargetManagers) {
        let s = l._targets.filter((_) => { return _.name == key });
        if (s.length) {
          s = s[0];
          if (s) {
            lst.push(s);
          }
        }
      }
      for (let s of lst) {
        s.influence = value;
        window.__dv_cbs_get("OnBabylonSetMorphTargetManager").fire(that, key, value, s);
      }
      window.__dv_cbs_get("OnBabylonSetMorphTarget").fire(that, key, value);
    }
  }

  pauseAnimsandMorphs() {
    var that = this;
    that.talk = false;
    that.stopMorphs();
    that.playIdleAnimation();
    window.__dv_cbs_get("OnBabylonPauseAnimsandMorphs").fire(that);
  }

  stopAnimsandMorphs() {
    var that = this;
    that.talk = false;
    that.stopMorphs();
    that.stopIdleAnimation();
    window.__dv_cbs_get("StopAnimsandMorphs").fire(that);
  }

  playAnimation(animationName, is_special) {
    var that = this;
    if (animationName["animation"] != null) {
      console.debug("trying to play = " + animationName["animation"]);
      let ani = that.scene.getAnimationGroupByName(animationName["animation"]);
      console.debug("playAnimation-ani--------->", ani)
      if (ani) {
        try {
          if (!that.special_playing) {
            ani.onAnimationEndObservable.addOnce(function () {
              that.special_playing = false;
            });
            ani.onAnimationGroupEndObservable.addOnce(function () {
              that.special_playing = false;
            });
            ani.play(false);
            ani.loopAnimation = false;
            ani.isAdditive = true;
          } else {
            console.warn('Special animation ' + that.special_playing + ' is playing, so skipping')
          }
          if (is_special) {
            that.special_playing = ani.name;
          }
          window.__dv_cbs_get("OnBabylonPlayAnimation").fire(that, animationName, is_special);
        } catch (error) {
          console.error(error);
        }
      }
    }
  }

  stopMorphs() {
    var that = this;
    // if (that.headers != []) {
    for (var i = 1; i < that.headers.length; i++) {
      var s = that.setMorphTarget(that.headers[i], 0.0000001);
    }
    window.__dv_cbs_get("OnBabylonStopMorphs").fire(that);
    // }
  }

  playIdleAnimation() {
    var that = this;
    that.isIdleAnimationActive = true;
    let ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight_shift/g) });
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/wieght/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle_pose/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle_animation/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idel/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/thinking_1/g) });
    }
    ani = ani[0];
    if (ani) {
      console.debug("trying to play idle animation", ani);
      try {
        ani.play(true);
        ani.loopAnimation = true;
      } catch (error) {
        console.error(error);
      }
    }
    that.setMorphTarget('smile', 0.4);
    that.generateIdleAnimations(that.idleBlinkArr, 'idleBlink');
    that.generateIdleAnimations(that.idleHeadArr, 'idleHead');
    window.__dv_cbs_get("OnBabylonPlayIdleAnimation").fire(that, ani);
  }

  stopIdleAnimation() {
    var that = this;
    that.isIdleAnimationActive = false;
    let ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight_shift/g) });
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/weight/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/wieght/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle_pose/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle_animation/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idle/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/idel/g) });
    }
    if (ani.length <= 0) {
      ani = that.scene.animationGroups.filter(function (a) { return a.name.match(/thinking_1/g) });
    }
    ani = ani[0];
    if (ani) {
      console.debug("trying to stop idle animation", ani);
      try {
        ani.stop();
        ani.loopAnimation = false;
      } catch (error) {
        console.error(error);
      }
    }
    window.__dv_cbs_get("OnBabylonStopIdleAnimation").fire(that, ani);
  }
  random_choice(array) {
    let i = Math.floor(Math.random() * array.length);
    return [i, array[i]]
  }

  createIdleAnimationsArray(ani, period, factor, offset, prev_shapes) {
    let il = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
    let p = period || 30;
    let fac = factor || 1;
    let off = offset || 0;
    let ret = prev_shapes || {
      "shapes": [],
      "time": 0,
      "index": 0,
    }
    let f = function (i, tp) {
      let val = Math.sin((Math.PI * i * fac) / 2) + off;
      let prev = ret.shapes.map((_) => { return _[0] }).indexOf(tp);
      if (prev >= 0) {
        ret.shapes[prev][1][ani] = val;
      } else {
        ret.shapes.push([tp, {}])
        ret.shapes[ret.shapes.length - 1][1][ani] = val;
      }
    }
    for (let t in il) {
      let i = il[t];
      let tp = t * p;
      f(i, tp);
    }
    il.reverse()
    for (let t in il) {
      let i = il[t];
      let tp = (2 * p * il.length) + (t * p);
      f(i, tp);
    }
    return ret;
  }

  generateIdleAnimations(array, attr, min_period, max_period, force) {
    let that = this;
    max_period = max_period || 12000;
    min_period = min_period || 2000;
    let period = Math.max(max_period - min_period, 0);
    let f = function (list) {
      if (!that.isIdleAnimationActive) {
        return; // Exit if the flag is false
      }

      let force = false;
      if (!list) {
        force = true;
      }
      if (!list || list.length <= 0) {
        list = array.slice();
      }
      const [i, ani] = that.random_choice(list)
      that[attr] = {
        shapes: [],
        index: 0,
        time: 0,
      }
      console.debug("Generated idle animation: ", Object.keys(ani.shapes));
      for (let s in ani.shapes) {
        let o = 0;
        if (ani.offsets && ani.offsets[s]) {
          o = ani.offsets[s]
        }
        that[attr] = that.createIdleAnimationsArray(s, ani.period, ani.shapes[s] * (Math.random() * 0.5 + 0.5), o, that[attr])
      }
      if (force) {
        that[attr]['time'] = 100 + Date.now();
      } else {
        that[attr]['time'] = (Math.random() * period) + Date.now();
      }
      console.debug("Trigger idle animation after: ", that[attr]['time']);
      that[attr]['callback'] = function () { f(list) }

    }
    f();
  }
  idleShapes(attr) {
    var that = this;
    if (!that[attr] || !that[attr].time) {
      return
    }
    let x = (Date.now() - that[attr].time);
    if (x < 0) {
      return;
    }
    let i = that[attr].index;
    for (let t = i; t < that[attr].shapes.length; t++) {
      if (that[attr].shapes[t][0] >= x) {
        that[attr].index = t;
        that.setMorphTargets(that[attr]["shapes"][t][1]);
        return;
      }
    }
    that.stopMorphs();
    if (that[attr]['callback'] && typeof (that[attr]['callback'] == 'function')) {
      that[attr]['callback'](that, attr);
    } else {
      that[attr] = null;
    }
    window.__dv_cbs_get("OnBabylonCompleteIdleShapes").fire(that, attr);
  }

  muteAvatar() {
    var that = this;
    if (that.snd) {
      that.snd.volume = 0.0;
    }
    that.volume = 0.0;
  }

  unmuteAvatar() {
    var that = this;
    if (that.snd) {
      that.snd.volume = 1.0;
    }
    that.volume = 1.0;
  }

  muteBGM() {
    console.log("MUTE AVATAR");
    var that = this;
    that.muteAvatar();
    //this.callGameManagerFunction("mute", "");
  }

  unmuteBGM() {
    console.log("UNMUTE AVATAR");
    var that = this;
    that.unmuteAvatar();
    //this.callGameManagerFunction("unmute", "");
  }

  stopTalking() {
    var that = this;
    console.debug("Stop talking called . IsVisible status -- ", that.isVisible);
    that.pauseAnimsandMorphs();
    that.queue = [];
    console.debug("queue--->", that.queue)
    window.__dv_cbs_get("OnBabylonStopTalking").fire(that);
    /*try {
      this.unityInstance.SendMessage(this.head_name, "stopTalking");
    } catch (e) {
      console.warn("Unable stop talking", e);
    }*/
  }

  startTalking(id, frames, shapes) {
    try {
      var that = this;
      // that.snd = new Audio(response["voice"]);
      that.shapes = that.processData(shapes);
      that.frames = that.processDataFrames(frames);
      that.shapesIndex = 0;
      that.framesIndex = 0;
      that.stopIdleAnimation()
      that.talk = true;
      that.lastTime = Date.now();
      receiveMessageFromUnity(id);
      that["idleBlink"] = null;
      that["idleHead"] = null;
      window.__dv_cbs_get("OnBabylonStartTalking").fire(that, id, frames, shapes);
      return that.snd;
    } catch (e) {
      console.warn("Unable to make avatar talk");
    }
  }

  showPersona(id) {
  }

  setTexture(bg) {
  }

  keyboardControlDisable() {
    console.debug("keyboard control disabled");

  }
}

class SetupEnviron {
  static load(_b, settings) {
    if (build_type == "babylon") {
      var scene = null;
      var camera = null;
      var assetsManager = null;
      var engine = null;

      function init(t, myResolve, myReject) {
        var canvas = document.getElementById("unityContainer");
        var createEngine = function () {
          return new BABYLON.Engine(
            canvas,
            true,
            {
              timeStep: 1.0 / 10.0,
              deterministicLockstep: true,
              lockstepMaxSteps: 30,
              limitDeviceRatio: 3
            },
            true
          );
        }
        var createCamara = function () {
          camera = new BABYLON.FreeCamera(
            "camera_dave",
            new BABYLON.Vector3(0, 1.5, 2),
            scene
          );
          camera.setTarget(new BABYLON.Vector3(0, 1.5, 0));
          camera.viewport = new BABYLON.Viewport(0, -1, 1, 2.75);
          //camera.attachControl(canvas, false);
          return camera;
        }
        var createScene = function () {
          scene = new BABYLON.Scene(engine);
          scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
          assetsManager = new BABYLON.AssetsManager(scene);
          return scene;
        };

        var onetime = 1;
        if (settings["engine_obj"]) {
          engine = settings["engine_obj"]()
        } else {
          engine = createEngine();
        }
        if (settings["scene_obj"]) {
          scene = settings["scene_obj"]()
        } else {
          scene = createScene();
        }
        if (settings["camera_obj"]) {
          camera = settings["camera_obj"]()
        } else {
          camera = createCamara();
        }


        _b = _b || "https://s3.ap-south-1.amazonaws.com/models.iamdave.ai/glb-models/dave_waist.glb";
        BABYLON.SceneLoader.ImportMesh("", "", _b, scene, function (mesh) {
          scene.createDefaultCameraOrLight(false, false, false);
          scene.animationGroups.forEach(function (animationGroup) {
            animationGroup.stop();
          });
          unityInstance = new BabylonManager(engine, scene, camera, settings);
          myResolve(unityInstance);
          window.__dv_cbs_get("OnBabylonLoadedAvatar").fire(unityInstance, scene, engine);
          // if (scene.isReady()) {
          //   unityInstance.render();
          // } else {
          //   scene.executeWhenReady(function () {
          //     unityInstance.render();
          //   });
          // }
        });
      }

      var prom = new Promise(function (myResolve, myReject) {
        init(3, myResolve, myReject);
      });
      prom.progress = handler => {
        notify = handler;
        return prom;
      };
      return prom;

      //this.loadScene(model_path, sceneLoadedCallback);
      // var that = this;
    } else {
      var progress = 0;
      var notify;
      var buildUrl = _b;

      if (settings && settings["default_bg"]) {
        default_bg = settings["default_bg"];
      } else if (
        settings &&
        settings["customizables"] &&
        settings["customizables"]["bg"]
      ) {
        default_bg = settings["customizables"]["bg"];
      } else if (settings && settings["space_id"]) {
        default_bg = settings["space_id"];
      }

      if (settings && (settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"])) {
        defaul_avatar_id = settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"];
      }

      function init(t, myResolve, myReject) {
        function UnityError(message, err) {
          console.error(message, err);
          //that.errorC("error", message);
          myReject(message);
        }
        function UnityLoaded(ut) {
          unityInstance = new UnityManager(ut, settings);
          myResolve(unityInstance);
        }
        function UnityProgress(prog) {
          if (prog == 1) {
            console.log("Model loaded");
          }
          progress = prog;
          console.debug("Loading..", progress);
          if (progress % 0.1) {
            console.log("Loading..", progress);
          }
          if (notify) {
            notify(progress);
          }
        }
        if (t == 0) {
          return;
        }
        try {
          var config = {
            dataUrl: buildUrl + "/ProjectBuild.data",
            frameworkUrl: buildUrl + "/ProjectBuild.framework.js",
            codeUrl: buildUrl + "/ProjectBuild.wasm",
            streamingAssetsUrl: "StreamingAssets",
            companyName: "SocioGraph Solution",
            productName: "Conversation Bot",
            productVersion: "0.1"
          };

          createUnityInstance(
            document.querySelector("#unityContainer"),
            config,
            UnityProgress
          )
            .then(UnityLoaded)
            .catch(function (a, b) {
              UnityError(a, b);
              init(t - 1, myResolve, myReject);
            });

          //that.unityInstance = UnityLoader.instantiate("unityContainer",  that.settings["project_build"] || e["project_build"] || "https://d3arh16v2im64l.cloudfront.net/static/conversation/ProjectBuild.json", {onProgress: UnityProgress, onerror: UnityError});
          //unityInstance = that.unityInstance;
          //that.con.unityInstance = that.unityInstance;
          //break
        } catch (e) {
          console.error("Cannot load unity due to error");
          console.error(e);
          if (t == 0) {
            alert("Cannot load 3d environment");
          }
          init(t - 1, myResolve, myReject);
        }
      }
      var prom = new Promise(function (myResolve, myReject) {
        init(3, myResolve, myReject);
      });
      prom.progress = handler => {
        notify = handler;
        return prom;
      };
      return prom;
    }
  }
  constructor() { }
}
