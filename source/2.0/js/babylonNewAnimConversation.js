//
build_type = "babylon";
function set_cc() { }
class BabylonConversation extends ConversationHandler {
  constructor(settings) {
    console.log("Settings", settings);
    settings["head_name"] = "SciFi_Head";
    super(settings);
    this.avatar_loaded = false;
    this.audios_list = [];
    this.pre_resp = "";
    this.head_name = "SciFi_Head";
    this.showenkeya = false;
    this.currentIndex = 0;
    this.transcript = null;
  }

  setPlaceholder(m, pos, title, time) {
    console.log(`Set Placeholder m--${m} --this.message_id-- ${this.message_id} --m.message-- ${m.message}`)

    if (typeof m == "string") {
      m = new Message(m, pos, title || m, time);
      this.messages.push(m);
    }
    if (this.message_id) {
      djq("#" + this.message_id).html(m.message);
    }
  }
  setUpConversationUI() {
    super.setUpConversationUI();
    super.set("message_id", "message");
    super.set("shortcuts_id", undefined);
    super.set("whiteboard_id", "whiteboard");
    super.set("quick_access_id", "quick_access");
    super.set("audio_id", "audio");
    super.set("textbox_id", "input-text");
    super.set("mic_id", "record");
    super.set("sendbutton_id", "input-send");
    super.set("searchable_id", "searchable");
    // super.set("sendbutton_id", "input-send");
    super.set("mute_button", "mute");

    djq("body").append(
      "<div id='dave_conversation__' class='dave_conversation full' style='display:none'></div>"
    );
    djq("#dave_conversation__").setupBabylonConversationUi({
      placeholder: this.default_placeholder || "Type here"
    });

    if (!this.settings["load_after_click"]) {
      this.loadAvatar();
    } else {
      console.log("Loading avatar after click on the chatbot..")
    }
    // this.loadAvatar();
    this.setUpEvents();
    // const scrollContainer = document.getElementById("quick_access");
    // scrollContainer.addEventListener("wheel", (evt) => {
    //     scrollContainer.scrollLeft += evt.deltaY;
    // });

    var that = this;
    this.audio = document.getElementById(this.settings["audio_id"] || "audio");
  }

  mute() {
    super.mute();
    if (this.audio) {
      this.audio.muted = true;
      this.audio.setAttribute("force-mute", true);
      djq("#" + this.mute_button)
        .find("i")
        .first()
        .attr("class", "fa fa-volume-off");
    }
  }

  unmute() {
    super.mute();
    if (this.audio) {
      this.audio.muted = false;
      this.audio.setAttribute("force-mute", false);
      djq("#" + this.mute_button)
        .find("i")
        .first()
        .attr("class", "fa fa-volume-up");
    }
  }

  stopTalking() {
    this.audio.pause();
    this.audio_playing = false;
    this.unityInstance.stopTalking();
  }

  pause() {
    super.pause();
    this.audio.src = undefined;
    this.unityInstance.stopTalking();
    this.audios_list = [];
    this.play_followups = [];
    //this.unityInstance.keyboardControlDisable();
  }

  resume() {
    var that = this;
    // super.resume();
    // if(that.audio){
    //     that.audio.play();
    // }
    that.unityInstance.isVisible = true;
    that.unityInstance.pauseAnimsandMorphs();
    if (that.conversation.session_id) {
      that.triggerEvents("resume", "resume");
      that.conversation.post_interaction({
        [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.RESUMED
      });
    } else {
      that.triggerEvents("opened", "opened");
      that.conversation.post_interaction({
        [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.OPENED
      });
    }
    that.createSession();
    that.audios_list = [];

    if (that.state == "idel") {
      setTimeout(function () {
        // set_cc();
        that.onResponse(that.conversation.current_response);
      }, 1000 * (that.conversation.session_id ? 1 : that.settings["init_reponse_delay"] || 1));
    }

    this.unmute();

    // this.unityInstance.showPersona('kotak_player');
  }

  /*updateQuickAccessButtons(res) {
    if (this.quick_access_id) {
      djq("#" + this.quick_access_id).empty();
      var that = this;
      var kws = this.conversation.getKeywords(6, res);
      for (var i of kws) {
        var butn = djq("<li  style='cursor:pointer'/>");
        // butn.addClass("butn butn-dark butn-sm");
        butn.html(i[0]);
        butn.attr("value", i[0]);
        // butn.attr("value", i);
        butn.attr("key", i[1]);
        // butn.attr("style", "margin-right: 5px");
        djq("#" + this.quick_access_id).append(butn);
        butn.click(function () {
          console.debug(
            djq(this).attr("value") || djq(this).attr("key"),
            djq(this).attr("key"),
            djq(this).attr("title")
          );
          that.postConversation(
            djq(this).attr("value"),
            djq(this).attr("key"),
            djq(this).attr("title"),
            "click"
          );
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK
          });
          that.triggerEvents("interaction", "click");
        });
      }
    }
  }*/

  audioRecord(stop) {
    console.log("AUDIO REOCRD", stop);
    var that = this;
    if (that.intervel) {
      clearInterval(that.intervel);
    }
    that.intervel = undefined;
    if (stop) {
      djq("#" + that.mic_id).removeClass("recording");
      djq("#" + that.mic_id).addClass("record");
      /*djq("#" + that.mic_id).html(
        '<i id="microphone" class="fa fa-microphone fa-2x"></i>'
      );*/

      djq(".input_box").first().show();
      djq("#" + this.quick_access_id).show();
      djq("#" + this.shortcuts_id).show();
      djq("#try-saying").show();

      djq("#" + that.mic_id).find("#record").show();
      djq("#" + that.mic_id).find("span").remove();
      djq("#" + that.mic_id).find("br").remove();
      window.__dv_cbs_get("audioRecordStopped").fire();

      return;
    }
    else {
      that.state = "recording";
      var sec = 0;
      djq("#" + that.mic_id).removeClass("record");
      var intv = djq("<span>00:00</span>");
      /*djq("#" + that.mic_id)
        .find(".fa-microphone")
        .hide();*/

      // djq("#" + that.mic_id)
      //   .find("#record")
      //   .hide();

      // djq("#" + that.mic_id).append(intv);
      djq("#" + that.mic_id).addClass("recording");
      //djq("#" + that.mic_id).append("<br />");
      // djq("#" + that.mic_id).append(
      //   '<span><i class="fa fa-stop"></i></span>' // fa-2x
      // );

      // that.intervel = setInterval(function () {
      //   sec++;
      //   var min = parseInt(sec / 60);
      //   var sc = sec % 60;
      //   intv.html(
      //     (min < 10 ? "0" + min : min) + ":" + (sec < 10 ? "0" + sc : sc)
      //   );
      // }, 1000);

      // djq(".input_box").first().hide();
      // djq("#quick_access").hide();
      djq("#shortcut").hide();
      djq("#try-saying").hide();
    }
    djq("#" + that.mic_id).show();
  }

  setUpEvents() {
    var that = this;
    if (this.textbox_id) {
      djq("#" + this.textbox_id).enter(function (val) {
        if (val.trim()) {
          djq(".searchable").hide();
          that.postConversation(val, null, val, "type");
          that.conversation.post_interaction({
            [that.settings["stage_attr"] ||
              "stage"]: InteractionStageEnum.TEXT_INPUT
          });
          that.triggerEvents("interaction", "text-input");
        }
      });
      djq("#" + this.textbox_id).keyup(function (e) {
        var val = e.currentTarget.value
        that.updateSearchables(val.trim());
      });
    }
    if (this.sendbutton_id) {
      djq("#" + this.sendbutton_id).click(function () {
        if (
          djq("#" + that.textbox_id)
            .val()
            .trim()
        ) {
          that.postConversation(
            djq("#" + that.textbox_id).val(),
            null,
            djq("#" + that.textbox_id).val(),
            "type"
          );
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: "text-input"
          });
          that.triggerEvents("interaction", "text-input");
        }
      });
    }
    if (this.mic_id) {
      djq("#" + this.mic_id).click(function () {
        if (that.state == "idel") {
          // that.audioRecord();
          djq(this).hide();
          that.conversation.startRecordResponse();
        } else if (that.state == "recording") {
          that.conversation.stopRecordResponse();
        }
      });
    }
    if (this.mute_button) {
      djq("#" + this.mute_button).click(function () {
        var cc_class = djq(this).attr("class");

        if (cc_class.indexOf(" mute") > -1) {
          that.unmute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] ||
              "stage"]: InteractionStageEnum.UNMUTED
          });
          that.triggerEvents("interaction", "unmuted");

          djq(this).removeClass("mute").addClass("unmute")//.css("background-image", "url(/source/2.0/css/icons/babylon/Unmute.png)");
        } else {
          that.mute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MUTED
          });
          that.triggerEvents("interaction", "muted");

          djq(this).removeClass("unmute").addClass("mute")//.css("background-image", "url(/source/2.0/css/icons/babylon/Mute.png)");
        }

        /*var cc_class = djq(this)
          .find("i")
          .first()
          .attr("class");
        if (cc_class.indexOf("fa-volume-up") == -1) {
          that.unmute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] ||
              "stage"]: InteractionStageEnum.UNMUTED
          });
          that.triggerEvents("interaction", "unmuted");
        } else {
          that.mute();
          that.conversation.post_interaction({
            [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MUTED
          });
          that.triggerEvents("interaction", "muted");
        }*/
      });
    }
    djq("#close_message").click(function () {
      console.log("CLOSE THE AVATAR");
      window.__dv_cbs_get("AvatarContainerClosed").fire();
      // djq("#dave_conversation__").hide(200);
      that.unityInstance.isVisible = false;
      that.pause();
      that.minimize=true;
      that.unityInstance.stopAnimsandMorphs();
      that.conversation.post_interaction({
        [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MINIMIZED
      });
      that.triggerEvents("interaction", "minimized");
    });


  }


  onLoadedCallback() {
    if (this.conversation_loaded && this.avatar_loaded) {
      this.addShortcutsButtons();
      this.conversationLoadedCallback(this.conversation.conversation_data);
      //this.unityInstance.keyboardControlDisable();
      this.loaded = true;
      var that = this;
      /*window.daveRegisterCallback("enteredArea", function(cs) {
        that.enteredAreaCallback(cs);
      });
      window.daveRegisterCallback("pointReached", function(cs) {
        that.pointReachedCallback(cs);
      });*/
      setTimeout(function () {
        if (!that.minimize) {
          that.createSession();
          //that.unityInstance.keyboardControlEnable();
          // that.unityInstance.showPersona('kotak_player');
          djq("#dave_conversation__").show(200, function () {
            that.unityInstance.isVisible = true;
            setTimeout(() => {
              that.onResponse(that.conversation.init_response);
            }, 1000 * (that.settings["init_reponse_delay"] || 1))

            // set_cc();
            that.triggerEvents("chat_started", "chat_started");

            // set_cc();
            that.conversation.post_interaction({
              [that.settings["stage_attr"] ||
                "stage"]: InteractionStageEnum.AUTO_OPENED
            });
          });
        }

        djq(window).on("beforeunload", function () {
          // that.clearSession(false);
          that.conversation.post_interaction(
            {
              [that.settings["stage_attr"] ||
                "stage"]: InteractionStageEnum.LEAVE_PAGE
            },
            true
          );
          that.clearSession(false);
        });
      }, 2000);
    }
    if (window.navigator.platform == "iPhone" && screen.width < 480) {
      djq('.actions').css({ "right": "0" });
      djq('#input-send').css({ "left": "90%" });
      // djq('#record').css({ "left": "10px", "position": "relative" });
    }
  }
  loadAvatar() {
    var that = this;
    this.settings["head_name"] = this.head_name;
    var t = SetupEnviron.load(this.settings["babylon_url"], this.settings);

    t.then(
      function (ut) {
        that.avatar_loaded = true;
        that.unityInstance = ut;
        that.onLoadedCallback();
        console.log("Babylon loaded callback")
        // set_cc();
      },
      function (err) {
        console.error(err);
      }
    );
    t.progress(function (pro) {
      console.debug(pro);
    });
  }
  playIdleAnimation() {
    var that = this;
    let ani = null;
    that.scene.animationGroups.every(function (a) {
      if (a.name.match(/weight_shift/g) || a.name.match(/idle/g)) {
        ani = a;
        return false;
      }
      return true;
    });
    if (ani) {
      console.debug("trying to play idle animation");
      try {
        ani.play(true);
        ani.loopAnimation = true;
      } catch (error) {
        console.log(error);
      }
    }
  }
  playAnimations(audo, force) {
    var ios = djq.browser.is_iphone;
    if (this.paused || this.audio_playing) {
      if (this.audio_playing && this.audios_list[0]["id"] == this.latest_audio_id) {
        this.audios_list.slice(0, 1)
        return;
      }
      setTimeout(() => {
        this.playAnimations(audo, force);
      }, 1000)
      return
    }
    console.log("PLAY ANIMATION", audo);
    var that = this;
    if (!this.audios_list.length && !audo) return;
    if (!audo) {
      audo = this.audios_list.splice(0, 1);
      audo = audo[0];
    }
    // if (this.unityInstance) {
    //   this.unityInstance.stopTalking();
    // }
    this.prev_audo = audo;
    this.temp_id = audo["id"];
    if (!ios) {

      var tt = this.audio.cloneNode(true);
      this.audio.remove();
      var muted = this.audio.getAttribute("force-mute");
      this.audio = tt;
      document.body.appendChild(this.audio);
    }
    this.audio.setAttribute("data-audio-id", that.temp_id);
    this.audio.setAttribute("force-mute", muted || false);
    // this.audio.muted = muted || false;
    this.audio.setAttribute("src", audo["audio"])
    this.audio.setAttribute("main-response", audo["main_response"])

    console.debug("play init id :: ", audo["id"]);

    this.audio_playing = true;
    function _lis() {
      if (
        audo["id"] != that.temp_id &&
        that.temp_id != that.audio.getAttribute("data-audio-id")
      ) {
        return;
      }


      function playShapes_frames(id, frames, shapes) {
        if (
          that.forced_stop &&
          that.forced_stop != that.audio.getAttribute("data-audio-id")
        ) {
          console.log("We have forced stopped, so don't play:" + id);
          return;
        }
        if (
          ["speech-detected", "processing", "idel"].indexOf(that.state) < 0 &&
          that.load__ &&
          !force
        ) {
          console.log(
            "State is not idle: " +
            that.state +
            " or we have loaded something else " +
            that.load__ +
            " already:" +
            id
          );
          return;
        }
        if (!audo["defaults"]) {
          that.onTalkingState();
        }
        if (!that.paused) {
          if (that.unityInstance.setAvatar) {
            //starting the idle to Talk transistion and stopping any other shape Key animations.
            if(that.unityInstance.currentShapeKeyAnimation){
              console.log("stop Idle Shape key animation")
              that.unityInstance.transistionTrack=true;
              that.unityInstance.stopSpecificAnimation(that.unityInstance.currentShapeKeyAnimation);
              that.unityInstance.specialShapeKeyFlag=true;
          }
          that.unityInstance.playTransition("idle_from","from")
            setTimeout(function () {
              that.unityInstance.startTalking(id, frames, shapes);
            }, 1);
          }
          // that.conversation.hold(true);
          that.playing_id = id;
          console.debug("++++++Playing ID:" + id);
        } else {
          console.debug("++++++Paused ID:" + id);
        }
        //djq("#message").html(list[i][1]);
      }
      that.audio.removeEventListener("canplaythrough", _lis)
      playShapes_frames(audo["id"], audo["frames_csv"], audo["shapes_csv"]);
    }
    this.audio.removeEventListener("canplaythrough", _lis);
    this.audio.addEventListener("canplaythrough", _lis);
    // this.audio.addEventListener("seeked", () => {
    //   if (ios) {
    //     that.audio.setAttribute("src", "data:audio/wav;base64,UklGRjIAAABXQVZFZm10IBIAAAABAAEAQB8AAEAfAAABAAgAAABmYWN0BAAAAAAAAABkYXRhAAAAAA==")
    //   }
    // })
    function ended() {
      that.audio_playing = false;
      that.currentIndex = 0;
      if (that.playing_id != that.audio.getAttribute("data-audio-id")) return;
      that.audio.removeEventListener('ended', ended);
      if (ios) {
        that.audio.removeEventListener('seeked', ended);
        that.audio.setAttribute("src", getEmptyAudio());
        // that.audio.setAttribute("src", getNoiseBase64());
      }
      that.unityInstance.stopTalking();
      if (!that.conversation.processing) {
        that.onIdelState();
      }
      if (that.state == "idel" && !that.load__) {
        that.pre_resp = "";
      }
      if (audo["onEnds"]) {
        audo["onEnds"](that, that.playing_id);
      } else {
        that.playFromTheQueue(that, that.playing_id);
      }
      if (that.audio.getAttribute("main-response")) {
        window.__dv_cbs_get("ResponsePlayEnded").fire(audo)
      }
    }

    if (ios) {
      this.audio.addEventListener("seeked", ended);
    } else {
      this.audio.addEventListener("ended", ended);
    }
    this.audio.load();
    console.log("transcript---", this.transcript)
    
    if (this.transcript && this.transcript.length > 0) {
      this.audio.addEventListener('playing', () => {
        document.getElementById(this.message_id).innerText = "";
        this.currentIndex = 0;
      })
      this.audio.addEventListener('timeupdate', () => {
        if (this.currentIndex < this.transcript.length) {
          let parts = this.transcript[this.currentIndex].split(",");
          let startTime = parseFloat(parts[0]);
          let word = parts[2].trim();
          console.log(startTime, word)
          if (this.audio.currentTime >= startTime) {
            document.getElementById(this.message_id).innerText += " " + word + " ";
            this.currentIndex++;
            document.getElementById(this.message_id).scrollTop = document.getElementById(this.message_id).scrollHeight;
          }
        }
      });
    }
  }

  playDefaults(state, message, that) {
    super.playDefaults(state, message);
    // eslint-disable-next-line no-redeclare
    var that = that || this;
    function __load(level) {
      djq.ajax({
        url:
          that.host_url +
          "/defaults/" +
          (that.settings["defaults_audio_id"] || that.voice_id) +
          "/" +
          level +
          "/" +
          that.enterprise_id,
        success: function (data) {
          that.addAudioToQueue(
            data["audio"],
            "level_" + state,
            "defaults",
            function (that, pid) {
              that.playFromTheQueue(that, pid);
            },
            data["frames"],
            data["shapes"],
            data["word_timings"] || null,
            data["subfolder"],
            false,
            false
          );
          window.__dv_cbs_get("on_play_defaults").fire(that, state, message, data);
        }
      });
    }
    __load(state);
  }

  playFromTheQueue(that, playing_id) {
    var that = that || this;
    if (that.audios_list.length) {
      that.playAnimations(null, false);
      return;
    }

    if (that._timer) {
      clearTimeout(that._timer);
    }
    if (playing_id != that.latest_audio_id) return;
    if (!that.play_followups.length) {
      return;
    }
    (function (tm) {
      var tf = function () {
        if (that.paused) {
          setTimeout(tf, tm);
        }
        if (that.state != "idel" || that.load__) return;
        if (that.audios_list.length) {
          that.playAnimations(null, false);
        } else if (that.play_followups.length) {
          that.playNudge(that.play_followups[0], function (data) {
            that.playNudge(data);
          });
          that.play_followups.splice(0, 1);
        } else {
        }
      };
      that._timer = setTimeout(tf, tm);
    })(that.conversation.current_response["wait"] || 10000);
  }
  addAudioToQueue(
    audio,
    id,
    conversation_id,
    onEnds,
    frames,
    shapes,
    word_timings,
    subfolder,
    force,
    main_response,
    response_data
  ) {
    var that = this;
    this.forced_stop = false;
    var id_ = id + "___" + makeid(4);
    console.debug("audio adding id :: ", id_);
    if (word_timings) {
      this.transcript = word_timings.trim().split("\n");
    } else {
      this.transcript = null
    }
    console.log("---------transcript -word_timings------", this.transcript)
    this.latest_audio_id = id_;
    if (force) {
      this.audios_list = [];
      //this.play_followups = [];
      /*if(this.audio){
                this.audio.muted = true;
                this.unityInstance.SendMessage("head", "stopTalking")
            }*/
      this.forced_stop = id_;
      if (this._timer) {
        clearTimeout(this._timer);
      }
    }
    if (this._timer && onEnds) {
      clearTimeout(this._timer);
    }

    var aurl;
    if (audio.indexOf("http") >= 0) {
      aurl = audio;
    } else {
      aurl = this.host_url + "/" + audio;
    }
    var conversation_id = conversation_id || this.conversation_id;
    var ob = {
      id: id_,
      audio: aurl,
      onEnds: onEnds,
      defaults: conversation_id == "defaults",
      main_response: main_response,
      response_data: response_data
    };
    this.audios_list.push(ob);

    var fil = "?voice_id=" + this.voice_id;
    if (subfolder) fil += "&subfolder=" + subfolder;

    function addFramesShapes(frames, shapes) {
      if (frames) {
        ob["frames_csv"] = frames;
      }
      if (shapes) {
        ob["shapes_csv"] = shapes;
      }
      if (ob["frames_csv"] && ob["shapes_csv"]) {
        if (!that.audio_playing) {
          that.playAnimations(null, force);
        }
      }
    }

    var furl, surl;
    if (!frames) {
      furl =
        this.host_url +
        "/frames/" +
        conversation_id +
        "/" +
        id +
        "/" +
        this.enterprise_id +
        fil;
      djq.ajax({
        url: furl,
        success: function (data) {
          addFramesShapes(data);
        }
      });
    } else {
      addFramesShapes(frames);
    }
    if (!shapes) {
      surl =
        this.host_url +
        "/shapes/" +
        conversation_id +
        "/" +
        id +
        "/" +
        this.enterprise_id +
        fil;
      djq.ajax({
        url: surl,
        success: function (data2) {
          addFramesShapes(null, data2);
        }
      });
    } else {
      addFramesShapes(null, shapes);
    }
  }

  nudgeResponse(response) {
    super.nudgeResponse(response);
    var that = this;

    if (response["response_channels"]) {
      if (
        response["response_channels"]["shapes"] &&
        response["response_channels"]["frames"]
      ) {
        // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
        if (!response["response_channels"]["shapes"].endsWith(".csv")) {
          this.addAudioToQueue(
            response["response_channels"]["voice"],
            "nudge_" + response["name"] + "_response",
            that.conversation_id,
            function (that, pid) {
              that.playFromTheQueue(that, pid);
            },
            response["response_channels"]["frames"],
            response["response_channels"]["shapes"],
            response["response_channels"]["word_timings"] || null,
            null,
            true,
            false,
            response
          );
        } else {
          this.addAudioToQueue(
            response["response_channels"]["voice"],
            "nudge_" + response["name"] + "_response",
            that.conversation_id,
            function (that, pid) {
              that.playFromTheQueue(that, pid);
            },
            null,
            null,
            null,
            true,
            false,
            response
          );
        }
      }
      //delete response["response_channels"];
    }
  }
  addNudges(lst) {
    this.play_followups = lst || [];
  }
  processSystemResponse(response, after_played, partial) {
    super.processSystemResponse(response);
    this.state = "speaking";
    window.__dv_cbs_get("StopProcessingInputState").fire();
    if (partial) {
      return;
    }
    // this.displayResponse();
    var that = this;
    if (response["data"]) {
      if (
        response["data"]["avatar_model_name"] ||
        response["data"]["avatar_id"]
      ) {
        that.unityInstance.showPersona(
          response["data"]["avatar_model_name"] || response["data"]["avatar_id"]
        );
        that.voice_id =
          response["data"]["voice_id"] || response["data"]["avatar_id"];
        // eslint-disable-next-line
        defaul_avatar_id =
          response["data"]["avatar_model_name"] ||
          response["data"]["avatar_id"];
      }
      if (response["data"]["bg"]) {
        that.unityInstance.setTexture(response["data"]["bg"]);
        // eslint-disable-next-line
        default_bg = response["data"]["bg"];
      }

      if (response["data"]["function"] && response["data"]["url"]) {
        that.unityInstance.callGameManagerFunction(
          response["data"]["function"],
          response["data"]["url"]
        );
      }
      if (response["data"]["redirect"]) {
        window.open(response["data"]["redirect"], "_blank");
      }
    }
    

    if (response["response_channels"]) {
      var voice = response["response_channels"]["voice"];
      var frames = response["response_channels"]["frames"];
      var shapes = response["response_channels"]["shapes"];
      var word_timings = response["response_channels"]["word_timings"] || null;
      if (response.data && response.data.redirect_response_channels) {
        voice = response.data.redirect_response_channels.voice || voice;
        frames = response.data.redirect_response_channels.frames || frames;
        shapes = response.data.redirect_response_channels.shapes || shapes;
        word_timings = response.data.redirect_response_channels.word_timings || word_timings
        delete response.data.redirect_response_channels;
      }

      if (voice && frames && shapes) {
        
        if (!shapes.endsWith(".csv")) {
          this.addAudioToQueue(
            voice,
            "cs_" + response["name"] + "_response",
            that.conversation_id,
            function (that, pid) {
              if (after_played) {
                after_played();
              }
              that.playFromTheQueue(that, pid);
            },
            frames,
            shapes,
            word_timings,
            null,
            true,
            true,
            response
          );
        } else {
          this.addAudioToQueue(
            voice,
            "cs_" + response["name"] + "_response",
            that.conversation_id,
            function (that, pid) {
              if (after_played) {
                after_played();
              }
              that.playFromTheQueue(that, pid);
            },
            frames || null,
            shapes || null,
            word_timings,
            null,
            true,
            true,
            response
          );
        }
      }
    }

  }
  onError(error) {
    super.onError(error);
    this.playDefaults("timeout");
  }

  onTalkingState() {
    console.log("AVATAR IS SPEAKING");
    var that = this;
    this.state = "speaking";
    if(!dci.unityInstance.isRendering){
      dci.unityInstance.render();
      dci.unityInstance.isRendering=true;
    }
    djq("#disable_layer")
      .first()
      .empty();
    djq(".chat_box").removeClass("dave-speech-stop");
    djq(".questions .dave-stop-speech").remove();
    djq(".chat_box").addClass("dave-speech-stop");
    // djq("#stop-talking").remove();
    // djq("#disable_layer")
    //   .first()
    //   .html('<span class="bubble typing bubble_text">....</span>');
    if (!this.conversation.processing) {
      // checking for the redirect status & hiding the stop button

      var cancel_ = djq(
        // "<button href='javascript:void(0)' class='butn butn-dark butn-sm dave-stop-speech' style='font-size: 10px;'> Stop</button>"
        // "<button class='btns stop-talking' id='stop-talking'></button>"
        "<div class='dave-stop-speech'><button class='dave-stop-img'></button>Stop Response</div>"
      );
      djq(".questions")// djq("#disable_layer")
        .first()
        .append(cancel_);
      cancel_.click(function () {
        that.stopTalking();
        that.unityInstance.stopMorphs();
        that.onIdelState(true);
        djq("#stop-talking").remove();
        // to know if stop button is clicked
        window.__dv_cbs_get("StopButtonClicked").fire();
      });
    }
    window.__dv_cbs_get("HideStopButtonForRedirect").fire();

    // djq("#disable_layer").remove();
    // djq("#chat_box").append('<div id="disable_layer"><span>Wait for Avatar to complete...</span></div>');
    this.toggle_response_panel("....");
  }
  onIdelState(repeat) {
    console.log("AVATAR IS ON IDEL");
    this.load__ = false;
    this.audio_playing = false;
    this.state = "idel";
    this.audioRecord(true);
    this.toggle_response_panel("", true);
    // this.displayResponse();
    this.conversation.processing = false;
    this.pre_resp = "";
    // djq("#disable_layer").remove();
  }

  onProcessingRequest(_text) {
    // TODO animation for processing
    console.debug("Reached on processing request", _text);

    // this.displayResponse(_text, true);
    this.toggle_response_panel("Processing....");
  }

  micRejected() { }
  onRecording() {
    // this.unityInstance.pauseMusic()
    console.log("Started detecting");
    this.audioRecord();
    // this.conversation.post_interaction({ [this.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
    this.triggerEvents("interaction", "speech-input");
  }
  onSpeechDetect(_text) {
    //TODO stub method
    console.log("On speech detected state", _text);
    djq(".input_box #input-text").val(_text);
    window.__dv_cbs_get("SpeechDetected").fire();
    // this.displayResponse(_text);
    this.toggle_response_panel(_text);
    djq("#" + this.mic_id).removeClass("recording");
    djq("#" + this.mic_id).addClass("record");
    this.conversation.post_interaction({
      [this.settings["stage_attr"] ||
        "stage"]: InteractionStageEnum.SPEECH_INPUT
    });

  }

  toggle_response_panel(message, idel) {
    // if (idel) {
    //   djq("#disable_layer").hide();
    //   djq("#disable_layer>button").first().remove();
    // } else {
    //   djq("#disable_layer").show();
    //   djq("#disable_layer>span").html(message || "Processing...");
    // }

    if (idel) {
      djq("#disable_layer").hide();
      // djq("#stop-talking").remove();
      djq(".questions .dave-stop-speech").remove();
      djq(".chat_box").removeClass("dave-speech-stop");
    }
    window.__dv_cbs_get("ProcessingInputState").fire(message);

  }
  //custom function
  displayResponse(txt, cancelFalse = true) {
    var that = this;
    if (!txt) {
      cancelFalse = false;
    }
    txt = txt || "....";
    // djq(".cb__list")
    //   .first()
    //   .empty();
    if (txt.indexOf(".") == -1) {
      that.pre_resp += " " + txt;
      // djq(".cb__list")
      //   .first()
      //   .html(
      //     '<span class="bubble typing bubble_text">' + that.pre_resp + "</span>"
      // );
      djq("#disable_layer>span").html(that.pre_resp);
    } else {
      // djq(".cb__list")
      //   .first()
      //   .html('<span class="bubble typing bubble_text">' + txt + "</span>");
      djq("#disable_layer>span").html(txt);
    }
    if (cancelFalse) {
      var cancel_ = djq(
        "<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Cancel</button>"
      );
      // djq(".cb__list")
      //   .first()
      //   .append(cancel_);
      djq("#disable_layer").append(cancel_)
      cancel_.click(function () {
        that.processSystemResponse(
          that.conversation.current_response,
          true,
          true
        );
        that.onIdelState(true);
        that.conversation.abortReq();
        that.conversation.abortAsrReq();
      });
    }
  }
  postConversation(text, state_ = null, title = null, query_type = null) {
    var sts = super.postConversation(text, state_, title, query_type);
    if (!this.conversation.engagement_id) {
      this.createSession();
    }
    return sts;
  }
  enteredAreaCallback(customer_state) {
    this.postConversation(customer_state, customer_state, null, "auto");
  }
  pointReachedCallback(value) {
    console.log(`Point reached :: ${value}`);
    var that = this;
    this.conversation.list(
      "point",
      { point_name: value, conversation_id: this.conversation.conversation_id },
      function (data) {
        if (data["data"].length > 0) {
          var val = data["data"][0]["point_value"];
          that.conversation.iupdate(
            "person",
            that.conversation.customer_id,
            { total_points: val, _async: true },
            function (data) {
              console.log(data);
            }
          );
          window.__dv_cbs_get("point_collected").fire(val);
        }
      },
      function (err) { }
    );
  }
}

