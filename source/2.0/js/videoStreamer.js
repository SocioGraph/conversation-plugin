class Did {
    constructor() {
        this.peerConnection;
        this.streamId;
        this.sessionId;
        this.sessionClientAnswer;
        this.statsIntervalId;
        this.videoIsPlaying;
        this.lastBytesReceived;
        this.presenterInputByService = {
            talks: {
                source_url: 'https://d-id-public-bucket.s3.amazonaws.com/or-roman.jpg',
            },
            clips: {
                presenter_id: 'rian-lZC6MmWfC1',
                driver_id: 'mXra4jY38i'
            }
        }
        
    }
    initiateConnection() {
        return new Promise((success, error) => {

            if (peerConnection && peerConnection.connectionState === 'connected') {
                return;
            }
            const sessionResponse = fetchWithRetries(`${DID_API.url}/${DID_API.service}/streams`, {
                method: 'POST',
                headers: {
                Authorization: `Basic ${DID_API.key}`,
                'Content-Type': 'application/json',
                },
                body: JSON.stringify(presenterInputByService[DID_API.service]),
            });
            
            const { id: newStreamId, offer, ice_servers: iceServers, session_id: newSessionId } = await sessionResponse.json();
            streamId = newStreamId;
            sessionId = newSessionId;
            
            try {
                sessionClientAnswer = createPeerConnection(offer, iceServers);
            } catch (e) {
                console.log('error during streaming setup', e);
                return;
            }
            const sdpResponse = await fetch(`${DID_API.url}/${DID_API.service}/streams/${streamId}/sdp`, {
                method: 'POST',
                headers: {
                Authorization: `Basic ${DID_API.key}`,
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                answer: sessionClientAnswer,
                session_id: sessionId,
                }),
            });
            
        })

    }
}
function createPeerConnection(offer, iceServers) {
    if (!peerConnection) {
      peerConnection = new RTCPeerConnection({ iceServers });
      peerConnection.addEventListener('icecandidate', onIceCandidate, true);
      peerConnection.addEventListener('track', onTrack, true);
    }
    await peerConnection.setRemoteDescription(offer);
    console.log('set remote sdp OK');
  
    const sessionClientAnswer = await peerConnection.createAnswer();
    console.log('create local sdp OK',sessionClientAnswer);
  
    await peerConnection.setLocalDescription(sessionClientAnswer);
    console.log('set local sdp OK');
  
    return sessionClientAnswer;
  }

const maxRetryCount = 3;
const maxDelaySec = 4;
function fetchWithRetries(url, options, retries = 1) {
    try {
      return await fetch(url, options);
    } catch (err) {
      if (retries <= maxRetryCount) {
        const delay = Math.min(Math.pow(2, retries) / 4 + Math.random(), maxDelaySec) * 1000;
  
        await new Promise((resolve) => setTimeout(resolve, delay));
  
        console.log(`Request failed, retrying ${retries}/${maxRetryCount}. Error ${err}`);
        return fetchWithRetries(url, options, retries + 1);
      } else {
        throw new Error(`Max retries exceeded. error: ${err}`);
      }
    }
  }





