
class AvatarConversation extends ConversationHandler{
    constructor(settings){
        super(settings);
        this.avatar_loaded = false;
        this.audios_list = [];
        this.pre_resp="";
    }
    setUpConversationUI(){
        super.setUpConversationUI();
        super.set("message_id", "message");
        super.set("shortcuts_id", "shortcut");
        super.set("whiteboard_id", "whiteboard");
        super.set("quick_access_id", "quick_access");
        super.set("searchable_id", "searchable");
        super.set("audio_id", "audio");
        super.set("textbox_id", "input-text");
        super.set("mic_id", "record")
        super.set("sendbutton_id", "input-send");
        super.set("mute_button", "mute");

        djq("body").append("<div id='dave_conversation__' class='dave_conversation' style='display:none'></div>");
        djq("#dave_conversation__").setupConversationUi({placeholder:this.default_placeholder || "Type here..."});
        // djq("body").append(`<audio id="audio" type="audio/wav" src="" style="display: none" controls>
        //            Your browser does not support the HTML5 Audio element.
        //            </audio>`);
        
        this.audio = document.getElementById((this.settings["audio_id"] || "audio"));
        this.loadAvatar();
        this.setUpEvents();
    }
    mute(){
        super.mute();
        if(this.audio){
            this.audio.muted = true;
            this.audio.setAttribute("force-mute", true);
            djq("#"+this.mute_button).find("i").first().attr("class", "fa fa-volume-off");
        }
    }
    unmute(){
        super.mute();
        if(this.audio){
            this.audio.muted = false;
            this.audio.setAttribute("force-mute", false);
            djq("#"+this.mute_button).find("i").first().attr("class", "fa fa-volume-up");
        }
    }
    stopTalking(){
        this.audio.pause();
        this.audio_playing = false;
        this.unityInstance.stopTalking();
        // djq("#"+this.message_id).placeholder_effect()
        // try{
        //     this.unityInstance.SendMessage("head", "stopTalking");
        //     djq("#"+this.message_id).placeholder_effect()
        // }catch(e){
        //     console.log("Were not able to do stop talking", e);
        // }
        
    }
    pause(){
        super.pause();
        this.audio.src = undefined;
        this.unityInstance.stopTalking();
        // try{
        //     this.unityInstance.SendMessage("head", "stopTalking");
        // }catch(e){
        //     console.log("Were not able to do stop talking", e);
        // }
        this.audios_list = [];
    }
    resume(){
        var that = this;
        // super.resume();
        djq("#dave_conversation__").show(200, function(){
            // if(that.audio){
            //     that.audio.play();
            // }
            if(that.conversation.session_id){
                that.triggerEvents("resume", "resume");
                that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.RESUMED});
            }else{
                that.triggerEvents("opened", "opened");
                that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.OPENED});
            }
            that.createSession();
            
            that.onResponse(that.conversation.current_response);
            setTimeout(function(){
                set_cc();
            }, 500);
        });
        this.unmute();
    }
    audioRecord(stop){
        var that = this;
        if(that.intervel){
            clearInterval(that.intervel);
        }
        that.intervel = undefined;
        if(stop){
            djq("#"+that.mic_id).removeClass("active");
            djq("#"+that.mic_id).html('<i id="microphone" class="fa fa-microphone fa-2x"></i>');
            djq(".text-wraper").first().show();
            djq("#"+this.quick_access_id).show();
            djq("#"+this.shortcuts_id).show();
            djq("#try-saying").show();
            return;
        }else{
            that.state = "recording";
            var sec=0;
            djq("#"+that.mic_id).addClass("active");
            var intv = djq("<span>00:00</span>");
            djq("#"+that.mic_id).find(".fa-microphone").hide();
            djq("#"+that.mic_id).append(intv);
            djq("#"+that.mic_id).append("<br />");
            djq("#"+that.mic_id).append('<span><i class="fa fa-stop fa-2x"></i></span>');
            
            that.intervel = setInterval(function(){
                sec++;
                var min = parseInt(sec/60);
                var sc = sec%60;
                intv.html( (min < 10 ? "0"+min : min) +":"+ ( sec < 10 ? "0"+sc : sc ));
            },1000);
            djq(".text-wraper").first().hide();
            djq("#quick_access").hide();
            djq("#shortcut").hide();
            djq("#try-saying").hide();
        }
        djq("#"+that.mic_id).show();
    }
    setUpEvents(){
        var that = this;
        if(this.textbox_id){
            djq("#"+this.textbox_id).enter(function(val){
                if(val.trim()){
                    that.postConversation(val, null, val, "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.TEXT_INPUT});
                    that.triggerEvents("interaction", "text-input");
                }
            });
            djq("#"+this.textbox_id).keyup(function(e){
                var val = e.currentTarget.value
                that.updateSearchables(val.trim());
            });
            
        }
        if(this.sendbutton_id){
            djq("#"+this.sendbutton_id).click(function(){
                if(djq("#"+that.textbox_id).val().trim()){
                    that.postConversation(djq("#"+that.textbox_id).val(), null, djq("#"+that.textbox_id).val(), "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: "text-input"});
                    that.triggerEvents("interaction", "text-input");
                }
            });
        }
        if(this.mic_id){
            djq("#"+this.mic_id).click(function(){
                if(that.state == "idel"){
                    // that.audioRecord();
                    djq(this).hide();
                    that.conversation.startRecordResponse();
                }else if(that.state == "recording"){
                    that.conversation.stopRecordResponse();
                }
            })
        }
        if(this.mute_button){
            djq("#"+this.mute_button).click(function(){
                var cc_class = djq(this).find("i").first().attr("class");
                if(cc_class.indexOf("fa-volume-up") == -1){
                    that.unmute();
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.UNMUTED});
                    that.triggerEvents("interaction", "unmuted");
                }else{
                    that.mute();
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MUTED});
                    that.triggerEvents("interaction", "muted");
                }
            });
        }
        djq("#close_message").click(function(){
            djq("#dave_conversation__").hide(200);
            that.pause();
            that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MINIMIZED});
            that.triggerEvents("interaction", "minimized");
        })
    }

    onLoadedCallback(){
        if(this.conversation_loaded && this.avatar_loaded){
            this.addShortcutsButtons();
            this.conversationLoadedCallback(this.conversation.conversation_data);
            this.loaded = true;
            var that = this;
            setTimeout(function(){
                if(!that.minimize){
                    that.createSession();
                    djq("#dave_conversation__").show(200, function(){
                        that.onResponse(that.conversation.init_response);
                        // set_cc();
                        that.triggerEvents("chat_started", "chat_started");
                        
                        set_cc();
                        that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.AUTO_OPENED});
                    });
                }

                djq(window).on("beforeunload", function(){
                    // that.clearSession(false);
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.LEAVE_PAGE});
                    that.clearSession(false);
                })

            }, 2000)
        }
    }
    loadAvatar(){
        var that = this;
        var t = SetupEnviron.load(this.settings["unity_url"], this.settings);
        t.then(function(ut){
            that.avatar_loaded = true;
            that.unityInstance = ut;
            that.onLoadedCallback();
            // set_cc();
        }, function(err){
            console.error(err);
        });
        t.progress(function(pro){
            console.debug(pro);
        })
    }
    

    playAnimations(audo, force) {
        var that = this;
        if (!this.audios_list.length && !audo) return;
        if (!audo) {
            audo = this.audios_list.splice(0, 1);
            audo = audo[0];
        }
        this.unityInstance.stopTalking();
        // try{
        //     this.unityInstance.SendMessage("head", "stopTalking");
        // }catch(e){
        //     console.log("Were not able to do stop talking", e);
        // }
        this.prev_audo = audo;
        this.temp_id = audo["id"];
        var tt = this.audio.cloneNode(true);
        var muted = this.audio.getAttribute("force-mute");
        this.audio.remove();
        this.audio = tt;
        this.audio.setAttribute("data-audio-id", that.temp_id);
        this.audio.setAttribute("force-mute", muted || false);
        this.audio.muted = muted || false;
        this.audio.src = audo["audio"];
        document.body.appendChild(this.audio);
        console.debug("play init id :: ", audo["id"]);
        this.audio.load();
        // this.audio.play();

        function _lis() {
            if (audo["id"] != that.temp_id && that.temp_id != that.audio.getAttribute("data-audio-id")) return;

            function playShapes_frames(id, frames, shapes) {
            if (that.forced_stop && that.forced_stop != that.audio.getAttribute("data-audio-id")) {
                console.log("We have forced stopped, so don't play:" + id)
                return;
            }
            if ((["speech-detected", "processing", "idel"].indexOf(that.state) < 0 && that.load__) && !force) {
                console.log("State is not idle: " + that.state + " or we have loaded something else " + that.load__ + " already:" + id)
                return;
            }
            if(!audo["defaults"]){
                that.audio.setAttribute("defaults", false);
                that.onTalkingState();
            }else{
                that.audio.setAttribute("defaults", true);
            }
            if (!that.paused) {
                that.unityInstance.startTalking(id, frames, shapes);
                // try{
                //     that.unityInstance.SendMessage('head', 'StartTalking', JSON.stringify({
                //         "frames": frames,
                //         "shapes": shapes,
                //         "audio_id": id
                //     }))
                // }catch(e){
                //     console.log("Start talking failed", e);
                // }
                
                that.audio_playing = true;
                // that.conversation.hold(true);
                that.playing_id = id;
                console.debug("++++++Playing ID:" + id);
            } else {
                console.debug("++++++Paused ID:" + id);
            }
            //djq("#message").html(list[i][1]);
            }
            playShapes_frames(audo["id"], audo["frames_csv"], audo["shapes_csv"]);
        }
        this.audio.removeEventListener("canplaythrough", _lis, false);
        this.audio.addEventListener("canplaythrough", _lis, true);
        this.audio.onended= function() {
            that.audio_playing = false;
            if(that.playing_id != that.audio.getAttribute("data-audio-id")) return;
            that.unityInstance.stopTalking()
            // try{
            //     that.unityInstance.SendMessage("head", "stopTalking");
            // }catch(e){
            //     console.log("Were not able to do stop talking", e);
            // }
            // that.conversation.hold(false);
            if(!that.conversation.processing && !that.load__){
                that.onIdelState();
            }
            if(that.state == "idel" && !that.load__){
                that.pre_resp = "";
            }
            if(audo["onEnds"]){
                audo["onEnds"](that, that.playing_id);
            }else{
                that.playFromTheQueue(that, that.playing_id);
            }
        }
    }

    playDefaults(state, message, that) {
        super.playDefaults(state, message);

        // eslint-disable-next-line no-redeclare
        var that = that || this;

        function __load(level) {
            djq.ajax({
                "url": that.host_url + "/defaults/" + that.voice_id + "/" + level + "/" + that.enterprise_id,
                "success": function (data) {
                    that.addAudioToQueue(data["audio"], state, "defaults", function (that, pid) {
                            that.playFromTheQueue(that, pid)
                        }, null, null, data["subfolder"], false);
                }
            });
        }
        __load(state);
    }

    playFromTheQueue(that, playing_id){
        var that = that || this;
        if(that.audios_list.length){
            that.playAnimations(null, false);
            return;
        }

        if(that._timer){
            clearTimeout(that._timer);
        }
        if(playing_id != that.latest_audio_id) return;
        if(!that.play_followups.length){
            return;
        }
        (function(tm){
            var tf = function(){
                if(that.paused){
                    setTimeout(tf, tm);     
                }
                if(that.state != "idel" || that.load__) return;
                if(that.audios_list.length){
                    that.playAnimations(null, false);
                }
                else if(that.play_followups.length){
                    that.playNudge(that.play_followups[0], function(data){that.playNudge(data)});
                    that.play_followups.splice(0, 1);
                }else{}
            }
            that._timer = setTimeout(tf, tm);
        })(that.conversation.current_response["wait"] || 10000);
    }    
    addAudioToQueue(audio, id, conversation_id, onEnds, frames, shapes, subfolder, force){
        var that =this;
        this.forced_stop = false;
        var id_ = id+"___"+makeid(4);
        console.debug("audio adding id :: ", id_);
        this.latest_audio_id = id_;
        if(force){
            this.audios_list = [];
            //this.play_followups = [];
            /*if(this.audio){
                        this.audio.muted = true;
                        this.unityInstance.SendMessage("head", "stopTalking")
                    }*/
            this.forced_stop = id_;
            if(this._timer){
                clearTimeout(this._timer);
            }
        }
        if(this._timer && onEnds){
            clearTimeout(this._timer);
        }

        var aurl;
        if(audio.indexOf("http")>=0){
            aurl = audio;
        }else{
            aurl = this.host_url+"/"+audio; 
        }
        var conversation_id = conversation_id || this.conversation_id;
        var ob = {
            id: id_,
            audio: aurl,
            onEnds: onEnds,
            defaults: (conversation_id=="defaults")
        }
        this.audios_list.push(ob);

        var fil ="?voice_id="+this.voice_id;
        if(subfolder)
            fil += "&subfolder="+subfolder;

        
        function addFramesShapes(frames, shapes) {
          if (frames) {
            ob["frames_csv"] = frames;
          }
          if (shapes) {
            ob["shapes_csv"] = shapes;
          }
          if (ob["frames_csv"] && ob["shapes_csv"]) {
            if (!that.audio_playing) {
              that.playAnimations(null, force);
            }
          }
        }
    
        var furl, surl;
        if(!frames){
            furl = this.host_url+"/frames/"+conversation_id+"/"+id+"/"+this.enterprise_id+fil;
            djq.ajax({
                "url": furl,
                "success":function(data){
                    addFramesShapes(data)
                }
            })
        }else{
            addFramesShapes(frames);
        }
        if(!shapes){
            surl = this.host_url+"/shapes/"+conversation_id+"/"+id+"/"+this.enterprise_id+fil;
            djq.ajax({
                "url": surl,
                "success": function(data2){
                    addFramesShapes(null, data2);
                }
            })
        }else{
            addFramesShapes(null, shapes);
        }
    }

    nudgeResponse(response) {
        super.nudgeResponse(response);
        var that = this;
        if (response["response_channels"]) {
            if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
            // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
            if (!response["response_channels"]["shapes"].endsWith(".csv")) {
                this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["voice"].split("/").slice(-2, 1)[0], that.conversation_id, function (that, pid) {
                that.playFromTheQueue(that, pid)
                }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
            } else {
                this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/").splice(-2, 1)[0], that.conversation_id, function (that, pid) {
                that.playFromTheQueue(that, pid)
                }, null, null, null, true);
            }
            }
            //delete response["response_channels"];
        }
    }
    addNudges(lst){
        this.play_followups = lst || [];
    }
    processSystemResponse(response, after_played, partial) {
        super.processSystemResponse(response);
        this.state="speaking";
        if(partial){
            return;
        }
        // this.displayResponse();
        var that = this;
        if (response["data"]) {
          if (response["data"]["avatar_model_name"] || response["data"]["avatar_id"]) {
            // try{
            //     that.unityInstance.SendMessage("GameManagerOBJ", "showPersona", response["data"]["avatar_model_name"] || response["data"]["avatar_id"]);
            // }catch(e){
            //     console.log("Were not able to update persona", e);
            // }
            that.unityInstance.showPersona(response["data"]["avatar_model_name"] || response["data"]["avatar_id"]);
            that.voice_id = response["data"]["voice_id"] || response["data"]["avatar_id"];
            // eslint-disable-next-line
            defaul_avatar_id = response["data"]["avatar_model_name"] || response["data"]["avatar_id"]
          }
          if (response["data"]["bg"]) {
            // try{
            //     that.unityInstance.SendMessage("GameManagerOBJ", "setTexture", response["data"]["bg"]);
            
            // }catch(e){
            //     console.log("Were not able to update texture", e);
            // }
            that.unityInstance.setTexture(response["data"]["bg"]);
            // eslint-disable-next-line
            default_bg = response["data"]["bg"];
          }
    
          if (response["data"]["function"] && response["data"]["url"]) {
            // try{
            //     that.unityInstance.SendMessage("GameManagerOBJ", response["data"]["function"], response["data"]["url"]);
            
            // }catch(e){
            //     console.log("Were not able to send game manger function", e);
            // }
            that.unityInstance.callGameManagerFunction(response["data"]["function"], response["data"]["url"])
          }
          if (response["data"]["redirect"]) {
            window.open(response["data"]["redirect"], '_blank');
          }
        }
        if (response["response_channels"]) {
          if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
            // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
            if (!response["response_channels"]["shapes"].endsWith(".csv")) {
              this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["voice"].split("/").slice(-2, 1)[0], that.conversation_id, function (that, pid) {
                if (after_played) {
                    after_played()
                }
                that.playFromTheQueue(that, pid)
              }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
            } else {
              this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/").splice(-2, 1)[0], that.conversation_id, function (that, pid) {
                if (after_played) {
                    after_played()
                }
                that.playFromTheQueue(that, pid)
              }, null, null, null, true);
            }
          }
          //delete response["response_channels"];
        }
    }
    onError(error){
        super.onError(error);
        this.playDefaults("timeout");
    }

    onTalkingState(){
        var that = this;
        this.state= "speaking";
        djq(".cb__list").first().empty();
        djq(".cb__list").first().html('<span class="bubble typing bubble_text">....</span>');
        if(!this.conversation.processing){
            var cancel_ = djq("<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Stop</button>");
            djq(".cb__list").first().append(cancel_);
            cancel_.click(function(){
                that.stopTalking();
                that.onIdelState(true);
                $("#message").placeholder_effect();
            });
        }
        this.toggle_response_panel();
    }
    onIdelState(repeat){
        this.load__ = false;
        this.audio_playing = false;
        this.state = "idel";
        this.audioRecord(true);
        this.toggle_response_panel(true);
        this.displayResponse();
        this.conversation.processing = false;
        this.pre_resp = "";
    }
 
    onProcessingRequest(_text) {
        // TODO animation for processing
        console.debug("Reached on processing request", _text);
        this.updateSearchables();
        this.displayResponse(_text, true);
        this.toggle_response_panel();
    }

    micRejected(){}
    onRecording(){
        this.updateSearchables();
        console.log("Started detecting");
        this.audioRecord();
        // this.conversation.post_interaction({ [this.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
        this.triggerEvents("interaction", "speech-input");
    }
    onSpeechDetect(_text) {
        //TODO stub method
        console.log("On speech detected state", _text)
        this.displayResponse(_text);
        this.toggle_response_panel();
        this.conversation.post_interaction({ [this.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
    }

    toggle_response_panel(idel){
        if(idel){
            djq("#reponse_panel").show();
            djq("#reponse_message_panel").hide();
        }else{
            djq("#reponse_panel").hide();
            djq("#reponse_message_panel").show();
        }
    }
    //custom function
    displayResponse(txt, cancelFalse=true){
        var that = this;
        if(!txt){
            cancelFalse = false
        }
        txt = txt || "....";
        djq(".cb__list").first().empty();
        if(txt.indexOf(".") == -1){
            that.pre_resp +=" "+ txt;
            djq(".cb__list").first().html('<span class="bubble typing bubble_text">'+that.pre_resp+'</span>');
        }else{
            djq(".cb__list").first().html('<span class="bubble typing bubble_text">'+txt+'</span>');
        }
        if(cancelFalse){
            var cancel_ = djq("<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Cancel</button>");
            djq(".cb__list").first().append(cancel_);
            cancel_.click(function(){
                that.processSystemResponse(that.conversation.current_response, true, true);
                that.onIdelState(true);
                that.conversation.abortReq();
                that.conversation.abortAsrReq();
            });
        }
    }
    postConversation(text, state_=null, title=null, query_type=null){
        var sts = super.postConversation(text, state_, title, query_type);
        if(!this.conversation.engagement_id){
            this.createSession();
        }
        return sts;
    }
}
