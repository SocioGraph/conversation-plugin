var me = document.querySelector('script[id="dave-settings"]');
var data = me.dataset;
let DAVE_ASSET_PATH = (data.asserts || (document.currentScript && (new URL(document.currentScript.src)).origin))+"/" || "https://unity-plugin.iamdave.ai/conversation-plugin/";
let UNITY_ASSET_PATH = data.unity_asserts || DAVE_ASSET_PATH;
let VERSION = data.version || '2.0';
let LOAD_TYPE = data.loadfunction || 'window-load';
let UNITY_FOLDER = data.unity_folder || 'brands_arena';
let catchclear = data.catchclear || "1.0";
function asset_path(){
    var s = DAVE_ASSET_PATH;
    if(s == "/"){
        s = "";
    }
    for(var i of arguments){
        if(!s){
            s = i;
            continue;
        }
        if(i.startsWith("/")){
            i = i.substring(1);
        }
        if(s.endsWith("/")){
            s+=i;
        }else{
            s+="/"+i;
        }
    }
    return s;
}


var callbacks = function(){
    return {
        list: [],
        add: function(){
            function isFunction( obj ) {
                return typeof obj === "function" && typeof obj.nodeType !== "number" &&
                typeof obj.item !== "function";
            }
            if(isFunction(arguments[0])){
                this.list.push(arguments[0]);
            }
        },
        fire: function(){
            for(var fun of this.list){
                fun.apply({},arguments);
            }
        },
        clear: function(){this.list=[]}
    }
}
const _davelibs = {
    "__default__": [
        { "__default__": asset_path(`lib/jquery-3.6.0.min.js`), "async": false },
        {
            "__default__": "//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        }
    ],
    "bootstrap": [{
        "__default__": "https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
    },{
        "__default__": "https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
    },{
        "__default__": "//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    }],
    "speech": [{
        "__default__": asset_path("dist/js/socket.io-mini.js")
    },{
        "__default__": asset_path("dist/js/socket.stream.io-mini.js")
    },{
        "__default__": asset_path("dist/js/RecordRTC-mini.js")
    },{
        "__default__": asset_path("lib/flac-dep.js")
    },{
        "__default__": asset_path("dist/js/vad-mini.js")
    }],
    "camara":[
        {
            "__default__": asset_path("lib/cameraControls.js")
        }
    ],
    "avatar-conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'avatar-conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'avatar-conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'avatar-conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/style.css'),
        "production": asset_path('dist', VERSION, 'style.min.css'),
        "staging": asset_path('dist', VERSION, 'style.min.css')
    }],
    "metaverse-conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'metaverse-conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'metaverse-conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'metaverse-conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/metaverse-style.css'),
        "production": asset_path('dist', VERSION, 'metaverse-style.min.css')
    }],
    "babylon-conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'babylon-conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'babylon-conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'babylon-conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/babylon-style.css'),
        "production": asset_path('dist', VERSION, 'babylon-style.min.css')
    }],
    "chatbot-conversation-libs":[{
        "__default__": asset_path('build', VERSION, 'chatbotcha-conversation-plugin.js'),
        "production": asset_path('dist', VERSION, 'chatbot-conversation-plugin.min.js'),
        "staging": asset_path('build', VERSION, 'chatbot-conversation-plugin.js')
    },{
        "__default__": asset_path('source', VERSION, 'css/chatbot-style.css'),
        "production": asset_path('dist', VERSION, 'chatbot-style.min.css')
    }],
    "unity": [
        {
            "__default__": UNITY_ASSET_PATH.replace("conversation-plugin", "unity")+UNITY_FOLDER+"/ProjectBuild.loader.js"
        }
    ],
    "babylon": [
    {
        "__default__": asset_path('lib/babylon.js')
        
    },
    {
        "__default__": asset_path('lib/babylonjs.loaders.min.js')
        
    }],
    "avatar-conversation":["avatar-conversation-libs", "speech", "camara", "unity"],
    "chatbot-conversation":["bootstrap", "chatbot-conversation-libs"],
    "kiosk": ["bootstrap", "speech", "unity", "avatar-conversation-libs"],
    "babylon-avatar-conversation":["bootstrap",  "speech", "camara", "babylon","babylon-conversation-libs"],
    "metaverse-conversation": ["bootstrap", "metaverse-conversation-libs", "speech", "camara", "unity"]
}
class LibFunction{
    
    constructor(environment = "staging") {
        this._dave_libs = _davelibs;
        this._scrpts =[];
        this.environment = environment;
        var libs = this.__fetch_component("__default__", environment);
        this.__load_file(libs, "on_dave_init");
    }
    __load_js(src, onload, trys=3){
        // Load the script
        console.log("loading ::", src)
        var that = this;
        const script = document.createElement("script");
        script.src = src+"?_vn="+catchclear;
        script.type = 'text/javascript';
        script.addEventListener('load', () => {
            console.log(src+" has been loaded successfully");
            if(onload) onload();
            // use jQuery below
        });
        script.addEventListener("error", (ev) => {
            console.log("Error on loading file", ev);
            if(trys){
                that.__load_js(src, onload, trys-1);
            }
        });
        document.head.appendChild(script);
    }
    __load_css(src, onload, trys=3){
        // Load the script
        var that = this;
        const script = document.createElement("link");
        script.href = src+"?_vn="+catchclear;
        script.rel = 'stylesheet';
        script.addEventListener('load', () => {
            console.log(src+" has been loaded successfully");
            if(onload) onload();
            // use jQuery below
        });
        script.addEventListener("error", (ev) => {
            console.log("Error on loading file", ev);
            if(trys){
                that.__load_css(src,  onload, trys-1);
            }
        });
        document.head.appendChild(script);
    }
    __fetch_component(key){
        if(key.endsWith("js") || key.endsWith("css")){
            return [[key, 1]]
        }
        var _scrpts = [];
        for(var z of this._dave_libs[key]){
            if(typeof z != 'object' && (z.indexOf("js") != -1 || z.indexOf("css") != -1)){
                _scrpts.push([z, 1]);
            }else if(typeof z != 'object'){
                _scrpts = _scrpts.concat(this.__fetch_component(z, this.environment))
            }else{
                _scrpts.push([(z[this.environment] || z["__default__"]), z["priority"] || 1]);
            }
        }
        return _scrpts;
    }
    __load_file(libs, callback_key="on_dave_libs_loaded"){
        if(libs.length == 0) return
        var fil = libs.shift()
        var that =this;
        var fn = this.__load_css;
        if(fil[0].endsWith("js")){
            fn = this.__load_js;
        }
        fn(fil[0], function(){
            if(fil[1] == 1){
                that.__load_file(libs, callback_key);
            }
            if(fil[1] != 3){
                that.total_libs--;
                if(that.total_libs == 0){
                    // window.__dv_cbs["on_dave_libs_loaded"].fire("Test arg")
                    window.__dv_cbs_get(callback_key).fire()
                }
            }
        });
        if(fil[1] == 3){
            that.total_libs--;
            if(that.total_libs == 0){
                // window.__dv_cbs["on_dave_libs_loaded"].fire("Test arg")
                window.__dv_cbs_get(callback_key).fire()
            }
        }
        if(fil[1] != 1){
            that.__load_file(libs, callback_key);
        }
        // console.log(fil);
    }
    __load_component(src){
        var that = this;
        var libs = [];
        var cslib = []
        var ll = src.split(",");
        for(var i of ll){
            if(i.indexOf(".") > -1){
                cslib = cslib.concat(this.__fetch_component(i, this.environment));
            }else{
                libs = libs.concat(this.__fetch_component(i, this.environment));
            }
        }
        console.log(libs);
        this.total_libs = libs.length;
        this.__load_file(libs);
        window.daveRegisterCallback("on_dave_libs_loaded", function(ref){
            that.__load_file(cslib, "on_custom_libs_loaded");
        });
    }
}
window.daveRegisterCallback = function(key, func){
    if(!window.__dv_cbs[key]){
        window.__dv_cbs[key] = callbacks();
    }
    window.__dv_cbs[key].add(func);
}
window.__dv_cbs = window.__dv_cbs || {
    "on_dave_libs_loaded": callbacks(),
    "on_dave_init": callbacks(),
    "on_action_callback": callbacks()
};
window.__dv_cbs_get = function(key){
    function __message(){
        console.warn(`no callback named ${key}`);
    }
    if(!window.__dv_cbs[key]){__message();window.__dv_cbs[key] = callbacks(); }
    return window.__dv_cbs[key];
}
function init_dave_plugin(tim=1){
    var after_loaded = function() {
        var ll = new LibFunction(data.environment);
        ll.__load_component(data.component);
        // __load_libs(data.component, data.environment);
    };
    setTimeout(after_loaded, tim);
}



if(LOAD_TYPE == "oninteraction"){
    var fu = function(){
        init_dave_plugin();
        window.removeEventListener('click', fu);
    }
    window.addEventListener('click', fu);
}else if(LOAD_TYPE == "onload"){
    init_dave_plugin();
}else if(LOAD_TYPE.startsWith("after-")){
    init_dave_plugin(parseInt(LOAD_TYPE.split("-")[1] || "1")*1000);
}else{
    window.addEventListener('load', init_dave_plugin);
}
