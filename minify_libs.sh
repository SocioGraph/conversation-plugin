echo "Minifing RecordRtc"
npx uglifyjs lib/RecordRTC.js --mangle --compress -o dist/js/RecordRTC-mini.js
echo "Minifing socket.io.js"
npx uglifyjs lib/socket.io.js --mangle --compress -o dist/js/socket.io-mini.js
echo "Minifing socket.stream.io.js"
npx uglifyjs lib/socket.stream.io.js --mangle --compress -o dist/js/socket.stream.io-mini.js
echo "Minifing vad.js"
npx uglifyjs lib/vad.js --mangle --compress -o dist/js/vad-mini.js
cp lib/socket.io.js.map dist/js/socket.io.js.map