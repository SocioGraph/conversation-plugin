#!/bin/bash
# rm dist/1.0/*

version=${release:-1.0}
base=${base:-1.0}
all=
level=
while [ $# -gt 0 ]; do

   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
        # echo $1 $2 // Optional to see the parameter:value result
   fi
  shift
done

echo $all
mkdir -p ./dist/$version/
mkdir -p ./dist/$version/
mkdir -p ./dist/js/
mkdir -p ./dist/css/

echo "Minifing conversation-plugin"
if [ -n "$level" ]
then
    npx uglifyjs build/${version}/$level-conversation-plugin.js --mangle --compress -o dist/${version}/${level}-conversation-plugin.min.js
else
    npx uglifyjs build/${version}/conversation-plugin.js --mangle --compress -o dist/${version}/conversation-plugin.min.js
fi


if [ $level == 'avatar' ]
then
    echo $level
    cp dist/${version}/${level}-conversation-plugin.min.js dist/${version}/conversation-plugin.min.js
fi

if [ -n "$all" ]
then
    bash minify_libs.sh
else
    echo "***Skipped minifing lib***"
fi

if [ -a "$level" ]
then
    if [ -f  ./source/${base}/css/${level}-style.css ]; then
        npx uglifycss source/${base}/css/${level}-style.css > dist/$version/${level}-style.min.css
    else
        npx uglifycss source/${base}/css/style.css > dist/$version/${level}-style.min.css
    fi
fi

npx uglifycss source/${base}/css/style.css > dist/$version/style.min.css
cp source/${base}/css/MyriadPro-It.ttf dist/$version/MyriadPro-It.ttf
cp source/${base}/css/OCRAStd.woff dist/$version/OCRAStd.woff
cp source/${base}/css/MyriadPro-Semibold.ttf dist/$version/MyriadPro-Semibold.ttf
