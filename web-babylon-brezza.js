var deployment_scenario = "new_brezza";
var event_id = "new_brezza";
var conversation_id = "deployment_new_brezza";
var channel_id = "arena";
var channel_name = "Arena";
var loading_img, loading_mobile_img, default_whiteboard, loading_whiteboard;
var avatar_id, default_bg, unity_folder;
loading_img =
  "https://d3nxn5bl526zn7.cloudfront.net/maruti_brands/new_brezza/web_loading.webp";
loading_mobile_img =
  "https://d3nxn5bl526zn7.cloudfront.net/maruti_brands/new_brezza/mobile_loading.webp";
default_whiteboard =
  "https://d3nxn5bl526zn7.cloudfront.net/maruti_brands/new_brezza/default_whiteboard.webp";
loading_whiteboard =
  "https://d3nxn5bl526zn7.cloudfront.net/maruti_brands/new_brezza/loading_whiteboard.webp";
default_bg = "bank";

var deployment_object = {
  conversation_id: "deployment_new_brezza",
  deployment_scenario: "new_brezza",
  deployment_type: "web-avatar",
  model_name: "dave",
  product_id: "VZ",
  unity_folder: "brands_arena"
};

var conversation_id;
if (deployment_object["conversation_id"]) {
  conversation_id = deployment_object["conversation_id"];
}
var product_id = deployment_object["product_id"];

console.log("Deployment object :: ", deployment_object);

var unity_url = "https://s3.ap-south-1.amazonaws.com/models.iamdave.ai/glb-models/dave_update_2.glb";

var createEngine = function () { 
  var canvas = document.getElementById("unityContainer");
  return new BABYLON.Engine(
    canvas,
    true,
    {
      timeStep: 1.0 / 10.0,
      deterministicLockstep: true,
      lockstepMaxSteps: 30,
      limitDeviceRatio: 3
    },
    true
  );
}
var camera, engine;
var createCamara = function () { 
  camera = new BABYLON.FreeCamera(
    "camera_dave",
    new BABYLON.Vector3(0, 1.5, 2),
    scene
  );
  camera.setTarget(new BABYLON.Vector3(0, 1.4, 0));
  camera.viewport = new BABYLON.Viewport(0,0, 1, 1)
  return camera;
}
var scene; //= createScene();
var createScene = function () {
  scene = new BABYLON.Scene(engine);
  scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
  //camera.attachControl(canvas, false);
  assetsManager = new BABYLON.AssetsManager(scene);
  return scene;
};

// camera = createCamara();
if (!loading_mobile_img) {
  loading_mobile_img =
    "https://s3.ap-south-1.amazonaws.com/unity-plugin.iamdave.ai/static/conversation/mobile_loading.gif";
}

if (!loading_img) {
  loading_img =
    "https://s3.ap-south-1.amazonaws.com/unity-plugin.iamdave.ai/static/conversation/web_loading.gif";
}

function loadImg(url) {
  var img = djq(
    "<img class='loading' src='" + url + "' crossorigin='anonymous'/>"
  );
  // var img = djq("<video src='/static/img/daveGif.mp4' autoplay loop muted height='100%' width='100%' />")
  img.on("load", function () {
    console.debug(`${url} :: loaded`);
  });
}

function __load_image(url) {
  let canvas = document.createElement("CANVAS");
  let img = document.createElement("img");
  //img.setAttribute('crossorigin', 'anonymous');
  img.src = url;

  img.onload = () => {
    canvas.height = img.height;
    canvas.width = img.width;
    let context = canvas.getContext("2d");
    context.drawImage(img, 0, 0);
    // let dataURL = canvas.toDataURL('image/*');
    // canvas = null;
    // callback(dataURL);
  };
}
if (default_bg && default_bg.startsWith("http")) {
  __load_image(default_bg);
}
if (default_whiteboard) {
  __load_image(default_whiteboard);
}
if (loading_whiteboard) {
  __load_image(loading_whiteboard);
}
djq(document).ready(function () {
  if (djq.browser.mobile) {
    __load_image(loading_mobile_img);
  } else {
    __load_image(loading_img);
  }
});

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

var dci;
var DAVE_BASE_URL,
  PRODUCTION = false;
var speech_url;
if (window.origin.indexOf("marutisuzuki.com") > -1) {
  DAVE_BASE_URL = "https://msil-brands.iamdave.ai";
  PRODUCTION = true;
  speech_url = "https://speech.iamdave.ai";
} else if (window.origin.indexOf("nexaexperience.com") > -1) {
  DAVE_BASE_URL = "https://msil-brands.iamdave.ai";
  PRODUCTION = true;
  speech_url = "https://speech.iamdave.ai";
} else {
  DAVE_BASE_URL = "https://staging.iamdave.ai";
  speech_url = "https://speech-test.iamdave.ai";
}
// DAVE_BASE_URL="https://msil-core.iamdave.ai";
speech_url = "https://speech.iamdave.ai";

(function () {
  var daveConversationSetup = function (ele, settings, successC, errorC) {
    var loaded_avatar = false;
    var _td_global_added = false;
    var chat_started = false;

    var loading_gif;
    function loadGif(url) {
      if (loading_gif) {
        loading_gif.show(200);
        return;
      }
      console.log("Image URL", url);
      var img = djq("<img class='loading' src='" + url + "'/>");
      // var img = djq("<video src='/static/img/daveGif.mp4' autoplay loop muted height='100%' width='100%' />")
      img.on("load", function () {
        console.log("gif is loaded");
      });
      img.on("error", function () {
        console.log("gif loading failed");
      });
      loading_gif = djq(
        '<div id="loading_gif" class="gifLoaderConversation" style=""></div>'
      );
      loading_gif.append(img);
      loading_gif.append(
        '<a id="gif_close" href="javascript:void(0)" style="z-index: 10;position: absolute;right: 10px; top: 10px; color: red;font-size: 15px;"><i class="fa fa-times-circle fa-2x" aria-hidden="true"></i></a>'
      );
      djq("body").append(loading_gif);
      djq("#gif_close").click(function () {
        console.log("click !!!!!!!!!!!!!!!!! 1233413943u4");
        loading_gif.hide(200);
        dci.set("minimize", true);
      });
    }
    function _signup() {
      var data = {
        user_id: Utills.getCookie("_td") || "dinesh+" + makeid(10) + "@i2ce.in",
        person_type: "visitor",
        source: "web-avatar",
        origin: window.location.origin,
        validated: true,
        referrer: document.referrer,
        application: "brands"
      };

      let up = getUrlParams();
      for (k of ["utm_source", "utm_medium", "utm_campaign", "utm_term"]) {
        if (up[k]) {
          data[k] = up[k];
        }
      }
      let tdgid = Utills.getCookie("_td_global");
      if (tdgid) {
        data["cdp_id"] = tdgid;
        _td_global_added = true;
      }
      return data;
    }
    function _conv(params) {
      if (!params.temporary_data) {
        params["temporary_data"] = {};
      }
      params.temporary_data["origin"] = window.location.origin;
      params.temporary_data["channel"] = channel_id;
      params.temporary_data["channel_name"] = channel_name;
      params.temporary_data["pathname"] = window.location.pathname.slice(1);
      let up = getUrlParams();
      for (k of ["utm_source", "utm_medium", "utm_campaign", "utm_term"]) {
        if (up[k]) {
          params[k] = up[k];
        }
      }
      params["production"] = PRODUCTION;
      params["product_id"] = product_id;
      params["deployment_scenario"] = deployment_scenario;
      return params;
    }
    function _td_submit(event_action, event_category, el) {
      console.debug(
        "Sent eventAction: '" +
        event_action +
        "', eventCategory: '" +
        event_category +
        "', eventLabel: '" +
        el +
        "'"
      );
      try {
        tdSubmit(
          {
            eventAction: event_action,
            eventCategory: event_category,
            eventLabel: el
          },
          "event_" + channel_id
        );
      } catch {
        console.log("Tdsubmit function is not there");
      }
    }
    //Updating interaction stage names
    InteractionStageEnum.set("CLICK", "click_query");
    InteractionStageEnum.set("TEXT_INPUT", "typed_query");
    InteractionStageEnum.set("SPEECH_INPUT", "voice_query");
    InteractionStageEnum.set("MIC_ALLOWED", "mic_allowed");
    InteractionStageEnum.set("MIC_REJECTED", "mic_rejected");
    InteractionStageEnum.set("RESUMED", "resumed");
    InteractionStageEnum.set("MINIMIZED", "closed");

    InteractionStageEnum.set("OPENED", "opened");
    InteractionStageEnum.set("AUTO_OPENED", "auto_opened");
    InteractionStageEnum.set("MIC_REQUESTED", "mic_requested");
    InteractionStageEnum.set("LEAVE_PAGE", "leave_page");

    function _actions(data, action) {
      var pathname = window.location.pathname.slice(1);
      var pathname = window.location.pathname.slice(1);
      if (pathname) {
        pathname = " - " + pathname;
      }
      let el;
      if (action == "signup") {
        if (!_td_global_added) {
          function __(times) {
            if (!times) {
              return;
            }
            console.log("Trying to get _td_global time :: ", times);
            let tdgid = Utills.getCookie("_td_global");
            if (tdgid) {
              dci.patchUser(
                { cdp_id: tdgid, _async: true },
                data => console.log("Patched user with tdglobal", data),
                err => console.error("Was not able to patch the user", err)
              );
            } else {
              setTimeout(() => __(times - 1), 1000);
            }
          }
          __(3);
        }
      } else if (action == "form") {
        el = channel_name + " - Form Submit" + pathname;
        _td_submit("submit", "DaveAvatar_Form_Submit", el);
        console.log(
          "tdSubmit",
          "submit",
          " -- ",
          "DaveAvatar_Form_Submit",
          " - ",
          el
        );
      } else if (action == "url_click") {
        el = channel_name + " - URL Click" + pathname;
        _td_submit("click", "DaveAvatar_Follow_URL", el);
        console.log(
          "tdSubmit",
          "click",
          " -- ",
          "DaveAvatar_Follow_URL",
          " -- ",
          el
        );
      } else if (action == "clicked") {
        console.log("CLICK EVENT TRIGGERED");
        el = channel_name + " - Button Click" + pathname;
        _td_submit("click", "DaveAvatar_Interaction", el);
        console.log(
          "tdSubmit",
          "click",
          " -- ",
          "DaveAvatar_Interaction",
          " -- ",
          el
        );
      } else if (action == "interaction") {
        el = channel_name + " - " + pathname;
        _td_submit(data || "click", "DaveAvatar_Interaction", el);
        console.log(
          "tdSubmit",
          "click",
          " -- ",
          "DaveAvatar_Interaction",
          " -- ",
          el
        );
      } else if (action == "conversation_response") {
        //data.client_actions = list of actions
        console.log("EVENT DATA", data);

        el = channel_name + " - " + data.title + pathname;
        _td_submit("click", "DaveAvatar_Query", el);
        console.log(
          "tdSubmit",
          "click",
          " -- ",
          "DaveAvatar_Query",
          " -- ",
          el
        );

        //Scroll to sections
        if (data["data"]["client_actions"]) {
          let actions = data["data"]["client_actions"];
          for (let i = 0; i < actions.length; i++) {
            let act = actions[i];
            console.log("ACT LOG", act);

            if (act.action == "redirect") { //redirect
              //act.to = "https://google.com";
              window.open(act.to);
            }
            else if (act.action == "scroll") {
              djq('html, body').animate({
                scrollTop: djq(act.to).offset().top
              }, 2000);
              djq(act.to).append("<div class='highlight'></div>");

              setInterval(function () {
                djq(act.to).find(".highlight").remove();
              }, 5000);
            }
          }
        }

        //Show-Hide the whiteboard
        if (data["whiteboard"] == null)
          djq("#whiteboard").hide();
        else djq("#whiteboard").show();

      } else if (action == "hotword" || action == "person_detected") {
        djq(ele).trigger("click");
      }
      else if (action == "resume") {
        djq("#dave_conversation__").show();
      }

      //interaction,
      console.log("Action callback", data, action, el);
    }
    function _session_end() {
      console.log("print session is expired----------------------");
      dci.updateNewUser();
    }
    function _onload(data) {
      console.log("Loaded Callback", data);
      djq("#loaded_whiteboard").show();

      setTimeout(function () {
        if (loading_gif) {
          loading_gif.remove();
        }
      }, 2000);
      loaded_avatar = true;
      if (successC) {
        successC();
      }
      // dci.startWakeupStream();
      // dci.startImageStream();
    }

    setTimeout(function () {
      var ob = {
        enterprise_id: "maruti_core",
        event_type: "deployment",
        event_id: event_id,
        host_url: DAVE_BASE_URL,
        babylon_url: unity_url,
        camera_obj: createCamara,
        scene_obj: createScene,
        engine_obj: () => {
          engine = createEngine();
          return engine
        },
        type: "popup",
        signup_apikey: "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
        recognizer_type: "google",
        session_model: "person_session",
        avatar_id: deployment_object["model_name"] || "dave",
        avatar_model_name: "dave-english-male",
        default_placeholder: "Type here",
        interaction_model: "interaction",
        voice_id: "english-male",
        language: "english",
        signup_params: _signup,
        additional_conversation_info: _conv,
        additional_session_info: {
          conversation_id: conversation_id,
          deployment_scenario: deployment_scenario,
          location_dict: "{agent_info.ip}",
          browser: "{agent_info.browser}",
          os: "{agent_info.os}",
          device_type: "{agent_info.device}",
          interaction_origin: window.location.origin
        },
        conversation_id: conversation_id,
        additional_interaction_info: {
          conversation_id: conversation_id,
          deployment_scenario: deployment_scenario,
          product_id: product_id,
          _async: true
        },
        user_id_attr: "user_id",
        speech_recognition_url: speech_url,
        wakeup_url: "https://wakeup.iamdave.ai",
        image_recognition_url: "https://image.iamdave.ai",
        session_time: 50,
        minimize: true,
        session_expired_callback: _session_end,
        actionCallbacks: _actions,
        conversationLoadedCallback: _onload,
        environ: "babylon"
      };

      // if (loading_whiteboard) {
      //   ob["loading_whiteboard"] = loading_whiteboard;
      // }
      // if (default_whiteboard) {
      //   ob["default_whiteboard"] = default_whiteboard;
      // }

      if (default_bg) {
        ob["default_bg"] = default_bg;
      }

      if (avatar_id || deployment_object["model_name"]) {
        ob["default_avatar_id"] = avatar_id || deployment_object["model_name"];
      }

      var obz = djq.extend(ob, settings || {});

      var loading_gif_img = null;
      if (djq.browser.mobile) {
        loading_gif_img = obz["loading_mobile_gif"] || loading_mobile_img;
      } else {
        loading_gif_img = obz["loading_gif"] || loading_img;
      }

      dci = new BabylonConversation(
        obz,
        function (sts) {
          djq("#loaded_whiteboard").show();
        },
        function (sts, error) {
          alert(error);
        }
      );
      djq(ele).show();

      djq(ele).click(function () {
        // dci.stopWakeupStream(true);
        // dci.stopImageStream(true);
        if (loaded_avatar) {
          dci.resume();
          if (loading_gif) {
            loading_gif.remove();
          }
        } else {
          loadGif(loading_gif_img);
        }
        dci.set("minimize", false);
      });

      return dci;
    }, 5000);
  };
  // djq("#loaded_whiteboard").show();
  // djq("#loaded_whiteboard").click(function(){

  // })
  djq(window).resize(function () {
    set_cc();
  });
  function click_() {
    if (dci) {
      dci.set("minimize", false);
    }
  }
  // djq( window ).scroll(click_);
  djq(window).click(click_);

  djq("body").append(
    "<div id='loaded_whiteboard' class='arena'><div style='width:100%; height: 100%;'><img src='//d3nxn5bl526zn7.cloudfront.net/maruti_brands/arena/dave_init.webp' width='100px' style='cursor: pointer'></div></div>"
  );
  daveConversationSetup(djq("#loaded_whiteboard"), {});
  set_cc.resize = [-80, -35];
})();
