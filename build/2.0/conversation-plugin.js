
var _empty_audio_played = false;
var _interacted = false; 
(function (djq) {
    var playBeep = (function beep() {
        var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
        return function() {
            snd.play();
        }
    })();
    
    var playEmptyAudio= (function empty(){
        
        return function(){
            if(_empty_audio_played)return;
            var snd = new Audio("data:audio/wav;base64,UklGRjIAAABXQVZFZm10IBIAAAABAAEAQB8AAEAfAAABAAgAAABmYWN0BAAAAAAAAABkYXRhAAAAAA==");
            snd.load();
            snd.addEventListener('canplaythrough',function(){
                console.log("Played empty audio")
                if(_empty_audio_played) return;
                snd.play();
            })
            _empty_audio_played=true;
        }
    })();
    djq("body").one("click", function () {
        _interacted = true;
        playEmptyAudio();
    })
})(djq);
function set_cc(noresize) {
    var x = window.innerWidth;
    let canvas = djq("#unityContainer");
    if(window.innerWidth < 767){
        canvas.css("max-height", "50%");
        let s = djq( "#unityContainerP" ).children().first().offset()['top'] - djq( ".dave-chat").offset()['top'] ;
        djq( ".chat-header").css("top", s + "px")
        djq( ".dave-whiteboard").css("height", s+"px");
        djq( ".options-panel.image img").css("height", s+"px");
        canvas.css("width", 2*x + "px");
        canvas.css("height", x + "px");
        // canvas.css("bottom", "67px");
        canvas.css("right", "-35%");
        canvas.css("position", "absolute");
    } else {
        djq( ".chat-header").css("top", "unset")
        canvas.css("width", "100%");
        canvas.css("height", "100%");
    }
    if ( (!noresize) && set_cc.resize && Array.isArray(set_cc.resize) )  {
        reset_size.apply({}, set_cc.resize)
    }
}
function reset_size(offsetX, offsetY, scaleX, scaleY, call){
    offsetX = offsetX || 0;
    offsetY = offsetY || 0;
    scaleX = scaleX || 1;
    scaleY = scaleY || 1;
    try{
        if(window.innerWidth < 767){
            set_cc(true);
            setTimeout(function(){
                let canvas = document.getElementById("unityContainer");
                canvas.GLctxObject.GLctx.viewport(offsetX, offsetY, scaleX*canvas.width, scaleY*canvas.height);
                set_cc(true);
            }, 3000);
        } else if (window.innerWidth < window.innerHeight ){
            set_cc(true);
            setTimeout(function(){
                let canvas = document.getElementById("unityContainer");
                canvas.GLctxObject.GLctx.viewport(offsetX, offsetY, scaleX*canvas.width, scaleY*canvas.height);
            }, 3000);
        }    
    }
    catch(e){}
}

function getUrlParams(qd){
    qd = qd || {};
    if (location.search) location.search.substr(1).split("&").forEach(function (item) {
        var s = item.split("="),
            k = s[0],
            v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
        //(k in qd) ? qd[k].push(v) : qd[k] = [v]
        (qd[k] = qd[k] || []).push(v) // null-coalescing / short-circuit
    })
    return qd;
}

function Trim(strValue) {
    return strValue.replace(/^\s+|\s+$/g, '');
}

function replaceAll(strValue, matchval, replaceval="") {
    while(strValue.indexOf(matchval) > -1){
        strValue = strValue.replace(matchval, replaceval);
    }
    return strValue;
}


function _getCookie(key) {
    var result = null;
    if(document.cookie) {
    var mycookieArray = document.cookie.split(';');
    for(i=0; i<mycookieArray.length; i++) {
        var mykeyValue = mycookieArray[i].split('=');
        if(Trim(mykeyValue[0]) == key) result = mykeyValue[1];
    }
    }
    try{
    return JSON.parse(result);
    }catch(err){
    if(result) {
        return result;
    }
    }
    return null;
}

var deleteAllCookies = function() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substring(0, eqPos) : cookie;
        Utills.setCookie(name, "", -1);
    }
};

function _setCookie(key, value, hoursExpire) {
    if ( hoursExpire === undefined ) {
        hoursExpire = 24
    }
    var ablauf = new Date();
    var expireTime = ablauf.getTime() + (hoursExpire * 60 * 60 * 1000);
    ablauf.setTime(expireTime);
    if ( typeof value == "object" ) {
        value = JSON.stringify(value);
    }
    document.cookie = key + "=" + value + "; expires=" + ablauf.toGMTString() + "; path=/";
}
function loadScript(url, callback){
    var srpt = djq("<script />");
    srpt.attr("type", "application/javascript");
    srpt.attr("src", url);
    srpt.attr("async", false);
    srpt.attr("defer", false);
    djq("body").append(srpt);
    srpt.ready(callback);
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


function getFormattedDate(dt){
    if(typeof dt == "string"){
        dt = new Date(dt);
    }
    return dt.getDate()+"-"+(dt.getMonth()+1)+"-"+dt.getFullYear();
}


const InteractionStageEnum = Object.seal({
    OPENED:"opened",
    AUTO_OPENED: "auto_opened",
    SPEECH_INPUT: "speech-input", 
    MIC_ALLOWED :"mic_allowed", 
    MIC_REJECTED: "mic_rejected",
    MIC_REQUESTED:"mic_requested",
    CLICK: "click",
    TEXT_INPUT: "text-input",
    UNMUTED:"unmuted",
    MUTED:'muted',
    MINIMIZED:"minimized",
    RESUMED: "resumed",
    LEAVE_PAGE:"leave_page",
    set: function(key, val){
        this[key] = val;
    }
});


var isTouch = (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));

class Utills {
    static isTouch = isTouch;
    static makeid = makeid;
    static loadScript = loadScript;
    static getCookie = _getCookie;
    static setCookie = _setCookie;
    static getFormattedDate = getFormattedDate;
    static deleteAllCookies = deleteAllCookies;
    static cookie = {
        getCookie : _getCookie,
        setCookie : _setCookie
    };
    static stringOp = {
        trim: Trim,
        replaceAll: replaceAll
    }
}


//Jquery add ons
(function(djq){
    function isMobile() {
        try{ document.createEvent("TouchEvent"); return true; }
        catch(e){ return false; }
    }
    djq.browser = {};
    djq.browser.mobile = isMobile();


    djq.makeOverlayGuide = function(settings){
        var ck = Utills.getCookie("overlayGuide");
        if(ck && ck["hints"]) return;
        var hints = settings["hints"];
        var arrow = settings["arrow"] || "http://d3chc9d4ocbi4o.cloudfront.net/arrow_guide.svg";
        var div = djq("<div />");
        if(djq.browser.mobile){
            div.attr("style", 'color: white; position: fixed;top: 0;left: 0;height: 100%;width: 100%;background: black;z-index: 100000;opacity: 0.5; font-size:15px');
        }else{
            div.attr("style", 'color: white; position: fixed;top: 0;left: 0;height: 100%;width: 100%;background: black;z-index: 100000;opacity: 0.5; font-size:20px');
        }
        var a = djq("<a href='javascript:void(0)' style='position: absolute; top: 10px; right: 10px; color: white;'>");
        a.html("Skip >");
        a.click(function(){
            div.remove();
            removed = true;
        });
        var removed = false;
        div.append(a);
        if(!settings["stepwise"]){
            div.click(function(){
                div.remove();
                removed = true;
            })
        }
        setTimeout(function(){
            if(!removed){
                div.remove();
            }
        }, 5000)
        djq("body").append(div);
        for(var i in hints){
            var offset = djq("#"+hints[i]["id"]).offset();
            if(!offset){
                continue;
            }
            var span = djq("<span style='position: absolute;'/>");
            var message;
            if(typeof hints[i]["hint"] == "object"){
                if(djq.browser.mobile){
                    message = hints[i]["hint"]["mobile"];
                }else{
                    message = hints[i]["hint"]["web"];
                }

            }else{
                message = hints[i]["hint"];
            }
            span.html("<b style='margin-left: -200px;margin-right: 30px; text-align: right; display: block;'>"+message+"</b>");
            var img = djq("<img src='"+arrow+"' style='height: 100px'/>");
            
            span.append(img);
            div.append(span);
            if(hints[i]["left"]){
                span.css("left", hints[i]["left"]);
            }else if((offset["left"] - djq("#"+hints[i]["id"])[0].offsetWidth/2) < 0){
                span.css("left", (offset["left"] + djq("#"+hints[i]["id"])[0].offsetWidth/2)+"px");
            }else{
                span.css("left", (offset["left"] - djq("#"+hints[i]["id"])[0].offsetWidth/2 )+"px");
            }
            span.css("top", (offset["top"] - (span.height() + djq(window).scrollTop() - 10)) +"px");
        }
        Utills.setCookie("overlayGuide", {"hints": "off"});
    }
})(djq);


(function(djq){
    djq.fn.enter = function(callback){
        var calb = djq.Callbacks();
        calb.add(callback);

        djq(this).keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                console.debug(djq(this).val(), "Entered")
                console.debug(calb);
                calb.fire(djq(this).val());
            }
        });
    }
    djq.fn.fit_text = function(text, min_size=null){
        let fontSize = 16;
        if( window.innerWidth < 1110 ){
            fontSize = 14;
            min_size = min_size || 10;
        }else{
            min_size = min_size || 13;
            fontSize = 16;
        }
        var list_text = text.split("|");
        var len = list_text[0];
        for(var i of list_text){
            if(i.length > len.length){
                len = i;
            }
        }
        var span = djq("<span style='width: 100%;font-size:"+fontSize+"px;'/>");
        span.html(len);
        let el = djq(this);
        el.html(span);
        let l = el.parent().parent().height() - 10;
        for (let i = fontSize; i >= min_size; i--) {
            span.css("font-size", fontSize+"px");
            if ( l >= (span.height() * list_text.length) ) {
                break
            }
            fontSize--;
        }
        el.html("");
        for(var i of list_text){
            var span = djq("<span style='width: 100%'/>");
            span.css("font-size", fontSize+"px");
            span.html(i);
            el.append(span);
        }
    }
    djq.fn.placeholder_effect = function(time){
        return;
        console.log(djq(this).html(), time);
        if(time == undefined){
            djq(this).find("span").each(function(){
                if(djq(this).attr("backup-text")){
                    djq(this).text(djq(this).text()+(" "+djq(this).attr("backup-text")));
                }
                djq(this).attr("backup-text", "");
                djq(this).show();
            })
            return;
        }
        var count = 0 ;
        djq(this).find("span").each(function(){
            for(var z of djq(this).text().split(" ")){
                count = count + z.length;
            }
            djq(this).attr("backup-text", djq(this).text());
            djq(this).hide();
            djq(this).text("");
        });
        var ms = (time * 1000) / (count + 10);
        var cur = 1;
        var el = djq(this);
        function show_next_word(){
            var span = el.find("span:nth-child("+cur+")");
            if(span.length == 0){
                if(span.attr("backup-text")){
                    span.text(
                        (span.text()+" "+span.attr("backup-text")).trim()
                    );
                    span.attr("backup-text", "");
                }
                return;
            }
            var sent = (span.attr("backup-text") || "").split(" ");
            const firstWord = sent.shift();
            if(firstWord){
                span.text(
                    (span.text()+" "+firstWord).trim()
                );
            }
            span.show();
            if(sent.length == 0){
                cur++;
            }
            span.attr("backup-text", sent.join(" "));
            setTimeout(show_next_word, ms*firstWord.length);
        }
        show_next_word();
    }
})(djq);

function encodeFlac(binData, recBuffers, isVerify, isUseOgg){
    var ui8_data = new Uint8Array(binData);
    var sample_rate=0,
    channels=0,
    bps=0,
    total_samples=0,
    block_align,
    position=0,
    recLength = 0,
    meta_data;
    
    function write_callback_fn(buffer, bytes, samples, current_frame){
        recBuffers.push(buffer);
        recLength += bytes;
        // recLength += buffer.byteLength;
    }
    
    function metadata_callback_fn(data){
        console.info('meta data: ', data);
        meta_data = data;
    }
    
    
    var wav_parameters = wav_file_processing_read_parameters(ui8_data);	
    // convert the PCM-Data to the appropriate format for the libflac library methods (32-bit array of samples)
    // creates a new array (32-bit) and stores the 16-bit data of the wav-file as 32-bit data
    var buffer_i32 = wav_file_processing_convert_to32bitdata(ui8_data.buffer, wav_parameters.bps, wav_parameters.block_align);
    
    if(!buffer_i32){
        var msg = 'Unsupported WAV format';
        console.error(msg);
        return {error: msg, status: 1};
    }
    
    var tot_samples = 0;
    var compression_level = 5;
    var flac_ok = 1;
    var is_verify = isVerify;
    var is_write_ogg = isUseOgg;
    
    var flac_encoder = Flac.create_libflac_encoder(
        wav_parameters.sample_rate, 
        wav_parameters.channels, 
        wav_parameters.bps, 
        compression_level, 
        tot_samples, 
        is_verify
    );
    if (flac_encoder != 0){
        var init_status = Flac.init_encoder_stream(flac_encoder, write_callback_fn, metadata_callback_fn, is_write_ogg, 0);
        flac_ok &= init_status == 0;
        console.debug("flac init: " + flac_ok);
    } else {
        Flac.FLAC__stream_encoder_delete(flac_encoder);
        var msg = 'Error initializing the decoder.';
        console.error(msg);
        return {error: msg, status: 1};
    }
    
    
    var isEndocdeInterleaved = true;
    var flac_return;
    if(isEndocdeInterleaved){		
        flac_return = Flac.FLAC__stream_encoder_process_interleaved(
            flac_encoder, 
            buffer_i32, buffer_i32.length / wav_parameters.channels
            );
    } else {	
        var ch = wav_parameters.channels;
        var len = buffer_i32.length;
        var channels = new Array(ch).fill(null).map(function(){ return new Uint32Array(len/ch)});
        for(var i=0; i < len; i+=ch){
            for(var j=0; j < ch; ++j){
                channels[j][i/ch] = buffer_i32[i+j];
            }
        }
        
        flac_return = Flac.FLAC__stream_encoder_process(flac_encoder, channels, buffer_i32.length / wav_parameters.channels);
    }
    
    if (flac_return != true){
        console.error("Error: FLAC__stream_encoder_process_interleaved returned false. " + flac_return);
        flac_ok = Flac.FLAC__stream_encoder_get_state(flac_encoder);
        Flac.FLAC__stream_encoder_delete(flac_encoder);
        return {error: 'Encountered error while encoding.', status: flac_ok};
    }
    
    flac_ok &= Flac.FLAC__stream_encoder_finish(flac_encoder);
    
    Flac.FLAC__stream_encoder_delete(flac_encoder);
    
    return {metaData: meta_data, status: flac_ok};
}
function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}
function _isVerify() {
    return true;
}
function _isUseOgg() {
    return false;
}

function extendData(data, extend_object){
    extend_object = extend_object || {};
    data = data || {};
    let myPromise = new Promise(function(myResolve, myReject) {
        if(typeof extend_object == 'function'){
            var extend_data = extend_object(data);
            if(typeof extend_data == "object"){
                myResolve(extend_data);
            }else{
                myReject("Failed to extend object");
            }
        }else if(typeof extend_object == 'string'){
            djq.getJSON(extend_object, function(exd){
                var d = {...data, ...exd}
                myResolve(d);
            }, function(err){
                myReject(err);
            })
        }else{
            var d = {...data, ...extend_object}
            myResolve(d);
        }
    });
    return myPromise;
}

function todata(data, extend_object, callback){
    if(typeof extend_object == 'function'){
        extend_object(data, function(data){
            callback(data);
        })
    }else{
        var d = {...data, ...extend_object}
        callback(d);
    }
}


function detectVoice(stream, start, end) {
	// Create MediaStreamAudioSourceNode
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
	audioContext = new AudioContext();

    var source = audioContext.createMediaStreamSource(stream);
    // Setup options
    
    var options = {
        source: source,
        energy_threshold_ratio_neg: 0.3,
        stop_timeout: 1500,
        voice_stop: function() {
            console.debug('voice_stop');
            end();
        }, 
        voice_start: function() {
            console.debug('voice_start');
            start();
        }
    }; 

    // Create VAD
    var vad = new VAD(options);
}



class WhiteBoard{
    constructor(options){
        //class Whiteboard
        this.id = options.white_board_id;
        this.ele = djq("<div />");
        this.ele.addClass(options.class || 'dave-whiteboard');
        
        this.defaultBody = options.html || '';
        this.callbacks = djq.Callbacks();
        this.eventCallbacks = djq.Callbacks();
        
        this.ele.on("scroll", {"type": "scroll"}, function(e){
            e.data["position"] = djq(this).scrollTop();
            this.fire(e);
        })
    }
    
    html(html){
        //class Whiteboard
        if(html){
            var comp = djq(html); 
            var that = this;
            comp.find(".dave-video").each(function(){
                var res = {};
                var events = (djq(this).attr("data-key") || "").split(",");
                //var events = (djq(this).attr("data-events") || "").split(",");
                if(!djq(this).attr("data-key")){
                    throw "dave-action defined without data-key";
                }
                for(var ev of events){
                    var ev_mp = ev.split(":");
                    res["key"] = ev_mp[0];
                    res["type"] = ev_mp[1];
                    var ls_t = 0;
                    djq(this).bind(ev_mp[1], res, function(e){
                        if(ev_mp[1] == "timeupdate"){
                            e.data.value = djq(this)[0].currentTime;
                            var t = Math.floor(djq(this)[0].currentTime);
                            if( t%2 == 0 ){
                                if(t != ls_t){
                                    that.fire(e, true);
                                }
                                ls_t = t;
                            }
                        }else if(ev_mp[1] == "ended"){
                            e.data.value = "ended";
                            that.fire(e, true);
                        }else if(ev_mp[1] == "pause"){
                            e.data.value = djq(this)[0].currentTime;
                            that.fire(e, true);
                        }
                    });
                }
                djq(this).on('click', res, function(e){
                    e.data.value = djq(this).attr("data-value")
                    that.fire(e);
                });
            })
            
            comp.find(".dave-click").each(function(){
                var res = {};
                res["key"] = djq(this).attr("data-key");
                res["value"] = djq(this).attr("data-value");
                res["title"] = djq(this).attr("data-title");
                if(!res["key"] && !res["value"]){
                    throw "dave-action defined without data-key or data-value";
                }
                res["type"] = "clicked";
                djq(this).on('click', res, function(e){
                    e.data.value = djq(this).attr("data-value")
                    that.fire(e);
                })
            })

            comp.find(".dave-url").each(function(){
                var res = {};
                res["key"] = djq(this).attr("data-key");
                res["value"] = djq(this).attr("data-value");
                res["url"] = djq(this).attr("href");
                if(!res["key"] && !res["value"]){
                    throw "dave-action defined without data-key or data-value";
                }
                res["type"] = "url_click";
                djq(this).on('click', res, function(e){
                    e.data.value = djq(this).attr("data-value")
                    that.fire(e, true);
                })
            })

            comp.find("form.dave-form").each(function(){
                djq(this).on('submit', {"type": "form", "key": djq(this).attr("data-key"),"title":djq(this).attr("data-title")},  function(e){
                    var dt = djq(this).serialize()
                    console.debug(dt);
                    var urs = new URLSearchParams(dt);
                    var obx = {};
                    var ks = urs.keys();
                    var nxt = ks.next();
                    while(!nxt.done){
                        obx[nxt.value] = urs.get(nxt.value);
                        nxt = ks.next();
                    }
                    console.debug(obx);
                    //const data = Object.fromEntries(new URLSearchParams(dt));;
                    e.data["value"] = obx;
                    that.fire(e);
                })
            })
            
            this.defaultBody = html;
            this.ele.append(comp);
        }
        return this.ele;
    }
    
    watch(callback){
        //class Whiteboard
        this.callbacks.add(callback);
    }
    watchEvents(callback){
        //class Whiteboard
        this.eventCallbacks.add(callback);
    }    
    fire(data, eventType){
        //class Whiteboard
        if(eventType){
            this.eventCallbacks.fire(data);
        }else{
            this.callbacks.fire(data);
        }
    }    
    
    unWatch(){
        //class Whiteboard
        this.callbacks.empty();
    }
}



class Message{
    constructor(message, position="left", title= "Dave.Ai", time=null){
        //class Message
        if(typeof message== "object"){
            this.message = "Form submited successfully";
        }else{
            this.message = message;
        }
        this.position = position;
        this.title = title || "";
        if(time){
            var tt = typeof time == "string" ? new Date(time) : time;
            if(getFormattedDate(new Date()) === getFormattedDate(tt)){
                this.time = tt.getHours()+":"+tt.getMinutes();
            }else{
                this.time = getFormattedDate(tt)+" "+tt.getHours()+":"+tt.getMinutes();
            }
        }else{
            this.time = "";
        }
        this.ele = djq("<li class='message "+this.position+"'/>");
        this.makeElement()
    }
    
    highlight(str){
        //class Message
        str = str? str: "";
        var msg = this.message.toLowerCase();
        if(str){ 
            var ls = msg.split(str);
            msg = ls.join( "<span class='highlight'>"+str+"</span>")
        }
        this.makeElement(msg);
    }
    
    makeElement(str){
        //class Message
        str = str || this.message;
        var inner = djq(`<div style="display: table;">
        <div style="display: table-row;">
        <div class="text_wrapper" style="display: table-cell">
        <div class="text">${str}</div>
        </div>
        </div>
        <p style="margin-left: 5px;display: table-row;">
        <small style="float: left; margin-right: 10px;">${this.title}</small>
        <small style="float: right;">${this.time}</small>
        </p>
        </div>`);
        if(this.position == "right"){
            inner.css("float","right");
        }
        this.ele.empty().html(inner);
        return this.ele;
    }
    html(){
        //class Message
        return this.ele;
    }
}


var product_obj,
unityInstance,
build_type = "unity";
function onPictureTaken(ImageBase64) {
  console.log(ImageBase64);
  
  if (!window.__dv_cbs["DownloadSelfy"]) {
    var a = document.createElement("a");
    a.href = ImageBase64;
    a.download = new Date().toISOString() + ".png";
    a.click();
  } else {
    window.__dv_cbs_get("DownloadSelfy").fire(ImageBase64);
  }
}
function keyaLoadedCallback() {
  console.log("Keya loaded on page");
  keya_loaded = true;
  window.__dv_cbs_get("KeyaLoadedCallback").fire();
}
function assistantLoadedCallback() {
  console.log("Assistant loaded on page");
  keya_loaded = true;
  window.__dv_cbs_get("AssistantLoadedCallback").fire();
}
function receiveMessageFromUnity(id) {
  var aud = document.getElementById("audio");
  console.debug("--------", id);
  if (aud.getAttribute("data-audio-id") != id) {
    unityInstance.stopTalking();
    // try{
    //     unityInstance.SendMessage("head", "stopTalking")
    // }catch(e){
    //     console.log("Were not able to do stop talking", e);
    // }
    return;
  }
  if (aud) {
    console.log(
      "Audio duration :: ",
      aud.duration,
      "Audio currentTime :: ",
      aud.currentTime
      );
      var muted = aud.getAttribute("force-mute") || "false";
      console.debug("Audio muted ::", muted);
      if (muted == "false") {
        aud.muted = false;
      } else {
        aud.muted = true;
      }
      // aud.play();
      const promise = aud.play();
      
      if (aud.getAttribute("defaults") == "false") {
        djq("#message").placeholder_effect(aud.duration);
      }
      if (!aud.onended) {
        aud.onended = function() {
          var audio = document.getElementById("audio");
          if (audio.getAttribute("data-audio-id") != id) {
            unityInstance.stopTalking();
            djq("#message").placeholder_effect();
          }
          // try{
          //     unityInstance.SendMessage("head", "stopTalking")
          // }catch(e){
          //     console.log("Were not able to do stop talking", e);
          // }
        };
      }
      if (promise !== undefined) {
        promise
        .then(() => {
          console.debug("Got the audio load promise here");
        })
        .catch(function(err) {
          console.log(err);
          // try{
          //     unityInstance.SendMessage("head", "stopTalking")
          // }catch(e){
          //     console.log("Were not able to do stop talking", e);
          // }
          aud.onended();
          window.__dv_cbs_get("AudioPlayFailed").fire();
        });
      }
    } else {
      console.log("Aud is empty");
    }
  }
  
  var default_bg,
  defaul_avatar_id = "dave";
  var screen_loading = true;
  function sceneLoadedCallback(params) {
    console.log("scene loaded callback");
    screen_loading = false;
    if (params) { 
      window.__dv_cbs_get("SceneLoaded").fire(params);
      return;
    }
    if (default_bg) {
      unityInstance.setTexture(default_bg);
    }
    if (build_type == "unity") {
      unityInstance.showPersona(defaul_avatar_id);
    }
    
  }
  
  function avatarLoadedCallback() {
    unityInstance.loadedchar = true;
    window.__dv_cbs_get("AvatarLoaded").fire();
  }
  
  function daveSceneLoaded() {
    //sceneLoadedCallback();
  }
  
  function enteredAreaCallback(type, value) {
    if (type == "cs") {
      window.__dv_cbs_get("enteredArea").fire(value);
    } else if (type == "point") {
      window.__dv_cbs_get("pointReached").fire(value);
    } else {
      window.__dv_cbs_get("enteredArea-" + type).fire(value);
    }
  }
  function requestData() {
    window.__dv_cbs_get("unityDataRequest").fire();
  }


function onCallback(jsString) { 
  var jsn = JSON.parse(jsString);
  console.debug("onCallback from unity", jsn);

  if (jsn["action"] == "SceneLoaded") {
    sceneLoadedCallback(jsString);
  } else { 
    window.__dv_cbs_get("UnityActionRequest").fire(jsn);
  }
}
  
  class UnityManager {
    constructor(unityInstance, settings) {
      this.unityInstance = unityInstance;
      this.setAvatar = false;
      this.head_name = settings["head_name"] || "head";

      this.dynamic_unity = settings["dynamic_unity"] || false;

      this.keyboard_enable = false;
      this.joystick_enable = false;
      this.muted = false;
    }
    updateSettings() {
      var sets = {
        "action": "ChangeSettings",
        "mute": this.muted,
        "keyboardActive": this.keyboard_enable,
        "joystick": this.joystick_enable
      }
      console.debug("Updating Settings ::: ", sets)
      this.callGameManagerFunction("UnityFunction", JSON.stringify(sets));
    }
    keyboardControlEnable() {
      console.log("keyboard control enabled");
      if (this.dynamic_unity) { 
        this.keyboard_enable = true;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "keyboardControlEnable",
          ""
        );
      } catch (e) {
        console.warn("Unable to enable keyboard controls", e);
      }
    }
    keyboardControlDisable() {
      console.log("keyboard control disabled");
      if (this.dynamic_unity) { 
        this.keyboard_enable = false;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "keyboardControlDisable",
          ""
        );
      } catch (e) {
        console.warn("Unable to disable keyboard controls", e);
      }
    }
    joystickControlEnable() {
      console.log("joystick control enable");
      if (this.dynamic_unity) { 
        this.joystick_enable = true;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "joystickControlEnable",
          ""
        );
      } catch (e) {
        console.warn("Unable to enable joystick controls", e);
      }
    }
    joystickControlDisable() {
      console.log("joystick control disabled");
      if (this.dynamic_unity) { 
        this.joystick_enable = false;
        this.updateSettings();
        return
      }
      try {
        this.unityInstance.SendMessage(
          "GameManagerOBJ",
          "joystickControlDisable",
          ""
        );
      } catch (e) {
        console.warn("Unable to disable joystick controls", e);
      }
    }
    stopTalking() {
      console.log("Trying to Stop talking");
      try {
        this.unityInstance.SendMessage(this.head_name, "stopTalking");
      } catch (e) {
        console.warn("Unable stop talking", e);
      }
    }
            
    startTalking(id, frames, shapes) {
      console.log("Trying to Start talking ::", id);
      try {
        this.unityInstance.SendMessage(
          this.head_name,
          "StartTalking",
          JSON.stringify({
            frames: frames,
            shapes: shapes,
            audio_id: id
          })
        );
      } catch (e) {
        console.warn("Unable to make avatar talk");
      }
    }
    showPersona(id) {
      console.log("Trying to show persona ::", id);
      if (this.dynamic_unity) { 
        this.callGameManagerFunction("UnityFunction", JSON.stringify({"action":"ShowPersona","avatar_name":id}));

        return;
      }
      try {
        this.unityInstance.SendMessage("GameManagerOBJ", "showPersona", id);
        this.setAvatar = true;
      } catch (e) {
        console.warn("Were not able to update persona", e);
      }
    }
    setTexture(bg) {
      console.log("Setting texture :: ", bg);
      try {
        this.unityInstance.SendMessage("GameManagerOBJ", "setTexture", bg);
      } catch (e) {
        console.warn("Were not able to update texture", e);
      }
    }
    callGameManagerFunction(key, val) {
      console.log("Calling game manager object :: ", key, val);
      try {
        this.unityInstance.SendMessage("GameManagerOBJ", key, val);
      } catch (e) {
        console.warn(`Failed to call GameManagerOBJ function '${key}'`, e);
      }
    }
    muteBGM() {
      if (this.dynamic_unity) { 
        this.muted = true;
        this.updateSettings();
        return
      }
      this.callGameManagerFunction("mute", "");
    }
    unmuteBGM() {
      if (this.dynamic_unity) { 
        this.muted = false;
        this.updateSettings();
        return
      }
      this.callGameManagerFunction("unmute", "");
    }
    callAssistantFunction(key, val) {
      console.log("Calling assistant function :: ", key, val);
      this.unityInstance.SendMessage("Assistantcode", key, val);
    }
  }

class BabylonManager {
  constructor(model_path, engine, scene, camera, settings) {
    console.log("Settings in Babylon Manager", settings);
    this.engine = engine;
    this.scene = scene;
    this.camera = camera;
    this.talk = false;
    this.lastTime = 0;
    this.shapesIndex = 0;
    this.framesIndex = 0;
    this.snd = null;
    this.shapes = null;
    this.frames = null;
    this.headers = [];
    this.volume = settings["volume"] || 1.0;
    this.queue = [];
    
    this.settings = settings;
    this.setAvatar = true;
    this.head_name = settings["head_name"] || "head";
    this.loadScene(model_path);
    //this.sceneLoadedCallback();
    this.render();
  }
  
  loadScene(model_path) {
    model_path =
    "https://s3.ap-south-1.amazonaws.com/models.iamdave.ai/glb-models/dave_waist.glb";
    var that = this;
    BABYLON.SceneLoader.ImportMesh("", "", model_path, that.scene, function() {
      that.scene.createDefaultCameraOrLight(false, false, false);
      that.scene.animationGroups.forEach(function(animationGroup) {
        animationGroup.stop();
      });
      //that.sceneLoadedCallback();
    });
  }
  
  render() {
    var that = this;
    that.engine.runRenderLoop(function() {
      if (that.talk) {
        let shape = that.shapes[that.shapesIndex];
        let x = (Date.now() - that.lastTime) / 1000;
        if (!shape) {
          that.talk = false;
          that.shapesIndex = 0;
          that.framesIndex = 0;
          that.scene.render();
          return;
        }
        while (x > shape["timestamp"]) {
          console.debug("Went ahead");
          that.shapesIndex += 1;
          shape = that.shapes[that.shapesIndex];
          if (!shape) {
            that.talk = false;
            that.shapesIndex = 0;
            that.framesIndex = 0;
            that.scene.render();
            return;
          }
        }
        if (x >= shape["timestamp"] - 1.0 / 25.0 && x <= shape["timestamp"]) {
          that.setMorphTargets(shape);
          that.shapesIndex += 1;
        } else if (x < shape["timestamp"] - 1.0 / 25.0) {
          console.debug("Lagging behind");
        }
        var frame = that.frames[that.framesIndex];
        if (frame != null && x >= frame["timestamp"]) {
          that.playAnimation(frame);
          that.framesIndex += 1;
        }
        if (that.shapesIndex >= that.shapes.length) {
          that.talk = false;
          that.shapesIndex = 0;
          that.framesIndex = 0;
        }
      }
      that.scene.render();
    });
  }
  
  //Callback function
  sceneLoadedCallback() {
    
  }
  
  play(response) {
    //console.log("PLAY IS CALLING", response);
    var that = this;
    if (that.snd) {
      console.warn("Sound already playing");
      that.queue.push(response);
      return that.snd;
    }
    that.snd = new Audio(response["voice"]);
    that.shapes = that.processData(response["shapes"]);
    that.frames = that.processDataFrames(response["frames"]);
    that.shapesIndex = 0;
    that.framesIndex = 0;
    that.snd.volume = that.volume; //that.volume;
    that.snd.addEventListener("ended", function() {
      that.snd.pause();
      that.pauseAnimsandMorphs();
      that.snd = null;
      if (that.queue.length > 0) {
        that.play(that.queue.shift());
      }
    });
    that.snd.addEventListener("canplay", function() {
      //DAVE_SETTINGS.execute_custom_callback("on_canplay_audio", [snd]);
      
      that.execute_custom_callback("on_canplay_audio", [that.snd]);
      that.lastTime = Date.now();
      that.talk = true;
      try {
        that.snd.play();
      } catch (err) {
        console.error(err);
        that.snd = null;
      }
    });
    that.snd.addEventListener("pause", function() {
      that.pauseAnimsandMorphs();
    });
    return that.snd;
  }
  
  execute_custom_callback(str, param) {
    console.log("Hello from execute_custom_callback");
  }
  
  processData(allText) {
    var that = this;
    var allTextLines = allText.split(/\r\n|\n/);
    that.headers = allTextLines[0].split(",");
    var lines = [];
    for (var i = 1; i < allTextLines.length; i++) {
      var data = allTextLines[i].split(",");
      if (data.length == that.headers.length) {
        var tarr = {};
        for (var j = 0; j < that.headers.length; j++) {
          tarr[that.headers[j]] = parseFloat(data[j]);
        }
        lines.push(tarr);
      }
    }
    return lines;
  }
  
  processDataFrames(allText) {
    var that = this;
    var allTextLines = allText.split(/\r\n|\n/);
    that.headers = allTextLines[0].split(",");
    var lines = [];
    for (var i = 1; i < allTextLines.length; i++) {
      var data = allTextLines[i].split(",");
      if (data.length == that.headers.length) {
        var tarr = {};
        for (var j = 0; j < that.headers.length; j++) {
          console.debug("got ani data = " + that.headers[j] + " = " + data[j]);
          if (that.headers[j] == "timestamp") {
            tarr[that.headers[j]] = parseFloat(data[j]);
          } else {
            tarr[that.headers[j]] = data[j];
          }
        }
        lines.push(tarr);
      }
    }
    return lines;
  }
  
  setMorphTargets(shape) {
    var that = this;
    for (var key in shape) {
      if (key != "timestamp" && shape[key] != null) {
        var s = that.scene.getMorphTargetByName(key);
        if (s !== null) {
          s.influence = shape[key];
        }
      }
    }
  }
  
  pauseAnimsandMorphs() {
    var that = this;
    that.talk = false;
    that.stopMorphs();
    that.playIdleAnimation();
  }
  
  playAnimation(animationName) {
    var that = this;
    if (animationName["animation"] != null) {
      console.log("trying to play = " + animationName["animation"]);
      let ani = that.scene.getAnimationGroupByName(animationName["animation"]);
      if (ani) {
        try {
          ani.play(false);
          ani.loopAnimation = false;
          ani.isAdditive = true;
        } catch (error) {
          console.error(error);
        }
      }
    }
  }
  
  stopMorphs() {
    var that = this;
    if (that.headers != []) {
      for (i = 1; i < that.headers.length; i++) {
        var s = that.scene.getMorphTargetByName(that.headers[i]);
        if (s != null) {
          s.influence = 0.00000001;
        }
      }
    }
  }
  
  playIdleAnimation() {
    var that = this;
    let ani = null;
    that.scene.animationGroups.every(function(a) {
      if (a.name.match(/weight_shift/g) || a.name.match(/idle/g)) {
        ani = a;
        return false;
      }
      return true;
    });
    if (ani) {
      console.debug("trying to play idle animation");
      try {
        ani.play(true);
        ani.loopAnimation = true;
      } catch (error) {
        console.log(error);
      }
    }
  }
  
  muteAvatar() {
    var that = this;
    if (that.snd) {
      that.snd.volume = 0.0;
    }
    that.volume = 0.0;
  }
  
  unmuteAvatar() {
    var that = this;
    if (that.snd) {
      that.snd.volume = 1.0;
    }
    that.volume = 1.0;
  }
  
  muteBGM() {
    console.log("MUTE AVATAR");
    var that = this;
    that.muteAvatar();
    //this.callGameManagerFunction("mute", "");
  }
  
  unmuteBGM() {
    console.log("UNMUTE AVATAR");
    var that = this;
    that.unmuteAvatar();
    //this.callGameManagerFunction("unmute", "");
  }
  
  stopTalking() {
    var that = this;
    if (that.snd) {
      that.snd.pause();
      that.pauseAnimsandMorphs();
      that.snd = null;
      that.queue = [];
    }
    
    /*try {
      this.unityInstance.SendMessage(this.head_name, "stopTalking");
    } catch (e) {
      console.warn("Unable stop talking", e);
    }*/
  }
  
  startTalking(id, frames, shapes) {
    console.log("START TALKING ID", id);
    console.log("START TALKING frames", frames);
    console.log("START TALKING shapes", shapes);
    try {
      var that = this;
      // that.snd = new Audio(response["voice"]);
      that.shapes = that.processData(shapes);
      that.frames = that.processDataFrames(frames);
      that.shapesIndex = 0;
      that.framesIndex = 0;
      that.talk = true;
      that.lastTime = Date.now();
      receiveMessageFromUnity(id);
      return that.snd;
    } catch (e) {
      console.warn("Unable to make avatar talk");
    }
  }
  
  showPersona(id) {
    
  }
  
  setTexture(bg) {
    
  }
}

class SetupEnviron {
  static load(_b, settings) {
    if (build_type == "babylon") {
      var scene = null;
      var camera = null;
      var assetsManager = null;
      
      function init(t, myResolve, myReject) {
        var canvas = document.getElementById("unityContainer");
        var engine = new BABYLON.Engine(
          canvas,
          true,
          {
            timeStep: 1.0 / 10.0,
            deterministicLockstep: true,
            lockstepMaxSteps: 30,
            limitDeviceRatio: 3
          },
          true
        );
          
        var createScene = function() {
          scene = new BABYLON.Scene(engine);
          scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
          camera = new BABYLON.FreeCamera(
            "camera_dave",
            new BABYLON.Vector3(0, 1.5, 2),
            scene
          );
          camera.setTarget(new BABYLON.Vector3(0, 1.5, 0));
          camera.viewport = new BABYLON.Viewport(0, -1, 1, 2.75);
          //camera.attachControl(canvas, false);
          assetsManager = new BABYLON.AssetsManager(scene);
          return scene;
        };
          
        var onetime = 1;
        scene = createScene();
        
        unityInstance = new BabylonManager(_b, engine, scene, camera, settings);
        myResolve(unityInstance);
      }
        
      var prom = new Promise(function(myResolve, myReject) {
        init(3, myResolve, myReject);
      });
      prom.progress = handler => {
        notify = handler;
        return prom;
      };
      return prom;
        
      //this.loadScene(model_path, sceneLoadedCallback);
      // var that = this;
    } else {
      var progress = 0;
      var notify;
      var buildUrl = _b;
      
      if (settings && settings["default_bg"]) {
        default_bg = settings["default_bg"];
      } else if (
        settings &&
        settings["customizables"] &&
        settings["customizables"]["bg"]
      ) {
        default_bg = settings["customizables"]["bg"];
      } else if (settings && settings["space_id"]) {
        default_bg = settings["space_id"];
      }
      
      if (settings && (settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"])) {
          defaul_avatar_id = settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"];
      }
        
      function init(t, myResolve, myReject) {
        function UnityError(message, err) {
          console.log(message, err);
          //that.errorC("error", message);
          myReject(message);
        }
        function UnityLoaded(ut) {
          unityInstance = new UnityManager(ut, settings);
          myResolve(unityInstance);
        }
        function UnityProgress(prog) {
          if (prog == 1) {
            console.log("Model loaded");
          }
          progress = prog;
          console.debug("Loading..", progress);
          if (progress % 0.1) {
            console.log("Loading..", progress);
          }
          if (notify) {
            notify(progress);
          }
        }
        if (t == 0) {
          return;
        }
        try {
          var config = {
            dataUrl: buildUrl + "/ProjectBuild.data",
            frameworkUrl: buildUrl + "/ProjectBuild.framework.js",
            codeUrl: buildUrl + "/ProjectBuild.wasm",
            streamingAssetsUrl: "StreamingAssets",
            companyName: "SocioGraph Solution",
            productName: "Conversation Bot",
            productVersion: "0.1"
          };
          
          createUnityInstance(
            document.querySelector("#unityContainer"),
            config,
            UnityProgress
          )
          .then(UnityLoaded)
          .catch(function(a, b) {
            UnityError(a, b);
            init(t - 1, myResolve, myReject);
          });
            
          //that.unityInstance = UnityLoader.instantiate("unityContainer",  that.settings["project_build"] || e["project_build"] || "https://d3arh16v2im64l.cloudfront.net/static/conversation/ProjectBuild.json", {onProgress: UnityProgress, onerror: UnityError});
          //unityInstance = that.unityInstance;
          //that.con.unityInstance = that.unityInstance;
          //break
        } catch (e) {
          console.log("Cannot load unity due to error");
          console.log(e);
          if (t == 0) {
            alert("Cannot load 3d environment");
          }
          init(t - 1, myResolve, myReject);
        }
      }
      var prom = new Promise(function(myResolve, myReject) {
        init(3, myResolve, myReject);
      });
      prom.progress = handler => {
        notify = handler;
        return prom;
      };
      return prom;
    }
  }
  constructor() {}
}
/*
var ds = new DaveService("https://test.iamdave.ai", "fashion_fitting", {....});
ds.list("measurement", {"_sort_by": "priority", "measurement": "hips"}, function(data){}, function(e){});
ds.get("measurement", "<id>", function(data){}, function(e){});
ds.post("measurement", {....}, function(data){}, function(e){});
ds.update("measurement","<id>" ,{....}, function(data){}, function(e){});
ds.remove("measurement","<id>", function(data){}, function(e){});
ds.signup({}, function(data){}, function(e){})
*/


/**
 * @description Service class for accessing the dave api's
 * @example var ds = new DaveService("https://test.iamdave,ai", "dave_test",{"signup_key": "<SIGNUP-API-KEY>"});
 */
class DaveService{
    logout(callback){
        deleteAllCookies();
        callback();
    }
    
    /**
     * @description Can authenticate using this static function, if on success will retrun DaveService object else returns error
     * @example DaveService.login("https://test.iamdave,ai", "dave_test", "john@dave_test.com", "123456")
     * @param {string} enterprise_id 
     * @param {string} user_id 
     * @param {string} password 
     * @returns if on success will retrun DaveService object else returns error
     */
    static login(host, enterprise_id, user_id, password, params){
        if(!enterprise_id || !user_id || !password){
            throw "Credentials missing";
        }
        var ds;
        var err;
        djq.ajax({
            url: host+"/login",
            method: "POST",
            dataType: "json",
            contentType: "json",
            headers:{
                "Content-Type":"application/json"
            },
            async: false,
            data: JSON.stringify({
                "enterprise_id": enterprise_id,
                "user_id": user_id,
                "password": password
            }),
            success: function(data) {
                console.debug("Logged in successfully :: ", data);
                params["user_id"] = data["user_id"];
                params["api_key"] = data["api_key"];
                ds = DaveService.getInstance(host, enterprise_id, params);
                // ds = new DaveService(host, enterprise_id, params);
            },
            error: function(e) {
                console.log("Error While posting object :: ", e)
                err = e.responseJSON || e;
            }
        });
        if(ds){
            return ds;
        }else{
            throw err["error"] || err;
        }
    }

    /**
     * @description Service class for accessing the dave api's
     * @example var ds = new DaveService("https://test.iamdave,ai", "dave_test",{"signup_key": "<SIGNUP-API-KEY>"});
     * @param {string} host host url example - https://test.iamdave.ai
     * @param {string} enterprise_id 
     * @param {Object} settings extra perameters like user_id, signup_key, api_key
     */
    constructor(host,enterprise_id, settings){
        //super()
        this.authentication_cookie_key = settings["authentication_cookie_key"] || "authentication";
        this.headers =Utills.getCookie(this.authentication_cookie_key) || null;
        this.host = host;
        this.host_url = host;
        this.user_id_attr = settings["user_id_attr"] || "user_id";
        this.enterprise_id = enterprise_id;
        this.signp_key = settings["signup_apikey"];
        this.api_key = settings["api_key"];
        this.user_id = settings["user_id"];
        this.settings = settings;
        this.checkAuth();
    }
    static _instances = [];
    static getInstance(host,enterprise_id, settings) { 
        var signp_key = settings["signup_apikey"];
        var api_key = settings["api_key"];
        var user_id = settings["user_id"];
        var ob;
        for (var z in DaveService._instances) { 
            if (z.host == host && z.enterprise_id == enterprise_id && signp_key == z.signp_key && api_key == api_key && user_id == user_id) { 
                ob = z;
                break
            }
        }
        if (!ob) { 
            return new DaveService(host, enterprise_id, settings);
        }
        return ob;
    }
    checkAuth(){
        if (!this.enterprise_id)
            throw "'enterprise_id' is required"
        if (this.headers) {
            this.user_id = this.headers["X-I2CE-USER-ID"];
            this.api_key = this.headers["X-I2CE-API-KEY"];
        }
        if (!this.signp_key && (!this.user_id || !this.api_key)) {
            throw "signp_key or (user_id and api_key) are required";
        } else if (this.user_id && this.api_key) {
            this.headers = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
                "X-I2CE-USER-ID": this.user_id,
                "X-I2CE-API-KEY": this.api_key
            }
            Utills.setCookie(this.authentication_cookie_key, JSON.stringify(this.headers), 240);
            this.signin_required = false;
            console.debug("Signin required set as false :: got from cookies");
            
            var csm = this.settings["customer_model_name"] ||Utills.getCookie("customer_model_name");
            var prm = this.settings["product_model_name"] ||Utills.getCookie("product_model_name");
            var inm = this.settings["interaction_model_name"] ||Utills.getCookie("interaction_model_name");
            var that = this;
            if(!csm){
                this.getRaw("/models/core/name?model_type=customer", function(data){
                    that.customer_model_name = data[0];
                    Utills.setCookie("customer_model_name", data[0]);
                }, (err)=> console.error("unable to fetch customer model name", err), false)
            }else{
                Utills.setCookie("customer_model_name", csm, 240);
            }
            if(!prm){
                this.getRaw("/models/core/name?model_type=product", function(data){
                    that.product_model_name = data[0];
                    Utills.setCookie("product_model_name", data[0]);
                }, (err)=> console.error("unable to fetch product model name", err), false)
            }else{
                Utills.setCookie("product_model_name", prm, 240);
            }
            if(!inm){
                this.getRaw("/models/core/name?model_type=intraction", function(data){
                    that.interaction_model_name = data[0];
                    Utills.setCookie("interaction_model_name", data[0]);
                }, (err)=> console.error("unable to fetch intraction model name", err), false)
            }else{
                Utills.setCookie("interaction_model_name", inm, 240);
            }
        } else {
            this.signin_required = true;
        }
        Utills.setCookie("host", this.host, 240);

        
        return !this.signin_required;
    }
    /**
     * @description Create new object/row in the model/table
     * @example creating customer with name john, 26, m
     *  - ds.post({"name": "john", "age": 26, "gender": "m"}, function(data){...}, function(err){...})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {Object} data Json object keys represents the attribute/column names of the model/table 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have posted row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    post(model, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] || false;
        delete data["async"];
        var that = this;
        console.debug("Posting "+model+" object :: ", data );
        return this.postRaw("/object/" + model, data, successCallback, errorCallback);
    }
    //"/transaction/sms"
    postRaw(url, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] == undefined ? true : data["async"];
        delete data["async"];
        var that = this;
        return djq.ajax({
            url: this.host + url,
            method: "POST",
            dataType: "json",
            contentType: "json",
            async: async,
            withCredentials: true,
            headers: this.headers,
            data: JSON.stringify(data),
            success: function(data) {
                console.debug("Posted object :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While posting object :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Update existing object/row to the model/table
     * @example if you want to update phone number of a customer with id 1
     *  - ds.update(1, {"phone_number": <number>}, function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to update...
     * @param {object} data Json object keys represents the attribute/column names of the model/table 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback  Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have updated row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    update(model, object_id, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] || false;
        delete data["async"];

        var that = this;
        console.debug("Updating "+model+" object :: ", data );
        return djq.ajax({
            url: this.host + "/object/" + model + "/" + object_id,
            method: "PATCH",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            headers: this.headers,
            async: async,
            data: JSON.stringify(data),
            success: function(data) {
                console.debug("Updated object :: ", data);
                successCallback(data);
            },
            error: function (err, e) {
                err = err.responseJSON || err;
                console.log("Error While updating object :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Delete existing object/row to the model/table
     * @example To delete a customer with id 10
     * - ds.remove("customer", 10, function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to delete...
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have deleted row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    remove(model, object_id, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] || false;
        delete data["async"];
        var that = this;
        console.debug("Deleteing " + model + " object :: ", object_id);
        return djq.ajax({
            url: this.host + "/object/" + model + "/" + object_id,
            method: "DELETE",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            async: async,
            headers: this.headers,
            data: JSON.stringify({}),
            success: function (data) {
                console.debug("Deleted object :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While deleting object :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Update existing object/row to the model/table
     * @example if you want to update phone number of a customer with id 1
     *  - ds.update(1, {"phone_number": <number>}, function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to update...
     * @param {object} data Json object keys represents the attribute/column names of the model/table 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback  Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have updated row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    iupdate(model, object_id, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
        }
        var async = data["async"] || false;
        delete data["async"];
        var that = this;
        console.debug("Updating "+model+" object :: ", data );
        return djq.ajax({
            url: this.host + "/iupdate/" + model + "/" + object_id,
            method: "PATCH",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            async: async,
            headers: this.headers,
            data: JSON.stringify(data),
            success: function(data) {
                console.debug("Updated object :: ", data);
                successCallback(data);
            },
            error: function (err, e) {
                err = err.responseJSON || err;
                console.log("Error While updating object :: ", err)
                errorCallback(err);
            }
        })
    }

    sendBeacon(url, data){
        if(this.signin_required){
            throw "Auth required";
            
        }
        data["_i2ce_user"] = {
            "enterprise_id": this.enterprise_id,
            "user_id": this.user_id,
            "api_key": this.api_key
        }
        navigator.sendBeacon(this.host + url, data);
    }

    /**
     * @description Get an existing object/row of model/table
     * @example To get a customer with id of 1
     *  - ds.get("customer", 1,function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to get...
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have row/object with specific object_id
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    get(model, object_id, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";            
        }
        var that = this;
        console.debug("Getting " + model + " object :: ", object_id);
        var url = "/object/" + model + "/" + object_id;
        return this.getRaw(url, successCallback, errorCallback);
    }
    getRaw(url, successCallback, errorCallback, async=true){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var that = this;
        return djq.ajax({
            url: this.host + url,
            method: "GET",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            async: async != undefined ? async : true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got object :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting object :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Get list of attributes of a model/table
     * @example To get a customer model/table attributes 
     *  - ds.getAttributes("customer",function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a list if objects 
     *  //[
            {
                "required": false,
                "type": "name",
                "id": false,
                "name": "name"
            },
            {
                "required": false,
                "type": "mobile_number",
                "name": "phone_number"
            },
            ....
        ]
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    getAttributes(model, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var that = this;
        console.debug("Getting " + model + " attributes");
        return djq.ajax({
            url: this.host + "/attriutes/" + model,
            method: "GET",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got attributes :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting attributes :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Get structure of a model/table
     * @example To get a customer model/table attributes
     *  - ds.getModel("customer",function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a list if objects 
     * //{
            "name": "person"
            "attributes": [
            {
                "required": false,
                "type": "name",
                "id": false,
                "name": "name"
            },
            {
                "required": false,
                "type": "mobile_number",
                "name": "phone_number"
            },
            ....
        ],
        ....
    }
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
     getModel(model, successCallback, errorCallback) {
        if(this.signin_required){
            throw "Auth required";
            
        }
        var that = this;
        console.debug("Getting " + model + " model");
        return djq.ajax({
            url: this.host + "/model/" + model,
            method: "GET",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got model :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting model :: ", err)
                errorCallback(err);
            }
        })
    }
    
    /**
     * @description List or filter objects/rows of model/table
     * @example To get list of customers with age 26
     *  - ds.list("customer", {"age": 26},function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {object} filter_attributes  Filter attributes
     * 
     * Example: 
     * 
     * - To list employees by a single value
     * -- ds.list("customer", {"role": "Developer"},function(data){}, function(err){})
     * 
     * - To list employees by a multiple value
     * -- ds.list("customer", {"role": ["Developer", "Programmer"]},function(data){}, function(err){})
     * 
     * - To list employees between two values
     * -- ds.list("customer", {"age": "23,28"},function(data){}, function(err){})
     * 
     * - To list employees grater than a value
     * -- ds.list("customer", {"age": "23,"},function(data){}, function(err){})
     * 
     * -To list employees less than a value
     * -- ds.list("customer", {"age": ",30"},function(data){}, function(err){})
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     * - Success response:- Will return one parameter, will have the following keys
     * -- <br>`is_first` - is first page or not true/false 
     * -- <br>`is_last` - is last page or not true/false
     * -- <br>`page_size` - Size of the current page
     * -- <br>`total_number` - Total number of objects/rows in the model/table
     * -- <br>`page_number` - Current page number
     * -- <br>`data` - list of objects
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    list(model, filter_attributes, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
        }
        var that = this;
        console.debug("Getting "+model+" objects :: ", filter_attributes);
        return djq.ajax({
            url: this.host + "/objects/" + model,
            method: "GET",
            dataType: "json",
            contentType: "json",
            data: filter_attributes,
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got objects :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting objects :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Pivot columns over all objects/rows of model/table
     * @example To get pivot of customers with 
     *  - ds.list("customer", {"age": 26},function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {object} filter_attributes  Filter attributes
     * 
     * Example: 
     * 
     * - To list employees by a single value
     * -- ds.list("customer", {"role": "Developer"},function(data){}, function(err){})
     * 
     * - To list employees by a multiple value
     * -- ds.list("customer", {"role": ["Developer", "Programmer"]},function(data){}, function(err){})
     * 
     * - To list employees between two values
     * -- ds.list("customer", {"age": "23,28"},function(data){}, function(err){})
     * 
     * - To list employees grater than a value
     * -- ds.list("customer", {"age": "23,"},function(data){}, function(err){})
     * 
     * -To list employees less than a value
     * -- ds.list("customer", {"age": ",30"},function(data){}, function(err){})
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     * - Success response:- Will return one parameter, will have the following keys
     * -- <br>`is_first` - is first page or not true/false 
     * -- <br>`is_last` - is last page or not true/false
     * -- <br>`page_size` - Size of the current page
     * -- <br>`total_number` - Total number of objects/rows in the model/table
     * -- <br>`page_number` - Current page number
     * -- <br>`data` - list of objects
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    pivot(model, pivot_attributes, params, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
        }
        var that = this;
        console.debug("Getting "+model+" objects :: ", pivot_attributes, params);
        var att = "";
        if(pivot_attributes && pivot_attributes.length){
             att = "/"+ pivot_attributes.join("/")
        }
        return djq.ajax({
            url: this.host + "/pivot/" + model + att,
            method: "GET",
            dataType: "json",
            contentType: "json",
            data: params,
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got objects :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting objects :: ", err)
                errorCallback(err);
            }
        })
    }

    /**
     * @description Upload file Like image(png, jpg, jpeg, bmp..), video(mp4, mkv...)..etc 
     * @example To upload image
     * file = <file refrence> 
     * ds.upload_file(file, "johns-profile.jpg", function(data){}, function(err){})
     * @param {File} file File to upload
     * @param {string} name Name of the file 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns returns through callback function
     *  - Success response:- Will return one parameter, will have uploaded file details 
     * //{
            "path": "<real path of the uploaded in the project>",
            "full_path": "<full static file path of the file can access outside the project>",
            ...
        }
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    upload_file(file, name, successCallback, errorCallback) {
        var formData = new FormData();
        formData.append('file', file, name);
        console.debug("uploading file :: ", name);
        return djq.ajax({
            url: this.host + "/upload_file?large_file=true&full_path=true",
            type: "POST",
            dataType: "json",
            contentType: false,
            processData: false,
            headers:{
                "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
                "X-I2CE-USER-ID": this.user_id,
                "X-I2CE-API-KEY": this.api_key
            },
            data:formData
        }).done(function(data) {
            if (data) {
                successCallback(data);
            }
            console.debug("uploaded file :: ", name);
        }).fail(function(err) {
            err = err.responseJSON || err;
            if (errorCallback) {
                errorCallback(err)
            }
            console.log("Error while uploading :: ", name, err);
        });
    }
    /**
     * @description To create dynamic login for customer/visitor data will be posted to customer type model/table Example: customer, visitor, person...etc
     * @param {object} data 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api 
     * @returns returns through callback functions
     *  - Success response:- Will return one parameter, will be a customer type object {...} will have posted row/object
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    signup(data, successCallback, errorCallback){
        var HEADERS;
        var signupurl = this.host+"/customer-signup";
        var datao = {
            "validated": true
        }
        if(data)
            djq.extend(datao, data)
        var that = this;
        if(this.settings["signup_model"]){
            signupurl = `${signupurl}/${this.settings["signup_model"]}`
        }
        return djq.ajax({
            url: signupurl,
            method: "POST",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
                "X-I2CE-SIGNUP-API-KEY": this.signp_key
            },
            data: JSON.stringify(datao),
            success: function(data) {
                HEADERS = {
                    "Content-Type": "application/json",
                    "X-I2CE-ENTERPRISE-ID": that.enterprise_id,
                    "X-I2CE-USER-ID": data[that.user_id_attr],
                    "X-I2CE-API-KEY": data.api_key
                }
                that.headers = HEADERS;
                Utills.setCookie(that.authentication_cookie_key, JSON.stringify(HEADERS), 24);
                that.user_id = data[that.user_id_attr];
                that.api_key = data.api_key;
                that.signin_required = false;
            },
            error: function (r) {
                console.log(r.responseJSON || r)
                errorCallback(r.responseJSON || r);
            }
        }).done(function (data) {
            // console.log(data)
            HEADERS = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": that.enterprise_id,
                "X-I2CE-USER-ID": data[that.user_id_attr],
                "X-I2CE-API-KEY": data.api_key
            }
            that.headers = HEADERS;
            Utills.setCookie(that.authentication_cookie_key, JSON.stringify(HEADERS), 240);
            successCallback(data);
            that.headers = HEADERS;
        });
    }


    changeUser(user_id, api_key){
        // this.current_response = {};
        this.user_id = user_id;
        this.api_key = api_key;
        this.headers = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
            "X-I2CE-USER-ID": this.user_id,
            "X-I2CE-API-KEY": this.api_key
        }
        Utills.setCookie(this.authentication_cookie_key, JSON.stringify(this.headers), 240);
        console.debug("user updated :: ", this.headers);
    }
}
class Utilities {
    static extendData(data, extend_object){
        extend_object = extend_object || {};
        data = data || {};
        let myPromise = new Promise(function(myResolve, myReject) {
            if(typeof extend_object == 'function'){
                var extend_data = extend_object(data);
                if(typeof extend_data == "object"){
                    myResolve(extend_data);
                }else{
                    myReject("Failed to extend object");
                }
            }else if(typeof extend_object == 'string'){
                var exd = {};
                try {
                    exd = JSON.parse(extend_object);
                    var d = {...data, ...exd}
                    myResolve(d);
                } catch (e) {
                    myReject(e);
                }
            }else{
                var d = {...data, ...extend_object}
                myResolve(d);
            }
        });
        return myPromise;
    }

    static makeid(length) {
        let result           = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
    
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
    
        return result;
    }

    static getTime() {
        return (new Date()).getTime()/1000;
    }

    static _base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    static encodeFlac(binData, recBuffers, isVerify, isUseOgg){
        var ui8_data = new Uint8Array(binData);
        var sample_rate=0,
            channels=0,
            bps=0,
            total_samples=0,
            block_align,
            position=0,
            recLength = 0,
            meta_data;

        function write_callback_fn(buffer, bytes, samples, current_frame){
            recBuffers.push(buffer);
            recLength += bytes;
            // recLength += buffer.byteLength;
        }

        function metadata_callback_fn(data){
            // console.info('meta data: ', data);
            meta_data = data;
        }


        var wav_parameters = wav_file_processing_read_parameters(ui8_data);	
        // convert the PCM-Data to the appropriate format for the libflac library methods (32-bit array of samples)
        // creates a new array (32-bit) and stores the 16-bit data of the wav-file as 32-bit data
        var buffer_i32 = wav_file_processing_convert_to32bitdata(ui8_data.buffer, wav_parameters.bps, wav_parameters.block_align);

        if(!buffer_i32){
            var msg = 'Unsupported WAV format';
            console.error(msg);
            return {error: msg, status: 1};
        }

        var tot_samples = 0;
        var compression_level = 5;
        var flac_ok = 1;
        var is_verify = isVerify;
        var is_write_ogg = isUseOgg;

        var flac_encoder = Flac.create_libflac_encoder(
            wav_parameters.sample_rate, 
            wav_parameters.channels, 
            wav_parameters.bps, 
            compression_level, 
            tot_samples, 
            is_verify
        );
        if (flac_encoder != 0){
            var init_status = Flac.init_encoder_stream(flac_encoder, write_callback_fn, metadata_callback_fn, is_write_ogg, 0);
            flac_ok &= init_status == 0;
            console.log("flac init: " + flac_ok);
        } else {
            Flac.FLAC__stream_encoder_delete(flac_encoder);
            var msg = 'Error initializing the decoder.';
            console.error(msg);
            return {error: msg, status: 1};
        }


        var isEndocdeInterleaved = true;
        var flac_return;
        if(isEndocdeInterleaved){		
            flac_return = Flac.FLAC__stream_encoder_process_interleaved(
                flac_encoder, 
                buffer_i32, buffer_i32.length / wav_parameters.channels
            );
        } else {	
            var ch = wav_parameters.channels;
            var len = buffer_i32.length;
            var channels = new Array(ch).fill(null).map(function(){ return new Uint32Array(len/ch)});
            for(var i=0; i < len; i+=ch){
                for(var j=0; j < ch; ++j){
                    channels[j][i/ch] = buffer_i32[i+j];
                }
            }

            flac_return = Flac.FLAC__stream_encoder_process(flac_encoder, channels, buffer_i32.length / wav_parameters.channels);
        }

        if (flac_return != true){
            console.error("Error: FLAC__stream_encoder_process_interleaved returned false. " + flac_return);
            flac_ok = Flac.FLAC__stream_encoder_get_state(flac_encoder);
            Flac.FLAC__stream_encoder_delete(flac_encoder);
            return {error: 'Encountered error while encoding.', status: flac_ok};
        }

        flac_ok &= Flac.FLAC__stream_encoder_finish(flac_encoder);

        Flac.FLAC__stream_encoder_delete(flac_encoder);

        return {metaData: meta_data, status: flac_ok};
    }


    static doFLAC(b64String) {
        console.log("Gonna return a promise that will do flac-ing..")
        let myPromise = new Promise(function(myResolve, myReject) {
            var fileInfo = [];

            var arrayBuffer = Utilities._base64ToArrayBuffer(b64String);
    
            var encData = [];
            var result = Utilities.encodeFlac(arrayBuffer, encData, isVerify(), isUseOgg());
            // console.log('encoded data array: ', encData);
            let metaData = result.metaData;
            
            // if(metaData){
                // console.log(metaData);
            // }
            
            var isOk = result.status;
            // console.log("processing finished with return code: ", isOk, (isOk == 1? " (OK)" : " (with problems)"));

            if(!result.error){
                // console.log("Encoded data : ");
                // console.log(encData, metaData);
                myResolve(encData);
                // forceDownload(blob, fileName);
            } else {
                
                myReject("Failed to encode", result.error);

            }
        });
        return myPromise;
    }

    
    static getUserMedia = function(params){
        if( navigator.mediaDevices &&  navigator.mediaDevices.getUserMedia){
            return navigator.mediaDevices.getUserMedia(params)
        }else if(navigator.webkitGetUserMedia){
            return navigator.webkitGetUserMedia(params)
        }else if (navigator.mozGetUserMedia){
            return navigator.mozGetUserMedia(params)
        }else{
            return navigator.getUserMedia(params)
        }
    }

}

class Streamer {
    
    BLOB = undefined;
    AUDIO = undefined;
    startRecording = undefined;
    stopRecording  = undefined;
    recordAudio = undefined;
    recognition_sid = undefined;
    recgIntervalMap = undefined;
    is_recording = false;
    selectedRecognizer = undefined;
    socketio = undefined;
    socket = undefined;
    resultpreview = undefined;
    startAudioRecording = undefined;
    stopAudioRecording = undefined;
    audioContext = undefined;
    onSocketConnect = undefined;
    onSocketDisConnect = undefined;
    onTranscriptionAvailable = undefined;
    onError = undefined;
    onStreamingResultsAvailable = undefined;
    onConversationResponseAvailable = undefined;
    uid = undefined;
    const_params = {};
    
    
    constructor(data) {
        console.info("Setting up streamer.");
        this.activeStream = undefined; //[];
        
        this.fps = 1
        this.asr_enabled = true
        this.wakeup_enabled = true;

        Object.assign(this, data);

        this.streamDestinationWSMap = {
            "asr" : undefined,
            "wakeup" : undefined,
            "image": undefined
        };

        this.const_params = {
            "enterprise_id": this.enterprise_id,//DAVE_SETTINGS.getCookie("dave_authentication")["X-I2CE-ENTERPRISE-ID"],
            "recognizer": this.recognizer,//DAVE_SETTINGS.RECOGNIZER,
            "model_name" : this.model_name,
            "origin" : window.location.href,
            "timestamp": Utilities.getTime(),
            "conversation_id" : this.conversation_id,//DAVE_SETTINGS.CONVERSATION_ID,
            "server" : this.base_url.split("//")[1],//DAVE_SETTINGS.BASE_URL.split("//")[1],
            "customer_id": this.customer_id,
            "engagement_id": this.engagement_id,
            "api_key": this.api_key
        }


        
        var that = this;

        if(navigator.permissions){
            if (this.asr && this.asr_type != "direct") {
                navigator.permissions.query(
                    {name : 'microphone'}
                ).then(
                    function(permissionStatus) {
                        console.log(permissionStatus);
                        if(permissionStatus.state == "denied"){
                            that.executeCallback("mic-denied", "Microphone Permission Denied");
                            console.error("Microphone access not granted by user "+permissionStatus.state);
                        } else if (permissionStatus.state == "granted") { // granted, denied, prompt
                            console.log("Microphone access granted.");
                            that.micAccess = true;
                            that.executeCallback("mic-granted", "Mic Access granted");
                        } else {
                            console.log()
                            console.error("Media device ");
                        }
                    
                        permissionStatus.onchange = function(){
                            console.log(this);
                            console.log("Microphone Permission changed to " + this.state);
                            that.executeCallback(this.state, this.state);
                        }
                    }
                );
            }
    
            
            if (this.image_processing && this.video_type != "direct") {
                navigator.permissions.query(
                    { name: 'camera' },
                ).then(
                    function(permissionStatus){
                        console.log(permissionStatus);
                        if(permissionStatus.state == "denied"){
                            that.executeCallback("camera-denied", "Camera Permission Denied");
                            console.error("Camera access not granted by user "+permissionStatus.state);
                        }else if (permissionStatus.state == "granted") { // granted, denied, prompt 
                            that.cameraAccess = true;
                            console.log("Camera access granted.");
                            that.executeCallback("camera-granted", "Camera access granted");
                        } else {
                            console.error("Media device ");
                        }
    
                        permissionStatus.onchange = function(){
                            console.log(this);
                            console.log("Permission changed to " + this.state);
                            that.executeCallback(this.state, this.state);
                        }
                    }
                );
    
                that.cc = CameraControls;
                // that.cc.cameraInit("vid");
            }
        }
        

        this.initSocket();           
        this.setupRTC();
    }
        
    set(key, value){
        this.const_params[key] = value;
    }
    get(key){
        return key ? this.const_params[key]: this.const_params;
    }
    //VAD
    // Define function called by getUserMedia 
    startUserMedia(stream) {
        var that = this;
        console.log(this.activeStreams);

        // this.activeStreams.push(stream);
        // Create MediaStreamAudioSourceNode
        this.vadSource = this.audioContext.createMediaStreamSource(stream);
        
        function __stop() {
            console.log("auto-stopped-recording")
            console.log('voice_stop');
            if (that.recordAudio) {
                let call_to_backend = true;
                let timeExceed = ((Date.now() - that.lastChunkSentTime) > 6000) ? true : false;
                if (!that.voiceActivity && timeExceed) {
                    call_to_backend = false;
                }
                that.stopAudioRecording(that.conv_params, call_to_backend);
                that.audioContext = undefined;
                that.voiceActivity = false;
                that.vadEnabled = false;
                // that.executeCallback("asr-stop_recoding", "auto_stopped_recording")
            }
        }
        // Setup options
        let options = {
            source: this.vadSource,
            voice_stop: __stop, 
            voice_start: function() {
                console.log('voice_start');
                // startVoiceRecording();
                that.voiceActivityActions();
                clearTimeout(time_out);
            }
        };

        // Create VAD
        this.vadobj = new VAD(options);
        var time_out = setTimeout(function(){
            that.executeCallback("asr-idel", "No voice activity");
            that.recordAudio = undefined;
            that.vadEnabled = false;
            that.is_recording=false;
            that.killAudioTracks();
        }, 5000);
    }

    voiceActivityActions(timeout = 15000) {
        let that = this;
        console.log("Setting Voice Activity to True.")
        this.voiceActivity = true;

        if (this.voiceActivityTimoutHandler) {
            clearTimeout(this.voiceActivityTimoutHandler);
        }

        this.voiceActivityTimoutHandler = setTimeout(function() {
            if(that.voiceActivity){
                that.executeCallback("asr-idel", "No voice activity");
                that.recordAudio = undefined;
                that.vadEnabled = false;
                that.is_recording=false;
                that.killAudioTracks();
            }
            that.voiceActivity = false;
        }, timeout);
    }

    setupVAD() {

        let that = this;
        console.log("Setting up VAD.");
        console.log(this.activeStreams);

        window.AudioContext =window.AudioContext || window.webkitAudioContext;
        this.audioContext = new AudioContext();

        
        if (!this.activeStream) {
            Utilities.getUserMedia(
                {audio: true}).then(
                function (stream) {
                    that.activeStream = stream;
                    that.startUserMedia(stream)
                },
                function(e) {
                    // console.log("No live audio input in this browser: " + e);
                });
        } else {
            this.startUserMedia(this.activeStream)
        }
    }

    cacheAndSendData(payload) {
        var that = this;
        console.log("Sending data to astream for "+this.streamDestination)
        Utilities.extendData(payload, this.asr_additional_data).then(
            function (data) {
                console.log(data);
                that.streamDestinationWSMap[that.streamDestination].emit("astream", data);
            },
            function () {
                that.executeCallback("stream_error", "extending payload failed");
                that.streamDestinationWSMap[that.streamDestination].emit("astream", payload);
            }
        )
        that.lastChunkSentTime = Date.now()
    }

    changeStreamDestination(dest) {
        console.log("Changing Stream destination to "+dest);
        this.streamDestination = dest;
        
        if (!this.streamDestinationWSMap[dest]) {
            throw dest+" Stream is not not defined";
        }

        if (dest == "asr") {
            this.stream_type = this.asr_type
        } else if(dest =="wakeup") {
            this.stream_type = this.wakeup_type            
        }

        //Implement image strem events here
    }

    executeCallback(event, data) {
        try {
            this.event_callback(event,data)
        } catch (e) {
            console.log(event, data);
            console.log("Executing callback failed.");
            console.error(e);
        }
        
    }

    initASREvents() {
        let that = this;
        this.streamDestinationWSMap.asr.on('connect', function(data) {
            that.executeCallback("asr-connect", "ASR Websocket connection established.");
        });

     
        this.streamDestinationWSMap.asr.on('disconnect', function () {
            that.executeCallback("asr-disconnect", "ASR Websocket connection is terminated.");
        });
        
        this.streamDestinationWSMap.asr.on('results', function (data) {
            if(that.recognition_sid != data["recognition_sid"]) return;
            
            // console.log(data);
            that.executeCallback("asr-results", data);
            that.stopPolling(that.audio_results_polling_manager);

        });
        
        this.streamDestinationWSMap.asr.on('intermediateResults', function(data) {
            console.log("ASR from speech server: ");
            // console.log(data);
        
            if(that.recognition_sid != data["recognition_sid"]) return;

            if (data["is_final"] == true) {
                console.log("Final data is received. Stopping polling.");
                that.executeCallback("asr-results",data);
                
                that.stopPolling(that.audio_results_polling_manager);
                // if (!that.vadEnabled) {
                // }
            } else {
                that.executeCallback("asr-intermediate-results",data);
            }            
        });
        
        this.streamDestinationWSMap.asr.on('error', function(data){
            console.log("backend error");
            if (that.onError && typeof(that.onError) == 'function' ) {
                that.executeCallback("asr-error", data);
            }
        });

        this.streamDestinationWSMap.asr.on('recText', function(data){

            if(typeof data != "object"){
                data = JSON.parse(data);
            }

            if(that.recognition_sid != data["recognition_sid"]) return;
            // console.log("recText: "+data);
            that.executeCallback("asr-recText", data);
            that.stopPolling(that.audio_results_polling_manager, false);
        });
        
        this.streamDestinationWSMap.asr.on('convResp', function(data){
            console.log("convResp:");
            console.log(data);
            
            if(typeof data != "object"){
                data = JSON.parse(data);
            }

            if(that.recognition_sid != data["recognition_sid"]) return;
            try {
                if(typeof data == "object"){
                    that.executeCallback("asr-convResp",data["conv_resp"] || data["conversation_api_response"] || {});
                }else{
                    that.executeCallback("asr-convResp",data);
                }
            } catch (e) {
                console.error(e);
            }
            that.stopPolling(that.audio_results_polling_manager);
            that.recognition_sid = undefined;
        });
    }

    initWakeupEvents() {

        let that = this;

        this.streamDestinationWSMap.wakeup.on('error', function(data){
            console.log("backend error");
            if (that.onError && typeof(that.onError) == 'function' ) {
                that.executeCallback("wakeup-error", data);
            }
        });
        
        
        
        this.streamDestinationWSMap.wakeup.on('hotword', function(data) {
            console.log(data);
            that.detectWakeup = false;
            that.stopPolling(that.audio_results_polling_manager);
            that.executeCallback("wakeup-hotword", data)
            // that.changeStreamDestination("asr");
            if (!that.auto_asr_detect_from_wakeup) {
                that.stopAudioRecording(this.conv_params, false)
            } else {
                that.stopAudioRecording(this.conv_params, false)
                that.start_asr()
            }
        });
    }

    initImageEvents() {
        let that = this;
        this.streamDestinationWSMap.image.on('connect', function(data) {
            that.executeCallback("imagews-connect", "Websocket connection to image server established.");
        });

     
        this.streamDestinationWSMap.image.on('disconnect', function () {
            that.executeCallback("imagews-disconnect", "Websocket connection to image server is terminated.");
        });

        this.streamDestinationWSMap.image.on('intermediateFaceDetectorResults', function(xe){
            try {
                console.log(xe);
            } catch(e) {
                console.error(e);
            }
        });

        this.streamDestinationWSMap.image.on("intermediateClassifierResults", function(e){
            try {
                // console.log(e);
                that.executeCallback("imagews-results", e);
            } catch(e) {
                console.error(e);
            }
        });
    }

    initSocket() {


        // this.changeStreamDestination("wakeup");

        this.uid = Utilities.makeid(6);

        console.log(`Setting up Socketio.`);
     
        if (this.image_processing && this.image_server) {
            console.log("Initng Image server.");
            this.streamDestinationWSMap.image = io(this.image_server, {query:"uid="+this.uid});
            this.initImageEvents();
            this.streamDestination = "image";
        }
        
        if (this.wakeup && this.wakeup_server) {
            this.streamDestinationWSMap.wakeup = io(this.wakeup_server, {query:"uid="+this.uid});
            this.initWakeupEvents();
            this.streamDestination = "wakeup";
        }
        
        if (this.asr && this.asr_server) {
            this.streamDestinationWSMap.asr = io(this.asr_server, {query:"uid="+this.uid, transports: ['websocket'] });
            this.initASREvents();
            this.streamDestination = "asr";
        }
    }
    
    start_asr() {
        if (this.asr == false) {
            if (this.detectWakeup) {
                console.log("Wakeup is not detected yet.");
                return;
            }
        }
        
        this.changeStreamDestination('asr');
        this.executeCallback("asr-recording", "recording");
        this.startAudioRecording();
    }

    check_is_recording() {
        // console.log("record check "+this.is_recording);
        if (this.is_recording == true) {
            return true;
        } else {
            return false;
        }
    }
    //Polling functions.
    //This function polls wesocket server for intermediate results every 2 seconds.
    createPolling(uid, recognition_sid, interval = 2000) {
        var that = this;
        var poll_counter = 50;
        console.log("Creating polling (for "+that.streamDestination+")");

        const intervalObject = setInterval(function() {
            // console.log(that.check_is_recording());
            if (that.check_is_recording() == false) {
                poll_counter = poll_counter - 1;
                console.log("Polling ends after "+poll_counter+" trials.")
                if (poll_counter <= 0) {
                    that.stopPolling(that.audio_results_polling_manager);
                }
            }
            // console.log("This messsage gets printed every 2 seconds.");
            if (that.streamDestination == "wakeup") {
                console.log("Polling for wakeup.");
                that.streamDestinationWSMap["wakeup"].emit("hotwordResults",{"uid":uid,"recognition_sid":recognition_sid});
            } else {
                console.log("Polling for asr.");
                that.streamDestinationWSMap["asr"].emit("intermediateResults",{"uid":uid,"sid":recognition_sid});
            }
        }, interval);
    
        return intervalObject;
    }

    stopPolling(intervalObject, clear = true) {
        console.log("Stopping polling..");
        clearInterval(intervalObject);
        if (clear) {
            this.recognition_sid = undefined;
        }
    }

    // startRecording(customer_id, system_response) {

    // }

    replaceAudio(src) { 
        var newAudio = document.createElement('audio');
        // var newAudio = document.getElementById("playback");
        newAudio.controls = true; 
        newAudio.autoplay = true; 
        if(src) { 
            newAudio.src = src;
        }
        
        // var parentNode = newAudio.parentNode; 
        // newAudio.innerHTML = ''; 
        // newAudio.appendChild(newAudio); 
        AUDIO = newAudio; 
    }

    setupRTC() {
        var that = this;
        console.log("Setting up RTC.");
        // if (that.vad) {
        //     this.setupVAD();
        // }
        this.startAudioRecording = function(conv_params = {}) {
                console.log("Record")
                
                // this.startRecordingButton.style.display = 'block';
                // this.stopRecordingButton.style.display = 'none';  
    
                that.is_recording = true;
                Utilities.getUserMedia(
                    {audio: true}).then(
                    function(stream) {
                        that.micAccess=true;
                        console.log("Active Stream set.");
                        that.activeStream = stream;
                        that.executeCallback(that.streamDestination+"-recording", "recording");
                        that.recordAudio = RecordRTC(
                            stream, 
                            {
                                type: 'audio',
                                mimeType: 'audio/wav',
                                sampleRate: 44100,
                                timeSlice: 2000,
                                bufferSize : 1024,
                                recorderType: StereoAudioRecorder,
                                numberOfAudioChannels: 1,
                            
                                ondataavailable: function(blob) {
                                    
                                    // console.log(that.stream_type);
                                    if (!that.recognition_sid) {
                                        that.recognition_sid = Utilities.makeid(8);
                                        that.audio_results_polling_manager = that.createPolling(that.uid, that.recognition_sid);
                                    }

                                    if (that.stream_type == "full") {
                                        that.BLOB = blob;
                                    } else if (that.stream_type == "chunks") {

                                        
                                        // console.log("ASR CHUNKS");
                                        conv_params = {...conv_params, ...that.const_params, ...{
                                            "size":blob.size,
                                            "blob":blob, 
                                            "recognition_sid":that.recognition_sid, 
                                            "is_recording":that.is_recording,
                                            "timestamp": Utilities.getTime(),
                                            "recognizer" : that.recognizer,
                                            "model_name" : that.model_name
                                        }};
                                        
                                        
                                        // console.log(payload);
                                        
                                        that.cacheAndSendData(conv_params);
                                        // this.socketio.emit('astream', audio_payload);
                                    }
                                }
                            }
                        );
                        
                        that.recordAudio.startRecording();
                        if(!that.vad){
                            setTimeout(function(){
                                if(that.is_recording){
                                    that.stopAudioRecording(conv_params)
                                }
                            }, 10000);
                        }
                        
                        // this.stopRecordingButton.style.display = 'block';
                    }, 
                    function(error) {console.error(JSON.stringify(error));}
                );
        };
    
        this.stopAudioRecording = function(conv_params={}, call_to_backend = true) {
            console.log(`Call to backedn ${call_to_backend}.`)
            if(that.micAccess){
                console.log("Stopping stream to ASR.");
                // console.log("Conversation Params");
                // console.log(conv_params);
                // recording stopped
                // this.startRecordingButton.style.display = 'block';
                // this.stopRecordingButton.style.display = 'none';
                                
                that.is_recording = false;

                if (call_to_backend) {

                    conv_params = { ...conv_params, ...that.const_params, ...{
                        "size":0,
                        "blob":"", 
                        "recognition_sid":that.recognition_sid, 
                        "is_recording":that.is_recording,
                        "timestamp": Utilities.getTime()
                    }};


                    
                    if (that.stream_type == "chunks") {
                        that.executeCallback(that.streamDestination+"-processing", "processing");
                        that.streamDestinationWSMap[that.streamDestination].emit('astream', conv_params);
                    }
                }
                
                if (!that.recordAudio) {
                    console.log("Audio recording already stopped.");
                    return;
                }
                //// stop audio recorder
                that.recordAudio.stopRecording(function() {
                    console.log("Stopping recording.");
                    if (that.stream_type == "full" && call_to_backend) {

                        // replaceAudio(URL.createObjectURL(that.BLOB));
                        // AUDIO.play();
                        // after stopping the audio, get the audio data
                        that.recordAudio.getDataURL(function(audioDataURL) {
                            if (call_to_backend) {
                                
                                var payload = { ...conv_params, ...that.const_params, ...{
                                    "size":0,
                                    "recognition_sid":that.recognition_sid, 
                                    "is_recording":that.is_recording,
                                    "timestamp": Utilities.getTime(),
                                    "recognizer" : that.recognizer,
                                    "model_name" : that.model_name
                                }};

                                if (that.recognizer == "google") {
                                    // console.log("Sending data for google recognition via flacking channel.");
                                    // that.onWavLoad(files.audio.dataURL.split(",")[1], selectedRecognizer);
                                    Utilities.doFLAC(audioDataURL.split(",")[1]).then(
                                        function (encData, metaData) {
                                            // console.log(encData);

                                            let blob = exportFlacFile(encData, metaData, false);
                                            // var fileName = getFileName("flactest", isUseOgg()? 'ogg' : 'flac');
                                            let reader = new FileReader();
                                            reader.onload = function() {
                                                
                                                payload.blob = reader.result;
                                                
                                                Utilities.extendData(payload, this.asr_additional_data).then(function() {
                                                    that.streamDestinationWSMap["asr"].emit("flacking", payload);
                                                    that.executeCallback(that.streamDestination+"-processing", "processing");
                                                },
                                                function () {
                                                    that.executeCallback("stream_error", "extending payload failed");
                                                    that.streamDestinationWSMap[that.streamDestination].emit("flacking", payload);
                                                });

                                                // console.log(payload);
                                                that.killAudioTracks();
                                            }

                                            reader.readAsDataURL(blob);

                                        },
                                        function (messageString, error) {
                                            console.log("There is an error.");
                                            console.log(messageString);
                                            console.log(error);
                                            that.executeCallback("asr-error", "FLAC encoding failed");
                                        }
                                    )
                                } else {
                                    payload.blob = audioDataURL;
                                    that.executeCallback(that.streamDestination+"-processing", "processing");
                                    that.streamDestinationWSMap["asr"].emit('stt', payload);
                                    that.killAudioTracks();
                                }
                            } 
                            
                        });
                    } else {   
                        that.killAudioTracks();
                    }

                });

            }
        };
    }

    getMedia(requests, callback){
        var that = this;
        
        if(Utilities.getUserMedia()){
            if(that._request_pending){
                that.executeCallback("interaction", InteractionStageEnum.MIC_REQUESTED);
            }
            Utilities.getUserMedia(requests, function(stream) {
                console.debug("Cool:::Got auth for audio input");
                if(that._request_pending){
                    that.executeCallback("interaction", InteractionStageEnum.MIC_ALLOWED);
                    that._request_pending=false;
                }
                callback(stream);
            }, function(error) {
                console.log("error while mic init");
                console.log(error);
                if(that._request_pending){
                    that.executeCallback("interaction", InteractionStageEnum.MIC_REJECTED);
                    that._request_pending=false;
                }
                callback(false, error);
                that.mediaFailed();
            });
        }else{
            callback(false);
        }
    }

    mediaPermissions(){
        //class Streamer
        var that = this;
        function callbacks(sts){
            that._request_pending = false;
            if(sts){
                that.executeCallback("interaction", InteractionStageEnum.MIC_ALLOWED);
            }else{
                that.executeCallback("interaction", InteractionStageEnum.MIC_REJECTED);
                that.mediaFailed();
            }
        }
        // this.getMedia({audio: true}, callbacks);
        
        if(navigator.permissions){
            navigator.permissions.query({name:'microphone'}).then(function(result) {
                console.debug(result.state)
                if(result.state == "denied"){
                    callbacks(false);
                }else if(result.state == "granted") {
                    callbacks(true);
                }
               });
        }
    }

    killAudioTracks() {
        if (this.vadEnabled) {
            this.vadSource.disconnect()
            this.vadobj.scriptProcessorNode.disconnect()
            this.vadobj = undefined;
            this.vadEnabled = false;
        }
        
        if (this.activeStream) {
            console.log(this.activeStream.getTracks());
            let tracks = this.activeStream.getTracks();
            tracks.forEach(track => {
                console.log("Killing track.");
                track.stop();
            });
        }
        this.recordAudio = undefined;
        this.activeStream = undefined;
    }

    forceStopRecording() {
        this.stopAudioRecording({},false);
        this.killAudioTracks();
        this.stopPolling(this.audio_results_polling_manager);
    }
    forceStopWakeup = this.forceStopRecording;

    cancelRecordStopTimer(id) {
        clearTimeout(id);
    }   

    startRecordStopTimer(T) {
        this.recordTimer = setTimeout(function() {
            console.log("Record timout stopping recoding.")
            this.stopVoiceRecording();
        }, T * 1000);
    }
    
    captureImage() {
        let image = this.cc.captureImage();
        return image;
    }
    captureVideo(){

    }

    stopImageResultsPolling() {
        console.log("Stopping image results polling.");
        clearInterval(this.image_results_polling_manager);
    }

    startImageResultsPolling() {
        let that = this;
        this.image_results_polling_manager = setInterval(function() {
            console.log("Polling for image classifier results.")
            that.streamDestinationWSMap.image.emit("intermediateClassifierResults", {"uid":that.uid, "sid":that.image_recognition_sid});
        }, 1000);
    }

    start_video_capture(frames_to_capture = undefined , subsampling_factor = undefined, img_data = undefined) {
        this.is_video_recording = true	
        this.cc.cameraInit("vid");
        var that =  this;
        if (!this.image_recognition_sid) {
            console.log("Generating image rsid.");
            this.image_recognition_sid = Utilities.makeid(8)
            this.startImageResultsPolling();
        }

        function _post(img_data){
            console.log("Sending image to server.");
            let imageData = img_data;
            console.log(that.image_classifier);
            let payload = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["'+that.image_classifier+'"]}||||'+imageData+'||||';
            that.streamDestinationWSMap.image.emit("imageStream",{"uid":that.uid,"sid":that.image_recognition_sid,"is_recording":that.is_video_recording, "data":payload});

        }
        if(!img_data){
            this.image_capture_manager = setInterval(function() {
                if (frames_to_capture == 0) {
                    that.executeCallback("imagews-capture_ended", "capture_ended");
                    that.stop_video_capture();
                    return
                } else {
                    console.log(`Will capture ${frames_to_capture} more frames.`)
                    frames_to_capture = frames_to_capture - 1;
                }
                _post(that.captureImage());	
            }, 1000/this.fps);
        }else{
            _post(img_data)
        }    
        that.executeCallback("image-recording", "recording");
    }

    stop_video_capture() {
        clearInterval(this.image_capture_manager);
        this.is_video_recording = false;
        this.cc.stopMediaTracks(this.cc.currentMediaStream)
        this.stopImageResultsPolling();
        this.image_recognition_sid = undefined; 
        this.executeCallback("image-stoprecording", "stoprecording");
    }

    startVoiceRecording(conv_params){
        //if any audio/video stream is ON just kill them
        if (this.vad && !this.vadEnabled) {
            this.setupVAD();
            this.vadEnabled = true;
        }
        this.conv_params = conv_params;
        
        this.changeStreamDestination("asr");
        this.startAudioRecording(conv_params)
        console.log(this.audio_auto_stop);
        // this.startRecordStopTimer(this.audio_auto_stop);
    }
    stopVoiceRecording(){
        this.stopAudioRecording(this.conv_params)
    }
    startWakeupRecording(conv_params){
        //if any audio/video stream is ON just kill them
        //same as startAsrRecording.
        this.conv_params = conv_params;
       
        this.changeStreamDestination("wakeup");
        this.startAudioRecording(conv_params)
    }
    stopWakeupRecording(){
        this.stopAudioRecording({}, false)
    }

    startVideoRecording(){
        this.start_video_capture(this.frames_to_capture, this.subsampling_factor)
    }
    stopVideoRecording(){
        this.stop_video_capture()
    }
} 

class Conversation extends DaveService {
    conversation_data = {};
    conversation = {};
    current_response = {};
    keywords = [];
    _req = null;
    session_id = null;
    session_start_time = null;
    init_response = null;
    voice_id = null;
    constructor(host, enterprise_id, conversation_id, settings) {
        //class Conversation
        super(host,enterprise_id, settings);
        this.settings = settings;

        //Setting the conversation and streamer
        this._wakeup = settings["wakeup"] ? settings["wakeup"] : (settings["wakeup_url"] ? true : false);
        this._asr = settings["asr"] ? settings["asr"] : (settings["speech_recognition_url"] ? true : false);
        this._image_processing = settings["image_processing"]? settings["image_processing"]: (settings["image_recognition_url"] ? true: false)
        this._wakeup_server = settings["wakeup_url"];
        this._asr_server = settings["speech_recognition_url"];
        this._image_server = settings["image_recognition_url"];
        this._image_classifier = settings["image_recognition_url"] ? "FaceDetectorCafe" : undefined;
        this._frames_to_capture = 30;
        this._recognizer = settings["recognizer_type"] || 'google'; //Google || Kaldi || Reverie || ..
        this._model_name = "indian_english";
        this._auto_asr_detect_from_wakeup = settings["auto_asr_detect_from_wakeup"] == undefined ? false : settings["auto_asr_detect_from_wakeup"];
        this._vad = settings["vad"];
        this._audio_auto_stop = 10;
        this._asr_type = settings["speech_recognition_type"] || "full"; //full-'flacking'||"chunks"-'asteam'||"direct"-'sending-flag',
        this._wakeup_type = settings["wakeup_type"] || "chunks"; //chunks-'asteam'||"direct"-'sending-flag',
        this._video_type = settings["video_stream_type"] || "chunks"; //chunks-'asteam'||"direct"-'sending-flag'
        this._additional_conversation_info = settings["additional_conversation_info"] || {};

        this.setConversation(conversation_id);


        //session info
        this._session_model = this.settings["session_model"];
        if (this._session_model) { 
            this._session_user_id_attr = this.settings["session_user_id_attr"] || this.settings["user_id_attr"];
            this._session_id_attr = this.settings["session_id_attr"] || this._session_model.toLowerCase()+"_id";
        }
    }
    setConversation(conversation_id) { 
        this.conversation_id = conversation_id;
        this._loading_conversation = true;
        this._loading_conversation_keywords = true;
        this.voice_id = this.settings["voice_id"];

        
        var streamer_settings = {
            wakeup : this._wakeup,
            asr : this._asr,
            image_processing : this._image_processing,
            wakeup_server : this._wakeup_server, 
            asr_server : this._asr_server, 
            image_server :  this._image_recognition_url,
            image_classifier : this._image_classifier,  
            frames_to_capture : this._frames_to_capture,
            recognizer : this._recognizer, //Google || Kaldi || Reverie || ..
            model_name : this._model_name,
            conversation_id : conversation_id,
            enterprise_id : this.enterprise_id, 
            base_url : this.host_url,
            auto_asr_detect_from_wakeup: this._auto_asr_detect_from_wakeup,
            vad : this._vad,
            audio_auto_stop: this._audio_auto_stop,
            asr_type :  this._asr_type,
            wakeup_type : this._wakeup_type,
            video_type : this._video_type,
            asr_additional_info :this._additional_conversation_info,
            event_callback : (event, data) => {
                console.log(event, data);
                this.eventCallback(event, data);
            }
        }
        this.streamer = new Streamer(streamer_settings);
        this.loadConversation();
        this.loadKeywords();
    }

    changeUser(user_id, api_key) {
        super.changeUser(user_id, api_key);
        this.user_id = user_id;
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.user_id;
        this.engagement_id = undefined;
        this.session_id = undefined;
    }
    
    triggerEvents(status, message){
        window.__dv_cbs_get("ConversationAction").fire(status, message);
    }

    post_interaction(obj, beacon=false){
        var user_id_attr = this.settings["interaction_user_id_attr"] || this.settings["user_id_attr"]
        obj[user_id_attr] = this.user_id;

        if(!obj[this.settings["stage_attr"] || "stage"]){
            console.debug("Interaction disabled")
            return;
        }

        extendData(obj, this.settings["additional_interaction_info"]).then((exdata)=>{
            if(!exdata){
                return;
            }
            if(beacon){
                this.sendBeacon(`/object/${this.settings["interaction_model"]}`, exdata);
            }else{
                this.post(this.settings["interaction_model"], exdata, (data)=>{ 
                    console.debug(data)
                    this.eventCallback("interaction_response", data);
                }, (err)=>{
                    console.error(err)
                    this.eventCallback("interaction_response", err.error || err);
                });
            }
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }

    startSession(async=false){
        if(!this._session_model || this.session_id){
            return;
        }
        
        this.session_start_time = (new Date()).getTime();
        var obj = {
            [this._session_user_id_attr]: this.user_id,
            "conversation_id": this.conversation_id,
            
        };
        if(async && crypto){
            obj["_async"] = true;
            obj[this._session_id_attr] = crypto.randomUUID();
        }
        extendData(obj, this.settings["additional_session_info"]).then((exdata)=>{
            this.post(this._session_model, exdata, (data)=>{ 
                console.debug(data)
                if(async && crypto){
                    data[this._session_id_attr] = obj[this._session_id_attr];
                }
                
                this.eventCallback("start_session", data);
                this.session_id = data[this._session_id_attr];
            }, (err)=>{
                console.error(err)
                this.eventCallback("start_session", err.error || err);
            });
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }
    updateSession(async, beacon=false){
        if(!this.settings["session_model"] || !this.session_start_time || !this.session_id){
            return;
        }
        var that = this;
        var diff = new Date().getTime() - this.session_start_time;
        var obj = {
            "_async": true,
            "async": async == undefined ? true : false,
            "conversation_id": this.conversation_id,
            [this.settings["session_duration_attr"] || "session_duration"]: diff/1000
        };
        this.session_start_time = undefined;
        if(beacon){
            this.sendBeacon(`/patch/${this._session_model}/${this.session_id}`, obj);
        }else{
            this.iupdate(this._session_model, this.session_id, obj, (data)=>{ 
                console.debug(data)
                this.eventCallback("update_session", data);
                // deleteAllCookies();
            }, function(err){ 
                console.error(err)
                this.eventCallback("update_session", err.error || err);
            });
        }
    }
    endSession(obj){
        if(!this._session_model){
            return;
        }
        var obj = { "_async": true, "conversation_id": this.conversation_id };
        this.update(this._session_model, this.session_id, obj, (data)=>{ 
            console.debug(data)
            this.eventCallback("end_session", data);
            deleteAllCookies();
        }, function(err){
            console.error(err)
            this.eventCallback("end_session", err.error || err);
        });
    }
    
    
    eventCallback(status, message){
        console.log(status, message);
        //class Conversation
        if(status == "error"){
            this.triggerEvents("idel");
            //that.precessError({});
            this.processing = false;
        }else if(status == "interaction"){
            this.post_interaction({[this.settings["stage_attr"] || "stage"]: message});
        }else if(status == "mic_rejected" || status == "socket_error"){
            this.triggerEvents("mic_rejected");
        }else if(status == "asr-convResp"){
            this.processing = false;
            this.state = "idel";
            if(this.timeout_level_3){
                clearTimeout(this.timeout_level_3);
                this.timeout_level_3 = undefined;
            }
            if(this.timeout_timeout){
                clearTimeout(this.timeout_timeout);
                this.timeout_timeout = undefined;
            }
            
            if( typeof message  === "object" ){
                var dt = message || {};
                dt["auto_recording"] = message["auto_recording"];
                if( !dt["name"] ){
                    console.log("--------------- error response", dt);
                    this.triggerEvents("conversation-error", dt);        
                }else{
                    this.triggerEvents("conversation-response", dt);
                }
            }else{
                console.debug("--------------- not a json response", message);
                this.triggerEvents("conversation-error", dt);
            }
        }else if(status == "asr-recText"){
            console.debug("level -2 callback")
            this.triggerEvents("speech-detected", message.recognized_speech || "");
            var that = this;
            that.timeout_level_3 = setTimeout(function(){
                console.debug("level -3 callback")

                if(that.processing){
                    that.triggerEvents("delay", message.recognized_speech || "");
                }
            }, 15000);

        }else if(status == "imagews-capture_ended"){
            this.triggerEvents("capture_ended", message);
        }else if(status == "imagews-results"){
            if(message["classifier_result"]){
                for(var z of message["classifier_result"]){
                    if(z["Result"] == "Face"){
                        this.triggerEvents("person_detected", message);
                        if(this.settings["auto_image_stream_stop"] != false){
                            this.stopImageStream();
                        }
                    }
                }
            }
        }else if(status == "more_delay"){
            this.triggerEvents("delay","");
        }else if(status == "timeout"){
            if(this.state != "idel" && this.processing){
                console.log("Speech reco timed out")
                this.triggerEvents("timeout", {"error": "I'm not able to get an answer for that currently. Can you try with something simpler?"});
                
                this.processing = false;
                // setTimeout(function(){
                //     that.startListenForWakeup();
                // }, 1500);
            }
        }else if(status=="wakeup-hotword"){
            console.debug("hotword detected", message);
            this.triggerEvents("hotword", message);
            if(this.settings["auto_wakeup_listern_stop"] != false){
                this.streamer.forceStopWakeup();
            }
        }
        else if(status=="asr-processing"){
            // that.socketio.emit('stt', payload);
            this.triggerEvents("speech_processing");
            this.processing = true;
            this.state = "processing";
        }
        else if(status == "asr-disconnect"){
            if(this.state == "processing"){
                this.triggerEvents("socket_disconnected", {"error": "Your Internet connection was disrupted during the query. Please try saying this again."});
                this.processing = false;
            }
            this.state = "idel";
        } else if(status == "asr-recording"){
            this.triggerEvents("recording");
            this.state = "recording";

            this.abortReq();
        }else if (status == "asr-idel"){
            this.triggerEvents("idel", message);
            this.state = "idel";
            this.processing = false;
            this.abortReq();
        } else if(status == "asr-processing"){
            this.triggerEvents("processing", message);
            this.state = "processing";
            this.processing = true;
            this.abortReq();
        } else{
            console.debug(status, message);
            this.triggerEvents(status, message)
        }
        
        
        if(["error", "mic_rejected", "socket_error"].indexOf(status) > -1){
            // if(that.record_button){
            //     that.record_button.attr("disabled", true);
            // }
        }else{
            // if(that.record_button){
            //     that.record_button.attr("disabled", false);
            // }
        }
    }
    
    abortReq(){
        //class Conversation
        if(this._req){
            this._req.abort();
        }
        
    }
    abortAsrReq(){
        this.streamer.forceStopRecording();
    }
    checkSetupDone(){
        //class Conversation
        if(!this._loading_conversation && !this._loading_conversation_keywords){
            window.__dv_cbs_get("ConversationLoaded").fire(this.conversation_id);
        }
    }
    
    loadConversation(){
        //class Conversation
        var ref = this;
        this.get("conversation", this.conversation_id, function(data){
            ref.conversation_data = data["data"];
            ref.conversation = data;
            ref._loading_conversation = false;
            ref.checkSetupDone();
        }, function(error){
            console.log(error);
            ref._loading_conversation = false;
            ref.checkSetupDone();
            throw error;
        })
    }
    
    loadKeywords(){
        //class Conversation
        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }
        
        var ref = this;
        this.getRaw("/conversation-keywords/"+ref.conversation_id, function(da){
            ref.keywords = da["keywords"] || [];
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        }, function(res){
            var msg;
            if(res.responseJSON){
                msg = "Looks like the system is under maintenance, I've been asked to say '"+res.responseJSON["error"]+"'";
            }else{
                msg = "Sorry, it seems like my team is tinkering around with me!! |I should be back online and ready to talk to you once this update is done..."
            }
            console.log(msg);
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        });
    }
    filterStates(kwd){
        //class Conversation
        if(!kwd) return this.keywords;
        function match_score(l, k){
            var x= l.filter(function(i){
                return i.toLowerCase().indexOf(k.toLowerCase()) != -1 ? true : false;
            });
            return x.length;
        }
        var keys = kwd.split(" ");
        keys.push(kwd);
        for(var i in this.keywords){
            var c = 0;
            for(var j in keys){
                c += match_score([this.keywords[i][0], this.keywords[i][1], this.keywords[i][2]], keys[j]);
                c += match_score(this.keywords[i][4], keys[j]);
            }
            this.keywords[i].score = c;
        }
        var x = this.keywords.filter(function(a){ return a.score; });
        return x.sort(function(a,b){ b.length - a.length });
    }
    
    getStateKeywords(state){
        //class Conversation
        for(var i of this.keywords){
            if(i[1] == state)
            return i
        }
        return [];
    }

    postConversationFeedback(rating, feedback, cb) {
        cb = cb || function () { };
        if(rating <= 5 && rating >=1){
            this.postRaw(`/conversation-feedback/dave/${this.engagement_id}`, {
                "usefulness_rating": rating,
                "feedback": feedback
            }, cb, cb)
        }
    }
    
    start(successCallback, errorCallback){
        var that = this;
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.user_id;
        extendData({"query_type": "auto"}, this._additional_conversation_info).then(function(data){
            that.getNext(data, null, function(data){
                successCallback(data)
                that.init_response = data;
            }, errorCallback);
        }).catch((err) => console.log("Failed to extend request object", err))

        
        this.streamer.set("customer_id", this.user_id);
        this.streamer.set("api_key", this.api_key);
    }

    getNext(data, title, successCallback, errorCallback){
        var ref = this;
        data = {
            ...data, ...{
                system_response: this.current_response ? this.current_response["name"] : null,
                voice_id: this.voice_id,
                engagement_id: this.engagement_id,
                synthesize_now: true,
                csv_response: true
            }
        }
        this.abortReq();
        console.debug("data: ", data);
        
        if(data["customer_response"]){
            var rx = title|| data["customer_response"];
            this.eventCallback("processing",  rx.indexOf("{") == -1 ? rx : (data.title || "Form response"));
        }
        this._req = this.postRaw(this.conversation_url, data, function(data){
            ref.engagement_id = data["engagement_id"]
            ref.current_response = data;
            data["data"] = data["data"] || {};
            ref.voice_id = data["data"]["voice_id"] || ref.voice_id;
            ref.streamer.set("engagement_id", data["engagement_id"]);
            successCallback(data);
            ref.eventCallback("idel");
            //ref.precessResponse(da, successCallback)
        }, function(error){
            errorCallback(error);
            ref.eventCallback("idel");
            //ref.processError(res, errorCallback);
        })
    }
    getNudge(state, successCallback, errorCallback){
        var ref = this;
        
        this.abortReq();
        var data = {
            customer_state: state,
            system_response: this.current_response ? this.current_response["name"] : null,
            engagement_id: this.engagement_id,
            synthesize_now: true,
            csv_response: true,
            query_type: "auto",
            voice_id: this.voice_id
        }
        extendData(data, this._additional_conversation_info).then(function(data){
            console.debug(data)
            ref.eventCallback("processing", "nudge");
            ref._req = ref.postRaw(ref.conversation_url, data, function(data){
                data["data"] = data["data"] || {};
                successCallback(data);
                ref.eventCallback("idel");
                //ref.precessResponse(da, successCallback)
            }, function(error){
                errorCallback(error);
                ref.eventCallback("idel");
                //ref.processError(res, errorCallback);
            })   
        }).catch((err) => console.log("Failed to extend request object", err))   
    }
    
    next(response, state=null, query_type=null, title=null, successCallback=null, errorCallback=null){
        //class Conversation
        var that = this;
        var data = {
            customer_response: response,
            system_response: this.current_response? this.current_response["name"]: null,
            customer_state: state,
            query_type: query_type
        };
        extendData(data, this._additional_conversation_info).then(function(data){
            that.getNext(data, title, successCallback, errorCallback);    
        }).catch((err) => console.log("Failed to extend request object", err))
    }
    getKeywords(c=4, res=null){
        var kws = []
        res = res || this.current_response
        if(res){
            for(var i in res["options"]){
                kws.push([res["options"][i], undefined]);
                if(kws.length == c) break
            }
            var l = [];
            for(var i in res["state_options"]){
                var kys = this.getStateKeywords(i);
                if(kys.length == 6){
                    if(kys[5].length > 0){
                        l.push([kys[2], i]);
                    }else{
                        l.push([res["state_options"][i], i]);
                    }
                    /*
                            for(var z of kys[5]){
                                if(z.indexOf("**") >= 0) continue;
                                while(z.indexOf("__") > -1)
                                z = z.replace("__", "\"")
                                x.push(z);
                            }
                            l.push(x);*/
                }
            }
            for(var j of l){
                kws.push(j);
                if(kws.length >= c){
                    break;
                }
            }
        }
        return kws;
    }


    startListenForWakeup(){
        this.streamer.startWakeupRecording({});
    }
    stopListenForWakeup(force){
        if(force){
            this.streamer.forceStopWakeup()
        }else{
            this.streamer.stopWakeupRecording();
        }
    }
    startImageStream(){
        this.streamer.startVideoRecording();
    }
    stopImageStream(){
        this.streamer.stopVideoRecording();
    }
    startRecordResponse(){
        // this.eventCallback("recording");
        // this.streamer.startRecordResponse(null, false, true, this.current_response["name"]);
        this.streamer.startVoiceRecording({
            voice_id: this.voice_id || 'english-male',
            query_type: "speech",
            language: this.language || 'english',
            system_response: this.current_response["name"]
        })
    }
    stopRecordResponse(){
        // this.eventCallback("speech_processing");
        // this.streamer.stopRecordingResponse();
        this.streamer.stopVoiceRecording();
    }
}
/**
 * @description Handler class is abstract class which has all the abstract functions which used to handle in ui, you have to extend this functanality in you type of conversation class
 * @example class AvatarConversation extends ConversationHandler
 */

 class ConversationHandler{
    /**
     * @description Handler class is abstract class which has all the abstract functions which used to handle in ui, you have to extend this functanality in you type of conversation class
     * @param {object} settings settings is and object type params which should contain host_url, conversation_ids, default_conversation, 
     * @example var settings = {
     * "enterprise_id": "dave_restaurant", 
     * "conversation_id": "deployment_kiosk",
     * "event_type": "deployment",
     * "event_id": "kiosk",
     * "host_url": "https://staging.iamdave.ai",
     * "signup_apikey": "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
     * "unity_url": "https://s3.ap-south-1.amazonaws.com/unity-plugin.iamdave.ai/unity/dev_empty_bg" //unity model url,
     * "type": "kiosk" //type of event,
     * "default_placeholder" : "Type here" //placeholder on input text box,
     * "recognizer_type": "google" //ASR engine name google/kaldi,
     * "session_model": "session" //session model name,
     * "user_id_attr": "customer_id" //user id attribute in the customer model,
     * "interaction_model": "interaction" // interaction model name,
     * "speech_recognition_url": "https://speech.iamdave.ai" //ASR speech server url,
     * "image_processing_url": "https://speech.iamdave.ai" //Image processing server url,
     * "avatar_id": "dave-english-male" //avatar id of the avatar need to load,
     * "voice_id": "english-male" //Speech languange,
     * "additional_session_info": {} // aditional session attributes will post to the session model ,
     * "signup_params": {} //aditional signup params to pass to while creating signup,
     * "session_time": 50 //waiting time for each session,
     * "conversationLoadedCallback": function(data){} //callback function will respond to this callback function,
     * "session_expired_callback": function(){} //Callback function for on session expired,
     * "actionCallbacks":function(data){} //will return some conversation data after every state
     * }
     */
    constructor(settings){
        settings["vad"] = settings["vad"] == undefined? true: settings["vad"];
        this.settings = settings;
        this.authentication_cookie_key = "authentication";
        this.minimize = false;
        for(var k in settings){
            this.set(k, settings[k]);
        }

        var _cids =  this.conversation_ids || this.conversation_id;
        if(!_cids){
            throw Error("Conversation ids required");
        }
        if(typeof _cids == "string"){
            _cids = _cids.split(",");
        }
        this.conversation_ids = _cids;
        this.conversation_id = _cids.length == 0 || !this.default_conversation ? _cids[0]: this.default_conversation;
        
        this.con_map = {};
        this.messages = [];
        this.whiteboards= [];
        

        //statuses
        this.state = "idel";
        this.audio_playing = false;
        this.load__ = false;    
        this.conversation_loaded = false;
        this.loaded = false;
        this.filtered_keywords = undefined;

        this.setupEnvironment();
    }
    
    triggerEvents(action, data){
        console.debug("Triggered event for ::", action);
        if(this.actionCallbacks && data !=undefined){
            this.actionCallbacks(data, action);
        }
    }
    onLoadedCallback(){
        if(this.conversation_loaded){
            this.onResponse(this.conversation.init_response);
            if(this.conversationLoadedCallback){
                this.conversationLoadedCallback(this.conversation.conversation_data);
                this.addShortcutsButtons();
                this.loaded =true;
            }
        }
    }
    set(key, value){
        this[key]= value;
    }
    get(key){
        return this[key];
    }
    /**
     * @description This function is use change the converstion 
     * @example ds.setConversation("deployment_kiosk_kannada")
     * @param {string} conversation_id Its id of conversation which you wants to change
     * @returns will returns true if the converstation id exists in conversations list sent through constructor
     */
    setConversation(conid){
        if(this.con_map[conid]){
            this.conversation_id = conid;
            this.conversation = this.con_map[conid];
            this.refreshUI();
            return true;
        }else{
            return false;
        }
    }



    /**
     * @description This function is use for updating the state of the coversation
     * @example this.evetCallback("idel", null)
     * @param {string} status type of the state
     * @param {string} conversation_id Its id of conversation which you wants to change
     * @returns null
     */
    eventCallback(status, message){
        if(status == "signup" || status == "interaction_response" || status == "start_session" || status == "end_session" || status == "hotword" || status == "person_detected" || status == "capture_ended"){
            this.triggerEvents(status, message);
            if(status == "signup"){
                this.status = status;
            }
        }
        if(!this.loaded){
            return;
        }
        
        if (status == "idel") {
            if (!this.load__) { 
                this.onIdelState();
                this.state = status;
            }
        }else if(status == "processing") {
            if(message != 'nudge'){
                this.playDefaults("level_one");    
                this.onProcessingRequest(message);
            }
            this.state = status;
        }
        else if(status == "speech_processing"){
            this.onProcessingRequest("....");
            this.state = "processing";
        }
        else if(status == "conversation-error") {
            this.state= "idel";
            this.onError(message);
            this.state = "idel";
        }else if(status == "conversation-response") {
            this.onResponse(message);
            this.state = "idel";
        }else if(status == "speech-detected") {
            this.onSpeechDetect(message);
            this.state = status;
        }else if(status == "delay"){
            this.playDefaults("more_delay");
            this.state = "processing";
        }else if(status == "timeout"){
            this.playDefaults("timeout");
            this.onError(message);
            this.onIdelState();
            this.state= "idel";
        }else if(status == "socket_disconnected"){
            this.onError(message);
            this.state= "idel";
        }else if(status == "recording"){
            this.onRecording();
            this.state= "recording";
        } else if(status == "end_session"){
            if(this.session_expired_callback){
                this.session_expired_callback();
            }
            this.state= "session_expired";
        }else if(status == "mic_rejected"){
            this.micRejected();
            this.state= "idel";
        }
        console.debug("Event callback in converstaion handiler", status, message);
    }

    micRejected(){}
    getAuthDetails(cb){
        var that = this;
        var ds = DaveService.getInstance(this.host_url, this.enterprise_id, this.settings);
        // var ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        if(!ds.checkAuth()){
            that.createUser(ds, function(ds){
                cb(ds);
            });
        }else{
            cb(ds);
            that.triggerEvents("signup", "User loaded from cookies");
        }
    }

    updateNewUser(){
        var that = this;
        this.createUser(undefined, function(ds){
            that.state = "idel";
            for(var con of that.conversation_ids){
                that.con_map[con].changeUser(ds.user_id, ds.api_key)
            }
        });
    }

    setupEnvironment(){
        var that = this;
        function _load(ds){
            var unity_url_exists = false;
            if(that.settings["unity_url"]){
                that.setUpConversationUI();
                unity_url_exists=true;
            }
            if(that.event_type && that.event_id){
                ds.get(that.event_type, that.event_id, function(evdata){
                    that.settings = djq.extend(evdata, that.settings);
                    if(!unity_url_exists){
                        that.setUpConversationUI();
                    }
                    that.loadConversations(evdata);
                }, function(error){
                    console.log(error.message || error);
                    that.triggerEvents("failed", error);
                })
            }else{
                if(!unity_url_exists){
                    that.setUpConversationUI();
                }
                that.loadConversations(that.settings);
                // cb(data);
            }    
        }
        this.getAuthDetails(function(ds){
            _load(ds)
        })
    }
    postConversationFeedback(rating, text, cb){
        this.conversation.postConversationFeedback(rating, text, cb);
    }
    createUser(ds,cb){
        var that = this;
        if(!ds){
            ds = DaveService.getInstance(this.host_url, this.enterprise_id, this.settings);
            // ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        }
        var ob = this.settings["signup_params"] || this.settings["additional_signup_info"] || {};
        extendData({}, ob).then(function(sdata){
            if(that.settings["email_attr"]){
                var em_att = that.settings["email_attr"];
                sdata[em_att] = "dinesh+"+makeid(10)+"@i2ce.in";
            }
            sdata["validated"] = true;
            ds.signup(sdata, function(data){
                console.log("Signup done", data);
                that.customer_id = ds.user_id;
                that.headers = ds.headers;
                that.triggerEvents("signup", data);
                cb(ds);
            }, function(error){
                console.log("Signup failed", error);
                that.triggerEvents("signup", error.error || error);
            })
        }).catch(function(err){
            console.error("Signup failed", err);
        })
    }
    patchUser(data, success, error){
        // var ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        var ds = DaveService.getInstance(this.host_url, this.enterprise_id, this.settings);
        console.debug(ds);
        ds.update(ds.customer_model_name, ds.customer_id, data, success, error);
    }

    /**
     * @description This function is use for loading all the conversations
     * @example //internal function will get called from constructor
     * @returns returns through information through 'onSignup' callback
     */
    loadConversations(){
        var that = this;
        
        var loadedcount = 0;
        for(var con of this.conversation_ids){
            (function(con){
                that.con_map[con] = new Conversation(that.host_url, that.enterprise_id, con, that.settings, function(){
                    console.log(con+" Conversation loaded");
                    // that.customer_id = that.con_map[con].user_id;
                    that.load_status = "initcon";
                    var default_response = false;
                    function __loaded(){
                        loadedcount++;
                        if(loadedcount == that.conversation_ids.length){
                            that.conversation_loaded = true;
                            that.onLoadedCallback();
                        }
                    }
                    if(that.settings["init_responses"] && that.settings["init_responses"][con]){
                        that.init_response = that.settings["init_responses"][con];
                        __loaded();
                        default_response = true;
                    }
                    that.con_map[con].start(function(data){
                        // that.con_map[con].init_response = data;
                        if(!default_response){
                            __loaded();
                        }
                    }, function(error){
                        that.con_map[con].init_response = error;
                        console.error(error);
                        if(!default_response){
                            __loaded();
                        }
                    }, function(s, m){
                        that.eventCallback(s, m);
                    });
                }, function(error){
                    console.error(con+" Conversation loading failed ", error);
                }, function(s, m){
                    that.eventCallback(s, m);
                } );
                if(that.conversation_id == con){
                    that.conversation = that.con_map[con];
                }
            })(con);
        }        
    }

    /**
     * @description abstract function will get call once the conversation is loaded
     */
    setUpConversationUI(){
        if(this.settings["default_avatar_id"])
            defaul_avatar_id = this.settings["default_avatar_id"];
        if(this.settings["default_bg"])
            default_bg = this.settings["default_bg"];
        
        window.__dv_cbs_get("SetupConversationUI").fire();
    }
    
    //conversation post functions
    playDefaults(state, text){
        // 1 2 3 level_one level_two level_three
        // 4 timeout
        // 0 speech_is_none
        var that = this;
        //do something
        
        if(state == "level_one" ){
            this.play_followups = [];
            that.setPlaceholder("Loading...", "left")
            
        }

        if(text){
            // that.onProcessingRequest(text);
            that.user_reponse = {"customer_response": text};
        }
    }

    
    /**
     * @description This function is for posting conversation which will call backend apis
     * @example //ds.postConversation("i am looking for a suv car", null, null);
     * @example //ds.postConversation("yes", "cs_confirm", "Yes");
     * @param {string} text customer response the reponse you got from the user
     * @param {string} state_ is optional which represents the customer_state in the conversation
     * @param {string} title title to show while processing
     * @returns returns through callbacks
     */
     postConversation(text, state_=null, title=null, query_type=null){
        var that = this;
        this.updateSearchables();
        if(!(text || state_) || this.state != "idel" || this.load__ || this.audio_playing){
            return;
        }
        if(this._timer){
            clearTimeout(this._timer);
        }
        this.user_reponse = {"customer_state": state_};
        
        // this.onTalkingState();
        
        this.state = "processing";
        this.play_followups = [];
        this.audio_playing = false;
        this.load__ = true; 
        
        title= title ? title: text;
        // this.onProcessingRequest((title.indexOf("{") == -1 ? title : "Form response"));
        //this.toggle_response_panel();
        //djq("audio").each(function(){ djq(this).remove(); })
        // this.mute();
        // this.playDefaults("level_one", "", that);
        this._level_two_timer = setTimeout(function(){
            if(that.load__){
                that.playDefaults("level_two","", that);
            } 
        }, 8000)
    
        this.setWhiteboard();
        // this.conversation.hold(true);
        var m = new Message(text, "right", "You");
        this.addMessage(m, "left")
        //djq("#whiteboard").append(loading_whiteboard.html())
        this.setPlaceholder("Loading...", "left");
        this.conversation.next(text, state_, query_type, title, function(data){that.onResponse(data)}, function(data){that.onError(data)});
        // this.setPlaceholder("Loading....");   
    }
    playNudge(state){
        var that = this;
        this.conversation.getNudge(state, function(data){
            that.nudgeResponse(data);
        }, function(error){
            // that.nudgeResponse(data);
            console.error(error);
        })
    }

    //onresponse ui actions
    whiteBoardActions(event, event1){
        var data = event.data;
        if(!data && event1){
            data = event1.data;
        }
        var m = new Message(data.value, "right", "You");
        // that.setPlaceholder(m)
        this.addMessage(m);
        if( typeof data.value == "object" ){
            data.value = JSON.stringify(data.value);
        }
        console.debug(data.value, data.key);
        this.triggerEvents(data["type"], data);
        this.postConversation(data.value, data.key, data.title, "click");
    }
    whiteBoardEvents(event, event1){
        var data = event.data;
        if(!data && event1){
            data = event1.data;
        }
        var m = new Message(data.value, "right", "You");
        // this.setPlaceholder(m)
        this.addMessage(m);
        if( typeof data.value == "object" ){
            data.value = JSON.stringify(data.value);
        }
        this.triggerEvents(data["type"], data); 
        this.conversation.postEvent(data.key, data.value);
    }
    setWhiteboard(response){
        var that=this;
        
        if(!this.whiteboard_id){
            return;
        }
        
        if(this.whiteboard_ov){
            this.wb_h = djq(".dave-whiteboard").css("height");
            this.whiteboard_ov.remove();
            this.whiteboard_ov = null;
        }
        if(!response){
            var loading_whiteboard = this.settings["loading_whiteboard"] || this.conversation.conversation_data['loading_whiteboard'];
            if ( loading_whiteboard ) {
                var wb = '<div data-id="loading"><div class="dave-whiteboard"><div class="options-panel image"><div><img src="' + loading_whiteboard + '" style="width: 100%;"></div></div></div></div>';
                var w = new WhiteBoard({"white_board_id": "loading_whiteboard"});
                w.html(wb);
                window.__dv_cbs_get("SetWhiteboard").fire(w);
            }
        }else{
            var wb = response["whiteboard"] ;
            if(wb){
                var w = new WhiteBoard({"white_board_id": response["name"]});
                w.html(wb);

                w.watch(function(e, e1){ that.whiteBoardActions(e, e1)});
                w.watchEvents(function(e, e1){ that.whiteBoardEvents(e, e1)});

                // this.whiteboard_ov = djq("<div/>");
                // this.whiteboard_ov.append(w.ele);

                // djq("#"+this.whiteboard_id).append(this.whiteboard_ov);
                // this.whiteboards.push(w.ele);
            }else{
                wb = this.settings["default_whiteboard"] || this.conversation.conversation_data['default_whiteboard'];
                wb = '<div data-id="loading"><div class="dave-whiteboard"><div class="options-panel image"><div><img src="' + (wb) + '" style="width: 100%;"></div></div></div></div>';
                var w = new WhiteBoard({ "white_board_id": "default_whiteboard" });
                w.html(wb);
                window.__dv_cbs_get("SetWhiteboard").fire(w);
            }
        }
    }
    addMessage(m, pos="left"){
        this.messages.push(m);
        // djq("#messages").html(m.html());
        //     djq(".messages-div").first().animate({
        //         scrollTop: djq(".message").last().offset().top+80
        //     })
    }

    setPlaceholder(m, pos, title, time){
        if(typeof m == "string"){
            m = new Message(m, pos, title || m, time);
            this.messages.push(m);
        }
        window.__dv_cbs_get("SetPlaceholder").fire(m);
        // if(this.message_id){
        //     djq("#"+this.message_id).fit_text(m.message);
        // }
    }
    updateQuickAccessButtons(res) {
        var kws = this.conversation.getKeywords(6, res);
        var lst = [];
        
        for (var i of kws) { 
            lst.push({
                "customer_state": i[0],
                "title": i[1]
            })
        }
        window.__dv_cbs_get("SetQuickAccessButtons").fire(lst);
        // if(this.quick_access_id){
        //     djq("#"+this.quick_access_id).empty();
        //     var that =this;
        //     var kws = this.conversation.getKeywords(6, res);
        //     for(var i of kws){
        //         var butn = djq("<button />");
        //         butn.addClass("butn butn-dark butn-sm");
        //         butn.html(i[0]);
        //         butn.attr("value", i[0]);
        //         // butn.attr("value", i);
        //         butn.attr("key", i[1]);
        //         butn.attr("style", "margin-right: 5px");
        //         djq("#"+this.quick_access_id).append(butn);
        //         butn.click(function(){
        //             console.debug(djq(this).attr('value') || djq(this).attr('key'), djq(this).attr('key'), djq(this).attr("title"));
        //             that.postConversation(djq(this).attr('value'), djq(this).attr('key'), djq(this).attr('title'), "click");
        //             that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK});
        //             that.triggerEvents("interaction", "click");
        //         });
        //     }
        // }
    }
    searchKeyword(key) { 
        if(!key || key.length < 3) return;
        var kws = 0;
        var lst = []; 
        for(var i of this.conversation.keywords){
            if (i[2].toLowerCase().indexOf(key.toLowerCase()) != -1) {
                lst.push({
                    "customer_state": i[1],
                    "title": i[2]
                })
                kws++;
                if(kws == 15){
                    break
                }
            }
        }
        return lst;
    }
    updateSearchables(text){
        if(this.searchable_id){
            djq("#"+this.searchable_id).empty();
            djq("#"+this.searchable_id).parent().hide();
            if(!text || text.length < 3) return;
            var that =this;
            var kws = 0;
            for(var i of this.conversation.keywords){
                if(i[2].toLowerCase().indexOf(text.toLowerCase()) != -1){
                    var li = djq("<li />");
                    li.addClass("list-group-item");
                    li.html(`<span>${i[2]}</span>`);
                    li.attr("value", i[2]);
                    // butn.attr("value", i);
                    li.attr("key", i[1]);
                    li.attr("style", "margin-right: 5px");
                    djq("#"+this.searchable_id).append(li);
                    li.click(function(){
                        console.debug(djq(this).attr('value') || djq(this).attr('key'), djq(this).attr('key'), djq(this).attr("title"));
                        that.postConversation(djq(this).attr('value'), djq(this).attr('key'), djq(this).attr('title'), "click");
                        that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK});
                        that.triggerEvents("interaction", "click");
                        that.updateInput();
                    });
                    kws++;
                    if(kws == 15){
                        break
                    }
                    djq("#"+that.searchable_id).parent().show();
                }
            }
        }
    }
    addShortcutsButtons() {
        if (this.conversation.conversation_data["buttons"]) {
            window.__dv_cbs_get("SetShortcutButtons").fire(this.conversation.conversation_data["buttons"]);
        }
    }
    onSpeechDetect(text){}
    onRecording(){}
    refreshUI(rs){
        rs = rs ? rs :this.conversation.init_response;
        if(!rs){ return };
        
        this.setPlaceholder(rs["placeholder"]);
        this.setWhiteboard(rs["whiteboard"]);
        this.updateQuickAccessButtons(rs);
        this.addShortcutsButtons();
    }

    //on response callbacks
    onResponse(response){
        var that =this;
        this.load__ = false;
        this.state = "speaking";
        //this.toggle_response_panel(true);
        console.debug(response)
        // this.conversation.hold(false);
        if(!response || Object.keys(response) == 0) return;

        if(this._level_two_timer){
            clearTimeout(this._level_two_timer);
            this._level_two_timer = null;
        }

        if(response["speech_is_none"]){
            that.playDefaults("speech_is_none");
            this.onIdelState(true);
            return;
        }else if(response["error"]){
            this.processSystemResponse({
                data: {},
                placeholder: response["error"]
            });
        }else{
            this.processSystemResponse(response);
        }

        
        // set_cc();
        //if(this.temp && this.temp["name"] == response["name"] && this.temp["placeholder"] == response["placeholder"] && !force){
        
        this.updateInput("");
        this.triggerEvents("conversation", response["data"]);
        
    }
    onError(error){
        this.load__ = false;
        if(this._level_two_timer){
            clearTimeout(this._level_two_timer);
            this._level_two_timer = null;
        }
        this.state = "idel";
        this.audio_playing = false;
        this.onIdelState(true);
        this.setPlaceholder(error.error || "Failed to process your request. Please try again.");
        if(this.conversation.current_response && this.conversation.current_response["name"] == error["name"]){
            if(this.user_reponse && this.user_reponse["customer_response"] && this.user_reponse["customer_response"].indexOf("{") == -1){
                djq("#"+that.textbox_id).val(this.user_reponse["customer_response"]);
            }
        }
    }
    nudgeResponse(response){
        var that =this;
        this.load__ = false;
        this.state = "idel";
        //this.toggle_response_panel(true);
        console.debug(response)
        // this.conversation.hold(false);
        if(!response || Object.keys(response).length == 0) return;

        var text = new Message(response["placeholder"]);
        djq("#message").fit_text(response["placeholder"]);
        //this.toggle_response_panel(false);
        this.onTalkingState();
        
        var ow = response["data"]["overwrite_whiteboard"] == undefined ? response["overwrite_whiteboard"] : response["data"]["overwrite_whiteboard"];
        var mw = this.conversation.current_response["data"]["maintain_whiteboard"] == undefined ? this.conversation.current_response["maintain_whiteboard"] : this.conversation.current_response["data"]["maintain_whiteboard"];
        
        if(response["whiteboard"] && ( ow || mw )){
            this.setWhiteboard(response);
        }
    }

    //playing audio and action sequence
    playAnimations(){}
    playFromTheQueue(){}
    addAudioToQueue(){}
    addNudges(lst){}
    
    //run sequence of actions like animation, audio, update whiteboard, update bg ...etc
    processSystemResponse(response, after_played){
        this.setPlaceholder(response["placeholder"], "left", null);
        this.setWhiteboard(response);
        this.updateQuickAccessButtons(response);

        if(response["data"]){
            this.addNudges(response["data"]["_follow_ups"]);
        }
        if(this.actionCallbacks){
            this.actionCallbacks(response, "conversation_response");
        }
        this.status = "idel";
    }
    

    //ui states
    onTalkingState(){}
    onIdelState(repeat){}
    onProcessingRequest(text){}

    updateInput(text){
        if(this.textbox_id){
            djq("#"+this.textbox_id).val(text || "");
        }
    }
    mute(){
        if(this.audio){
            this.audio.muted = true;
        }
    }
    unmute(){
        if(this.audio){
            this.audio.muted = false;
        }
    }
    pause(){
        this.audio_playing = false;
        this.mute();
        this.conversation.updateSession();
    }
    resume(){
        this.unmute();
        this.conversation.startSession();
        this.onResponse(this.conversation.current_response);
    }

    createSession(){
        console.log("Started new session for user :: ", this.customer_id);
        this.conversation.startSession();
    }
    clearSession(async){
        console.log("End of session for user :: ", this.customer_id);
        // this.conversation.endSession();
        this.conversation.updateSession(async, true);
        // this.triggerEvents("session_expired_callback", true);
    }


    startWakeupStream(){
        this.conversation.startListenForWakeup();
    }
    stopWakeupStream(force){
        this.conversation.stopListenForWakeup(force);
        // this.triggerRandomEvent(true);
    }

    startImageStream(){
        this.conversation.startImageStream();
    }
    stopImageStream(){
        this.conversation.stopImageStream();
    }

}