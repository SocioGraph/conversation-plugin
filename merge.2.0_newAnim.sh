#!/bin/bash

level=${level:-avatar}
version=${version:-2.0}
base=${base:-2.0}
config=
all=

while [ $# -gt 0 ]; do

   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
        # echo $1 $2 // Optional to see the parameter:value result
   fi

  shift
done

echo $version
echo $base
echo $level

if [ ! -f ./source/${base}/js/${level}Conversation.js ]; then
    echo ${level}Conversation file doesnt exists
    exit 1
fi

mkdir -p ./build/$version
# echo 'var $=djq.noConflict();var jQuery = djq.noConflict();' > ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/dave_utils.js > ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/unity_handlerNewAnim.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/daveservice.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/streamer.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/conversation.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/conversation_handler.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/dave-libs.js > ./build/$version/dave-libs.js
cat ./source/${base}/libs-config.js > ./build/$version/libs-config.js
cat ./source/${base}/load-libs.js > ./build/$version/load-libs.js
if [ -n "$level" ] 
then
cat ./source/${base}/js/${level}Conversation.js >> ./build/$version/$level-conversation-plugin.js
fi

if [ -n "$config" ] 
then
cat ./load/$config/init_conversation.js > ./build/$config-init_conversation.js
else
echo "There is no init file"
fi



echo "Generating minified file"
bash minify_build.sh --version $version --level $level --base $base

if [ -n "$all" ]
then
    bash minify_libs.sh
else
    echo "***Skipped minifing lib***"
fi


npx uglifyjs ./build/$version/dave-libs.js --mangle --compress -o ./build/$version/dave-libs.min.js