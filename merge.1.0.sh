#!/bin/bash

level=${level:-avatar}
version=${release:-1.0}
base=${base:-1.0}
config=
all=

while [ $# -gt 0 ]; do

   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
        # echo $1 $2 // Optional to see the parameter:value result
   fi

  shift
done

echo $version
echo $base
echo $level

if [ ! -f ./source/${base}/js/${level}Conversation.js ]; then
    echo ${level}Conversation file doesnt exists
    exit 1
fi

mkdir -p ./build/$version
cat ./source/${base}/js/dave_utils.js > ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/unity_handler.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/daveservice.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/conversation.js >> ./build/$version/$level-conversation-plugin.js
cat ./source/${base}/js/conversation_handler.js >> ./build/$version/$level-conversation-plugin.js

if [ -n "$level" ] 
then
cat ./source/${base}/js/${level}Conversation.js >> ./build/$version/$level-conversation-plugin.js
fi

if [ -n "$config" ] 
then
cat ./load/$config/init_conversation.js > ./build/$config-init_conversation.js
else
echo "There is no init file"
fi



echo "Generating minified file"
bash minify_build.sh --version $version --level $level --base $base --all $all
